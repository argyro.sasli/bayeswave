FROM conda/miniconda3-centos7 AS build
ARG BUILD_DATE
ARG CI_COMMIT_SHA

# http://label-schema.org/rc1/
LABEL org.label-schema.schema-version="1.0"
LABEL org.label-schema.name="bayeswave-conda-env"
LABEL org.label-schema.description="BayesWave: distinguish gravitational wave signals from noise and instrumental glitches"
LABEL org.label-schema.url="https://docs.ligo.org/lscsoft/bayeswave/"
LABEL org.label-schema.vcs-url="https://git.ligo.org/lscsoft/bayeswave"
LABEL org.label-schema.build-date="${BUILD_DATE}"
LABEL org.label-schema.vcs-ref="${CI_COMMIT_SHA}"

## Build bayeswave conda environment
RUN conda update -yq -n base conda
COPY environment.yaml .
RUN conda env create -f environment.yaml
SHELL ["conda", "run", "-n", "bayeswave", "/bin/bash", "-c"]

## Use conda-pack to create a standalone enviornment
## in /venv:
RUN conda install -c conda-forge conda-pack
RUN conda-pack -n bayeswave -o /tmp/env.tar && \
  mkdir /venv && cd /venv && tar xf /tmp/env.tar && \
  rm /tmp/env.tar

## We've put venv in same path it'll be in final image,
## so now fix up paths:
RUN /venv/bin/conda-unpack
  
## Next stage
FROM centos:7

## Essentials
RUN yum install -y git which && \
      yum clean all && \
      rm -rf /var/cache/yum
    
## Copy /venv from the previous stage:
COPY --from=build /venv /venv

ENV PATH="/venv/bin:$PATH"
SHELL ["/bin/bash", "-c"]

