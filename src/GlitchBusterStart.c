/**************************************************************************
 
 Copyright (c) 2019 Neil Cornish
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 ************************************************************************/

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "GlitchBuster.h"
#include "GlitchBusterConstants.h"

//#ifndef _OPENMP
//#define omp ignore
//#endif

//OSX
// clang -Xpreprocessor -fopenmp -lomp -w -o glitchstart glitchstart.c GlitchBuster.c -lgsl -lgslcblas  -lm

// CIT
// gcc -std=gnu99 -fopenmp -w -o glitchstart glitchstart.c GlitchBuster.c -lgsl -lgslcblas  -lm

// non parallel
// gcc -o glitchstart glitchstart.c -lgsl -lgslcblas -lm


int main(int argc, char *argv[])
{
    int i, N, m;
    double *data;
    double x, y, z, dt, Tobs, ttrig, tstart, fny, fmax, fmin;
    double *SN, *SM, *ASD;
    
    char command[1024];
    char ifo[6];
    
    // allocate some arrays
    double fac;
		int Dmax = 200;
    
    int verbose;
    
    FILE *in;
    
    if(argc!=5)
    {
        printf("./glitchstart ifo Tobs trig_time fmax\n");
        return 1;
    }
    
    
    m = atoi(argv[1]);
    Tobs = atof(argv[2]);
    ttrig = atof(argv[3]);
    fmax = atof(argv[4]);
    
    tstart = ttrig - Tobs/2.0;
    
    if(m==0) sprintf(ifo, "%s", "H1");
    if(m==1) sprintf(ifo, "%s", "L1");
    if(m==2) sprintf(ifo, "%s", "V1");
    if(m==3) sprintf(ifo, "%s", "K1");
    
    verbose = 1; // set to 1 to print progress
    
    sprintf(command, "PSD_%d_%d_%d.dat", (int)(Tobs), (int)ttrig, m);
    in = fopen(command,"r");
    N = -1;
    while(!feof(in))
    {
        fscanf(in,"%lf%lf%lf", &x, &y, &z);
        N++;
    }
    rewind(in);
    N *= 2;
    if(verbose==1) printf("Number of points = %d\n", N);
    
    data = (double*)malloc(sizeof(double)* (N));
    
    dt = Tobs/(double)(N);
    fny = 1.0/(2.0*dt);  // Nyquist
    
    fmin = fmn;
    if(fmax > fny) fmax = fny;
    
    ASD = (double*)malloc(sizeof(double)*(N/2));
    SM = (double*)malloc(sizeof(double)*(N/2));
    SN = (double*)malloc(sizeof(double)*(N/2));
    
    if(verbose==1) printf("Data volume %f seconds, %f Hz\n", Tobs, fny);
    
    fac = Tobs/((double)(N)*(double)(N));
    
    for (i = 0; i < N/2; ++i)
    {
        fscanf(in,"%lf%lf%lf\n", &x, &SN[i], &SM[i]);
        SN[i] /= fac;
        ASD[i] = sqrt(SN[i]);
    }
    fclose(in);
    
    
    // read in FTT of data
    sprintf(command, "dataF_%d_%d_%d.dat", (int)(Tobs), (int)ttrig, m);
    in = fopen(command,"r");
    for(i = 1; i < N/2; i++) fscanf(in,"%lf%lf", &data[i], &data[N-i]);
    fclose(in);
    data[0] = 0.0;
    data[N/2] = 0.0;
    
    int NW;
    double **glitch_params = NULL; //holds the glitch parameters

    glitch_params = GlitchBusterWrapper(m, N, Tobs, ttrig, fmax, SN, ASD, data, &NW, Dmax);
        
    return 0;
    
}








