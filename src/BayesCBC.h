#ifndef BAYESCBC_H
#define BAYESCBC_H
#include <complex.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_sort_double.h>
#include <gsl/gsl_statistics.h>
#include <gsl/gsl_fft_real.h>
#include <gsl/gsl_fft_halfcomplex.h>
#include <gsl/gsl_cdf.h>
#include <gsl/gsl_spline.h>
#include <gsl/gsl_eigen.h>
#include "IMRPhenomD_internals.h"

// Detector network structure for BayesCBC
struct Net
{
    int Nifo;
    double tmax;
    double tmin;
    double Tobs;
    int *labels;
    double *tds;
    double **delays;

    double **location; // The three components, in an Earth-fixed Cartesian coordinate system, of the position vector from the center of the Earth to the detector in meters
    double ***response; // The Earth-fixed Cartesian components of the detector's response tensor \f$d^{ab}\f$ 

    // These are only used locally and updated per iteration
    double *Fplus; // antenna pattern functions
    double *Fcross; // antenna pattern functions
    double *dtimes; //time delay between reference location and IFOs

};

// Heterodyned likelihood
struct Het
{
    double hh;
    double hr;
    double logLb;
    double **rc;
    double **rs;
    double ***lc;
    double ***ls;
    double *pref;
    
    // Part of the heterodyne that needs to computed once
    // and not on every iteration for the extrinsic heterodyne.
    // So this holds everything that does not change with
    // changing data.
    int M;
    int MM;
    int ds;
    int ell;
    int icbc;
    double **S0;
    double **S1;
    double **AS;
    double **SN;
    double **amp;
    double **phase;
    RealVector *freq;
    double **hb;
    double **sp;
    double **cp;
    double **x;

    int initFlag; // Is zero when Heterodyne has not been initiliazed yet with initial reasonable solution
};


// Structure to hold BayesCBC run settings (that were previously hardcoded in QuickCBC header)
struct bayesCBC
{
    double fmin;
    double fmax;
    double fref;
    int intrinsic_only; // evolve only semi-intrinsic parameters (NX), not sky
    double gmst;
    double *logLx;
    double **pallx;
    // pall usually holds geocenter values
    // pall  [0] log(Mc) [1] log(Mt) [2] chi1 [3] chi2 [4] 2phi0 [5] tp [6] log(DL)
    //       (if BNS) [7] lambda1 [8] lambda 2
    //       [NX] alpha [NX + 1] sindelta [NX + 2] psi [NX + 3] ciota

    gsl_rng **rvec; // holds r for each cbc chain
                    // (temporarily as needed when running multitheaded)
                    // Note: moved to BW Chain structure!

    int debug; // if > 0, prints debug statements in BayesCBCs
    int verbose; // set by --verbose flag

    // keep track of (intrinsic) proposals, counts, and acceptances
    // TODO should be moved over to chain
    int count;      // maximum number of MCMC iterations
    int iteration;  // current step (of maximum count)
    int fisher_acc; // acceptance numbers of different proposals
    int diff_evolution_acc;
    int jiggle_acc;
    int history_acc;
    int fisher_prop; // numbers of tries of different proposals
    int diff_evolution_prop;
    int jiggle_prop;
    int history_prop;

    struct Net *net;
    double **D;
    double ***global;
    double **skyx;
    int *who;
    double *heat;
    double **paramx;
    // holds (ifo 0) values for
    // paramx  [0] log(Mc) [1] log(Mt) [2] chi1 [3] chi2 [4] 2phi0 [5] tp [6] log(DL)
    //         (if BNS) [7] lambda 1 [8] lambda 2
    double ***history; // NC x NH + 1 x NX (stores former chain values )
    double **SN;
    int *mxc;
    RealVector *freq;
    double fbw;    // width of frequency bands used in penalized likelihoodin burnin

    FILE *chainA;    // Chains file for main MCMC 
    FILE *chainS;    // Chains file for initial signal search

    double cap;     // cap on log likelihood proposal (sets SNR^2/2 threshold)
    double qfac;    // geometric progression in mass ratio
    double ladder;  // temperature ladder
    double twidth;  // width of prior on merger time (used when given non-integer merger time)
    double dTsearch; // temperature spacing for hot chains in CBC search phase
    double CBC_trigtime; // Sets location of CBC window in GPS time (by default this not used and set to -1)
    double fres;    // Frequency resolution used in heterodyne

    int NX; // number of quasi-intrinsic parameters (Mc, Mt, chi1, chi2, phic, tc, DL)
    int NP; // total number of entries in the full parameter arrays NX+4
    int NS; // number of quasi-extrinsic parameters (alpha, sin(delta), psi, ellipticity, scale, phi0=2*phic, dt)
    int NC; // number of chains
    int NCC; // number of cold chains 
    int NH; // length of history
    int NQ; // number of mass ratios in global proposal
    int NM; // number of chirp masses in global proposal
    int MI; // number of intrinsic search iterations

    int resizeSearchFlag;      // use dynamic number of chains + temperature spacing in cbc search
    int constantLogLFlag;      // fix the likelihood to a constant
    int injectionStartFlag;    // start at injection values (instead of burnin)
    int waveformFlag;          // temporary flag to use custom (faster) WF generation 
    int heterodyneFlag;        // to use the heterodyned likelihood (this forces the use of cbc burnin)

    double *max; // Array that will hold upper boundary prior range
    double *min; // Array that will hold lower boundary prior range
    double DLmin; // Min & max distance in parsec
    double DLmax;
    double chi1min;  // Min/max for chi1 and chi2
    double chi1max;
    double chi2min;
    double chi2max;
    double mcmax;   // maximum chirp mass in Msun.  Mc is at most 0.435 times Mt
    double mcmin;    // minimum chirp mass in Msun
    double mtmax;   // maximum total mass in Msun (should be not more than 2.3 times mcmax)
    double mtmin;    // minimum total mass in Msun
    double lambda1min;       // Min/max for lambda1 and lambda2
    double lambda1max;   // Only used if NRTidal_value != NoNRT_V and
    double lambda2min;       // lambda_type_value = lambda 
    double lambda2max;

    double lambdaTmin;       // Min/max for lambdaT and dlambdaT
    double lambdaTmax;       // Only used if NRTidal_value != NoNRT_V and
    double dlambdaTmin;      // lambda_type_value = lambdaTilde 
    double dlambdaTmax;

    // Tidal injection options
    NRTidal_version_type NRTidal_version; // Version of NRTides; 
    Lambda_type lambda_type_version; // Sampler type for tidal parameters: can be lambda (draw independently from lambda1 and lambda2) or lambdaTilde (draw from lambda tilde and delta_lambda to compute lambda1 & lambda2*/
    double init_tidal1; // Injection value for lamda1 if lambda_type_value = lambda, lamdaT if lambda_type_value = lambdaTilde
    double init_tidal2; // Injection value for lamda2 if lambda_type_value = lambda, dlamdaT if lambda_type_value = lambdaTilde

    struct Het *het;    // Heterodyned likelihood
    struct FixedHet *fixedhet; // Fixed part of heterodyne likelihood (used in extrinsic sampler)
};


int check_bound(const double val, const double lower_bound, const double upper_bound);
void print_bounds(double **pallx, const struct bayesCBC *rundata);

void sample_pallx_uniform(double *pallx, const struct bayesCBC *rundata, gsl_rng *r);
void sample_mc_mt(double* pallx, const struct bayesCBC *rundata, gsl_rng *r);

void initialize_net(struct Net *net, double Tobs, double ttrig, int Ndet, char *argv[], double twidth);
void burnin_bayescbc(int N, double Tobs, double ttrig, double **D, double **SN, gsl_rng *r, char *xml_file_path, int start_at_injection, struct bayesCBC *rundata);
void set_bayescbc_priors(struct Net *net, struct bayesCBC *rundata);
bool within_CBC_prior(const double *params, const struct bayesCBC *rundata, bool verbose);
void enforce_CBC_priors(double *params, const struct bayesCBC *rundata);
double return_above_or_below(const double param, const double min, const double max);
void sample_CBC_intinsic_prior(double *params, const struct bayesCBC *rundata, gsl_rng *seed);
void sample_CBC_extrinsic_prior(double *params, const struct bayesCBC *rundata, gsl_rng *seed);
void reinitialize_history(double ***history, double **pallx, int NH, int NC, int NP, gsl_rng *r);
void resize_search_chains(struct Net *net, double Tobs, double **D, double **SN, int N, int NCmain, gsl_rng *r, struct bayesCBC *rundata);
void reset_search_chains(struct Net *net, int *indices, int NCmain, gsl_rng *r, struct bayesCBC *rundata) ;

void BayesCBC_MCMC(int M, int N, double Tobs, double ttrig, gsl_rng *r, int ncycle, struct bayesCBC *rundata, int ic, double *amphase);
void geowave(double *gwave, RealVector *freq, double *params, int N, struct bayesCBC *rundata);
void projectCBCWaveform(double *ampphase, int N, int NI, double fmin, double Tobs, double *extParams, double **response, double *dtimes, double *Fplus, double *Fcross);
void projectCBCWaveformExtrinsic(double *ampphase, int N, int NI, double fmin, double Tobs, double *extParams, double **response, double *dtimes, double *Fplus, double *Fcross, double *proposal_shifts);
void extrinsic_proposal_cbc_geoshifts(double *geoshifts, double *proposed_shifts, double dtime0, double phi0y, double scaley);
void print_projected_cbc_waveform(double **SN, double Tobs, double ttrig, double **projected, double **data, int N, int iter, struct bayesCBC *rundata);

double z_DL(double DL);
double DL_fit(double z);
void LambdaTsEta2Lambdas(double lambdaT, double dLambdaT, double eta, double *lambda1, double *lambda2);

// Helper functions for extrinsic burnin
void skymcmc(struct Net *net, int MCX, int *mxc, FILE *chain, double **paramx, double **skyx, double **pallx,
    int *who, double *heat, double dtx, int nt, double *DD, double **WW, double ***DHc,  double ***DHs, double ***HH,
    double Tobs, gsl_rng *r, struct bayesCBC *rundata, int skyFixFlag);

// Templates
void templates(struct Net *net, double **hwave, RealVector *freq, double *params, int N, struct bayesCBC *rundata);
void geotemplate(double *gwave, RealVector *freq, double *params, int N, struct bayesCBC *rundata);


void skyring(struct Net *net, double *params, double *sky, double *pall, double *SNRsq, RealVector *freq, double **D, double **SN, int N, double Tobs, gsl_rng *r, int NX, int NS, double gmst);
void ringfind(struct Net *net, const double *tdelays, double *params, const double *SNRsq, gsl_rng *r, double gmst);
double skylike(struct Net *net, double *params, double *D, double *H, double **DHc,  double **DHs, double dt, int nt, int flag, struct bayesCBC *rundata);
void skylikesetup(struct Net *net, double **data,  double **wave, double *D, double *H, double **DHc,  double **DHs, double Tobs, int n, int bn, int nt, int imin, int imax);
void fisherskysetup(struct Net *net, double **wave, double **HH, double Tobs, int n);

void compute_smooth_psd_model(double Tobs, int N, double *psd, double *smooth_psd);
void compute_smooth_psd_model_quickcbc(double Tobs, int N, double fmx, double *psd, double *smooth_psd, gsl_rng *r);

double log_likelihood(struct Net *net, double **D, double *params, RealVector *freq, double **SN, int N, double Tobs, struct bayesCBC *rundata);
double log_likelihood_full(struct Net *net, double **D, double *params, RealVector *freq, double **SN, double *rho, int N, double Tobs, struct bayesCBC *rundata, int extFlag);
double log_likelihood_test(struct Net *net, struct Het *het, double **D, double *params, RealVector *freq, double **SN, int N, double Tobs, struct bayesCBC *rundata);
double log_likelihood_het(struct Net *net, struct Het *het, double *params, double Tobs, struct bayesCBC *rundata, int recomputeFlag);
double log_likelihood_full_old(struct Net *net, double **D, double *params, RealVector *freq, double **SN, double *rho, int N, double Tobs, struct bayesCBC *rundata, int extFlag);

void upsample(int n, double T, int *nt, int *bn);


// Conversion routines (between geocenter and detector frames)
void dshifts(struct Net *net, double *sky, double *params, int NX, struct bayesCBC *rundata);
void pmap_all(struct Net *net, double *pall, double *param, double *sky, int NX, double gmst);

void TimeDelays(struct Net *net, double alpha, double sindelta, double *dtimes, double gmst);
void ComputeDetFant(struct Net *net, double psi, double alpha, double sindelta, double *Fplus, double *Fcross, int id, double gmst);

// Heterodyne routines
void heterodyne(struct Net *net, struct Het *het, double **D, double *params, RealVector *freq, double **SN, double **SM, int N, double Tobs, struct bayesCBC *rundata, int ic);
void fixedheterodyne(struct Net *net, struct Het *het, double *params, RealVector *freq, double **SN, double **SM, int N, double Tobs, struct bayesCBC *rundata, int ic);
double fullheterodyne_likelihood(struct Net *net, struct Het *het, double **D, double *cbcparams, double *extparams, double Tobs, struct bayesCBC *rundata, int N);
void freehet(struct Net *net, struct Het *het);

#endif /*BAYESCBC_H*/