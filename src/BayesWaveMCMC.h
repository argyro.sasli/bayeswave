/*
 *  Copyright (C) 2018 Neil J. Cornish, Tyson B. Littenberg
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with with program; see the file COPYING. If not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 *  MA  02111-1307  USA
 */

/* ********************************************************************************** */
/*                                                                                    */
/*                                  MCMC Samplers                                     */
/*                                                                                    */
/* ********************************************************************************** */

void RJMCMC(struct Data *data, struct Model **model, struct BayesLineParams ***bayesline, struct bayesCBC *bayescbc,struct Chain *chain, struct Prior *prior, struct GlitchBuster *gb, double *logEvidence, double *varLogEvidence);
void PTMCMC(struct Model **model, struct Chain *chain, gsl_rng *seed, int NC);

void EvolveBayesLineParameters(struct Data *data, struct Model **model, struct BayesLineParams ***bayesline, struct Chain *chain, struct Prior *prior, int ic);
void EvolveIntrinsicParameters(struct Data *data, struct Prior *prior, struct Model **model, struct Chain *chain, struct TimeFrequencyMap *tf, gsl_rng *seed, int ic);
void EvolveExtrinsicParameters(struct Data *data, struct Prior *prior, struct Model **model, struct Chain *chain, struct bayesCBC *bayescbc, gsl_rng *seed, int ic);
void EvolveBayesCBCParameters(struct Data *data, struct Model **model, struct bayesCBC *bayescbc, struct Chain *chain, int ic);

void burnin_cbc(struct bayesCBC *bayescbc, struct Data *data, struct Model **model, struct Chain *chain);
void extrinsic_burnin_cbc(struct bayesCBC *bayescbc, struct Data *data, struct Model **model, struct Chain *chain);

void kickstart_glitch_model_glitchbuster(struct Data *data, struct Prior *prior, struct Model *model, struct GlitchBuster *gb);
void kickstart_glitch_model_from_file(struct Data *data, struct Prior *prior, struct Model *model);

/* ********************************************************************************** */
/*                                                                                    */
/*                                    MCMC tools                                      */
/*                                                                                    */
/* ********************************************************************************** */

void adapt_temperature_ladder(struct Chain *chain, int NC);
