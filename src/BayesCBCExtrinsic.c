/*******************************************************************************************

Copyright (c) 2020-2021 Katerina Chatziioannou, Neil Cornish, Sophie Hourihane, Marcella Wijngaarden 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

**********************************************************************************************/


#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <omp.h>


#include <signal.h>
#include <unistd.h>
#include <time.h>

#include "BayesCBC.h"
#include "BayesCBC_internals.h"
#include "Constants.h"
// #include "IMRPhenomD.h"

#include <lal/LALDetectors.h>
#include <lal/Date.h>
#include <lal/TimeDelay.h>
#include <lal/DetectorSite.h>
#include <lal/DetResponse.h>
#include <lal/LALSimInspiral.h>


/*******************************************************************************************

This file contains the extrinsic routines for the BayesCBC code. Unlike main BayesCBC, these 
routines do depend on BW/LAL because we want to use the BW/LAL timedelays/projections/etc.- routines

**********************************************************************************************/

void skymcmc(struct Net *net, int MCX, int *mxc, FILE *chain, double **paramx, double **skyx, double **pallx, int *who,
             double *heat, double dtx, int nt, double *DD, double **WW, double ***DHc,  double ***DHs, double ***HH,
             double Tobs, gsl_rng *r, struct bayesCBC *rundata, int skyFixFlag)
{
    // paramx should already be intialized
    int i, j, k, q, ic, id1, id2, istart;
    int scount, sacc, hold, mcount;
    int ac, rc, rca, clc, cla, PRcnt, POcnt;
    int sdx, Ax, mc;
    double alpha, beta;
    double Mchirp, Mtot, eta, dm, m1, m2, chieff, ciota;
    double qxy, qyx, Jack, phi;
    int rflag, cflag;
    double **sky, **skyy, **skyh;
    double x, y, z, DL, scale, logLy;
    double *logLx;
    double pAy, logH, pAx;
    double *param;
    double *dtimes, *dtimes2;
    double ***fishskyx, ***fishskyy;
    double ***skyvecsx, ***skyvecsy;
    double **skyevalsx, **skyevalsy;
    double Fp, Fc, Fs, ps;
    double *jump, *sqH;
    double ldetx, ldety;
    double scmax, scmin;
    double DLx, DLy;
    int fflag, fc, fac;
    int uflag, uc, uac;
    double Ap, Ac, Fcross, Fplus, lambda, lambda2, Fs2;
    double sindelta, psi;
    int NX = rundata->NX;
    int NP = rundata->NP;
    int NS = rundata->NS;
    int NC = rundata->NC; // number of chains
    int NCC = rundata->NCC; // number of cold chains 
    int NH = rundata->NH; // length of history
    int NQ = rundata->NQ; // number of mass ratios in global proposal
    int NM = rundata->NM; // number of chirp masses in global proposal
    
    dtimes = (double*)malloc(sizeof(double)*5);
    dtimes2 = (double*)malloc(sizeof(double)*5);
    
    param = (double*)malloc(sizeof(double)*(NX+3*net->Nifo));
    
    // sky parameter order
    //[0] alpha, [1] sin(delta) [2] psi [3] ciota [4] scale [5] phi0 [6] dt
      
    // max and min of rescaling parameter
    scmin = 0.1;
    scmax = 10.0;
    
    sky = double_matrix(NC,NS);
    skyh = double_matrix(NC,NS);
    skyy = double_matrix(NC,NS);
    logLx = double_vector(NC);
    sqH = double_vector(NC);
    
    for(k = 0; k < NC; k++)
    {
        for(i = 0; i < NS; i++) sky[k][i] = skyx[k][i];
        for(i = 0; i < NS; i++) skyh[k][i] = skyx[k][i];
    }
    
    for(k = 0; k < NC; k++) sqH[k] = sqrt(heat[k]);
    
    ic = who[0];
    
    for(k = 0; k < NC; k++) logLx[k] = skylike(net, skyx[k], DD, WW[k], DHc[k], DHs[k], dtx, nt, 0, rundata);
    
    
    fishskyx = double_tensor(NC,NS,NS);
    fishskyy = double_tensor(NC,NS,NS);
    skyvecsx = double_tensor(NC,NS,NS);
    skyvecsy = double_tensor(NC,NS,NS);
    skyevalsx = double_matrix(NC,NS);
    skyevalsy = double_matrix(NC,NS);
    jump = double_vector(NS);
    
    
    for(k = 0; k < NC; k++)
    {
    fisher_matrix_fastsky(net, skyx[k], fishskyx[k], HH[k], NS, rundata->gmst);
    FisherEvec(fishskyx[k], skyevalsx[k], skyvecsx[k], NS);
    }
    

    printf("  Extrinsic MCMC\n");
    
    
    ac = 0;
    rc = 1;
    rca = 0;
    fc = 1;
    uc = 1;
    
    clc = 0;
    cla = 0;
    fac = 0;
    uac = 0;
    
    PRcnt = 0;
    POcnt = 0;
    
    sdx = 0.0;
    Ax = 0.0;
    
    scount = 0;
    sacc = 0;
    mcount = 0;
    
    // Don't update sky parameters when using skyFixFlag
    if (skyFixFlag == 1) istart = 2;
    else istart = 0;

    for(mc = 0; mc < MCX; mc++)
    {
        
        
        if(mc > 1 && mc%1000==0)
        {
            // update the Fisher matrices
            for(k = 0; k < NC; k++)
            {
                fisher_matrix_fastsky(net, skyx[k], fishskyx[k], HH[k], NS, rundata->gmst);
                FisherEvec(fishskyx[k], skyevalsx[k], skyvecsx[k], NS);
            }
        }
        
        alpha = gsl_rng_uniform(r);
        
        if((NC > 1) && (alpha < 0.2))  // decide if we are doing a MCMC update of all the chains or a PT swap
        {
            
            // chain swap
            scount++;
            
            alpha = (double)(NC-1)*gsl_rng_uniform(r);
            j = (int)(alpha);
            beta = exp((logLx[who[j]]-logLx[who[j+1]])/heat[j+1] - (logLx[who[j]]-logLx[who[j+1]])/heat[j]);
            alpha = gsl_rng_uniform(r);
            if(beta > alpha)
            {
                hold = who[j];
                who[j] = who[j+1];
                who[j+1] = hold;
                sacc++;
            }            
        }
        else      // MCMC update
        {
            
            mcount++;

            
            for(k = 0; k < NC; k++)
            {
                for(i = 0; i < NS; i++) skyy[k][i] = skyx[k][i];
            }
            
            for(k=0; k < NC; k++)
            {
                q = who[k];
                
                qxy = 0.0;
                qyx = 0.0;
                
                Jack = 0.0;
                
                rflag = 0;
                cflag = 0;
                fflag = 0;
                uflag = 0;
                
                
                alpha = gsl_rng_uniform(r);

                if(alpha > 0.8 && net->Nifo > 1 && skyFixFlag == 0)  // ring
                {

                  id1 = id2 = 0;

                  // Pick a pair of interferometers to define sky ring
                  while (id1 == id2)
                  {
                    id1 = (int)((double)(net->Nifo)*gsl_rng_uniform(r));
                    id2 = (int)((double)(net->Nifo)*gsl_rng_uniform(r));
                  }
                  // get new sky locations with the same time delays
                  Ring(net, skyx[q], skyy[q], id1, id2, r, rundata->gmst);
                  qyx = 1.0;
                  qxy = 1.0;
                  skymap(net, skyx[q], skyy[q], id1, id2, 0, rundata->gmst);
                  // calculates jacobian
                  Jack = log(skydensity(net, skyx[q], skyy[q], id1, id2, 0, NS, rundata->gmst));

                  // The mapping only covers half the phi, psi space. Can cover it all by radomly shifting both by a half period
                  x = gsl_rng_uniform(r);
                  if(x > 0.5)
                  {
                      skyy[q][5] += PI;
                      skyy[q][2] += PI/2.0;
                  }

                  if(k==0) rc++;

                  rflag = 1;
                    
                }
                else if (alpha > 0.2)  // Fisher matrix
                {
                    
                    if(k==0) fc++;
                    
                    fflag = 1;
                    
                    fisher_skyproposal(r, skyvecsx[q], skyevalsx[q], jump, rundata);
                    
                    for(i=istart; i< NS; i++) skyy[q][i] = skyx[q][i]+sqH[k]*jump[i];
                    
                    // If the Fisher matrix was updated after each Fisher jump we would
                    // need these proposal densities. Since Fisher held fixed for blocks
                    // of iterations, we don't need the densities
                    // pfishxy = fisher_density(fishskyx, ldetx, skyx, skyy);
                    // fisher_matrix_fastsky(net, skyy, fishskyy, HH);
                    // fisher_skyvectors(fishskyy, skyvecsy, skyevalsy, &ldety);
                    //  pfishyx = fisher_density(fishskyy, ldety, skyy, skyx);
                }
                else  // jiggle (most useful early when Fisher not effective)
                {
                    
                    uflag = 1;
                    
                    if(k==0) uc++;
                    
                    beta = 0.01*pow(10.0, -floor(3.0*gsl_rng_uniform(r)))*sqH[k];
                    for(i = istart; i < NS-1; i++) skyy[q][i] = skyx[q][i]+beta*gsl_ran_gaussian(r,1.0);
                    skyy[q][6] = skyx[q][6]+0.01*beta*gsl_ran_gaussian(r,1.0);
                }
                
                
                while(skyy[q][0] > TPI) skyy[q][0] -= TPI;
                while(skyy[q][0] < 0.0) skyy[q][0] += TPI;
                while(skyy[q][2] > PI) skyy[q][2] -= PI;
                while(skyy[q][2] < 0.0) skyy[q][2] += PI;
                while(skyy[q][5] > TPI) skyy[q][5] -= TPI;
                while(skyy[q][5] < 0.0) skyy[q][5] += TPI;
                
                // skyy:
                //[0] alpha, [1] sin(delta) [2] psi [3] cos(iota) [4] scale [5] phi0 [6] dt
                DLy = exp(pallx[q][6])/(skyy[q][4]*PC_SI);

                if(DLy < rundata->DLmin || DLy > rundata->DLmax ||    // distance is in range
                  fabs(skyy[q][1]) > 1.0 || fabs(skyy[q][3]) > 1.0 || // cos and sin are in bounds
                  fabs(skyy[q][6]) > dtmax)                           // time delay is less than maximum
                {
                    logLy = -1.0e60;
                    
                    pAy = 0.0;
                    pAx = 0.0;
                    
                }
                else
                {
                    logLy = skylike(net, skyy[q], DD, WW[q], DHc[q], DHs[q], dtx, nt, 0, rundata);
                    
                    // Need a Jacobian a factor here since we sample uniformly in amplitude.
                    // Jacobian between D cos(theta) phi cos(iota) psi and A cos(theta) phi cos(iota) psi
                    // Since D = D_0/A, boils down to just D^2 |dD/dA| = D_0^3/A^4 = D^3/A
                
                    DLx = exp(pallx[q][6])/(skyx[q][4]);
                    pAx = 3.0*log(DLx)-log(skyx[q][4]);

                    DLy = exp(pallx[q][6])/(skyy[q][4]);
                    pAy = 3.0*log(DLy)- log(skyy[q][4]);
                }
                
                
                logH = Jack + (logLy-logLx[q])/heat[k] +pAy-qyx-pAx+qxy;
                
                alpha = log(gsl_rng_uniform(r));
                
                
                if(logH > alpha)
                {
                    for(i=0; i< NS; i++) skyx[q][i] = skyy[q][i];
                    logLx[q] = logLy;
                    
                    if(k==0)
                    {
                        ac++;
                        if(rflag == 1) rca++;
                        if(fflag == 1) fac++;
                        if(uflag == 1) uac++;
                    }
                    
                }
                
            }  // ends loop over chains
            
        }  // ends choice of update
        
        
        if(mc%100 == 0)
        {
            ic = who[0];
            phi = skyx[ic][0];
            while(phi > TPI) phi -= TPI;
            while(phi < 0.0) phi += TPI;
            skyx[ic][0] = phi;
            Mchirp = exp(paramx[ic][0])/MSUN_SI;
            Mtot = exp(paramx[ic][1])/MSUN_SI;
            eta = pow((Mchirp/Mtot), (5.0/3.0));
            if(eta > 0.25) eta = 0.25;
            dm = sqrt(1.0-4.0*eta);
            m1 = Mtot*(1.0+dm)/2.0;
            m2 = Mtot*(1.0-dm)/2.0;
            chieff = (m1*paramx[ic][2]+m2*paramx[ic][3])/Mtot;

           
            // counter, log likelihood, chirp mass, total mass, effective spin,  phase shift , time shift, distance, RA , sine of DEC,
            // polarization angle, cos inclination
            
            
            DL = exp(pallx[ic][6])/(1.0e6*PC_SI*skyx[ic][4]);
            z = z_DL(DL);
            
            // Note that skyx[ic][5], skyx[ic][6], hold different quantities than what is printed by the other MCMCs
 
            fprintf(rundata->chainS,"%d %e %e %e %e %e %e %e %e %e %e %e %e %e %e %e %e %e %e %e %e\n", mxc[1], logLx[ic],
                    Mchirp, Mtot, chieff, skyx[ic][5], skyx[ic][6], DL, skyx[ic][0], skyx[ic][1], skyx[ic][2],
                    skyx[ic][3], z, Mchirp/(1.0+z), Mtot/(1.0+z), m1/(1.0+z), m2/(1.0+z), m1, m2, paramx[ic][5], pallx[ic][5]);
            
        
                    
            
             mxc[1] += 1;
            
          } 
        
            if(mc%100000 == 0 && rundata->debug == 1)
            {
             ic = who[0];
             DL = exp(pallx[ic][6])/(1.0e6*PC_SI*skyx[ic][4]);
             printf("%d %i %f %f %f %f %f %f\n", mc, ic, logLx[ic], DL, skyx[ic][0], skyx[ic][1], skyx[ic][2], skyx[ic][3]);
            }

        
    }

    // update the amplitude, time and phase shifts between detectors in preparation for extrinsic updates
    for(k=0; k < NC; k++) dshifts(net, skyx[k], paramx[k], NX, rundata);

    
    // sky  [0] alpha, [1] sin(delta) [2] psi [3] ciota [4] scale [5] dphi [6] dt
    // param [0] log(Mc) [1] log(Mt) [2] chi1 [3] chi2 [4] phi0 [5] tp0 [6] log(DL0) then relative amplitudes, time, phases

    // update the extrinsic parameters
    for(k = 0; k < NC; k++)
    {
        // move reference point from geocenter to ref detector
        // Note that sky[4],sky[5], sky[6] hold shifts relative to the reference geocenter waveform
        // To map back to the reference detector
        
        ciota = skyh[k][3];
        Ap = (1.0+ciota*ciota)/2.0;
        Ac = ciota;
        alpha = skyh[k][0];
        sindelta = skyh[k][1];
        psi = skyh[k][2];
        // 0 indicates we are using the reference detector (which is the first --ifo argument)
        ComputeDetFant(net, psi, alpha, sindelta, &Fplus, &Fcross, 0, rundata->gmst);
        Fs =  Fmag(Ap, Ac, Fcross, Fplus); // magnitude of response
        lambda = phase_from_polarizations(Ap, Ac, Fcross, Fplus); // get phase from polarizations
        
        TimeDelays(net, alpha, sindelta, dtimes, rundata->gmst);
        
        ciota = skyx[k][3];
        Ap = (1.0+ciota*ciota)/2.0;
        Ac = ciota;
        alpha = skyx[k][0];
        sindelta = skyx[k][1];
        psi = skyx[k][2];
        
        ComputeDetFant(net, psi, alpha, sindelta, &Fplus, &Fcross, 0, rundata->gmst);
        Fs2 =  Fmag(Ap, Ac, Fcross, Fplus); // magnitude of response
        lambda2 = phase_from_polarizations(Ap, Ac, Fcross, Fplus); // get phase from polarizations
        
        TimeDelays(net, alpha, sindelta, dtimes2, rundata->gmst);
        
        paramx[k][4] += 0.5*(skyx[k][5]+lambda2-lambda);
        while(paramx[k][4] > PI) paramx[k][4] -= PI;
        while(paramx[k][4] < 0.0) paramx[k][4] += PI;
        paramx[k][5] += skyx[k][6] + dtimes2[0] - dtimes[0];
        paramx[k][6] -= log(skyx[k][4]*Fs2/Fs);
        
        // sky will be re-aligned with geocenter so reset
        skyx[k][4] = 1.0;
        skyx[k][5] = 0.0;
        skyx[k][6] = 0.0;
       
    }

    if(rundata->debug == 1)
    {   printf("  Extrinsic burnin ended with:\n");
        printf("   Swap Acceptance = %f\n", (double)sacc/(double)(scount));
        printf("   MCMC Acceptance = %f\n", (double)ac/(double)(mcount));
        printf("   Ring Acceptance = %f\n", (double)rca/(double)(rc));
        printf("   Fisher Acceptance = %f\n", (double)fac/(double)(fc));
        printf("   Jiggle Acceptance = %f\n", (double)uac/(double)(uc));        
    }
    
    free_double_matrix(skyy,NC);
    free_double_matrix(skyh,NC);
    free_double_matrix(sky,NC);
    free_double_vector(logLx);
    free_double_vector(sqH);
    free_double_tensor(fishskyx,NC,NS);
    free_double_tensor(fishskyy,NC,NS);
    free_double_tensor(skyvecsx,NC,NS);
    free_double_tensor(skyvecsy,NC,NS);
    free_double_matrix(skyevalsx,NC);
    free_double_matrix(skyevalsy,NC);
    free_double_vector(jump);
    
    free(dtimes);
    free(dtimes2);
    free(param);
    
    return;
    
}

double skylike(struct Net *net, double *params, double *D, double *H, double **DHc,  double **DHs, double dt, int nt, int flag, struct bayesCBC *rundata)
{
    int i, j, k, l, id;
    double alpha, sindelta, dphi, t0, A, FA, ecc, ciota, psi;
    double *dtimes, *F, *lambda;
    double tdelay, tx, toff, dc, ds, DH;
    double dcc, dss;
    double Fplus, Fcross;
    double Ap, Ac;
    double clam, slam, x;
    double cphi, sphi;
    double logL;
    
    
    logL = 0.0;
    
    if(rundata->constantLogLFlag == 0)
    {
        
        dtimes = (double*)malloc(sizeof(double)* 5);
        F = (double*)malloc(sizeof(double)* 5);
        lambda = (double*)malloc(sizeof(double)* 5);
        
        k = (nt-1)/2;
        
        alpha = params[0];
        sindelta = params[1];
        psi = params[2];
        ciota = params[3];
        
        Ap = (1.0+ciota*ciota)/2.0;
        Ac = ciota;
        
        A = params[4];
        dphi = params[5];
        toff = params[6];
        
        cphi = cos(dphi);
        sphi = sin(dphi);
        
        TimeDelays(net, alpha, sindelta, dtimes, rundata->gmst);

        for(id = 0; id<net->Nifo; id++)
        {
            ComputeDetFant(net, psi, alpha, sindelta, &Fplus, &Fcross, id, rundata->gmst);
            F[id] = sqrt(Ap*Ap*Fplus*Fplus+Ac*Ac*Fcross*Fcross);  // magnitude of response
            lambda[id] = atan2(-1*Ac*Fcross,Ap*Fplus);
            while(lambda[id] < 0.0) lambda[id] += TPI;
        }
        
        logL = 0.0;
        
        // Note that F[] and lambda[] have already used the label array to get the correct detector ordering while dtimes uses a fixed ordering
        // so needs the label array to get the correct delays
        
        for(id = 0; id<net->Nifo; id++)
        {
            // everything is reference to geocenter
            tdelay = toff + dtimes[id];
            
            i = (int)(floor(tdelay/dt));
            
            tx = (tdelay/dt - (double)(i));
            
            // if (flag == 1) printf("%d %d %d %f %f\n", i, i+k, nt, tx, tdelay/dt);

            // printf("toff %f, dtimes[%i] = %f (label= %i)\n", toff, id, dtimes[net->labels[id]], net->labels[id]);
            // printf("%d %d %d %f %f\n", i, i+k, nt, tx, tdelay/dt);
            
            // linear interpolation
            i += k;
            
            slam = sin(lambda[id]);
            clam = cos(lambda[id]);
            
            FA = A*F[id];
            
            if(i >= 0 && i < nt-2)
            {
                dc = DHc[id][i]*(1.0-tx)+DHc[id][i+1]*tx;
                ds = DHs[id][i]*(1.0-tx)+DHs[id][i+1]*tx;
                
                
                // put in phase rotation
                dcc = cphi*dc+sphi*ds;
                dss = -sphi*dc+cphi*ds;
                
                DH = clam*dcc+slam*dss;
                
                // relative likelihood
                x = -(FA*FA*H[id]-2.0*FA*DH)/2.0;
                
                // if (flag == 1) printf("%d %f %f\n", id, tdelay, x);
                // printf("%d %f %f\n", id, tdelay, x);
                logL += x;
                
            }
            else
            {
                logL -= 1.0e10;
            }
            
        }
        
        free(dtimes);
        free(F);
        free(lambda);
        
    }
    
    return(logL);
    
}

void fisher_matrix_fastsky(struct Net *net, double *params, double **fisher, double **HH, int NS, double gmst)
{
    int i, j, k, l, id;
    double alpha, sindelta, phi0, t0, A, ecc, ciota, psi;
    double Ap, Ac;
    double dtimesP[4], dtimesM[4], dt0[4], dt1[4];
    double tdelay, tx, toff, dc, ds, DH;
    double dcc, dss;
    double Fplus, Fcross, epss;
    double FplusP, FcrossP, FplusM, FcrossM;
    double *dFplus, *dFcross;
    double *dlambda, *dtoff, *dF;
    double F, lambda, clam, slam, x, y, z;
    double cphi, sphi;
    double logL;
    
    dFplus = double_vector(NS);
    dFcross = double_vector(NS);
    dF = double_vector(NS);
    dlambda = double_vector(NS);
    dtoff = double_vector(NS);
    
    for(i = 3; i<NS; i++) dFplus[i] = 0.0;
    for(i = 3; i<NS; i++) dFcross[i] = 0.0;
    
    dF[5] = 0.0;
    dF[6] = 0.0;
    
    dlambda[4] = 0.0;
    dlambda[5] = 1.0;  // minus sign in phase definition
    dlambda[6] = 0.0;
    
    
    dtoff[2] = 0.0;
    dtoff[3] = 0.0;
    dtoff[4] = 0.0;
    dtoff[5] = 0.0;
    dtoff[6] = -1.0;
    
    alpha = params[0];
    sindelta = params[1];
    psi = params[2];
    ciota = params[3];
    A = params[4];
    phi0 = params[5];
    toff = params[6];
    
    Ap = 0.5*(1.0+ciota*ciota);
    Ac = -1*ciota;
    
    
    cphi = cos(phi0);
    sphi = sin(phi0);
    
    epss = 1.0e-6;
    
    TimeDelays(net, alpha+epss, sindelta, dtimesP, gmst);
    TimeDelays(net, alpha-epss, sindelta, dtimesM, gmst);
    for(id = 0; id<net->Nifo; id++)
    {
      // difference in geocenter time delays (in ifo id) after epsilon of time
      dt0[id] = (dtimesP[id] - dtimesM[id])/(2.0*epss);
    }
    TimeDelays(net, alpha, sindelta+epss, dtimesP, gmst);
    TimeDelays(net, alpha, sindelta-epss, dtimesM, gmst);
    for(id = 0; id<net->Nifo; id++)
    {
      // difference in geocenter time delays after epsilon of sindelta
      dt1[id] = (dtimesP[id] - dtimesM[id])/(2.0*epss);
    }
    
    for(i = 0; i<NS; i++)
    {
        for(j = 0; j<NS; j++)
        {
            fisher[i][j] = 0.0;
        }
    }
    
    for(id = 0; id<net->Nifo; id++)
    {
        
        dtoff[0] = -dt0[id];
        dtoff[1] = -dt1[id];
        
        //Calculate antenna pattern once for each detector
        //This is only OK because psi is constant over the orbits...not so if spin is added
        ComputeDetFant(net, psi, alpha, sindelta, &Fplus, &Fcross, id, gmst);
              
        F = A*sqrt(Ap*Ap*Fplus*Fplus+Ac*Ac*Fcross*Fcross);
        
        ComputeDetFant(net, psi, alpha+epss, sindelta, &FplusP, &FcrossP, id, gmst);
        ComputeDetFant(net, psi, alpha-epss, sindelta, &FplusM, &FcrossM, id, gmst);
        
        dFplus[0] = (FplusP-FplusM)/(2.0*epss);
        dFcross[0] = (FcrossP-FcrossM)/(2.0*epss);
        
        ComputeDetFant(net, psi, alpha, sindelta+epss, &FplusP, &FcrossP, id, gmst);
        ComputeDetFant(net, psi, alpha, sindelta-epss, &FplusM, &FcrossM, id, gmst);
        
        dFplus[1] = (FplusP-FplusM)/(2.0*epss);
        dFcross[1] = (FcrossP-FcrossM)/(2.0*epss);
        
        ComputeDetFant(net, psi+epss, alpha, sindelta, &FplusP, &FcrossP, id, gmst);
        ComputeDetFant(net, psi-epss, alpha, sindelta, &FplusM, &FcrossM, id, gmst);
        
        dFplus[2] = (FplusP-FplusM)/(2.0*epss);
        dFcross[2] = (FcrossP-FcrossM)/(2.0*epss);
        
        x = A*A/(F);
        dF[0] = x*(Ap*Ap*Fplus*dFplus[0]+Ac*Ac*Fcross*dFcross[0]);
        dF[1] = x*(Ap*Ap*Fplus*dFplus[1]+Ac*Ac*Fcross*dFcross[1]);
        dF[2] = x*(Ap*Ap*Fplus*dFplus[2]+Ac*Ac*Fcross*dFcross[2]);
        dF[3] = x*(Ap*ciota*Fplus*Fplus+Ac*Fcross*Fcross);
        dF[4] = F/A;
        
        x = A*A/(F*F);
        dlambda[0] = x*Ap*Ac*(Fplus*dFcross[0]-Fcross*dFplus[0]);
        dlambda[1] = x*Ap*Ac*(Fplus*dFcross[1]-Fcross*dFplus[1]);
        dlambda[2] = x*Ap*Ac*(Fplus*dFcross[2]-Fcross*dFplus[2]);
        dlambda[3] = x*Fcross*Fplus*(Ap-Ac*ciota);
        
        x = F*F;
        y = TPI*x;
        z = TPI*y;
        for(i = 0; i<NS; i++)
        {
            for(j = 0; j<NS; j++)
            {
                fisher[i][j] += HH[id][0]*(dF[i]*dF[j]+x*dlambda[i]*dlambda[j])+HH[id][1]*y*(dlambda[i]*dtoff[j]+dlambda[j]*dtoff[i])+HH[id][2]*z*dtoff[i]*dtoff[j];
            }
        }
        
        
    }
    
    /*
     printf("\n");
     for(i = 0; i<NS; i++)
     {
     for(j = 0; j<NS; j++)
     {
     printf("%e ", fisher[i][j]);
     }
     printf("\n");
     }*/
    
    free_double_vector(dFplus);
    free_double_vector(dFcross);
    free_double_vector(dF);
    free_double_vector(dlambda);
    free_double_vector(dtoff);
}


void dshifts(struct Net *net, double *sky, double *params, int NX, struct bayesCBC *rundata)
{
    int i, j, k, id;
    double dtimes[4];
    double Fplus[4], Fcross[4], F[4];
    double lambda[4];
    double logL, logLmax, ecc;
    

    double ciota = sky[3];
    
    double Ap = (1.0+ciota*ciota)/2.0;
    double Ac = ciota;
    
    // Times(alpha, sindelta, dtimes, gmst);
        
        // F_ant(psi, alpha, sindelta, &Fplus, &Fcross, net->labels[id], gmst);


    // sky parameter order
    //[0] alpha, [1] sin(delta) [2] psi [3] cos(iota) [4] scale [5] dt [6] phi0

    TimeDelays(net, sky[0], sky[1], dtimes, rundata->gmst);

    for(id = 0; id<net->Nifo; id++)
    {
        ecc = 2.0*sky[3]/(1.0+sky[3]*sky[3]);
        ComputeDetFant(net, sky[2], sky[0], sky[1], &Fplus[id], &Fcross[id], id, rundata->gmst);

        F[id] = sqrt(Ap*Ap*Fplus[id]*Fplus[id]+Ac*Ac*Fcross[id]*Fcross[id]);  // magnitude of response
        lambda[id] = atan2(-1*Ac*Fcross[id],Ap*Fplus[id]);

        // TODO: IS THIS CORRECT? I don't think so.
        // F[id] = sqrt(Fplus[id]*Fplus[id]+ecc*ecc*Fcross[id]*Fcross[id]);
        // lambda[id] = atan2(-1*ecc*Fcross[id],Fplus[id]);

        while(lambda[id] < 0.0) lambda[id] += TPI;
    }
    
    // Note that F[] and lambda[] have already used the label array to get the correct detector ordering while dtimes uses a fixed detector ordering so needs the label array to get the correct delays
    
    for(id = 1; id<net->Nifo; id++)
    {
      params[(id-1)*3+NX] = lambda[id]-lambda[0]; // GW phase offset
      while(params[(id-1)*3+NX] < 0.0) params[(id-1)*3+NX] += TPI;
      while(params[(id-1)*3+NX] > TPI) params[(id-1)*3+NX] -= TPI;
      params[(id-1)*3+NX+1] = dtimes[id] - dtimes[0]; // time offset between geocenter
                                                      // shifts in different detectors
      params[(id-1)*3+NX+2] = F[id]/F[0]; // amplitude ratio
    }
    
}

void skyring(struct Net *net, double *params, double *sky, double *pall, double *SNRsq, RealVector *freq, double **D, double **SN, int N, double Tobs, gsl_rng *r, int NX, int NS, double gmst)
{
    int i, j, k, id;
    double *delt;
    double Fp, Fc, Fs;
    double logL, logLmax;
    double *stry;

    stry = double_vector(NS);
    delt = double_vector(net->Nifo);

    for(id = 1; id< net->Nifo; id++)
    {
        delt[id] = -params[(id-1)*3+NX+1]; // time difference between (id) detector time and geocenter
    }
    
    // sky parameter order
    //[0] alpha, [1] sin(delta) [2] psi [3] cosi [4] scale [5] dt [6] dphi
    
    // Scale changes the overall distance reference at geocenter
    // dt shifts the geocenter time
    // dphi shifts the geocenter phase
    
    stry[4] = 1.0;
    stry[5] = 0.0;
    stry[6] = 0.0;
    
    // find sky locations consistent with time delays
    ringfind(net, delt, stry, SNRsq, r, gmst);

    stry[2] = PI*gsl_rng_uniform(r);
    stry[3] = -1.0+2.0*gsl_rng_uniform(r);
        
    // map intrinsic + sky params back to geocenter params
    pmap_all(net, pall, params, stry, NX, gmst);

    for(j = 0; j< NS; j++) sky[j] = stry[j];
    
    free_double_vector(delt);
    free_double_vector(stry);
    
}


/*
 * Quick search to find sky locations roughly consistent with time delays
 * (to set start parameters of sky mcmc burnin)
 */
void ringfind(struct Net *net, const double *tdelays, double *params, const double *SNRsq, gsl_rng *r, double gmst)
{
    double alpha, sindelta, beta;
    double alphax, sindeltax, lx;
    double *dtimes;
    double SNRT;
    double tolt, x, y, logp, dtd;
    int i, id, j, k;
    
    dtimes = (double*)malloc(sizeof(double)* 5);
    
    tolt = 2.0e-4;
    
    lx = -1.0e30;
    
    SNRT = 0.0;
    for(id = 1; id<net->Nifo; id++) SNRT += SNRsq[id];

    for(i = 0; i< 40000; i++)  // 40K trials is roughly a 1 degree search
    {

        alpha = TPI*gsl_rng_uniform(r);
        sindelta = -1.0+2.0*gsl_rng_uniform(r);
        TimeDelays(net, alpha, sindelta, dtimes, gmst);
        
        logp = 0.0;
        for(id = 1; id<net->Nifo; id++)
        {
            dtd = dtimes[0]-dtimes[id];
            x = (tdelays[id]-dtd)/tolt;
            y = x*x*SNRsq[id]/SNRT;
            logp -= y/2.0;
        }
 
        if(logp > lx)
        {
            lx = logp;
            alphax = alpha;
            sindeltax = sindelta;
        }
  
    }
    
    params[0] = alphax;
    params[1] = sindeltax;
    
    free(dtimes);
    
    return;
}

void skylikesetup(struct Net *net, double **data,  double **wave, double *D, double *H, double **DHc,  double **DHs, double Tobs, int n, int bn, int nt, int imin, int imax)
{
    double *corr, *corrf;
    double dt;
    int i, j, k, kk, l, ii, id;
    
    dt = Tobs/(double)(bn);
    
    corr = double_vector(bn);
    corrf = double_vector(bn);
    
    for(id = 0; id<net->Nifo; id++)
    {
        
        D[id] = 4.0*f_nwip(data[id], data[id], n);
        H[id] = 4.0*f_nwip(wave[id], wave[id], n);
        
        // The arrays are zero padded to increase the sample cadence
        
        for (i=0; i < bn; i++)
        {
            corr[i] = 0.0;
            corrf[i] = 0.0;
        }
        
        // the non-zero part
        for (i=1; i < n/2; i++)
        {
            l=i;
            k=bn-i;
            kk = n-i;
            corr[l] = ( data[id][l]*wave[id][l] + data[id][kk]*wave[id][kk]);
            corr[k] = ( data[id][kk]*wave[id][l] - data[id][l]*wave[id][kk]);
            corrf[l] = corr[k];
            corrf[k] = -corr[l];
        }
        
        
        gsl_fft_halfcomplex_radix2_inverse(corr, 1, bn);
        gsl_fft_halfcomplex_radix2_inverse(corrf, 1, bn);
        
        
        k = (nt-1)/2;
        
        for(i=-k; i<k; i++)
        {
            j = i+k;
            
            if(i < 0)
            {
                DHc[id][j] = 2.0*(double)(bn)*corr[bn+i];
                DHs[id][j] = 2.0*(double)(bn)*corrf[bn+i];
            }
            else
            {
                DHc[id][j] = 2.0*(double)(bn)*corr[i];
                DHs[id][j] = 2.0*(double)(bn)*corrf[i];
            }
            
        }
        
        
    }
    
    free_double_vector(corr);
    free_double_vector(corrf);

}

void FisherEvec(double **fish, double *ej, double **ev, int d)
{
    // Calculates eigenvectors (ev) and 1 / sqrt(eigen values) (ej) for a dxd matrix
    int i, j, ec, sc;
    double x, maxc;

    // makes sure that the fisher matrix is suitable well-behaved (invertible)
    // this seems like a proxy for a determinant,
    // but there are invertible where this is true... and non-invertible matrices where this is untrue!
    ec = 0;
    for (i = 0 ; i < d ; i++) if(fabs(fish[i][i]) < 1.0e-16) ec = 1;
    
    if(ec == 0)
    {
        // initialize matrix form for fisher matrix
        gsl_matrix *m = gsl_matrix_alloc (d, d);

        for (i = 0 ; i < d ; i++)
        {
            for (j = 0 ; j < d ; j++)
            {
                gsl_matrix_set(m, i, j, fish[i][j]);
            }
        }

        gsl_vector *eval = gsl_vector_alloc (d);
        gsl_matrix *evec = gsl_matrix_alloc (d, d);
        
        gsl_eigen_symmv_workspace * w = gsl_eigen_symmv_alloc (d);
        
        ec = gsl_eigen_symmv (m, eval, evec, w);
        
        gsl_eigen_symmv_free (w);
        
     
        sc = gsl_eigen_symmv_sort (eval, evec, GSL_EIGEN_SORT_ABS_ASC);
        
        for (i = 0; i < d; i++)
        {
            ej[i] = gsl_vector_get (eval, i);
            
            // printf("eigenvalue[%i] = %g\n", i, ej[i]);
            for (j = 0 ; j < d ; j++)
            {
                ev[i][j] = gsl_matrix_get(evec, j, i);
                // printf("evec[%i, %i] = %f, ", i, j, ev[i][j]);
            }
            // printf("\n");
            
        }
        
        for (i = 0; i < d; i++)
        {
            // make sure the largest eigen values are not too small
            //if(ej[i] < 1.0 && i < 2) ej[i] = 1.0;
            if (ej[i] == 0) ej[i] = 1.0; // do not divide by 0
            // turn into 1-sigma jump amplitudes
            ej[i] = 1.0/sqrt(ej[i]);
            // printf("jump %d = %g\n", i, ej[i]);
        }
        
        gsl_matrix_free (m);
        gsl_vector_free (eval);
        gsl_matrix_free (evec);
        
    }
    else
    {
        // places where fisher matrix is not invertible (low curvature regions), set jump proposal high,
        // and jump along paramter directions
        for (i = 0; i < d; i++)
        {
            ej[i] = 10000.0;
            for (j = 0 ; j < d ; j++)
            {
                ev[i][j] = 0.0;
                if(i==j) ev[i][j] = 1.0;
            }
        }
    }
    return;
    
}

void fisherskysetup(struct Net *net, double **wave, double **HH, double Tobs, int n)
{
    double f;
    int i, j, k, l, bn, ii, id;
    double **wavef;
    
    wavef = double_matrix(net->Nifo,n);
    
    for(id = 0; id<net->Nifo; id++)
    {
        wavef[id][0] = 0.0;
        wavef[id][n/2] = 0.0;
        for(i = 1; i<n/2; i++)
        {
            f = (double)(i)/Tobs;
            j = i;
            k = n-i;
            wavef[id][j] = f*wave[id][j];
            wavef[id][k] = f*wave[id][k];
        }
    }
    
    for(id = 0; id<net->Nifo; id++)
    {
        HH[id][0] =  4.0*f_nwip(wave[id], wave[id], n);
        HH[id][1] =  4.0*f_nwip(wave[id], wavef[id], n);
        HH[id][2] =  4.0*f_nwip(wavef[id], wavef[id], n);
    }
    
    free_double_matrix(wavef,net->Nifo);
    
}

void fisher_skyproposal(gsl_rng *r, double **skyvecs, double *skyevals, double *jump, struct bayesCBC *rundata)
{
    int i, j, k;
    int a;
    double scale, x;
    int NS = rundata->NS;
    
    // pick eigenvector to jump along
    a = (int)(gsl_rng_uniform(r)*(double)(NS));
    
    // set size of jump
    scale = gsl_ran_gaussian(r,1.0)*skyevals[a];
    
    
    //decompose eigenjumps into parameter directions
    
    for (j=0; j<NS; j++) jump[j] = scale*skyvecs[a][j];
    
}


// finds the extrinsic parameters at the new sky location that preserve the waveforms
void skymap(struct Net *net, double *paramsx, double *paramsy, int ifo1, int ifo2, int iref, double gmst)
{
    double phi, costheta, sintheta, cosdelta;
    double alpha, delta, sindelta, phi0, Phase, psi, psi0;
    double ecc, scale;
    double tl;
    double lAmin, lAmax, A;
    double *fyplus, *fycross;
    double *fxplus, *fxcross;
    double ux, vx, wx, zx;
    double uy, vy, wy, zy;
    double phiy, psiy, Ay, ciotay;
    double phix, psix, Ax, ciotax;
    int i, j, mc, ii, test;
    double Fp, Fc, Fs;
    double x, y, px, py;
    int nfunk;
    double *dtimesx, *dtimesy;
    
    fyplus = double_vector(2);
    fycross = double_vector(2);
    fxplus = double_vector(2);
    fxcross = double_vector(2);
    dtimesx = double_vector(5);
    dtimesy = double_vector(5);
    
    // extract the primitives f+, fx for each detector at x
    alpha = paramsx[0];
    sindelta = paramsx[1];
    ComputeDetFant(net, 0.0, alpha, sindelta, &fxplus[1], &fxcross[1], ifo1, gmst);
    ComputeDetFant(net, 0.0, alpha, sindelta, &fxplus[2], &fxcross[2], ifo2, gmst);
    
    TimeDelays(net, alpha, sindelta, dtimesx, gmst);
    
    // extract the primitives f+, fx for each detector at y
    alpha = paramsy[0];
    sindelta = paramsy[1];
    ComputeDetFant(net,0.0, alpha, sindelta, &fyplus[1], &fycross[1], ifo1, gmst);
    ComputeDetFant(net,0.0, alpha, sindelta, &fyplus[2], &fycross[2], ifo2, gmst);
    
    TimeDelays(net, alpha, sindelta, dtimesy, gmst);
    
    uvwz(&ux, &vx, &wx, &zx, paramsx);
    
    uvwz_sol(&uy, &vy, &wy, &zy, ux, vx, wx, zx, fxplus[1], fyplus[1], fxcross[1], fycross[1], fxplus[2], fyplus[2], fxcross[2], fycross[2]);
    
    exsolve(&phiy, &psiy, &Ay, &ciotay, uy, vy, wy, zy);
    
    paramsy[2] = psiy;
    paramsy[3] = ciotay;
    paramsy[4] = Ay;
    paramsy[5] = phiy;
    
    paramsy[6] = paramsx[6]+dtimesx[ifo1]-dtimesy[ifo1];
    
    //[0] alpha, [1] sin(delta) [2] psi [3] cos(iota) [4] scale [5] phi0 [6] dt
    
    
    free_double_vector(dtimesx);
    free_double_vector(dtimesy);
    free_double_vector(fyplus);
    free_double_vector(fycross);
    free_double_vector(fxplus);
    free_double_vector(fxcross);
    
}

void Ring(struct Net *net, const double *skyx, double *skyy, int d1, int d2, gsl_rng *r, double gmst)
{

    int i;
    double *ifo1, *ifo2;
    double *kv, *kvr;
    double *zv, *uv, *vv;
    double sind, cosd, rot, alpha, x;
    double cr, sr;
    double calpha, salpha;
    double H1mag, L1mag, zmag;
    double cmu, smu;

    ifo1 = (double*)malloc(sizeof(double)* 3);
    ifo2 = (double*)malloc(sizeof(double)* 3);

    zv = (double*)malloc(sizeof(double)* 3); // unit vector pointing from ifo 2 to ifo 1
    uv = (double*)malloc(sizeof(double)* 3);
    vv = (double*)malloc(sizeof(double)* 3);
    kv = (double*)malloc(sizeof(double)* 3); // gw propogation direction
    kvr = (double*)malloc(sizeof(double)* 3);

    ifo1[0] = net->location[d1][0]/CLIGHT;
    ifo1[1] = -1 * net->location[d1][1]/CLIGHT;
    ifo1[2] = net->location[d1][2]/CLIGHT;

    ifo2[0] = net->location[d2][0]/CLIGHT;
    ifo2[1] = -1 * net->location[d2][1]/CLIGHT;
    ifo2[2] = net->location[d2][2]/CLIGHT;

    // From https://dcc.ligo.org/public/0072/P000006/000/P000006-C.pdf  (note sign flip on y axis)

    // Normalize unit vector
    for (int i = 0; i < 3; i++){
      zv[i] = ifo1[i] - ifo2[i];
    }
    zmag = 0.0;
    for(i = 0; i <3; i++) zmag += zv[i]*zv[i];
    zmag = sqrt(zmag);

    // Unet->Nifot vector between the sites
    for(i = 0; i <3; i++) zv[i] /= zmag;

    //remap gmst back to [0:2pi]
    double intpart;
    double decpart;
    double gmst_rad = gmst/LAL_TWOPI;
    intpart = (int)( gmst_rad );
    decpart = gmst_rad - (double)intpart;
    gmst_rad = decpart*LAL_TWOPI;

    alpha = skyx[0];
    sind = skyx[1];
    cosd = sqrt(1.0-sind*sind);
    calpha = cos(alpha-gmst_rad);
    salpha = -sin(alpha-gmst_rad);

    // Unusual convention on spherical angle
    // propagation direction
    kv[0] = cosd*calpha;
    kv[1] = cosd*salpha;
    kv[2] = sind;

    // cosine of angle (mu) between propagation direction and vector connecting the sites
    cmu = 0.0;
    for(i = 0; i <3; i++) cmu += zv[i]*kv[i];

    if(cmu > 0.999999) cmu = 0.999999;
    if(cmu < -0.999999) cmu = -0.999999;
    // sin(mu)
    smu = sqrt(1.0-cmu*cmu);

    // vector in plane of prop vector and line connecting sites
    for(i = 0; i <3; i++) uv[i] = (kv[i] - cmu*zv[i])/smu;

    //completing the triad
    vv[0] = (zv[1]*kv[2]-zv[2]*kv[1])/smu;
    vv[1] = (zv[2]*kv[0]-zv[0]*kv[2])/smu;
    vv[2] = (zv[0]*kv[1]-zv[1]*kv[0])/smu;

    // Rotating about zv axis will produce new sky locations with the same time delay between ifo1 and ifo2
    rot = TPI*gsl_rng_uniform(r);
    cr = cos(rot);
    sr = sin(rot);

    // find rotated k vector
    for(i = 0; i <3; i++) kvr[i] = cmu * zv[i] + smu * (cr * uv[i] + sr * vv[i]);

    skyy[1] = kvr[2];

    rot = atan2(kvr[1], kvr[0]);
    rot = gmst_rad - rot;
    while(rot < 0.0) rot += TPI;

    skyy[0] = rot;

    free(ifo1);
    free(ifo2);
    free(kv);
    free(kvr);
    free(zv);
    free(uv);
    free(vv);
}

/**
 * Map all parameters in sky & paramx array to `pall` array (which is relative to geocenter 
 * and holds sky params too). Version of pmap that updates extrinsic projection (Fcross/Fplus/dtimes).
 */
void pmap_all(struct Net *net, double *pall, double *param, double *sky, int NX, double gmst)
{
    int j;
    double ciota, Ap, Ac, alpha, sindelta, psi, Fs;
    double *dtimes;
    double Fplus, Fcross, lambda;

    // sky  [0] alpha, [1] sin(delta) [2] psi [3] ciota [4] scale [5] dphi [6] dt
    // param [0] log(Mc) [1] log(Mt) [2] chi1 [3] chi2 [4] phi0  [5] tp0 [6] log(DL0) then relative amplitudes, time, phases
    // pall  [0] log(Mc) [1] log(Mt) [2] chi1 [3] chi2 [4] 2phi0 [5] tp [6] log(DL) [7] alpha [8] sindelta [9] psi [10] ciota

    dtimes = (double*)malloc(sizeof(double)* 5);

    for (j = 0; j < NX; ++j)
    {
        pall[j] = param[j];
    }

    ciota = sky[3];
    Ap = (1.0+ciota*ciota)/2.0;
    Ac = ciota;
    alpha = sky[0];
    sindelta = sky[1];
    psi = sky[2];

    TimeDelays(net, alpha, sindelta, net->dtimes, gmst);

    ComputeDetFant(net, psi, alpha, sindelta, &net->Fplus[0], &net->Fcross[0], 0, gmst);
    Fs =  Fmag(Ap, Ac, net->Fcross[0], net->Fplus[0]); // magnitude of response
    lambda = phase_from_polarizations(Ap, Ac, net->Fcross[0], net->Fplus[0]); // get phase from polarizations

    // move reference point from ref detector to geocenter
    pall[4] = 2.0*param[4] - lambda;
    while (pall[4] > TPI){pall[4] -= TPI;}
    while (pall[4] < 0){pall[4] += TPI;}
    pall[5] = param[5] - net->dtimes[0];
    pall[6] = param[6] + log(Fs); // SOPHIE THIS GETS KNOCKED OUT OF BOUNDS

    pall[NX] = alpha;
    pall[NX+1] = sindelta;
    pall[NX+2] = psi;
    pall[NX+3] = ciota;
    
    free(dtimes);
}


double log_likelihood_full_old(struct Net *net, double **D, double *params, RealVector *freq, double **SN, double *rho, int N, double Tobs, struct bayesCBC *rundata, int extFlag)
{
    int id;
    double logL, logLdet;
    double **twave;
    double HH, HD, x;
    int imin, imax;
    double fmin, fmax;
    double *ampphase;
    double *sky;

    double ciota, Ap, Ac, alpha, sindelta, psi;

    fmin = rundata->fmin;
    fmax = rundata->fmax;
    logL = 0.0;

    // clock_t start, end;
    // double cpu_time_used;
    
    // int i;
    if(rundata->constantLogLFlag == 0)
    {
            // start = clock();

            // for (i=0; i<1000; i++){

                imin = (int)(Tobs*fmin);
                imax = (int)(Tobs*fmax);
                if(imax > N/2) imax = N/2;
                
                twave = double_matrix(net->Nifo,N);
                ampphase = double_vector(N);
                sky = double_vector(rundata->NS);

                ciota = params[rundata->NX+3];
                Ap = (1.0+ciota*ciota)/2.0;
                Ac = ciota;
                alpha = params[rundata->NX];
                sindelta = params[rundata->NX+1];
                psi = params[rundata->NX+2];
                
                sky[0] = alpha;
                sky[1] = sindelta;
                sky[2] = psi;
                sky[3] = ciota;
                sky[4] = 1.0;
                sky[5] = 0.0;
                sky[6] = 0.0;
            
            
                // If external update flag is 1, we recompute the time delays and
                // detector responses here. This is used in the extrinsic burnin.
                if (extFlag == 1)
                {
                    TimeDelays(net, alpha, sindelta, net->dtimes, rundata->gmst);
                    for(id = 0; id < net->Nifo; id++) ComputeDetFant(net, psi, alpha, sindelta, &net->Fplus[id], &net->Fcross[id], id, rundata->gmst);

                }
                
                // fmin = fbegin(params);
                
                // Compute template with current parameters
                geowave(ampphase, freq,  params, N, rundata);
                projectCBCWaveform(ampphase, N, net->Nifo, fmin, Tobs, sky, twave, net->dtimes, net->Fplus, net->Fcross);         
                
                // double logLfull;
                logL = 0.0;
                for(id = 0; id < net->Nifo; id++)
                {
                    HH = fourier_nwip_cbc(twave[id], twave[id], SN[id], imin, imax, N);
                    HD = fourier_nwip_cbc(D[id], twave[id], SN[id], imin, imax, N);
                    rho[id] = HD/sqrt(HH);
                    logLdet = HD-0.5*HH;
                    logL += logLdet;
                }
                
                // printf("log_likelihood_het: %f, logLfull: %f, diff: %f\n", logL, logLfull, logL - logLfull);

                free_double_matrix(twave,net->Nifo);

                free_double_vector(ampphase);
                free_double_vector(sky);
            }

            // end = clock();
            // cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
            // // printf("start: %e, end: %e, CLOCKS_PER_SEC: %e\n", (double)(start),(double)(end), (double)(CLOCKS_PER_SEC));
            // printf("Full likelihood took %e s\n", cpu_time_used/1000.0);
        // }
    // }
    
    return(logL);
    
}

double log_likelihood_full(struct Net *net, double **D, double *params, RealVector *freq, double **SN, double *rho, int N, double Tobs, struct bayesCBC *rundata, int extFlag)
{
    int id;
    double logL, logLdet;
    double **twave;
    double HH, HD, x;
    int imin, imax;
    double fmin, fmax;
    double *ampphase;
    double *sky;

    double ciota, Ap, Ac, alpha, sindelta, psi;

    fmin = rundata->fmin;
    fmax = rundata->fmax;
    logL = 0.0;

    // clock_t start, end;
    // double cpu_time_used;
    
    // int i;
    if(rundata->constantLogLFlag == 0)
    {
        // Select which Likelihood version to use (note: in burnin the heterodyne likelihood is never used)
        if (rundata->heterodyneFlag && rundata->het->initFlag == 1) 
        {   
            // start = clock();
            // for (i=0; i<1000; i++) 
            logL = log_likelihood_het(net, rundata->het, params, Tobs, rundata, extFlag);
            // end = clock();
            // cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
            // // printf("start: %e, end: %e, CLOCKS_PER_SEC: %e\n", (double)(start),(double)(end), (double)(CLOCKS_PER_SEC));
            // printf("Heterodyne likelihood took %e s\n", cpu_time_used/1000.0);
            // fflush(stdout);

        } 
        else 
        {
            // start = clock();

            // for (i=0; i<1000; i++){

                imin = (int)(Tobs*fmin);
                imax = (int)(Tobs*fmax);
                if(imax > N/2) imax = N/2;
                
                twave = double_matrix(net->Nifo,N);
                ampphase = double_vector(N);
                sky = double_vector(rundata->NS);

                ciota = params[rundata->NX+3];
                Ap = (1.0+ciota*ciota)/2.0;
                Ac = ciota;
                alpha = params[rundata->NX];
                sindelta = params[rundata->NX+1];
                psi = params[rundata->NX+2];
                
                sky[0] = alpha;
                sky[1] = sindelta;
                sky[2] = psi;
                sky[3] = ciota;
                sky[4] = 1.0;
                sky[5] = 0.0;
                sky[6] = 0.0;
            
            
                // If external update flag is 1, we recompute the time delays and
                // detector responses here. This is used in the extrinsic burnin.
                if (extFlag == 1)
                {
                    TimeDelays(net, alpha, sindelta, net->dtimes, rundata->gmst);
                    for(id = 0; id < net->Nifo; id++) ComputeDetFant(net, psi, alpha, sindelta, &net->Fplus[id], &net->Fcross[id], id, rundata->gmst);

                }
                
                // fmin = fbegin(params);
                
                // Compute template with current parameters
                geowave(ampphase, freq,  params, N, rundata);
                projectCBCWaveform(ampphase, N, net->Nifo, fmin, Tobs, sky, twave, net->dtimes, net->Fplus, net->Fcross);         
                
                for(id = 0; id < net->Nifo; id++)
                {
                    HH = fourier_nwip_cbc(twave[id], twave[id], SN[id], imin, imax, N);
                    HD = fourier_nwip_cbc(D[id], twave[id], SN[id], imin, imax, N);
                    rho[id] = HD/sqrt(HH);
                    logLdet = HD-0.5*HH;
                    logL += logLdet;
                }
                
                // printf("log_likelihood_het: %f, logLfull: %f, diff: %f\n", logL, logLfull, logL - logLfull);

                free_double_matrix(twave,net->Nifo);

                free_double_vector(ampphase);
                free_double_vector(sky);
        }

            // end = clock();
            // cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
            // // printf("start: %e, end: %e, CLOCKS_PER_SEC: %e\n", (double)(start),(double)(end), (double)(CLOCKS_PER_SEC));
            // printf("Full likelihood took %e s\n", cpu_time_used/1000.0);
    }
        
    return(logL);
    
}

/* ********************************************************************************** */
/*                                                                                    */
/*                        Repetitive tasks for projection                             */
/*                                                                                    */
/* ********************************************************************************** */

/*
 * Magnitude of detector response
 */
double Fmag(double Ap, double Ac, double Fcross, double Fplus)
{
    double Fmag = sqrt(Ap*Ap*Fplus*Fplus+Ac*Ac*Fcross*Fcross);  
    return Fmag;
}

/*
 * Phase from polarizations
 */
double phase_from_polarizations(double Ap, double Ac, double Fcross, double Fplus)
{
    double lambda;
    lambda = atan2(-Ac*Fcross,Ap*Fplus);
    while(lambda < 0.0) lambda += TPI;
    return lambda;
}


/* ********************************************************************************** */
/*                                                                                    */
/*                                Wrappers for LAL                                    */
/*                                                                                    */
/* ********************************************************************************** */

/*
 * 
 * (to set start parameters of sky mcmc burnin)
 */
void TimeDelays(struct Net *net, double alpha, double sindelta, double *dtimes, double gmst)
{
    int ifo;
    LIGOTimeGPS gps;

    // convert gmst double to ligo GPS Time
    // gps = XLALGreenwichMeanSiderealTimeToGPS((REAL8) gmst, &gps);
    
    for(ifo=0; ifo<net->Nifo; ifo++)
    {   
        dtimes[ifo] = XLALTimeDelayFromEarthCenter(net->location[ifo], alpha, asin(sindelta), XLALGreenwichMeanSiderealTimeToGPS((REAL8) gmst, &gps));
        // dtimes[ifo] = -1*dtimes[ifo];
    }
}

void ComputeDetFant(struct Net *net, double psi, double alpha, double sindelta, double *Fplus, double *Fcross, int id, double gmst)
{   
    int i, j;
    REAL4 response[3][3];

    for(i=0; i<3; i++) 
    {
        for(j=0; j<3; j++) response[i][j] = (REAL4) net->response[id][i][j];
    }

    XLALComputeDetAMResponse(Fplus, Fcross, (const REAL4(*)[3])response, alpha, asin(sindelta), psi, gmst);
}

/* ********************************************************************************** */
/*                                                                                    */
/*                                    CBC Templates                                   */
/*                                                                                    */
/* ********************************************************************************** */

// Note, returns hwave in array structure real[<N/2], imag[>N/2]
// Only used in burnin (extrinsic updates)
void templates(struct Net *net, double **hwave, RealVector *freq, double *params, int N, struct bayesCBC *rundata)
{
    
    AmpPhaseFDWaveform *ap = NULL;
    NRTidal_version_type NRTidal_version;
    Lambda_type lambda_type_version;
    double phi0, fRef_in, mc, q, m1_SI, m2_SI, chi1, chi2, f_min, f_max, distance;
    int ret, flag, i, j;
    double p, cp, sp;
    double f, x, y, deltaF, ts, Tobs;
    double pd, Ar, td, A, fs;
    double *extraParams;
    double mt, eta, dm;
    double lambda1, lambda2; 
    double lambdaT, dLambdaT, sym_mass_ratio_eta;

    int NX = rundata->NX;
    int NP = rundata->NP;
    int NS = rundata->NS;
    int NC = rundata->NC; // number of chains
    int NCC = rundata->NCC; // number of cold chains 
    int NH = rundata->NH; // length of history
    int NQ = rundata->NQ; // number of mass ratios in global proposal
    int NM = rundata->NM; // number of chirp masses in global proposal

    extraParams = (double*)malloc(sizeof(double)* 2);

    Tobs = 1.0/freq->data[1];
    
    fs = fbegin(params);
    
    mc = exp(params[0]);
    mt = exp(params[1]);
    
    eta = pow((mc/mt), (5.0/3.0));
    if(eta > 0.25)
    {
        dm = 0.0;
    }
    else
    {
        dm = sqrt(1.0-4.0*eta);
    }
    m1_SI = mt*(1.0+dm)/2.0;
    m2_SI = mt*(1.0-dm)/2.0;
    
    
    chi1 = params[2];
    chi2 = params[3];
    
    phi0 = params[4];
    ts = Tobs-params[5];

    distance = exp(params[6]);

    /* ==== TIDAL PARAMETERS ==== */        
    fRef_in = rundata->fref;
    NRTidal_version = rundata->NRTidal_version;
    lambda_type_version = rundata->lambda_type_version;

    if (NRTidal_version != NoNRT_V) {

        // Compute lambda1 & lambda2
        if (lambda_type_version == lambdaTilde) {
            lambdaT = params[7];
            dLambdaT = params[8];
            sym_mass_ratio_eta = m1_SI*m2_SI/((m1_SI+m2_SI)*(m1_SI+m2_SI));
            LambdaTsEta2Lambdas(lambdaT, dLambdaT, sym_mass_ratio_eta, &lambda1, &lambda2);

            extraParams[0] = lambda1; // lambda1
            extraParams[1] = lambda2; // lambda2         
        } else {
            extraParams[0] = params[7]; // lambda1
            extraParams[1] = params[8]; // lambda2            
        }
    }

    GenerateWaveform(&ap, freq, phi0, fRef_in, m1_SI, m2_SI, chi1, chi2, distance, extraParams, NRTidal_version, rundata->waveformFlag);
    // ret = IMRPhenomDGenerateh22FDAmpPhase(&ap, freq, phi0, fRef_in, m1_SI, m2_SI, chi1, chi2, distance, extraParams, NRTidal_version);

    for (j=0; j< net->Nifo; j++)
    {
        pd = 0.0; // phase offset
        td = 0.0; // time offset
        Ar = 1.0; // amplitude ratio
            
        if(j > 0)
        {
         pd = params[(j-1)*3+NX]; // phase offset
         td = params[(j-1)*3+NX+1]; // time offset
         Ar = params[(j-1)*3+NX+2]; // amplitude
        }
        hwave[j][0] = 0.0;
        hwave[j][N/2] = 0.0;
            
         for (i=1; i< N/2; i++)
         {
            f = freq->data[i];
             
            if(f > fs)
            {
            A = Ar*h22fac*ap->amp[i];
            p = ap->phase[i];
    
            x = TPI*f*(ts-td)+pd-p;
        
             hwave[j][i] = A*cos(x);
             hwave[j][N-i] = A*sin(x);
             }
             else
             {
                 hwave[j][i] = 0.0;
                 hwave[j][N-i] = 0.0;
             }
        }
    }
    
    DestroyAmpPhaseFDWaveform(ap);
    free(extraParams);
    
}

// Returns hwave in array structure real[<N/2], imag[>N/2]
// Only used in burnin (extrinsic updates)
void geotemplate(double *gwave, RealVector *freq, double *params, int N, struct bayesCBC *rundata)
{
    
    AmpPhaseFDWaveform *ap = NULL;
    NRTidal_version_type NRTidal_version;
    double phi0, fRef_in, mc, q, m1_SI, m2_SI, chi1, chi2, f_min, f_max, distance;
    int ret, flag, i, j, id;
    double p, cp, sp;
    double f, x, y, deltaF, ts, Tobs, sqT;
    double pd, Ar, td, A;
    double *extraParams;
    double mt, eta, dm, fs;
    double lambda1, lambda2;
    
    extraParams = (double*)malloc(sizeof(double)* 2);

    fs = fbegin(params);
 
    
    Tobs = 1.0/freq->data[1];
    sqT = 1.0;//sqrt(Tobs);
    
    mc = exp(params[0]);
    mt = exp(params[1]);
    
    eta = pow((mc/mt), (5.0/3.0));
    if(eta > 0.25)
    {
        dm = 0.0;
    }
    else
    {
        dm = sqrt(1.0-4.0*eta);
    }
    m1_SI = mt*(1.0+dm)/2.0;
    m2_SI = mt*(1.0-dm)/2.0;
    
    
    chi1 = params[2];
    chi2 = params[3];
    
    phi0 = 0.5*params[4];  // I'm holding the GW phase in [4], while PhenomD wants orbital
    ts = Tobs-params[5];


    distance = exp(params[6]);
    
    /* ==== TIDAL PARAMETERS ==== */        
    fRef_in = rundata->fref;
    NRTidal_version = rundata->NRTidal_version;
    

    double lambdaT, dLambdaT, sym_mass_ratio_eta;
    Lambda_type lambda_type;
    lambda_type = rundata->lambda_type_version;

    if (NRTidal_version != NoNRT_V) {

        // Compute lambda1 & lambda2
        if (lambda_type == lambdaTilde) {
            lambdaT = params[7];
            dLambdaT = params[8];
            sym_mass_ratio_eta = m1_SI*m2_SI/((m1_SI+m2_SI)*(m1_SI+m2_SI));
            LambdaTsEta2Lambdas(lambdaT, dLambdaT, sym_mass_ratio_eta, &lambda1, &lambda2);;

            extraParams[0] = lambda1; // lambda1
            extraParams[1] = lambda2; // lambda2         
        } else {
            extraParams[0] = params[7]; // lambda1
            extraParams[1] = params[8]; // lambda2            
        }
    }

    GenerateWaveform(&ap, freq, phi0, fRef_in, m1_SI, m2_SI, chi1, chi2, distance, extraParams, NRTidal_version, rundata->waveformFlag);

    // ret = IMRPhenomDGenerateh22FDAmpPhase(&ap, freq, phi0, fRef_in, m1_SI, m2_SI, chi1, chi2, distance, extraParams, NRTidal_version);
        

        gwave[0] = 0.0;
        gwave[N/2] = 0.0;
        
        for (i=1; i< N/2; i++)
        {
            f = freq->data[i];
            if(f > fs)
            {
            A = h22fac*ap->amp[i]/sqT;
            p = ap->phase[i];
            x = TPI*f*ts-p;
            gwave[i] = A*cos(x);
            gwave[N-i] = A*sin(x);
            }
            else
            {
                gwave[i] = 0.0;
                gwave[N-i] = 0.0;
            }
        }
    
    DestroyAmpPhaseFDWaveform(ap);
    free(extraParams);
    
}


// Sky network orientation proposal routines (used for skymcmc - extrinsic burnin)

void uvwz(double *u, double *v, double *w, double *z, double *params)
{
    double psi, ciota, ecc, Amp, phi;
    double cphi, sphi, c2p, s2p;
    double Ap, Ac;
    
    psi = params[2];
    ciota = params[3];
    Amp = params[4];
    phi  = params[5];
    
    Ap = 0.5*(1.0+ciota*ciota);
    Ac = -ciota;
    ecc = Ac/Ap;
    
    Amp *= Ap;
    
    cphi = cos(phi);
    sphi = sin(phi);
    c2p = cos(2.0*psi);
    s2p = sin(2.0*psi);
    
    *u = Amp*(cphi*c2p+ecc*sphi*s2p);
    *v = Amp*(cphi*s2p-ecc*sphi*c2p);
    *w = Amp*(sphi*c2p-ecc*cphi*s2p);
    *z = Amp*(sphi*s2p+ecc*cphi*c2p);
    
    return;
    
}

void uvwz_sol(double *uy, double *vy, double *wy, double *zy, double ux, double vx, double wx, double zx, \
              double fp1x, double fp1y, double fc1x, double fc1y, double fp2x, double fp2y, double fc2x, double fc2y)
{
    
    double den;
    
    den = fc1y*fp2y-fc2y*fp1y;
    
    *uy = (vx*(fc1y*fc2x-fc1x*fc2y)+ux*(fc1y*fp2x - fc2y*fp1x))/den;
    
    *vy = (vx*(fc1x*fp2y-fc2x*fp1y)+ux*(fp2y*fp1x-fp1y*fp2x))/den;
    
    *wy = (zx*(fc1y*fc2x-fc1x*fc2y)+wx*(fc1y*fp2x - fc2y*fp1x))/den;
    
    *zy = (zx*(fc1x*fp2y-fc2x*fp1y)+wx*(fp2y*fp1x-fp1y*fp2x))/den;
    
    return;
    
}

void exsolve(double *phiy, double *psiy, double *Ay, double *ciotay, double uy, double vy, double wy, double zy)
{
    
    double q, rad;
    double x, p, ecc, ci;
    int flag1, flag2, flag3;
    
    q = 2.0*(uy*wy+vy*zy)/((uy*uy+vy*vy) - (wy*wy+zy*zy));
    
    rad = sqrt(1.0+q*q);
    
    x = atan2(2.0*(uy*wy+vy*zy),  ((uy*uy+vy*vy) - (wy*wy+zy*zy)));
    while(x < 0.0) x += TPI;
    
    *phiy = 0.5*x;
    
    flag1 = 0;
    if(cos(x) > 0.0) flag1 = 1;
    
    flag2 = 0;
    if(cos(x/2.0) > 0.0) flag2 = 1;
    
    flag3 = 0;
    if(sin(x/2.0) > 0.0) flag3 = 1;
    
    p = ((rad+1.0)*(uy*uy+vy*vy)+(rad-1.0)*(wy*wy+zy*zy)+2.0*q*(uy*wy+vy*zy))/(2.0*rad);
    
    
    
    if (flag1 == 1)
    {
        ecc = (uy*zy-vy*wy)/p;
    }
    else
    {
        ecc = p/(uy*zy-vy*wy);
    }
    
    ci = -(1.0-sqrt(1.0-ecc*ecc))/ecc;
    
    *ciotay = ci;
    
    
    if(flag1 == 1 && flag2 == 1)
    {
        x = atan2((1.0+rad)*vy+q*zy,(1.0+rad)*uy+q*wy);
    }
    else if (flag1 == 1 && flag2 == 0)
    {
        x = atan2(-((1.0+rad)*vy+q*zy),-((1.0+rad)*uy+q*wy));
    }
    else if (flag1 == 0 && flag3==1)
    {
        x = atan2( ecc*((1.0+rad)*uy+q*wy), -ecc*((1.0+rad)*vy+q*zy));
    }
    else if (flag1 == 0 && flag3 == 0)
    {
        x = atan2(-ecc*((1.0+rad)*uy+q*wy), ecc*((1.0+rad)*vy+q*zy));
    }
    
    // printf("flag1 %d flag2 %d flag3 %d x %f\n", flag1, flag2, flag3, x);
    
    while(x < 0.0) x += TPI;
    
    
    
    *psiy = 0.5*x;
    
    if(flag1 == 1)
    {
        x = sqrt(p);
    }
    else
    {
        x = sqrt(p)/fabs(ecc);
    }
    
    *Ay = 2.0*x/(1.0+ci*ci);
    
    return;
    
}
