/**************************************************************************
 
 Copyright (c) 2019 Neil Cornish
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 ************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include <gsl/gsl_fft_real.h>
#include <gsl/gsl_fft_halfcomplex.h>
#include <gsl/gsl_rng.h>

#include "GlitchBuster.h"
#include "GlitchBusterConstants.h"
#include "BayesWaveMath.h"


/*
 *
 * Common to both Spec and GlitchStart
 *
 */

void whiten(double *data, double *ASD, int N)
{
    double x;
    int i;
    
    data[0] = 0.0;
    data[N/2] = 0.0;
    
    for(i=1; i< N/2; i++)
    {
        x = 1.0/ASD[i];
        data[i] *= x;
        data[N-i] *= x;
    }
    
}

static void recursive_phase_evolution_glitchbuster(double dre, double dim, double *cosPhase, double *sinPhase)
{
    // TODO why is this different from the one in bayeswave math?
    /* Update re and im for the next iteration. */
    double cosphi = *cosPhase;
    double sinphi = *sinPhase;
    double x, y;

    x = (cosphi*dre + sinphi*dim);
    y = (sinphi*dre - cosphi*dim);

    double newRe = cosphi - x;
    double newIm = sinphi - y;

    *cosPhase = newRe;
    *sinPhase = newIm;

}

double f_nwip_imin_imax(double *a, double *b, int n, int imin, int imax)
{
    int i, j, k;
    double arg, product;
    double ReA, ReB, ImA, ImB;
    
    arg = 0.0;
    for(i=imin; i<imax; i++)
    {
        j = i;
        k = n-1;
        ReA = a[j]; ImA = a[k];
        ReB = b[j]; ImB = b[k];
        product = ReA*ReB + ImA*ImB;
        arg += product;
    }
    
    return(arg);
    
}

void SineGaussianFreq(double *hs, double *sigpar, double Tobs, int N)
{
    double f0, t0, Q, sf, sx, Amp;
    double fmax, fmin;//, fac;
    double phi, f;
    double tau;
    double re,im;
    double q, p, u;
    double A, B, C;
    double Am, Bm, Cm;
    double a, b, c;
    double cosPhase_m, sinPhase_m, cosPhase_p, sinPhase_p;
    
    int i, imid, imin,imax,even,odd;
    
    t0  = sigpar[0];
    f0  = sigpar[1];
    Q   = sigpar[2];
    Amp = sigpar[3];
    phi = sigpar[4];
    
    tau = Q/(TPI*f0);
    
    fmax = f0 + 2.0/tau;  // no point evaluating waveform past this time (many efolds down)
    fmin = f0 - 2.0/tau;  // no point evaluating waveform before this time (many efolds down)
    
    i = (int)(f0*Tobs);
    imin = (int)(fmin*Tobs);
    imax = (int)(fmax*Tobs);
    if(imax - imin < 10)
    {
        imin = i-5;
        imax = i+5;
    }
    
    if(imin < 0) imin = 1;
    if(imax > N/2) imax = N/2;
    
    hs[0] = 0.0;
    hs[N/2] = 0.0;
    
    for(i = 1; i < N/2; i++)
    {
        hs[i] = 0.0;
        hs[N-i] = 0.0;
    }
    
    /* Use recursion relationship  */
    
    //incremental values of exp(iPhase)
    double dim = sin(TPI*t0/Tobs);
    double dre = sin(0.5*(TPI*t0/Tobs));
    dre = 2.0*dre*dre;
    
    double amplitude = 0.5*(Amp)*RTPI*tau;
    
    
    imid = (int)(f0*Tobs);
    
    
    q = Q*Q/(f0*Tobs);
    p = PI*PI*tau*tau/(Tobs*Tobs);
    u = PI*PI*tau*tau/Tobs*(2.0*f0*Tobs-1.0);
    
    Am = exp(-q*(double)(imid));
    Bm = exp(-p*(((double)(imid)-f0*Tobs)*((double)(imid)-f0*Tobs)));
    Cm = 1.0;
    
    a = exp(-q);
    b = exp(-p*(1.0+2.0*((double)(imid)-f0*Tobs)));
    c = exp(-2.0*p);
    
    // sine and cosine of phase at reference frequency
    f = (double)(imid)/Tobs;
    double phase = TPI*f*t0;
    double cosPhase_m0  = cos(phase-phi);
    double sinPhase_m0  = sin(phase-phi);
    double cosPhase_p0  = cos(phase+phi);
    double sinPhase_p0  = sin(phase+phi);
    
    // start in the middle and work outwards
    
    A = Am;
    B = Bm;
    C = Cm;
    
    cosPhase_m  = cosPhase_m0;
    sinPhase_m  = sinPhase_m0;
    cosPhase_p  = cosPhase_p0;
    sinPhase_p  = sinPhase_p0;
    
    for(i = imid; i < imax; i++)
    {
        even = 2*i;
        odd = even + 1;
        f = (double)(i)/Tobs;
        
        sf = amplitude*B;
        sx = A;
        re = sf*(cosPhase_m+sx*cosPhase_p);
        im = -sf*(sinPhase_m+sx*sinPhase_p);
        
        //  printf("%e %e\n", exp(-Q2*f)-A, exp(-pi2tau2*(f-f0)*(f-f0))-B);
        
        hs[i] = re;
        hs[N-i] = im;
        
        A *= a;
        B *= (C*b);
        C *= c;
        
        /* Now update re and im for the next iteration. */
        recursive_phase_evolution_glitchbuster(dre, dim, &cosPhase_m, &sinPhase_m);
        recursive_phase_evolution_glitchbuster(dre, dim, &cosPhase_p, &sinPhase_p);
    }
    
    // reset to midpoint
    
    A = Am;
    B = Bm;
    C = Cm;
    
    cosPhase_m  = cosPhase_m0;
    sinPhase_m  = sinPhase_m0;
    cosPhase_p  = cosPhase_p0;
    sinPhase_p  = sinPhase_p0;
    
    a = 1.0/a;
    b = exp(p*(-1.0+2.0*((double)(imid)-f0*Tobs)));
    // c unchanged
    
    // interate backwards in phase
    dim *= -1.0;
    
    for(i = imid; i > imin; i--)
    {
        even = 2*i;
        odd = even + 1;
        f = (double)(i)/Tobs;
        
        sf = amplitude*B;
        sx = A;
        re = sf*(cosPhase_m+sx*cosPhase_p);
        im = -sf*(sinPhase_m+sx*sinPhase_p);
        
        // printf("%e %e\n", exp(-Q2*f)-A, exp(-pi2tau2*(f-f0)*(f-f0))-B);
        
        hs[i] = re;
        hs[N-i] = im;
        
        A *= a;
        B *= (C*b);
        C *= c;
        
        /* Now update re and im for the next iteration. */
        recursive_phase_evolution_glitchbuster(dre, dim, &cosPhase_m, &sinPhase_m);
        recursive_phase_evolution_glitchbuster(dre, dim, &cosPhase_p, &sinPhase_p);
    }
    
    
}

static void phase_blind_time_shift(double *corr, double *corrf, double *data1, double *data2, int n, int imin, int imax)
{
    int nb2, i, l, k;
    
    nb2 = n / 2;
    
    for (i=0; i < n; i++)
    {
        corr[i]    = 0.0;
        corrf[i] = 0.0;
    }
    
    for (i=imin; i < imax; i++)
    {
        l=i;
        k=n-i;
        
        corr[l]    = (data1[l]*data2[l] + data1[k]*data2[k]);
        corr[k]    = (data1[k]*data2[l] - data1[l]*data2[k]);
        corrf[l] = corr[k];
        corrf[k] = -corr[l];
    }
    
    gsl_fft_halfcomplex_radix2_inverse(corr, 1, n);
    gsl_fft_halfcomplex_radix2_inverse(corrf, 1, n);
    
    
}

static double *double_vector(int N)
{
  return malloc( (N+1) * sizeof(double) );
}

static void free_double_vector(double *v)
{
   free(v);
}

static double **double_matrix(int N, int M)
{
  int i;
  double **m = malloc( (N+1) * sizeof(double *));
  
  for(i=0; i<N+1; i++)
  {
    m[i] = malloc( (M+1) * sizeof(double));
  }

  return m;
}

static void free_double_matrix(double **m, int N)
{
  int i;
  for(i=0; i<N+1; i++) free_double_vector(m[i]);
  free(m);
}

static double ***double_tensor(int N, int M, int L)
{
  int i,j;
  
  double ***t = malloc( (N+1) * sizeof(double **));
  for(i=0; i<N+1; i++)
  {
    t[i] = malloc( (M+1) * sizeof(double *));
    for(j=0; j<M+1; j++)
    {
      t[i][j] = malloc( (L+1) * sizeof(double));
    }
  }
  
  return t;
}

static void free_double_tensor(double ***t, int N, int M)
{
  int i;
  
  for(i=0; i<N+1; i++) free_double_matrix(t[i],M);
  
  free(t);
}


/*
 *
 * Spec Functions
 *
 */

void GlitchBusterSpecWrapper(double *data, int N, double Tobs, double fmax, double dt, double *SN, double *SM, double *PS)
{
    
    int Ns = (int)(Tobs*fmax);

    specest(data, N, Ns, dt, fmax, SN, SM, PS);

    // Tukey window parameter. Flat for (1-alpha) of data
    //double tuke = 0.4;    // Tukey window rise (s) DEFINED IN GlitchBuster.h
    double alpha = (2.0*tuke/Tobs);

    // Tukey window
    tukey(data, alpha, N);

    // FFT
    gsl_fft_real_radix2_transform(data, 1, N);
    
    
}

void specest(double *data, int N, int Ns, double dt, double fmax, double *SN, double *SM, double *PS)
{
    int i, Nf;
    
    
    int imin, imax;
    double SNR;
    double Tobs, df, x, dx;
    double fmin, Q, fny, scale, dlnf;
    double *freqs;
    double *Draw;
    double *D;
    double *Sn;
    double *specD, *sspecD;
    
    double *sqf;
    
    
    int subscale, octaves;
    
    double SNRold;
    double alpha;
    double t_rise, s1, s2, fac, Dfmax;
     
    
    Q = Qs;  // Q of transform
    Dfmax = 8.0;  // width of smoothing window in Hz
    Tobs = (double)(N)*dt;  // duration
    
    // Tukey window parameter. Flat for (1-alpha) of data
    t_rise = 0.4; // Standard LAL setting
    alpha = (2.0*t_rise/Tobs);
    
    df = 1.0/Tobs;  // frequency resolution
    fny = 1.0/(2.0*dt);  // Nyquist
    
    // Choose the range of the spectrogram. fmin, fmax must be a power of 2
    fmin = 8.0;
    if(fmax > fny) fmax = fny;
    
    D = (double*)malloc(sizeof(double)* (N));
    Draw = (double*)malloc(sizeof(double)* (N));
    
    // Copy data over
    for(i=0; i< N; i++)
    {
        Draw[i] = data[i];
        D[i] = data[i];
    }
    
    
    // Normalization factor
    tukey_scale(&s1, &s2, alpha, N);
    
    
    // Time series data is corrupted by the Tukey window at either end
    // imin and imax define the "safe" region to use
    imin = (int)(2.0*t_rise/dt);
    imax = N-imin;
    
    
    // Prepare to make spectogram
    
    // logarithmic frequency spacing
    subscale = 40;  // number of semi-tones per octave
    octaves = (int)(rint(log(fmax/fmin)/log(2.0))); // number of octaves
    Nf = subscale*octaves+1;
    freqs = (double*)malloc(sizeof(double)* (Nf));   // frequencies used in the analysis
    sqf = (double*)malloc(sizeof(double)* (Nf));
    dx = log(2.0)/(double)(subscale);
    dlnf = dx;
    x = log(fmin);
    for(i=0; i< Nf; i++)
    {
        freqs[i] = exp(x);
        sqf[i] = sqrt(freqs[i]);
        x += dx;
    }
    
    //printf("frequency layers = %d\n", Nf);
    
    scale = Getscale(freqs, Q, Tobs, fmax, N, Nf);
    
    
    sspecD = (double*)malloc(sizeof(double)*(N/2));
    specD = (double*)malloc(sizeof(double)*(N/2));
    Sn = (double*)malloc(sizeof(double)*(N/2));
    
    
    SNRold = 0.0;
    clean(D, Draw, sqf, freqs, Sn, specD, sspecD, df, Q, Tobs, scale, alpha, Nf, N, imin, imax, &SNR, 0);
    
    //return;
        
    // if big glitches are present we need to rinse and repeat
    i = 0;
    while(i < 10 && (SNR-SNRold) > 10.0)
    {
        SNRold = SNR;
        clean(D, Draw, sqf, freqs, Sn, specD, sspecD, df, Q, Tobs, scale, alpha, Nf, N, imin, imax, &SNR, 0);
        i++;
    }
    
    // final clean and save Q-scan data to file if final input value set to 1
    clean(D, Draw, sqf, freqs, Sn, specD, sspecD, df, Q, Tobs, scale, alpha, Nf, N, imin, imax, &SNR, 0);
    
    
    // re-compute the power spectrum using the cleaned data
    tukey(D, alpha, N);
    
    gsl_fft_real_radix2_transform(D, 1, N);
    
    // Form spectral model for whitening data (lines plus a smooth component)
    spectrum(D, Sn, specD, sspecD, df, N);
    
    fac = Tobs/((double)(N)*(double)(N));
    
    for (i = 0; i < Ns; ++i) SM[i] = sspecD[i]*fac;
    for (i = 0; i < Ns; ++i) SN[i] = specD[i]*fac;
    for (i = 0; i < Ns; ++i) PS[i] = Sn[i]*fac;
    
    
    
    
    free(D);
    free(Draw);
    free(sspecD);
    free(specD);
    free(Sn);
    free(freqs);
    free(sqf);
    
    
}



void clean(double *D, double *Draw, double *sqf, double *freqs, double *Sn, double *specD, double *sspecD, double df, double Q, double Tobs, double scale, double alpha, int Nf, int N, int imin, int imax, double *SNR, int pflag)
{
    
    int i, j, k;
    int flag;
    int ii, jj;
    double x, dt, c1;
    double S;
    
    double t, f;
    
    // allocate some arrays
    double **tfDR, **tfDI;
    double **tfD;
    double **live;
    double **live2;
    
    double *scl;
    double *Dtemp;
    
    
    double *hist;
    double *cuml;
    
    FILE *out;
    
    live= double_matrix(Nf,N);
    live2= double_matrix(Nf,N);
    tfDR = double_matrix(Nf,N);
    tfDI = double_matrix(Nf,N);
    tfD = double_matrix(Nf,N);
    
    dt = Tobs/(double)(N);
    
    
    
    Dtemp = (double*)malloc(sizeof(double)*(N));
    
    for (i = 0; i < N; i++) Dtemp[i] = Draw[i];
    
    // D holds the previously cleaned data
    // Draw holds the raw data
    
    // D is used to compute the spectrum. A copy of Draw is then whitened using this spectrum and glitches are then identified
    // The glitches are then re-colored, subtracted from Draw to become the new D
    
    
    // Tukey window
    tukey(D, alpha, N);
    tukey(Dtemp, alpha, N);
    
    
    // FFT
    gsl_fft_real_radix2_transform(D, 1, N);
    gsl_fft_real_radix2_transform(Dtemp, 1, N);
    
    // Form spectral model for whitening data (lines plus a smooth component)
    spectrum(D, Sn, specD, sspecD, df, N);
    
    // whiten data
    double *ASD = malloc((N/2)*sizeof(double));
    for(int n=0; n<N/2; n++) ASD[n] = sqrt(specD[n]);
    whiten(Dtemp, ASD, N);
    free(ASD);
    
    // Wavelet transform
    TransformC(Dtemp, freqs, tfD, tfDR, tfDI, Q, Tobs, N, Nf);
    
    
    
    hist = (double*)malloc(sizeof(double)*(1000));
    cuml = (double*)malloc(sizeof(double)*(1000));
    scl = (double*)malloc(sizeof(double)*(Nf));
    
    ii = (int)(1.0/dt);
    jj = N - 2*ii;
    
    c1 = 1.0-exp(-1.0);  // cumulant at 1-sigma
    
    for(j = 0; j < Nf; j++)
    {
        
        
        for(k = 0; k < 1000; k++)
        {
            hist[k] = 0.0;
            cuml[k] = 0.0;
        }
        
        for(i = ii; i < N-ii; i++)
        {
            k = tfD[j][i]*50.0;
            if(k < 1000) hist[k]++;
        }
        
        for(k = 0; k < 1000; k++) hist[k] /= (double)(jj);
        cuml[0] = hist[0];
        for(k = 1; k < 1000; k++) cuml[k] = cuml[k-1]+hist[k];
        
        flag = 0;
        for(k = 0; k < 1000; k++)
        {
            if(cuml[k] > c1 && flag == 0)
            {
                flag = 1;
                scl[j] = ((double)(k)+1.0)*0.02;
            }
        }
        
    }
    
    free(hist);
    free(cuml);
    
    /*
     out = fopen("scale.dat","w");
     for(j = 0; j < Nf; j++)
     {
     f = freqs[j];
     fprintf(out,"%f %f\n", f, scl[j]);
     }
     fclose(out);
     
     fac = Tobs/((double)(N)*(double)(N));
     
     out = fopen("spec.dat","w");
     for(j = 0; j < Nf; j++)
     {
     f = freqs[j];
     i = (int)(f*Tobs);
     fprintf(out,"%f %e %e\n", f, fac*sspecD[i], fac*sspecD[i]*scl[j]);
     }
     fclose(out);
     */
    
    
    // rescale power estimate. We don;t rescale the real and imaginayr parts since they get re-colored using the
    // original PSD estimate.
    
    for(j = 0; j < Nf; j++)
    {
        x = 1.0/scl[j];
        
        for(i = 0; i < N; i++)
        {
            tfD[j][i] *= x;
        }
    }
    
    
    free(scl);
    
    
    
    if(pflag == 1)
    {
        out = fopen("Qtransform.dat","w");
        for(j = 0; j < Nf; j++)
        {
            f = freqs[j];
            
            for(i = 0; i < N; i++)
            {
                t = (double)(i)*dt-Tobs/2.0;  // trigger time is midway
                fprintf(out,"%e %e %e\n", t, f, tfD[j][i]);
            }
            
            fprintf(out,"\n");
        }
        fclose(out);
        
        /*
         hist = (double*)malloc(sizeof(double)*(100));
         cuml = (double*)malloc(sizeof(double)*(100));
         
         ii = (int)(1.0/dt);
         jj = N - 2*ii;
         
         for(j = 0; j < Nf; j++)
         {
         f = freqs[j];
         
         sprintf(command, "hist_%.2f.dat", f);
         out = fopen(command,"w");
         for(k = 0; k < 100; k++)
         {
         hist[k] = 0.0;
         cuml[k] = 0.0;
         }
         
         for(i = ii; i < N-ii; i++)
         {
         k = tfD[j][i]*5.0;
         if(k < 100) hist[k]++;
         }
         
         for(k = 0; k < 100; k++) hist[k] /= (double)(jj);
         cuml[0] = hist[0];
         for(k = 1; k < 100; k++) cuml[k] = cuml[k-1]+hist[k];
         for(k = 0; k < 100; k++) hist[k] /= 0.2;
         for(k = 0; k < 100; k++) fprintf(out,"%f %f %f\n", ((double)(k)+0.5)*0.2, hist[k], cuml[k]);
         fclose(out);
         }
         
         free(hist);
         free(cuml); */
        
    }
    
    k = 0;
    //  apply threshold
    for(j = 0; j < Nf; j++)
    {
        for (i = 0; i < N; i++)
        {
            live[j][i] = -1.0;
            if(tfD[j][i] > sthresh) live[j][i] = 1.0;
            if(tfD[j][i] > sthresh) k++;
            live2[j][i] = live[j][i];
        }
    }
    
    
    // dig deeper to extract clustered power
    for(j = 1; j < Nf-1; j++)
    {
        for (i = 1; i < N-1; i++)
        {
            
            flag = 0;
            for(jj = -1; jj <= 1; jj++)
            {
                for(ii = -1; ii <= 1; ii++)
                {
                    if(live[j+jj][i+ii] > 0.0) flag = 1;
                }
            }
            if(flag == 1 && tfD[j][i] > warm1) live2[j][i] = 1.0;
        }
    }
    
    for(j = 0; j < Nf; j++)
    {
        for (i = 0; i < N; i++)
        {
            live[j][i] = live2[j][i];
        }
    }
    
    // one more dig to extract clustered power
    for(j = 1; j < Nf-1; j++)
    {
        for (i = 1; i < N-1; i++)
        {
            
            flag = 0;
            for(jj = -1; jj <= 1; jj++)
            {
                for(ii = -1; ii <= 1; ii++)
                {
                    if(live[j+jj][i+ii] > 0.0) flag = 1;
                }
            }
            if(flag == 1 && tfD[j][i] > warm2) live2[j][i] = 1.0;
        }
    }
    
    
    
    
    // build the excess power model
    for (i = 0; i < N; i++)
    {
        Dtemp[i] = 0.0;
    }
    
    k = 0;
    for(j = 0; j < Nf; j++)
    {
        for (i = imin; i < imax; i++)
        {
            if(live2[j][i] > 0.0) Dtemp[i] += scale*sqf[j]*tfDR[j][i];
        }
    }
    
    // Compute the excess power (relative to the current spectral model
    S = 0.0;
    for (i = imin; i < imax; ++i) S += Dtemp[i]*Dtemp[i];
    S = sqrt(S);
    
    printf("   Excess SNR = %f\n", S);
    
    *SNR = S;
    
    
    //Unwhiten and subtract the excess power so we can compute a better spectral estimate
    // Back to frequency domain
    
    gsl_fft_real_radix2_transform(Dtemp, 1, N);
    
    
    // only use smooth spectrum in the un-whitening
    Dtemp[0] = 0.0;
    for(i=1; i< N/2; i++)
    {
        x = sqrt(sspecD[i]);
        Dtemp[i] *= x;
        Dtemp[N-i] *= x;
    }
    Dtemp[N/2] = 0.0;
    
    gsl_fft_halfcomplex_radix2_inverse(Dtemp, 1, N);
    
    
    x = sqrt((double)(2*N));
    
    for(i=0; i< N; i++)
    {
        D[i] = Draw[i]-Dtemp[i]/x;
    }
    
    
    free(Dtemp);
    free_double_matrix(live,Nf);
    free_double_matrix(live2,Nf);
    free_double_matrix(tfDR,Nf);
    free_double_matrix(tfDI,Nf);
    free_double_matrix(tfD,Nf);
    
    
    return;
    
    
}

void spectrum(double *data, double *S, double *Sn, double *Smooth, double df, int N)
{
    double Dfmax, x;
    double Df1, Df2;
    int mw, k, i, j;
    int mm, kk;
    int end1, end2, end3;
    
    double *chunk;
    
    // log(2) is median/2 of chi-squared with 2 dof
    
    
    for(i=1; i< N/2; i++) S[i] = 2.0*(data[i]*data[i]+data[N-i]*data[N-i]);
    S[0] = S[1];
    
    Dfmax = 16.0; // is the  width of smoothing window in Hz
    
    // Smaller windows used initially where the spectrum is steep
    Df2 = Dfmax/2.0;
    Df1 = Dfmax/4.0;
    
    // defines the ends of the segments where smaller windows are used
    end1 = (int)(32.0/df);
    end2 = 2*end1;
    
    mw = (int)(Dfmax/df)+1;  // size of median window
    //printf("numer of bins in smoothing window %d\n", mw);
    k = (mw+1)/2;
    chunk = double_vector(mw);
    
    end3 = N/2-k;  // end of final chunk
    
    // Fill the array so the ends are not empty - just to be safe
    for(i=0;i< N/2;i++)
    {
        Sn[i] = S[i];
        Smooth[i] = S[i];
    }
    
    
    mw = (int)(Df1/df)+1;  // size of median window
    k = (mw+1)/2;
    
    for(i=4; i< k; i++)
    {
        mm = i/2;
        kk = (mm+1)/2;
        
        for(j=0;j< mm;j++)
        {
            chunk[j] = S[i-kk+j];
        }
        
        gsl_sort(chunk, 1, mm);
        Sn[i] = gsl_stats_median_from_sorted_data(chunk, 1, mm)/LN2;
        
        Smooth[i] = Sn[i];
        
    }
    
    
    i = k;
    do
    {
        for(j=0;j< mw;j++)
        {
            chunk[j] = S[i-k+j];
        }
        
        gsl_sort(chunk, 1, mw);
        Sn[i] = gsl_stats_median_from_sorted_data(chunk, 1, mw)/LN2;
        
        Smooth[i] = Sn[i];
        
        i++;
        
    }while(i < end1);
    
    
    
    mw = (int)(Df2/df)+1;  // size of median window
    k = (mw+1)/2;
    
    
    do
    {
        for(j=0;j< mw;j++)
        {
            chunk[j] = S[i-k+j];
        }
        
        gsl_sort(chunk, 1, mw);
        Sn[i] = gsl_stats_median_from_sorted_data(chunk, 1, mw)/LN2;
        
        Smooth[i] = Sn[i];
        
        i++;
        
    }while(i < end2);
    
    mw = (int)(Dfmax/df)+1;  // size of median window
    k = (mw+1)/2;
    
    do
    {
        for(j=0;j< mw;j++)
        {
            chunk[j] = S[i-k+j];
        }
        
        gsl_sort(chunk, 1, mw);
        Sn[i] = gsl_stats_median_from_sorted_data(chunk, 1, mw)/LN2;
        
        Smooth[i] = Sn[i];
        
        i++;
        
    }while(i < end3);
    
    
    for(i=end3; i< N/2-4; i++)
    {
        mm = (N/2-i)/2;
        kk = (mm+1)/2;
        
        for(j=0;j< mm;j++)
        {
            chunk[j] = S[i-kk+j];
        }
        
        gsl_sort(chunk, 1, mw);
        Sn[i] = gsl_stats_median_from_sorted_data(chunk, 1, mm)/LN2;
        
        Smooth[i] = Sn[i];
        
    }
    
    
    free_double_vector(chunk);
    
    
    
    
    // zap the lines.
    for(i=1;i< N/2;i++)
    {
        x = S[i]/Sn[i];
        if(x > 10.0)
        {
            Sn[i] = S[i];
        }
    }
    
    
    
}


double Getscale(double *freqs, double Q, double Tobs, double fmax, int n, int m)
{
    double *data, *intime, *ref, **tfR, **tfI, **tf;
    double f, t0, delt, t, x, dt;
    double scale, sqf;
    int i, j;
    
    
    
    data = double_vector(n);
    ref = double_vector(n);
    intime = double_vector(n);
    
    tf = double_matrix(m,n);
    tfR = double_matrix(m,n);
    tfI = double_matrix(m,n);
    
    f = fmax/4.0;
    t0 = Tobs/2.0;
    delt = Tobs/8.0;
    dt = Tobs/(double)(n);
    
    //out = fopen("packet.dat","w");
    for(i=0; i< n; i++)
    {
        t = (double)(i)*dt;
        x = (t-t0)/delt;
        x = x*x/2.0;
        data[i] = cos(TPI*t*f)*exp(-x);
        ref[i] = data[i];
        //fprintf(out,"%e %e\n", t, data[i]);
    }
    // fclose(out);
    
    
    gsl_fft_real_radix2_transform(data, 1, n);
    
    
    
    TransformC(data, freqs, tf, tfR, tfI, Q, Tobs, n, m);
    
    
    for(i = 0; i < n; i++) intime[i] = 0.0;
    
    for(j = 0; j < m; j++)
    {
        
        f = freqs[j];
        
        
        sqf = sqrt(f);
        
        for(i = 0; i < n; i++)
        {
            intime[i] += sqf*tfR[j][i];
        }
        
    }
    
    x = 0.0;
    j = 0;
    // out = fopen("testtime.dat","w");
    for(i=0; i< n; i++)
    {
        // fprintf(out,"%e %e %e\n",times[i], intime[i], ref[i]);
        
        if(fabs(ref[i]) > 0.01)
        {
            j++;
            x += intime[i]/ref[i];
        }
    }
    //fclose(out);
    
    x /= sqrt((double)(2*n));
    
    scale = (double)j/x;
    
    // printf("scaling = %e %e\n", x/(double)j, (double)j/x);
    
    free_double_vector(data);
    free_double_vector(ref);
    free_double_vector(intime);
    free_double_matrix(tf,m);
    free_double_matrix(tfR,m);
    free_double_matrix(tfI,m);
    
    return scale;
    
}


void TransformC(double *a, double *freqs, double **tf, double **tfR, double **tfI, double Q, double Tobs, int n, int m)
{
    int j;
    double fix;
    
    // [0] t0 [1] f0 [2] Q [3] Amp [4] phi
    
    fix = sqrt((double)(n/2));
    
//#pragma omp parallel for
    for(j = 0; j < m; j++)
    {
        layerC(a, freqs[j], tf[j], tfR[j], tfI[j], Q, Tobs, fix, n);
    }
    
}


void layerC(double *a, double f, double *tf, double *tfR, double *tfI, double Q, double Tobs, double fix, int n)
{
    int i;
    double *AC, *AF;
    double *b;
    double *params;
    double bmag;
    int imin, imax;
    
    params= double_vector(6);
    AC=double_vector(n);  AF=double_vector(n);
    b = double_vector(n);
    
    params[0] = 0.0;
    params[1] = f;
    params[2] = Q;
    params[3] = 1.0;
    params[4] = 0.0;
    
    SineGaussianC(b, params, Tobs, n, &imin, &imax);
    
    bmag = sqrt(f_nwip_imin_imax(b, b, n, imin, imax)/(double)n);
    
    bmag /= fix;
    
    phase_blind_time_shift(AC, AF, a, b, n, imin, imax);
    
    for(i = 0; i < n; i++)
    {
        tfR[i] = AC[i]/bmag;
        tfI[i] = AF[i]/bmag;
        tf[i] = tfR[i]*tfR[i]+tfI[i]*tfI[i];
    }
    
    free_double_vector(AC);  free_double_vector(AF);
    free_double_vector(b);  free_double_vector(params);
    
}





void SineGaussianC(double *hs, double *sigpar, double Tobs, int N, int *imn, int *imx)
{
    double f0, t0, Q, sf, Amp;
    double fmax, fmin;//, fac;
    double phi, f;
    double tau;
    
    double p;
    double B, C;
    double Bm, Cm;
    double b, c;
    double dt, fac;
    
    
    int i, imid,imin,imax;
    
    t0  = sigpar[0];
    f0  = sigpar[1];
    Q   = sigpar[2];
    Amp = sigpar[3];
    phi = sigpar[4];
    
    tau = Q/(TPI*f0);
    
    fmax = f0 + 2.0/tau;  // no point evaluating waveform past this time (many efolds down)
    fmin = f0 - 2.0/tau;  // no point evaluating waveform before this time (many efolds down)
    
    i = (int)(f0*Tobs);
    imin = (int)(fmin*Tobs);
    imax = (int)(fmax*Tobs);
    if(imax - imin < 10)
    {
        imin = i-5;
        imax = i+5;
    }
    
    if(imin < 0) imin = 1;
    if(imax > N/2) imax = N/2;
    
    *imn = imin;
    *imx = imax;
    
    for(i = 0; i < N; i++) hs[i] = 0.0;
    
    dt = Tobs/(double)(N);
    fac = sqrt(sqrt(2.0)*PI*tau/dt);
    
    /* Use recursion relationship  */
    
    imid = (int)(f0*Tobs);
    
    p = PI*PI*tau*tau/(Tobs*Tobs);
    
    Bm = exp(-p*(((double)(imid)-f0*Tobs)*((double)(imid)-f0*Tobs)));
    Cm = 1.0;
    
    b = exp(-p*(1.0+2.0*((double)(imid)-f0*Tobs)));
    c = exp(-2.0*p);
    
    // start in the middle and work outwards
    
    B = Bm;
    C = Cm;
    
    
    for(i = imid; i < imax; i++)
    {
        
        f = (double)(i)/Tobs;
        
        sf = fac*B;
        
        //  printf("%e\n", exp(-pi2tau2*(f-f0)*(f-f0))-B);
        
        hs[i] = sf;
        hs[N-i] = 0.0;
        
        B *= (C*b);
        C *= c;
        
    }
    
    // reset to midpoint
    
    B = Bm;
    C = Cm;
    
    b = exp(p*(-1.0+2.0*((double)(imid)-f0*Tobs)));
    // c unchanged
    
    for(i = imid; i > imin; i--)
    {
        
        f = (double)(i)/Tobs;
        
        sf = fac*B;
        
        // printf("%e\n", exp(-pi2tau2*(f-f0)*(f-f0))-B);
        
        hs[i] = sf;
        hs[N-i] = 0.0;
        
        B *= (C*b);
        C *= c;
        
        
    }
    
    
}

/*
 *
 * GlitchStart Functions
 *
 */

double **GlitchBusterWrapper(int m, int N, double Tobs, double ttrig, double fmax, double *SN, double *ASD, double *data, int *NWavelet, const int Dmax)
{
    int i, j, k, ii, jj, iQ, cnt, imax, jmax, iQmax, Nfmax;
    int imn, imx;
    int tmin, tmax;
    double *dataw;
    double *times, *atom, *resid, *signal, *signalw, *signalT;
    
    double x, y, dt, tstart, fny, fmin;
    double *close;
    
    char command[1024];
    char ifo[6];
    
    // allocate some arrays
    double **tfnorm, ***tfphase;
    double ***tfD;
    int Nf;
    double **freqs, **sqf;
    double dx, t, f, fac, max;
    double f1, f2;
    double **dis;
    
    double **sigpar;
    double **par;
    
    double *CSNR;
    
    double *A, *C;
    double **B, **Binv;
    
    int *cluster;
    int ccnt, rcnt;
    int *nclust;
    
    double *SNRC;
    
    double fminx, fmaxx;
    
    
    double **waves;
    
    double MM, dlnf, dlnQ, logL;
    
    int NW, NWK;
    
    double *QV;
    double *Dlnf;
    int *NFQ;
    
    double *SNRW;
    
    int *cname;
    
    int pout;
    int verbose;
    int scanout;
    
    
    FILE *out;
    
    
    tstart = ttrig - Tobs/2.0;
    
    if(m==0) sprintf(ifo, "%s", "H1");
    if(m==1) sprintf(ifo, "%s", "L1");
    if(m==2) sprintf(ifo, "%s", "V1");
    if(m==3) sprintf(ifo, "%s", "K1");
    
    pout = 0;  // set to 1 to print waveforms
    verbose = 0; // set to 1 to print progress
    scanout = 0;  // set to 1 to print Qscan data
    
    //      sprintf(command, "PSD_%d_%d_%d.dat", (int)(Tobs), (int)ttrig, m);
    //      in = fopen(command,"r");
    //      N = -1;
    //      while(!feof(in))
    //      {
    //          fscanf(in,"%lf%lf%lf", &x, &y, &z);
    //          N++;
    //      }
    //      rewind(in);
    //      N *= 2;
    //      if(verbose==1) printf("Number of points = %d\n", N);
    
    QV = (double*)malloc(sizeof(double)* (NUMBER_Q_LAYERS));
    Dlnf = (double*)malloc(sizeof(double)* (NUMBER_Q_LAYERS));
    NFQ = (int*)malloc(sizeof(int)* (NUMBER_Q_LAYERS));
    
    cluster = (int*)malloc(sizeof(int)* (Dmax));
    
    SNRW = (double*)malloc(sizeof(double)* (Dmax));
    sigpar = double_matrix(Dmax,NWP);
    dis = double_matrix(Dmax,Dmax);
    close = (double*)malloc(sizeof(double)* (Dmax));
    times = (double*)malloc(sizeof(double)* (N));
    dataw = (double*)malloc(sizeof(double)* (N));
    atom = (double*)malloc(sizeof(double)* (N));
    signal = (double*)malloc(sizeof(double)* (N));
    signalw = (double*)malloc(sizeof(double)* (N));
    signalT = (double*)malloc(sizeof(double)* (N));
    resid = (double*)malloc(sizeof(double)* (N));
    
    
    dt = Tobs/(double)(N);
    fny = 1.0/(2.0*dt);  // Nyquist
    
    fmin = fmn;
    if(fmax > fny) fmax = fny;
    
    if(verbose==1) printf("Data volume %f seconds, %f Hz\n", Tobs, fny);
    
    fac = Tobs/((double)(N)*(double)(N));
    
    
    
    for (i = 0; i < N; ++i) dataw[i] = data[i];
    
    // whiten data
    whiten(dataw, ASD, N);
    
    MM = 0.95;
    
    // Any wavelet can at most be half-way between the grid, so we can double the
    dlnQ = 2.0*sqrt((1.0-MM)*2.0);
    // dlnQ is sufficiently close to ln(2) to justify going with a geometric progression in powers of 2.
    // 1 2 4 8 16 32
    
    
    
    // stay off the boundaries
    fminx = fmin+4.0;
    fmaxx = fmax-8.0;
    
    Nfmax = 0;
    for (iQ = 0; iQ < NUMBER_Q_LAYERS; ++iQ)
    {
        QV[iQ] = pow(2.0,(double)(iQ));  // start at Q=1
        dlnf = sqrt(8.0*sqrt(1.0-MM)/(2.0+QV[iQ]*QV[iQ]));
        Nf = (int)(rint(log(fmaxx/fminx)/dlnf))+1;
        dlnf = log(fmaxx/fminx)/(double)(Nf-1);
        Dlnf[iQ] = dlnf;
        NFQ[iQ] = Nf;
        if(Nf > Nfmax) Nfmax = Nf;
    }
    
    freqs = double_matrix(NUMBER_Q_LAYERS,Nfmax); // frequencies used in the analysis
    sqf = double_matrix(NUMBER_Q_LAYERS,Nfmax);
    
    for (iQ = 0; iQ < NUMBER_Q_LAYERS; ++iQ)
    {
        x = log(fminx);
        for(j=0; j< NFQ[iQ]; j++)
        {
            freqs[iQ][j] = exp(x);
            sqf[iQ][j] = sqrt(freqs[iQ][j]);
            x += Dlnf[iQ];
        }
    }
    
    tfnorm = double_matrix(NUMBER_Q_LAYERS,Nfmax);
    tfphase = double_tensor(NUMBER_Q_LAYERS,Nfmax,N);
    tfD = double_tensor(NUMBER_Q_LAYERS,Nfmax,N);
    
    
    for(i = 0; i < N; i++) resid[i] = dataw[i];
    for(i = 0; i < N; i++) signal[i] = 0.0;
    
    // set up the Q-transforms
    for (iQ = 0; iQ < NUMBER_Q_LAYERS; ++iQ)
    {
        Transform(resid, freqs[iQ], ASD, tfD[iQ], tfnorm[iQ], tfphase[iQ], QV[iQ], Tobs, N, NFQ[iQ]);
    }
    
    tmin = (int)((tuke*1.5)/dt);
    tmax = (int)((Tobs-(tuke*1.5))/dt);
    
    cnt = -1;
    
    do
    {
        
        // find brightest wavelet
        max = 0.0;
        for (iQ = 0; iQ < NUMBER_Q_LAYERS; ++iQ)  // loop over Q's
        {
            for(j = 0; j < NFQ[iQ]; j++)  // loop over f's
            {
                for(i = tmin; i < tmax; i++)  // loop over t's
                {
                    if(tfD[iQ][j][i] > max)
                    {
                        max = tfD[iQ][j][i];
                        iQmax = iQ;
                        imax = i;
                        jmax = j;
                    }
                }
            }
        }
        
        
        if(max > thresh)
        {
            cnt++;
            sigpar[cnt][0] = ((double)(imax)*dt-Tobs);
            sigpar[cnt][1] = freqs[iQmax][jmax];
            sigpar[cnt][2] = QV[iQmax];
            sigpar[cnt][3] = sqrt(tfD[iQmax][jmax][imax])/tfnorm[iQmax][jmax]/(double)(N);
            sigpar[cnt][4] = tfphase[iQmax][jmax][imax];
            SineGaussianW(atom, sigpar[cnt], ASD, Tobs, N, &imn, &imx);
            SNRW[cnt] = 4.0*f_nwip_imin_imax(atom, atom, N, imn, imx);
            if(verbose==1) printf("%d %f %f %f %f %f %e %f\n", cnt, max, SNRW[cnt], 0.5*Tobs+sigpar[cnt][0], sigpar[cnt][1], sigpar[cnt][2], sigpar[cnt][3], sigpar[cnt][4]);
            for(i = 0; i < N; i++) signal[i] += atom[i];
            for(i = 0; i < N; i++) resid[i] = dataw[i]-signal[i];
            
            f1 = (double)(imn)/Tobs;
            f2 = (double)(imx)/Tobs;
            
            // update Q-transforms
            for (iQ = 0; iQ < NUMBER_Q_LAYERS; ++iQ)
            {
                
                if(max > 100.0)
                {
                    Transform(resid, freqs[iQ], ASD, tfD[iQ], tfnorm[iQ], tfphase[iQ], QV[iQ], Tobs, N, NFQ[iQ]);
                }
                else
                {
                    
                    ii = 0;
                    jj = NFQ[iQ];
                    
                    // find the frequency range that covers the subtracted wavelet
                    x = 1.0e20;
                    for(j = 0; j < NFQ[iQ]; j++)
                    {
                        y = fabs(freqs[iQ][j]-f1);
                        
                        if(y < x)
                        {
                            ii = j-1;
                            x = y;
                        }
                    }
                    
                    x = 1.0e20;
                    for(j = 0; j < NFQ[iQ]; j++)
                    {
                        y = fabs(freqs[iQ][j]-f2);
                        
                        
                        
                        if(y < x)
                        {
                            jj = j+1;
                            x = y;
                        }
                        
                    }
                    
                    if(ii < 0) ii = 0;
                    if(jj > NFQ[iQ]-1) jj = NFQ[iQ]-1;
                    
                    // printf("%f %f %f %f %f %f\n", QV[iQ], params[1], f1, freqs[iQ][ii], f2, freqs[iQ][jj]);
                    
                    deltaTransform(ii, jj, resid, freqs[iQ], ASD, tfD[iQ], tfnorm[iQ], tfphase[iQ], QV[iQ], Tobs, N, NFQ[iQ]);
                }
                
            }
            
        }
        
        
    }while(cnt < Dmax && max > thresh);
    
    for (i = 0; i < Dmax; ++i) cluster[i] = -1;
    
    k = 0;
    for(i = 0; i < cnt; i++)
    {
        for(j = i+1; j < cnt; j++)
        {
            dis[i][j] = tfdistance(sigpar[i],sigpar[j]);
            
            if(dis[i][j] < cdmax)
            {
                if(cluster[i] == -1 && cluster[j] == -1)  // start new cluster
                {
                    k++;
                    cluster[i] = k;
                    cluster[j] = k;
                }
                if(cluster[i] == -1 && cluster[j] > 0) cluster[i] = cluster[j];
                if(cluster[j] == -1 && cluster[i] > 0) cluster[j] = cluster[i];
            }
        }
    }
    
    for(i = 0; i < cnt; i++) dis[i][i] = 0.0;
    
    for(i = 0; i < cnt; i++)
    {
        for(j = i+1; j < cnt; j++)
        {
            dis[j][i] = dis[i][j];
        }
    }
    
    // initial cluster count
    ccnt = k;
    
    // some clusters may actually be linked. have to sweep and re-label
    for(k = 1; k <= ccnt; k++)
    {
        for(ii = 0; ii < cnt; ii++)  // multiple sweeps to get all the clusters re-painted
        {
            for(i = 0; i < cnt; i++)
            {
                for(j = i+1; j < cnt; j++)
                {
                    if(dis[i][j] < cdmax)
                    {
                        if(cluster[i] == k)  cluster[j] = k;
                        if(cluster[j] == k)  cluster[i] = k;
                    }
                }
            }
        }
    }
    
    // next pick up any loud singles that are not in clusters
    
    SNRC = double_vector(ccnt+1);
    for(i = 0; i < cnt; i++)
    {
        if(cluster[i] == -1 && SNRW[i] > cthresh)
        {
            ccnt++;
            cluster[i] = ccnt;
        }
    }
    
    nclust = (int*)malloc(sizeof(int)* (ccnt+1));
    
    // count number in each cluster
    for(j = 1; j <= ccnt; j++) nclust[j] = 0;
    for(i = 0; i < cnt; i++)
    {
        for(j = 1; j <= ccnt; j++)
        {
            if(cluster[i] == j) nclust[j]++;
        }
    }
    
    // some clusters may now have zero members since they may have been added to a different cluster
    
    // count wavelets that are in clusters
    NW = 0;
    for(j = 1; j <= ccnt; j++)
    {
        NW += nclust[j];
    }
    
    if(NW > 0)
    {
        
        if(verbose==1) for(j = 1; j <= ccnt; j++) printf("   cluster %d has %d wavelets\n", j, nclust[j]);
        
        // count number of clusters that still have members
        rcnt = 0;
        for(j = 1; j <= ccnt; j++)
        {
            if(nclust[j] > 0) rcnt++;
        }
        
        par = double_matrix(NW,NWP);
        A = (double*)malloc(sizeof(double)*(2*NW));
        B = double_matrix(2*NW,2*NW);
        Binv = double_matrix(2*NW,2*NW);
        C = (double*)malloc(sizeof(double)*(2*NW));
        
        cname = (int*)malloc(sizeof(int)*NW);
        
        
        j = 0;
        for(i = 0; i < cnt; i++)
        {
            while(sigpar[i][4] < 0.0) sigpar[i][4] += TPI;
            while(sigpar[i][4] > TPI) sigpar[i][4] -= TPI;
            if(cluster[i] > -1)
            {
                if(verbose==1) printf("%d %f %f %f %e %f\n", i, 0.5*Tobs + sigpar[i][0], sigpar[i][1], sigpar[i][2], sigpar[i][3], sigpar[i][4]);
                for(k = 0; k < NWP; k++) par[j][k] = sigpar[i][k];
                cname[j] = cluster[i];
                j++;
            }
        }

        waves = double_matrix(2*NW,N);

        // now re-make signal rejecting un-clustered pixels
        for(i = 0; i < N; i++) signal[i] = 0.0;
        for(i = 0; i < NW; i++)
        {
            SineGaussianFreq(waves[2*i], par[i], Tobs, N);
            for(j = 0; j < N; j++) signal[j] += waves[2*i][j];
            flip(waves[2*i], waves[2*i+1], N);   // pi/2 flip
        }
        x = nwip(data, signal, SN, N);
        y = nwip(signal, signal, SN, N);
        if(verbose==1) printf("logL = %f  SNR = %f\n", x-0.5*y, sqrt(y));
        
        
        for(i = 0; i < 2*NW; i++)
        {
            A[i] = nwip(data, waves[i], SN, N);
            for(j = i; j < 2*NW; j++)
            {
                B[i][j] = nwip(waves[i], waves[j], SN, N);
            }
        }
        
        for(i = 0; i < 2*NW; i++)
        {
            for(j = (i+1); j < 2*NW; j++) B[j][i] = B[i][j];
        }
        
        Inverse(B, Binv, 2*NW);
        
        
        logL = 0.0;
        for(i = 0; i < 2*NW; i++)
        {
            for(j = 0; j < 2*NW; j++)
            {
                logL += 0.5*Binv[i][j]*A[i]*A[j];
            }
        }
        
        if(verbose==1) printf("Maximum logL = %f\n", logL);
        
        for(i = 0; i < 2*NW; i++)
        {
            C[i] = 0.0;
            for(j = 0; j < 2*NW; j++)
            {
                C[i] += Binv[i][j]*A[j];
            }
        }
        
        
        for(i = 0; i < NW; i++)
        {
            x = sqrt(C[2*i]*C[2*i]+C[2*i+1]*C[2*i+1]);
            y = atan2(C[2*i+1],C[2*i]);
            par[i][3] *= x;
            par[i][4] -= y;
        }
        
    }
    
    CSNR = (double*)malloc(sizeof(double)* (ccnt+1));
    
    for(k = 1; k <= ccnt; k++)
    {
        for(i = 0; i < N; i++) signal[i] = 0.0;
        for(i = 0; i < NW; i++)
        {
            if(cname[i] == k)
            {
                SineGaussianFreq(atom, par[i], Tobs, N);
                for(j = 0; j < N; j++) signal[j] += atom[j];
            }
        }
        y = nwip(signal, signal, SN, N);
        CSNR[k] = y;
        printf("   cluster %d has SNR = %f\n", k, sqrt(y));
        
        if(pout == 1)
        {
            whiten(signal, ASD, N);
            gsl_fft_halfcomplex_radix2_inverse(signal, 1, N);
            sprintf(command, "clusterTW_%d_%d_%d_%d.dat", k, (int)(Tobs), (int)ttrig, m);
            out = fopen(command,"w");
            for(i = 0; i < N; i++)
            {
                t = (double)(i)*dt-Tobs/2.0;
                fprintf(out,"%e %e\n", t, signal[i]);
            }
            fclose(out);
        }
    }
    
    // t f Q A phi
    
    // save parameters of wavelets
    
    // those that we keep in the glitch model
    NWK = 0;
    for(k = 0; k < NW; k++)
    {
        if(CSNR[cname[k]] > cthresh && par[k][0] > 1.0-Tobs && par[k][0] < -1.0) NWK++;
    }
    
    // tstart = ttrig - Tobs/2.0;
    
    sprintf(command, "glitch_%d_%d_%d.dat", (int)(Tobs), (int)ttrig, m);
    out = fopen(command,"w");
    fprintf(out, "%d ", NWK);
    for(k = 0; k < NW; k++)
    {
        if(CSNR[cname[k]] > cthresh && par[k][0] > 1.0-Tobs && par[k][0] < -1.0)
        {
            // output parameters in BW conventions
            fprintf(out, "%.16e %.16e %.16e %.16e %.16e ", par[k][0]+tstart, par[k][1], par[k][2], par[k][3]*Tobs/(double)(N), par[k][4]);
        }
    }
    fprintf(out, "\n");
    fclose(out);
    
    
    
    
    if(pout == 1)
    {
        
        for(i = 0; i < N; i++) signal[i] = 0.0;
        for(k = 0; k < NW; k++)
        {
            // checks to see if glitch is within window
            if(CSNR[cname[k]] > cthresh && par[k][0] > 1.0-Tobs && par[k][0] < -1.0)
            {
                SineGaussianFreq(atom, par[k], Tobs, N);
                for(i = 0; i < N; i++) signal[i] += atom[i];
            }
        }
        
        x = nwip(data, signal, SN, N);
        y = nwip(signal, signal, SN, N);
        printf("logL = %f  SNR = %f\n", x-0.5*y, sqrt(y));
        
        
        sprintf(command, "fullmodelF_%d_%d_%d.dat", (int)(Tobs), (int)ttrig, m);
        out = fopen(command,"w");
        for(i = 1; i < N/2; i++) fprintf(out,"%.16e %.16e\n", signal[i], signal[N-i]);
        fclose(out);
        
        for(i = 0; i < N; i++) signalw[i] = signal[i];
        whiten(signalw, ASD, N);
        for(i = 0; i < N; i++) resid[i] = dataw[i]-signalw[i];
        
        // find time domain whitened signal
        whiten(signal, ASD, N);
        gsl_fft_halfcomplex_radix2_inverse(signal, 1, N);
        sprintf(command, "fullmodelTW_%d_%d_%d.dat", (int)(Tobs), (int)ttrig, m);
        out = fopen(command,"w");
        for(i = 0; i < N; i++)
        {
            t = (double)(i)*dt-Tobs/2.0;
            fprintf(out,"%e %e\n", t, signal[i]);
        }
        fclose(out);
        
    }
    
    if(scanout == 1)
    {
        
        
        for(i = 0; i < N; i++) signal[i] = 0.0;
        for(k = 0; k < NW; k++)
        {
            if(CSNR[cname[k]] > cthresh && par[k][0] > 1.0-Tobs && par[k][0] < -1.0)
            {
                SineGaussianFreq(atom, par[k], Tobs, N);
                for(i = 0; i < N; i++) signal[i] += atom[i];
            }
        }
        for(i = 0; i < N; i++) signalw[i] = signal[i];
        whiten(signalw, ASD, N);
        for(i = 0; i < N; i++) resid[i] = dataw[i]-signalw[i];
        
        printf("Saving Qscans\n");
        
        // for making display Qscans
        double Qdisplay = 8.0;
        double *tfnormd, **tfphased;
        double **tfDd;
        int Nfd;
        double *freqsd, *sqfd;
        
        // for making display Qscans
        int subscale, octaves;
        
        subscale = 40;  // number of semi-tones per octave
        octaves = (int)(rint(log(fmax/fmin)/log(2.0))); // number of octaves
        Nfd = subscale*octaves+1;
        
        freqsd = double_vector(Nfd);   // frequencies used in the analysis
        sqfd = double_vector(Nfd);
        tfnormd = double_vector(Nfd);
        tfphased = double_matrix(Nfd,N);
        tfDd = double_matrix(Nfd,N);
        
        dx = log(2.0)/(double)(subscale);
        x = log(fmin);
        for(i=0; i< Nfd; i++)
        {
            freqsd[i] = exp(x);
            sqfd[i] = sqrt(freqsd[i]);
            x += dx;
        }
        
        
        // Wavelet transform signal and save to file
        Transform(signalw, freqsd, ASD, tfDd, tfnormd, tfphased, Qdisplay, Tobs, N, Nfd);
        out = fopen("QtransformS.dat","w");
        for(j = 0; j < Nfd; j++)
        {
            f = freqsd[j];
            
            for(i = 0; i < N; i++)
            {
                t = (double)(i)*dt-Tobs/2.0;
                fprintf(out,"%e %e %e\n", t, f, tfDd[j][i]);
            }
            
            fprintf(out,"\n");
        }
        fclose(out);
        
        
        // Wavelet transform data and save to file
        Transform(dataw, freqsd, ASD, tfDd, tfnormd, tfphased, Qdisplay, Tobs, N, Nfd);
        out = fopen("QtransformD.dat","w");
        for(j = 0; j < Nfd; j++)
        {
            f = freqsd[j];
            
            for(i = 0; i < N; i++)
            {
                t = (double)(i)*dt-Tobs/2.0;
                fprintf(out,"%e %e %e\n", t, f, tfDd[j][i]);
            }
            
            fprintf(out,"\n");
        }
        fclose(out);
        
        
        // Wavelet transform residual and save to file
        Transform(resid, freqsd, ASD, tfDd, tfnormd, tfphased, Qdisplay, Tobs, N, Nfd);
        out = fopen("QtransformR.dat","w");
        
        x = 0.0;
        k = 0;
        
        for(j = 0; j < Nfd; j++)
        {
            f = freqsd[j];
            
            for(i = 0; i < N; i++)
            {
                t = (double)(i)*dt-Tobs/2.0;
                fprintf(out,"%e %e %e\n", t, f, tfDd[j][i]);
                if(t > 0.5 && t < Tobs-0.5)
                {
                    x += tfDd[j][i];
                    k++;
                }
            }
            
            fprintf(out,"\n");
        }
        fclose(out);
        
        x /= (double)k;
        
        printf("SD of resdiual = %f\n", sqrt(x));
        
    }
    
    *NWavelet = NW;
    
    free(QV);
    free(Dlnf);
    free(NFQ);
    free_double_matrix(freqs,NUMBER_Q_LAYERS);
    free_double_matrix(sqf,NUMBER_Q_LAYERS);
    free_double_matrix(tfnorm,NUMBER_Q_LAYERS);
    free_double_tensor(tfphase,NUMBER_Q_LAYERS,Nfmax);
    free_double_tensor(tfD,NUMBER_Q_LAYERS,Nfmax);
    
    free(times);
    free(resid);
    free(atom);
    
    return par;
}

void Inverse(double **M, double **IM, int d)
{
    int i, j;
    int s;
    
    gsl_matrix *m = gsl_matrix_alloc (d, d);
    
    for (i = 0 ; i < d ; i++)
    {
        for (j = 0 ; j < d ; j++)
        {
            gsl_matrix_set(m, i, j, M[i][j]);
        }
    }
    
    gsl_permutation *p = gsl_permutation_alloc(d);
    
    // Compute the LU decomposition of this matrix
    gsl_linalg_LU_decomp(m, p, &s);
    
    // Compute the  inverse of the LU decomposition
    gsl_matrix *inv = gsl_matrix_alloc(d, d);
    
    gsl_linalg_LU_invert(m, p, inv);
    
    gsl_permutation_free(p);
    
    
    for (i = 0; i < d; i++)
    {
        for (j = 0 ; j < d ; j++)
        {
            IM[i][j] = gsl_matrix_get(inv, j, i);
        }
    }
    
    
    gsl_matrix_free (inv);
    
    return;
    
}

double tfdistance(double *par1, double *par2)
{
    double t1, t2;
    double tau1, tau2;
    double f1, f2;
    double x, y;
    double dis;
    double taumin = 2.0e-3;
    double taumax = 0.5;
    
    t1 = par1[0];
    t2 = par2[0];
    f1 = par1[1];
    f2 = par2[1];
    
    tau1 = par1[2]/(TPI*f1);
    tau2 = par2[2]/(TPI*f2);
    
    // setting a max and min helps stop very extreme wavelet shapes from joining clusters
    if(tau1 < taumin) tau1 = taumin;
    if(tau2 < taumin) tau2 = taumin;
    if(tau1 > taumax) tau1 = taumax;
    if(tau2 > taumax) tau2 = taumax;
    
    
    x = tau1*tau2;
    y = (tau1*tau1+tau2*tau2);
    
    dis = ((t1-t2)*(t1-t2) + PI*PI*x*x*(f1-f2)*(f1-f2))/y+0.5*log(y/(2.0*x));
    
    return(dis);
    
}




void deltaTransform(int jmin, int jmax, double *a, double *freqs, double *ASD, double **tf, double *tfnorm, double **tfphase, double Q, double Tobs, int n, int m)
{
    
    int j;
    double fix;
    
    if(jmin==jmax)
    {
        jmin -= 1;
        jmax += 1;
    }
    
    if(jmin < 0) jmin = 0;
    if(jmax > m-1) jmax = m-1;
    
    fix = sqrt((double)(n/2));
    
    //printf("%d %d %d\n", jmin, jmax, m);
    
    
//#pragma omp parallel for
    for(j = jmin; j <= jmax; j++)
    {
        layer(a, freqs[j], ASD, tf[j], &tfnorm[j], tfphase[j], Q, Tobs, fix, n);
    }
    
}


void Transform(double *a, double *freqs, double *ASD, double **tf, double *tfnorm, double **tfphase, double Q, double Tobs, int n, int m)
{
    int j;
    double fix;
    
    // [0] t0 [1] f0 [2] Q [3] Amp [4] phi
    
    fix = sqrt((double)(n/2));
    
//#pragma omp parallel for
    for(j = 0; j < m; j++)
    {
        layer(a, freqs[j], ASD, tf[j], &tfnorm[j], tfphase[j], Q, Tobs, fix, n);
    }
    
}

void layer(double *a, double f, double *ASD, double *tf, double *tfnorm, double *tfphase, double Q, double Tobs, double fix, int n)
{
    int i;
    double *AC, *AF;
    double *b;
    double *params;
    double bmag;
    int imin, imax;
    
    
    params= double_vector(6);
    AC=double_vector(n);  AF=double_vector(n);
    b = double_vector(n);
    
    params[0] = 0.0;
    params[1] = f;
    params[2] = Q;
    params[3] = 1.0;
    params[4] = 0.0;
    
    SineGaussianW(b, params, ASD, Tobs, n, &imin, &imax);   // This makes colored wavelets, then whitens them (same as BW)
    
    bmag = sqrt(f_nwip_imin_imax(b, b, n, imin, imax)/(double)n);
    
    bmag /= fix;
    
    *tfnorm = bmag;
    
    phase_blind_time_shift(AC, AF, a, b, n, imin, imax);
    
    for(i = 0; i < n; i++)
    {
        tfphase[i] = atan2(AF[i],AC[i]);
        tf[i] = (AC[i]*AC[i]+AF[i]*AF[i])/(bmag*bmag);
    }
    
    free_double_vector(AC);  free_double_vector(AF);
    free_double_vector(b);  free_double_vector(params);
    
}

void flip(double *a, double *af, int n)
{
    int nb2, i, l, k;
    
    nb2 = n / 2;
    
    a[0] = 0.0;
    af[0] = 0.0;
    a[nb2] = 0.0;
    af[nb2] = 0.0;
    
    for (i=1; i < nb2; i++)
    {
        l=i;
        k=n-i;
        af[l] = a[k];
        af[k] = -a[l];
    }
    
}

void SineGaussianW(double *hs, double *sigpar, double *ASD, double Tobs, int N, int *imn, int *imx)
{
    double f0, t0, Q, sf, sx, Amp;
    double fmax, fmin;//, fac;
    double phi, f;
    double tau;
    double re,im;
    double q, p, u;
    double A, B, C;
    double Am, Bm, Cm;
    double a, b, c;
    double cosPhase_m, sinPhase_m, cosPhase_p, sinPhase_p;
    
    int i, imid,imin,imax,even,odd;
    
    t0  = sigpar[0];
    f0  = sigpar[1];
    Q   = sigpar[2];
    Amp = sigpar[3];
    phi = sigpar[4];
    
    tau = Q/(TPI*f0);
    
    fmax = f0 + 2.0/tau;  // no point evaluating waveform past this time (many efolds down)
    fmin = f0 - 2.0/tau;  // no point evaluating waveform before this time (many efolds down)
    
    i = (int)(f0*Tobs);
    imin = (int)(fmin*Tobs);
    imax = (int)(fmax*Tobs);
    if(imax - imin < 10)
    {
        imin = i-5;
        imax = i+5;
    }
    
    if(imin < 0) imin = 1;
    if(imax > N/2) imax = N/2;
    
    *imn = imin;
    *imx = imax;
    
    hs[0] = 0.0;
    hs[N/2] = 0.0;
    
    for(i = 1; i < N/2; i++)
    {
        hs[i] = 0.0;
        hs[N-i] = 0.0;
    }
    
    /* Use recursion relationship  */
    
    //incremental values of exp(iPhase)
    double dim = sin(TPI*t0/Tobs);
    double dre = sin(0.5*(TPI*t0/Tobs));
    dre = 2.0*dre*dre;
    
    
    double amplitude = 0.5*(Amp)*RTPI*tau;
    
    
    imid = (int)(f0*Tobs);
    
    
    q = Q*Q/(f0*Tobs);
    p = PI*PI*tau*tau/(Tobs*Tobs);
    u = PI*PI*tau*tau/Tobs*(2.0*f0*Tobs-1.0);
    
    Am = exp(-q*(double)(imid));
    Bm = exp(-p*(((double)(imid)-f0*Tobs)*((double)(imid)-f0*Tobs)));
    Cm = 1.0;
    
    a = exp(-q);
    b = exp(-p*(1.0+2.0*((double)(imid)-f0*Tobs)));
    c = exp(-2.0*p);
    
    // sine and cosine of phase at reference frequency
    f = (double)(imid)/Tobs;
    double phase = TPI*f*t0;
    double cosPhase_m0  = cos(phase-phi);
    double sinPhase_m0  = sin(phase-phi);
    double cosPhase_p0  = cos(phase+phi);
    double sinPhase_p0  = sin(phase+phi);
    
    // start in the middle and work outwards
    
    A = Am;
    B = Bm;
    C = Cm;
    
    cosPhase_m  = cosPhase_m0;
    sinPhase_m  = sinPhase_m0;
    cosPhase_p  = cosPhase_p0;
    sinPhase_p  = sinPhase_p0;
    
    for(i = imid; i < imax; i++)
    {
        even = 2*i;
        odd = even + 1;
        f = (double)(i)/Tobs;
        
        sf = amplitude*B/ASD[i];
        sx = A;
        re = sf*(cosPhase_m+sx*cosPhase_p);
        im = -sf*(sinPhase_m+sx*sinPhase_p);
        
        //  printf("%e %e\n", exp(-Q2*f)-A, exp(-pi2tau2*(f-f0)*(f-f0))-B);
        
        hs[i] = re;
        hs[N-i] = im;
        
        A *= a;
        B *= (C*b);
        C *= c;
        
        /* Now update re and im for the next iteration. */
        recursive_phase_evolution_glitchbuster(dre, dim, &cosPhase_m, &sinPhase_m);
        recursive_phase_evolution_glitchbuster(dre, dim, &cosPhase_p, &sinPhase_p);
    }
    
    // reset to midpoint
    
    A = Am;
    B = Bm;
    C = Cm;
    
    cosPhase_m  = cosPhase_m0;
    sinPhase_m  = sinPhase_m0;
    cosPhase_p  = cosPhase_p0;
    sinPhase_p  = sinPhase_p0;
    
    a = 1.0/a;
    b = exp(p*(-1.0+2.0*((double)(imid)-f0*Tobs)));
    // c unchanged
    
    // interate backwards in phase
    dim *= -1.0;
    
    for(i = imid; i > imin; i--)
    {
        even = 2*i;
        odd = even + 1;
        f = (double)(i)/Tobs;
        
        sf = amplitude*B/ASD[i];
        sx = A;
        re = sf*(cosPhase_m+sx*cosPhase_p);
        im = -sf*(sinPhase_m+sx*sinPhase_p);
        
        // printf("%e %e\n", exp(-Q2*f)-A, exp(-pi2tau2*(f-f0)*(f-f0))-B);
        
        hs[i] = re;
        hs[N-i] = im;
        
        A *= a;
        B *= (C*b);
        C *= c;
        
        /* Now update re and im for the next iteration. */
        recursive_phase_evolution_glitchbuster(dre, dim, &cosPhase_m, &sinPhase_m);
        recursive_phase_evolution_glitchbuster(dre, dim, &cosPhase_p, &sinPhase_p);
    }
    
    
}

double nwip(double *a, double *b, double *Sn, int n)
{
    int i, j, k;
    double arg, product;
    double ReA, ReB, ImA, ImB;
    
    arg = 0.0;
    for(i=1; i< n/2; i++)
    {
        j = i;
        k = n-i;
        ReA = a[j]; ImA = a[k];
        ReB = b[j]; ImB = b[k];
        product = (ReA*ReB + ImA*ImB)/Sn[i];
        arg += product;
    }
    
    return(4.0*arg);
    
}

