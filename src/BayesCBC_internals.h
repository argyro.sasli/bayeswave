#define NR_END 1
#define FREE_ARG char*
#define func(x) ((*func)(x))
#define funk(y) ((*func)(y[]))
static double swap, tempr;
#define SWAP(a,b) {swap=(a);(a)=(b);(b)=swap;}
#define SWAPP(a,b) tempr=(a);(a)=(b);(b)=tempr;
#define SHFT2(a,b,c) (a)=(b);(b)=(c);
#define SHFT3(a,b,c,d) (a)=(b);(b)=(c);(c)=(d);
#define ffunc(x) ((*func) (x, *y, **z))
#define GET_PSUM							\
for (j=1;j<=ndim;j++) {						\
for (sum=0.0,i=1;i<=mpts;i++) sum += p[i][j];			\
psum[j]=sum;}
#define ROTATE(a,i,j,k,l) g=a[i][j];h=a[k][l];a[i][j]=g-s*(h+g*tau);	\
a[k][l]=h+s*(g-h*tau);
#define SIGN(a,b) ((b) >= 0.0 ? fabs(a) : -fabs(a))
static double maxarg1, maxarg2;
#define FMAX(a, b) (maxarg1=(a),maxarg2=(b),(maxarg1) > (maxarg2)?	\
(maxarg1):(maxarg2))
#define SQR(x) ((x) * (x))
#define CUBE(x) ((x) * (x) * (x))
#define FOURTH(x) ((x) * (x) * (x) * (x))

#define TINY 1.0e-10
#define IM1 2147483563
#define IM2 2147483399
#define AM (1.0/IM1)
#define IMM1 (IM1-1)
#define IA1 40014
#define IA2 40692
#define IQ1 53668
#define IQ2 52774
#define IR1 12211
#define IR2 3791
#define NTAB 32
#define NDIV (1+IMM1/NTAB)
#define eps 1.2e-7
#define RNMX (1.0-eps)
#define JMAX 40
#define JMAXP (JMAX+1)




#include <gsl/gsl_math.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_sort_double.h>
#include <gsl/gsl_statistics.h>
#include <gsl/gsl_fft_real.h>
#include <gsl/gsl_fft_halfcomplex.h>
#include <gsl/gsl_cdf.h>
#include <gsl/gsl_spline.h>
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_sort.h>
#include <gsl/gsl_sort_vector.h>
#include "IMRPhenomD.h"

void GenerateWaveform(
    // double *wave,           /**< [out] FD waveform */
    AmpPhaseFDWaveform** h22,           /**< [out] FD waveform */
    RealVector* freq,                   /**< Input: frequencies (Hz) on which to evaluate h22 FD - will be copied in the output AmpPhaseFDWaveform. Frequencies exceeding max freq covered by PhenomD will be given 0 amplitude and phase. */
    const double phi0,                  /**< Orbital phase at fRef (rad) */
    const double fRef_in,               /**< reference frequency (Hz) */
    const double m1_SI,                 /**< Mass of companion 1 (kg) */
    const double m2_SI,                 /**< Mass of companion 2 (kg) */
    const double chi1,                  /**< Aligned-spin parameter of companion 1 */
    const double chi2,                  /**< Aligned-spin parameter of companion 2 */
    const double distance,               /**< Distance of source (m) */
    double* extraParams,
    NRTidal_version_type NRTidal_version, /**< Version of NRTides; can be one of NRTidal versions or NoNRT_V for the BBH baseline */
    int customFlag                      /**< Temporary flag to use custom (faster) WF generation */
);

double *double_vector(int N);
void free_double_vector(double *v);

double **double_matrix(int N, int M);
void free_double_matrix(double **m, int N);

double ***double_tensor(int N, int M, int L);
void free_double_tensor(double ***t, int N, int M);

int *int_vector(int N);
void free_int_vector(int *v);

int **int_matrix(int N, int M);
void free_int_matrix(int **m, int N);

void f_of_t(double *param, double *times, double *freqs, int N);
void t_of_f(double *param, double *times, double *freqs, int N);
double t_at_f(double *param, double f);

double fQNM(double m1, double m2, double chi1, double chi2);
double fringdown(double *param);

double Fmag(double Ap, double Ac, double Fcross, double Fplus);
double phase_from_polarizations(double Ap, double Ac, double Fcross, double Fplus);

// double Fmag(double *sky, int id, double gmst);
double fbegin(double *param);

//void whiten(double *data, double *Sn, int N);

void pmap(struct Net *net, double *pallx, const double *paramx, const double *sky, int NX, double gmst);
void pmap_back(struct Net *net, double *pall, double *param, double *sky, int NX, double gmst);

void printwave(struct Net *net, int N, RealVector *freq, double **paramx, int *who, double Tobs, double ttrig, int iter, struct bayesCBC *rundata);
void printwaveall(struct Net *net, int N, RealVector *freq, double *paramx, double **SN, double Tobs, double ttrig, int iter, struct bayesCBC *rundata);

double DL_fit(double z);
double z_DL(double DL);

void pairs(struct Net *net);
void time_delays(struct Net *net);

void pbt_shift(double *corr, double *corrf, double *data1, double *data2, double *Sn, int imin, int imax, int N);
double fourier_nwip_cbc(double *a, double *b, double *Sn, int imin, int imax, int N);
double fourier_nwip_bw(double *a, double *b, double *Sn, int imin, int imax, int N);

double f_nwip(double *a, double *b, int n);
double fourier_nwip_shift(double *a, double *b, double delt, double pshift, int n, double Tobs, int imin, int imax);
double fb_nwip(double *a, double *b, int n, int imin, int imax);

void cossin(struct Net *net, double *hcos, double *hsin, RealVector *freq, double *params, int N);
void extrinsictemplates(struct Net *net, double **hwave, RealVector *freq, double *hcos, double *hsin, double *params, int N);

void fulltemplates(struct Net *net, double **hwave, RealVector *freq, double *params, int N, struct bayesCBC *rundata);
void fullphaseamp(struct Net *net, double **amp, double **phase, RealVector *freq, double *params, int N, struct bayesCBC *rundata, int recomputeFlag);
void Fisher_All(struct Net *net, double **fish, double *params, RealVector *freq, double **SN, int N, double Tobs, struct bayesCBC *rundata, int scale);

double het_diff(struct Net *net, double *params, double *dparams, RealVector *freq, double **SM, int N, double Tobs, struct bayesCBC *rundata, int scale);
void eigen_value_rescaling(struct Net *net, double *params, RealVector *freq, double **SM, int N, double Tobs, double *eval, double **evec, double zs, struct bayesCBC *rundata, int scale);

double log_likelihood_max(struct Net *net, double **D, double *params, RealVector *freq, double **SN, int N, double Tobs, double tmin, double tmax, int pflag, struct bayesCBC *rundata);
double log_likelihood_penalized(int ii, double *params, double *D, double *H, double *AU, double *PU, double *TDU, double *SN, int N, double Tobs, int pflag, struct bayesCBC *rundata);
double log_likelihood_ext(struct Net *net, double **D, double *params, RealVector *freq, double *hcos, double *hsin, double **SN, double *rho, int N, double Tobs);
void jacobi(double **a, int n, double e[], double **v, int *nrot);


void RingPlot(double *skyx, int d1, int d2, double *theta, double *phi, int M, int NX, double gmst);
void Ring(struct Net *net, const double *skyx, double *skyy, int d1, int d2, gsl_rng *r, double gmst);
double skydensity(struct Net *net, double *paramsx, double *paramsy, int ifo1, int ifo2, int iref, int NS, double gmst);
void skymap(struct Net *net, double *paramsx, double *paramsy, int ifo1, int ifo2, int iref, double gmst);

double det(double *A, int N);

void uvwz(double *u, double *v, double *w, double *z, double *params);
void exsolve(double *phiy, double *psiy, double *Ay, double *ey, double uy, double vy, double wy, double zy);
void uvwz_sol(double *uy, double *vy, double *wy, double *zy, double ux, double vx, double wx, double zx, \
              double fp1x, double fp1y, double fc1x, double fc1y, double fp2x, double fp2y, double fc2x, double fc2y);
void fisher_matrix_fastsky(struct Net *net, double *params, double **fisher, double **HH, int NS, double gmst);
void fisher_skyproposal(gsl_rng *r, double **skyvecs, double *skyevals, double *jump, struct bayesCBC *rundata);

static const gsl_rng_type *rngtype;
static const gsl_rng *rng;

void KILL(char* Message);

void AmpPhase(double *Af, double *Pf, RealVector *freq, double *params, int N, struct bayesCBC *rundata);

void Fisher_Full(struct Net *net, double **fish, int d, double *params, RealVector *freq, double **SN, int N, double Tobs, struct bayesCBC *rundata);

void de_jump(double *paramsx, double *paramsy, double **history, int m, int d, gsl_rng *r, int *evolveparams);

void PDwave(double *wavef, RealVector *freq, double *params, int N, struct bayesCBC *rundata);

void PDtemplates(double *waveH, double *waveL, RealVector *freq, double *params, int N, struct bayesCBC *rundata);

void FisherEvec(double **fish, double *eval, double **evec, int d);

void SNRvsf(struct Net *net, double **D, double *params, RealVector *freq, double **SN, int N, double Tobs, struct bayesCBC *rundata);

double globeden(double ***global, double *max, double *min, double Tobs, double *params, int N);
double globe(double ***global, double *max, double *min, double Tobs, double *params, int N, gsl_rng *r, int lmax, struct bayesCBC *rundata);

void CBC_start(struct Net *net, int *mxc, FILE *chainI, double **paramx, double **skyx, double **pallx, double *heat, double ***history, double ***global, RealVector *freq, double **D, double **SN, int N, double Tobs, gsl_rng *r, struct bayesCBC *rundata);

void MCMC_intrinsic(struct Net *net, int lmax, int *mxc, int M, FILE *chain, double **paramx, double **skyx, double **pallx, int *who, double *heat, double ***history, double ***global, RealVector *freq, double **D, double **SN, int N, double Tobs, gsl_rng *r, struct bayesCBC *rundata);

void updatei(int k, struct Net *net, int lmax, double *logLx, double **paramx, double **paramy, double *min, double *max, double *Fscale, int *who, double *heat, double ***history, double ***global, RealVector *freq, double **D, double **SN, double **ejump, double ***evec, int N, double Tobs, int **cv, int **av, gsl_rng *r, struct bayesCBC *rundata);

void update_chain(int k, struct Net *net, double *logLx, double *rhox, double *paramx, double *paramy, double *min, double *max, double heat, double **history, double ***global, RealVector *freq, double **D, double **SN, double *ejump, double **evec, int N, double Tobs, int *cv, int *av, gsl_rng *r, int *evolveparams, struct bayesCBC *rundata);

void ParseInjectionXMLParameters(char *file_path, char **injection_columns, char **injection_values);

void GetXMLInjectionParameter(char *name, double *value, char **injection_columns, char **injection_values);
void gsl_sort_vector2_custom(gsl_vector * v1, gsl_vector * v2);