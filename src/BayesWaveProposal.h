/*
 *  Copyright (C) 2018 Neil J. Cornish, Tyson B. Littenberg
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with with program; see the file COPYING. If not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 *  MA  02111-1307  USA
 */
#ifndef BAYESWAVEPROPOSAL_H
#define BAYESWAVEPROPOSAL_H
#ifdef __GNUC__
#define UNUSED __attribute__ ((unused))
#else
#define UNUSED
#endif



/* ********************************************************************************** */
/*                                                                                    */
/*                                    Prior proposals                                 */
/*                                                                                    */
/* ********************************************************************************** */

void extrinsic_uniform_proposal(gsl_rng *seed, double *y);

void draw_glitch_amplitude(double *params, double *Snf, gsl_rng *seed, double Tobs, double **range, double SNRpeak);

void draw_signal_amplitude(double *params, double *Snf, gsl_rng *seed, double Tobs, double **range, double SNRpeak);

void intrinsic_halfcycle_proposal(double *x, double *y, gsl_rng *seed);

/* ********************************************************************************** */
/*                                                                                    */
/*                                Fisher matrix proposals                             */
/*                                                                                    */
/* ********************************************************************************** */

void fisher_matrix_proposal(struct FisherMatrix *fisher, double *params, gsl_rng *seed, double *y);

void extrinsic_fisher_information_matrix(struct FisherMatrix *fisher, double *extparams, double **invSnf, struct Model *model, double **glitch, struct Data *data, struct bayesCBC *bayescbc);//void extrinsic_fisher_information_matrix(struct FisherMatrix *fisher, double *params, double **invSnf, double *Sn, struct Model *model, double **glitch, struct Data *data);

void extrinsic_fisher_update(struct Data *data, struct Model *model, struct bayesCBC *bayescbc);

void intrinsic_fisher_update(double *params, double *dparams, double *Snf, double Tobs, int NW, int TFQFLAG);

void intrinsic_fisher_proposal(UNUSED struct Model *model, double **range, double *paramsx, double *paramsy, double *Snf, double *logpxAp, double *logpyAp, double *logpxtfQ, double *logpytfQ, gsl_rng *seed, double Tobs, int *test, int NW, int TFQFLAG);

/* ********************************************************************************** */
/*                                                                                    */
/*                       Custom intrinsic proposal distributions                      */
/*                                                                                    */
/* ********************************************************************************** */

double wavelet_proximity_density(double f, double t, double **params, int *larray, int cnt, struct Prior *prior);

void wavelet_proximity(double *paramsy, double **params, int *larray, double **range, int cnt, gsl_rng *seed);

void wavelet_proximity_new(double *paramsy, double **params, int *larray, double **range, int cnt, gsl_rng *seed);

void wavelet_sample(double *params, double **range, double **prop, int tsize, double Tobs, double pmax, gsl_rng *seed);

void draw_wavelet_params(double *params, double **range, gsl_rng *seed, int NW);

void draw_time_frequency(int ifo, int ii, struct Wavelet *wx, struct Wavelet *wy, double **range, gsl_rng *seed, double fr, struct TimeFrequencyMap *tf, int *prop);

void TFprop_draw(double *params, double **range, struct TimeFrequencyMap *tf, int ifo, gsl_rng *seed);

double TFprop_density(double *params, double **range, struct TimeFrequencyMap *tf, int ifo);

void TFprop_setup(struct Data *data, struct Model *model, double **range, struct TimeFrequencyMap *tf, int ifo);

void TFprop_plot(double **range, struct TimeFrequencyMap *tf, double dt, int ifo);

void TFprop_signal(struct Data *data, double **range, struct TimeFrequencyMap *tf, struct Network *net);

/* ********************************************************************************** */
/*                                                                                    */
/*                       Custom extrinsic proposal distributions                      */
/*                                                                                    */
/* ********************************************************************************** */

void sky_ring_proposal(const double *x, double *y, const struct Data *data, gsl_rng *seed);

void sky_fix_proposal(double *x, double *y, struct Data *data, gsl_rng *seed);

void uniform_orientation_proposal(double *y, gsl_rng *seed);

void network_orientation_proposal(double *paramsx, double *paramsy, struct Data *data, double *logJ, gsl_rng *seed);


/* ********************************************************************************** */
/*                                                                                    */
/*                          Stochastic background proposals                           */
/*                                                                                    */
/* ********************************************************************************** */

void stochastic_background_proposal(struct Background *bkg_x, struct Background *bkg_y, gsl_rng *seed, int *test);
#endif // BAYESWAVEPROPOSAL_H
