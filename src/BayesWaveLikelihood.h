/*
 *  Copyright (C) 2018 Neil J. Cornish, Tyson B. Littenberg
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with with program; see the file COPYING. If not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 *  MA  02111-1307  USA
 */

#ifdef __GNUC__
#define UNUSED __attribute__ ((unused))
#else
#define UNUSED
#endif

/* ********************************************************************************** */
/*                                                                                    */
/*                             Likelihood Functions                                   */
/*                                                                                    */
/* ********************************************************************************** */

double loglike(int imin, int imax, double *r, double *Snf);
double loglike_stochastic(int NI, int imin, int imax, double **r, double ***invCij, double *detC);

void recompute_residual(struct Data *data, struct Model **model, struct Chain *chain);

double EvaluateConstantLogLikelihood (int typ, int ii, int det, UNUSED struct Model *mx, struct Model *my, struct Prior *prior, UNUSED struct Chain *chain, UNUSED struct Data *data);
double EvaluateMarkovianLogLikelihood(int typ, int ii, int det, struct Model *mx, struct Model *my, struct Prior *prior, struct Chain *chain, struct Data *data);

double EvaluateExtrinsicSearchLogLikelihood   (struct Network *projection, double *params, double **invSnf, UNUSED double *Sn, struct Wavelet **geo, double **g, double *amphase, struct Data *data, double fmin, double fmax, double *geoshifts, struct bayesCBC *bayescbc);
double EvaluateExtrinsicConstantLogLikelihood (UNUSED struct Network *projection, double *params, UNUSED double **invSnf, UNUSED struct Wavelet **geo, UNUSED double **g, UNUSED double *amphase, UNUSED struct Data *data, UNUSED double fmin, UNUSED double fmax, UNUSED double *geoshifts, struct bayesCBC *bayescbc);
double EvaluateExtrinsicMarkovianLogLikelihood(struct Network *projection, double *params, double **invSnf, struct Wavelet **geo, double **g, double *amphase, struct Data *data, double fmin, double fmax, double *geoshifts, struct bayesCBC *bayescbc);
double ExtrinsicLikelihoodOld(struct Network *projection, double *params, double **invSnf, struct Wavelet **geo, double **g, double *amphase, struct Data *data, double fmin, double fmax, double *geoshifts, struct bayesCBC *bayescbc);
void phase_blind_time_shift(double *corr, double *corrf, double *data1, double *data2, double *invpsd, int n);
