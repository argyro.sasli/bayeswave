/*
 *  Copyright (C) 2018 Neil J. Cornish, Tyson B. Littenberg, Katerina Chatziioannou
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with with program; see the file COPYING. If not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 *  MA  02111-1307  USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <math.h>
#include <time.h>

#include "BayesCBC.h"
#include "BayesLine.h"
#include "BayesWave.h"
#include "BayesWaveIO.h"
#include "BayesWaveMCMC.h"
#include "BayesWaveMath.h"
#include "BayesWavePrior.h"
#include "BayesWaveModel.h"
#include "BayesWaveWavelet.h"
#include "BayesWaveProposal.h"
#include "BayesWaveEvidence.h"
#include "BayesWaveLikelihood.h"
#include "GlitchBuster.h"

/* ********************************************************************************** */
/*                                                                                    */
/*                                  Checkpointing                                     */
/*                                                                                    */
/* ********************************************************************************** */
#ifdef __GNUC__
#define UNUSED __attribute__ ((unused))
#else
#define UNUSED
#endif

/* This is checked by the main loop to determine when to checkpoint */
static volatile sig_atomic_t __bayeswave_saveStateFlag = 0;

/* This indicates the main loop should terminate */
static volatile sig_atomic_t __bayeswave_exitFlag = 0;

static struct itimerval checkpoint_timer;

/* Signal handler for SIGINT, which is produced by condor
 * when evicting a job, or when the user presses Ctrl-C */
static void catch_interrupt(UNUSED int sig, UNUSED siginfo_t *siginfo,UNUSED void *context)
{

  __bayeswave_saveStateFlag=1;
  __bayeswave_exitFlag=1;
}

/* Signal handler for SIGALRM, for periodic checkpointing */
static void catch_alarm(UNUSED int sig, UNUSED siginfo_t *siginfo,UNUSED void *context)
{
  __bayeswave_saveStateFlag=1;
  // XXX self-checkpoint by exiting with code checkpoint_exit_code (see below)
  __bayeswave_exitFlag=1;
}


void constrain_hplus_hcross(struct Wavelet **wave, int i)
{
  /*
   give hx special treatment (wavelets at same TFQ as h+, but independenta A,phi
   */
  wave[1]->intParams[i][0] = wave[0]->intParams[i][0]; //t0
  wave[1]->intParams[i][1] = wave[0]->intParams[i][1]; //f0
  wave[1]->intParams[i][2] = wave[0]->intParams[i][2]; //Q
  if(wave[0]->dimension > 5)
  {
    wave[1]->intParams[i][5] = wave[0]->intParams[i][5]; //beta (~fdot for chirplets)
  }
}

void kickstart_glitch_model_from_file(struct Data *data, struct Prior *prior, struct Model *model)
{
  /*
   * Given waveletStartFile (glitch_{IFO}.dat), starts glitch wavelets from those values
   *
   * REQUIRES
   *  glitch_{IFO}.dat exists for all ifos
   * MODIFIES
   *  model->glitch[ifo]->templates[i]
   *
   * EFFECTS
   *  sets up model from filename
   */
  int i;
  int ifo;
  char filename[128];
  FILE *waveletStartFile;
  double *params = malloc(data->NW*(sizeof(double)));
  int size;
  int d;
  int d_counter;

  for(ifo=0; ifo<data->NI; ifo++)
  {
    d = 0;
    sprintf(filename,"glitch_%s.dat",data->ifos[ifo]);
    if( (waveletStartFile = fopen(filename,"r")) == NULL )
    {
      fprintf(stdout,"ERROR: Wavelet parameter file %s not found\n",filename);
      exit(1);
    }
    else
    {
      //Zero out signal->templates array (holds the linear combination of wavelets)
      for(i=0; i<data->N; i++) model->glitch[ifo]->templates[i] = 0.0;

      //Get the number of wavelet basis functions for the current sample
      fscanf(waveletStartFile, "%i", &size);
      //model->size += model->glitch[ifo]->size;

      //Get each wavelets parameters and fill up signal structure
      for(d_counter=1; d_counter<=size; d_counter++)
      {
        //model->glitch[ifo]->index[d]=d;
        for(i=0; i<data->NW; i++)
        {
          //fscanf(waveletStartFile,"%lg", &model->glitch[ifo]->intParams[d][i]);
          fscanf(waveletStartFile,"%lg", &params[i]);
        }

        //shift time parameter to trigtime
        params[0] -= data->starttime;


        //screen for wavelets outside of prior
        if(!checkrange(params, prior->range, data->NW))
        {
          d++;
          model->size++;
          for(i=0; i<data->NW; i++) model->glitch[ifo]->intParams[d][i] = params[i];

          //Append new wavelet to model->glitch[ifo]->templates array
          model->wavelet(model->glitch[ifo]->templates, model->glitch[ifo]->intParams[d], data->N, 1, data->Tobs);

        }
      }
      model->glitch[ifo]->size=d;

      fclose(waveletStartFile);

      //fill residual
      for(i=0; i<data->N; i++) data->r[ifo][i] = data->s[ifo][i] - model->glitch[ifo]->templates[i];
    }
  }
  free(params);
}

void kickstart_glitch_model_glitchbuster(struct Data *data, struct Prior *prior, struct Model *model, struct GlitchBuster *gb)
{
  /*
   * Given an already initialized glitchbuster, initalizes our wavelet model
   *
   * Modifies
   *    model->glitch[:]->intParams[:][:]
   *    data->r (residual)
   */
  int i,j;
  int ifo;
  int d;

  int NI = data->NI;
  int N = data->N;

  double **glitch_params;
  int NW;

  for(ifo=0; ifo<NI; ifo++)
  {
    glitch_params = gb->wavelet_params[ifo];
    NW = gb->Nwavelet[ifo];

    d=0;
    for(j=0; j<NW; j++)
    {
      //screen for wavelets outside of prior
      if((!checkrange(glitch_params[j], prior->range, data->NW)) && d < data->Dmax)
      {
        d++;
        model->size++;
        for(i=0; i<data->NW; i++) model->glitch[ifo]->intParams[d][i] = glitch_params[j][i];

        //Append new wavelet to model->glitch[ifo]->templates array
        model->wavelet(model->glitch[ifo]->templates, model->glitch[ifo]->intParams[d], data->N, 1, data->Tobs);

      }
      if (d >= data->Dmax){

        printf("kickstart_glitch_model_glitchbuster: WARNING railing against dmax! Dmax is %i glitchbuster is %i in ifo %i \n",
               data->Dmax, NW, ifo);
      }
    }

    model->glitch[ifo]->size=d;

    //fill residual
    for(i=0; i<data->N; i++) data->r[ifo][i] = data->s[ifo][i] - model->glitch[ifo]->templates[i];

  }
}

static void restart_sampler(struct Data *data, struct Chain *chain, struct Prior *prior, struct Model **model, struct GlitchBuster *gb, char modelname[], struct bayesCBC *bayescbc)
{
  int i,j,n;
  int ic,ifo;

  int NC = chain->NC;
  int NI = data->NI;

  double Tobs = data->Tobs;

  struct Wavelet **signal = NULL;
  struct Wavelet **glitch = NULL;

  char filename[128];



  /****************************************************/
  /*                                                  */
  /* PTMCMC TEMPERATURE LADDER                        */
  /*                                                  */
  /****************************************************/
  chain->mc = 0;
  for(ic=0; ic<chain->NC; ic++)
  {
    chain->dT[ic]=chain->tempStep;
    chain->index[ic]=ic;
    if(ic==0)chain->temperature[0] = chain->tempMin;
    else chain->temperature[ic] = chain->temperature[ic-1]*chain->dT[ic-1];
    chain->ptprop[ic]=1;
    chain->ptacc[ic]=0;
    chain->A[ic]=1;
    chain->avgLogLikelihood[ic] = 0.0;
    chain->varLogLikelihood[ic] = 0.0;
  }
  if(chain->NC==1)data->adaptTemperatureFlag=0;

  if(data->adaptTemperatureFlag)// && data->runPhase!=0)
  {
    chain->temperature[chain->NC-1] = 1.0e6;
    chain->dT[0] = pow(chain->temperature[chain->NC-1],(1./(double)(NC-1))*(1./(double)(NC-1)));

    for(ic=0; ic<chain->NC-1; ic++)
    {
      chain->temperature[ic] = pow(chain->dT[0],ic*ic);
    }

  }


  /****************************************************/
  /*                                                  */
  /* MODEL PARAMETERS                                 */
  /*                                                  */
  /****************************************************/
  for(ic=0; ic<NC; ic++)
  {
    model[ic]->size = 0;

    signal = model[ic]->signal;
    glitch = model[ic]->glitch;

    // clear out whatever is in the signal/glitch/cbc model
    
    for(i=0; i<data->N; i++)
    {
      for(n=0; n<model[ic]->Npol; n++) signal[n]->templates[i] = 0.0;
      for(ifo=0; ifo<NI; ifo++)
      {
        model[ic]->response[ifo][i] = 0.0;
        model[ic]->cbctemplate[ifo][i] = 0.0;
        glitch[ifo]->templates[i] = 0.0;
      }
    }
    

    // start model parameters anywhere
    if(data->glitchFlag)
    {
      //TODO: kickstart has to skip wavelets that are out of bounds
      if(data->waveletStartFlag || data->glitchBusterFlag) {
        // Start wavelets from wavelets held inside of glitchbuster or inside of glitch_{IFO}.dat
        if (data->glitchBusterFlag){
          kickstart_glitch_model_glitchbuster(data, prior, model[ic], gb);
        }
        else{
          kickstart_glitch_model_from_file(data, prior, model[ic]);
        }
      }
      else
      {
        for(ifo=0; ifo<NI; ifo++)
        {
          glitch[ifo]->size = 1;
          if(data->Dmin>1) glitch[ifo]->size = data->Dmin;
          model[ic]->size++;
          for(j=1; j<=glitch[ifo]->size; j++)
          {
            draw_wavelet_params(glitch[ifo]->intParams[j], prior->range, chain->seed, data->NW);
            if(data->amplitudePriorFlag)  data->glitch_amplitude_proposal(glitch[ifo]->intParams[j], model[ic]->Snf[ifo], chain->seed, data->Tobs, prior->range, prior->gSNRpeak);
            
            model[ic]->wavelet(glitch[ifo]->templates, glitch[ifo]->intParams[j], data->N, 1, Tobs);
          }
        }
      }
    }

    if(data->signalFlag || data->cbcFlag)
    {

      extrinsic_uniform_proposal(chain->seed,model[ic]->extParams);

      if(data->skyFixFlag == 1)
      {
        model[ic]->extParams[0] = data->FIX_RA;
        model[ic]->extParams[1] = sin(data->FIX_DEC);
      }

      // Fix extrinsic parameters for GW150914
      // model[ic]->extParams[0] = 1.75;
      // model[ic]->extParams[1] = sin(-1.22);
      // model[ic]->extParams[2] = 1.5000;
      // model[ic]->extParams[3] = -0.9999999999999999;

      //FIXME: Fix ugly psi=0 hack for unpolarized case
      //if(!data->polarizationFlag) model[ic]->extParams[2] = 0.0;

      /*
       WaveformProject() was split into two functions:
       -computeProjectionCoeffs() computes the time delays, F+ and Fx, and
       exp(i Phi(f)), which only change with new extrinsic parameters.
       -waveformProject uses the stored coeffs and does the convolution between the
       current geocenter waveform and the detector response function.
       */


      computeProjectionCoeffs(data, model[ic]->projection, model[ic]->extParams, data->fmin, data->fmax);
      Shf_Geocenter_full(data, model[ic]->projection, model[ic]->Snf, model[ic]->SnGeo, model[ic]->extParams);
    }

    if(data->signalFlag)
    {
      
      for(n=0; n<model[ic]->Npol; n++)
      {
        signal[n]->size = 1;
        if(data->rjFlag==0)signal[n]->size=data->Dmin;
        
        if(n==0) model[ic]->size++;
        
        for(j=1; j<=signal[n]->size; j++)
        {
          draw_wavelet_params(signal[n]->intParams[j], prior->range, chain->seed, data->NW);
          if(data->amplitudePriorFlag)
            data->signal_amplitude_proposal(signal[n]->intParams[j], model[ic]->SnGeo, chain->seed, data->Tobs, prior->range, prior->sSNRpeak);
          
          //void constrain_hplus_hcross(struct Wavelet **wave, int i)
          if(n==1)constrain_hplus_hcross(signal, j);
          
          model[ic]->wavelet(signal[n]->templates, signal[n]->intParams[j], data->N, 1, Tobs);
        }
      }
      
      combinePolarizations(data, model[ic]->signal, model[ic]->h, model[ic]->extParams, model[ic]->Npol);
      waveformProject(data, model[ic]->projection, model[ic]->extParams, model[ic]->response, model[ic]->h, data->fmin, data->fmax);
    }

    // BayesCBC model
    if (data->runPhase && data->cbcFlag) 
    {
      // Pass the most recent model parameters to BayesCBC structure
      // TODO TEST THIS FOR CHECKPOINTING!
      setup_bayescbc_model(bayescbc, data, chain, model[ic], ic, ic, 0);
    }


    // form up residual and compute initial likelihood
    if(data->stochasticFlag)
    {
      //TODO: No support for glitch model in stochastic likelihood
      ComputeNoiseCorrelationMatrix(data, model[ic]->Snf, model[ic]->background);

      model[ic]->logL = loglike_stochastic(data->NI, data->imin, data->imax, data->r, model[ic]->background->Cij, model[ic]->background->detCij);
    }
    else
    {
      model[ic]->logL = 0.0;
      model[ic]->logLnorm = 0.0;

      for(ifo=0; ifo<NI; ifo++)
      {
        model[ic]->detLogL[ifo] = 0.0;

        for(i=0; i<data->N; i++)
        {
          data->r[ifo][i] = data->s[ifo][i];
          if(data->signalFlag) data->r[ifo][i] -= model[ic]->response[ifo][i];
          if(data->glitchFlag) data->r[ifo][i] -= glitch[ifo]->templates[i];
          if(data->cbcFlag) data->r[ifo][i] -= model[ic]->cbctemplate[ifo][i];
        }
        if(!data->constantLogLFlag)
        {
          model[ic]->detLogL[ifo] = loglike(data->imin, data->imax, data->r[ifo], model[ic]->invSnf[ifo]);
          model[ic]->logL += model[ic]->detLogL[ifo];
          for(i=0; i<data->N/2; i++)
          {
            model[ic]->logLnorm -= log(model[ic]->Snf[ifo][i]);
          }

        }
      }
    }

    /****************************************************/
    /*                                                  */
    /* CHAIN FILES                                      */
    /*                                                  */
    /****************************************************/
    if(ic==0 || data->verboseFlag)
    {
      //Basic chain file with likelihood and model dimensions
      sprintf(filename,"chains/%s_model.dat.%i",modelname,ic);
      chain->intChainFile[ic] = fopen(filename,"w");

      //Parameters for reproducing glitch model
      if(data->glitchFlag)
      {
        for(ifo=0; ifo<NI; ifo++)
        {
          sprintf(filename,"chains/%s_params_%s.dat.%i",modelname,data->ifos[ifo],ic);
          chain->intGlitchChainFile[ifo][ic] = fopen(filename,"w");
        }
      }

      //Parameters for reproducing signal model
      if(data->signalFlag)
      {
        //intrinsic
        for(n=0; n<data->Npol; n++)
        {
          sprintf(filename,"chains/%s_params_h%i.dat.%i",modelname,n,ic);
          chain->intWaveChainFile[n][ic] = fopen(filename,"w");
        }
      }

      //Parameters for reproducing PSD model
      if(data->bayesLineFlag)
      {
        for(ifo=0; ifo<NI; ifo++)
        {
          sprintf(filename,"chains/%s_spline_%s.dat.%i",modelname,data->ifos[ifo],ic);
          chain->splineChainFile[ic][ifo] = fopen(filename,"w");

          sprintf(filename,"chains/%s_lorentz_%s.dat.%i",modelname,data->ifos[ifo],ic);
          chain->lineChainFile[ic][ifo] = fopen(filename,"w");
        }
      }

      //Parameters for reproducing CBC model
      if(data->cbcFlag)
      {
        sprintf(filename,"chains/%s_params.dat.%i",modelname,ic);
        chain->cbcChainFile[ic] = fopen(filename,"w");        
      }
      
    }//end chain files initialization
  }
  // BayesCBC model burnin when starting up
  if (data->runPhase && data->cbcFlag) 
  {
    // before entering burnin_cbc start the wavelet models off where we want them
    // NOTE only subtracts off wavelets from the 0th chain
    if(data->waveletStartFlag)
    {
      for(ifo=0; ifo<NI; ifo++)
      {
        // Copy over current multi-template & current residual
        for(i=0; i<data->N; i++)
        {
          data->r[ifo][i] = data->s[ifo][i];
          if(data->signalFlag) data->r[ifo][i] -= model[0]->response[ifo][i];
          if(data->glitchFlag) data->r[ifo][i] -= model[0]->glitch[ifo]->templates[i];
          bayescbc->D[ifo][i] = data->r[ifo][i];
        }
      }
    }

    // If starting from scratch, option for CBC intrinsic and extrinsic burnins
    burnin_cbc(bayescbc, data, model, chain);

    // after CBC has been burned in, find glitchpower
    if(data->glitchBusterFlag)
    {
      for(ifo=0; ifo<NI; ifo++)
      {
        // Copy over current multi-template & current residual
        for(i=0; i<data->N; i++)
        {
          data->r[ifo][i] = data->s[ifo][i];
          if(data->glitchFlag) data->r[ifo][i] -= model[0]->glitch[ifo]->templates[i];
          bayescbc->D[ifo][i] = data->r[ifo][i];
        }
      }
    }

    reinitialize_history(bayescbc->history, bayescbc->pallx, bayescbc->NH, bayescbc->NC, bayescbc->NX, chain->seed);

    // Only evolve extrinsic params from now on
    bayescbc->intrinsic_only = 1;
  }

}

static void resume_sampler(struct Data *data, struct Chain *chain, struct Model **model, struct BayesLineParams ***bayesline, struct bayesCBC *bayescbc)
{
  //TODO: set internal mcmc flags right based on chain->mc
  int i,n;
  int ic,ifo;

  int NI = data->NI;
  int NG[NI];
  int NS;

  double logL;

  struct Wavelet **signal = NULL;
  struct Wavelet **glitch = NULL;

  char filename[128];
  char modelname[128];

  FILE *fptr   = NULL;
  FILE **fptr2 = NULL;

  double **h  = NULL;
  double **g = NULL;

  h = malloc(data->NI*sizeof(double *));
  g = malloc(data->NI*sizeof(double *));
  for(ifo=0; ifo<NI; ifo++)
  {
    g[ifo] = malloc(data->N*sizeof(double));
    h[ifo] = malloc(data->N*sizeof(double));
  }

  /****************************************************/
  /*                                                  */
  /* STATE VECTOR                                     */
  /*                                                  */
  /****************************************************/
  sprintf(filename,"checkpoint/state.dat");
  if( (fptr = fopen(filename,"r")) == NULL )
  {
    fprintf(stderr,"Error:  Could not checkpoint run state\n");
    fprintf(stderr,"        Parameter file %s does not exist\n",filename);
    fprintf(stderr,"        Exiting to system (1)\n");
    exit(1);
  }
  fscanf(fptr,"%s",modelname);
  fscanf(fptr,"%i",&data->cleanModelFlag);
  fscanf(fptr,"%i",&data->noiseModelFlag);
  fscanf(fptr,"%i",&data->glitchModelFlag);
  fscanf(fptr,"%i",&data->signalModelFlag);
  fscanf(fptr,"%i",&data->fullModelFlag);
  fscanf(fptr,"%i",&data->cleanOnlyFlag);
  fscanf(fptr,"%i",&data->loudGlitchFlag);
  fclose(fptr);


  /****************************************************/
  /*                                                  */
  /* DATA AND PSD                                     */
  /*                                                  */
  /****************************************************/
  for(ifo=0; ifo<NI; ifo++)
  {
    sprintf(filename,"checkpoint/data.dat.%i",ifo);
    if( (fptr = fopen(filename,"r")) == NULL )
    {
      fprintf(stderr,"Error:  Could not checkpoint data structure\n");
      fprintf(stderr,"        Parameter file %s does not exist\n",filename);
      fprintf(stderr,"        Exiting to system (1)\n");
      exit(1);
    }
    for(i=0; i<data->N/2; i++)
    {
      fscanf(fptr,"%lg %lg",&data->s[ifo][2*i],&data->s[ifo][2*i+1]);//,&data->Snf[ifo][i],&data->invSnf[ifo][i]);
    }
    fclose(fptr);
  }


  /****************************************************/
  /*                                                  */
  /* PTMCMC TEMPERATURE LADDER                        */
  /*                                                  */
  /****************************************************/
  //Read in dT, index, temperature, ptprop, ptacc, A, avg&var loglikelihood?
  sprintf(filename,"checkpoint/temperature.dat");
  if( (fptr = fopen(filename,"r")) == NULL )
  {
    fprintf(stderr,"Error:  Could not checkpoint temperature model\n");
    fprintf(stderr,"        Parameter file %s does not exist\n",filename);
    fprintf(stderr,"        Exiting to system (1)\n");
    exit(1);
  }
  fscanf(fptr,"%i",&chain->mc);
  fscanf(fptr,"%i",&chain->NC);
  fscanf(fptr,"%i",&chain->zcount);
  
  //if(NC != chain->NC) resize_model(data, chain, prior, model, bayesline, model[0]->Snf, NC);

  for(ic=0; ic<chain->NC; ic++)
  {
    fscanf(fptr,"%lg", &chain->dT[ic]);
    fscanf(fptr,"%i",  &chain->index[ic]);
    fscanf(fptr,"%lg", &chain->temperature[ic]);
    fscanf(fptr,"%i",  &chain->ptprop[ic]);
    fscanf(fptr,"%i",  &chain->ptacc[ic]);
    fscanf(fptr,"%lg", &chain->A[ic]);
    fscanf(fptr,"%lg", &chain->avgLogLikelihood[ic]);
    fscanf(fptr,"%lg", &chain->varLogLikelihood[ic]);
  }
  chain->tempMin = chain->temperature[0];
  fclose(fptr);

  sprintf(filename,"checkpoint/rng.bin");
  if( (fptr = fopen(filename,"rb")) == NULL )
    {
      fprintf(stderr,"Error:  Could not checkpoint the rng\n");
      fprintf(stderr,"        Parameter file %s does not exist\n",filename);
      fprintf(stderr,"        Exiting to system (1)\n");
      exit(1);
    }
  gsl_rng_fread(fptr,chain->seed);
  fclose(fptr);

  sprintf(filename,"checkpoint/logLchain.dat");
  if( (fptr = fopen(filename,"r")) == NULL )
  {
    fprintf(stderr,"Error:  Could not checkpoint likelihood chain\n");
    fprintf(stderr,"        Parameter file %s does not exist\n",filename);
    fprintf(stderr,"        Exiting to system (1)\n");
    exit(1);
  }
  for(i=0; i<chain->zcount; i++)
  {
    for(ic=0; ic<chain->NC; ic++)
    {
      fscanf(fptr,"%lg",&chain->logLchain[ic][i]);
    }
  }
  fclose(fptr);

  /****************************************************/
  /*                                                  */
  /* READ IN STORED MODEL PARAMETERS                  */
  /*                                                  */
  /****************************************************/
  for(ic=0; ic<chain->NC; ic++)
  {
    signal = model[ic]->signal;
    glitch = model[ic]->glitch;

    //Basic chain file with likelihood and model dimensions
    sprintf(filename,"checkpoint/model.dat.%i",ic);
    if( (fptr = fopen(filename,"r")) == NULL )
    {
      fprintf(stderr,"Error:  Could not checkpoint model settings\n");
      fprintf(stderr,"        Parameter file %s does not exist\n",filename);
      fprintf(stderr,"        Exiting to system (1)\n");
      exit(1);
    }

    fscanf(fptr,"%i %lg",&i,&logL); //iteration and logL
    fscanf(fptr,"%i",&NS);                                //signal model dimension
    for(i=0; i<NI; i++)fscanf(fptr,"%i",&NG[i]);          //glitch model dimension
    fclose(fptr);

    signal = model[ic]->signal;
    glitch = model[ic]->glitch;

    model[ic]->size = 0;
    signal[0]->size = 0;
    for(ifo=0; ifo<NI; ifo++) glitch[ifo]->size = 0;

    // Glitch model
    if(data->glitchFlag)
    {
      fptr2 = malloc(NI*sizeof(FILE *));
      for(ifo=0; ifo<NI; ifo++)
      {
        sprintf(filename,"checkpoint/params_%s.dat.%i",data->ifos[ifo],ic);
        if( (fptr2[ifo] = fopen(filename,"r")) == NULL)
        {
          fprintf(stderr,"Error:  Could not checkpoint glitch model\n");
          fprintf(stderr,"        Parameter file %s does not exist\n",filename);
          fprintf(stderr,"        Exiting to system (1)\n");
          exit(1);
        }
      }
      parse_glitch_parameters(data, model[ic], fptr2, g);
      for(ifo=0; ifo<NI; ifo++) fclose(fptr2[ifo]);
      free(fptr2);
    }

    // Signal model
    if(data->signalFlag||data->cbcFlag)
    {
      fptr2 = malloc(data->Npol*sizeof(FILE *));
      for(n=0; n<data->Npol; n++)
      {
        sprintf(filename,"checkpoint/params_h%i.dat.%i",n,ic);
        if( (fptr2[n] = fopen(filename,"r")) == NULL)
        {
          fprintf(stderr,"Error:  Could not checkpoint signal model\n");
          fprintf(stderr,"        Parameter file %s does not exist\n",filename);
          fprintf(stderr,"        Exiting to system (1)\n");
          exit(1);
        }
      }
      parse_signal_parameters(data, model[ic], fptr2, h);
      for(n=0; n<data->Npol; n++) fclose(fptr2[n]);
      free(fptr2);
    }

    // PSD model
    for(ifo=0; ifo<NI; ifo++)
    {
      sprintf(filename,"checkpoint/psd_%s.dat.%i",data->ifos[ifo],ic);
      if( (fptr = fopen(filename,"r")) == NULL)
      {
        fprintf(stderr,"Error:  Could not checkpoint psd model\n");
        fprintf(stderr,"        Parameter file %s does not exist\n",filename);
        fprintf(stderr,"        Exiting to system (1)\n");
        exit(1);
      }
      for(i=0; i<data->N/2; i++)
      {
        fscanf(fptr,"%lg %lg",&model[ic]->Snf[ifo][i],&model[ic]->invSnf[ifo][i]);
      }
      fclose(fptr);

      if(data->bayesLineFlag)
      {
        sprintf(filename,"checkpoint/line_%s.dat.%i",data->ifos[ifo],ic);
        if( (fptr = fopen(filename,"r")) == NULL)
        {
          fprintf(stderr,"Error:  Could not checkpoint line model\n");
          fprintf(stderr,"        Parameter file %s does not exist\n",filename);
          fprintf(stderr,"        Exiting to system (1)\n");
          exit(1);
        }
        parse_line_model(fptr, bayesline[ic][ifo]);
        fclose(fptr);

        sprintf(filename,"checkpoint/spline_%s.dat.%i",data->ifos[ifo],ic);
        if( (fptr = fopen(filename,"r")) == NULL)
        {
          fprintf(stderr,"Error:  Could not checkpoint spline model\n");
          fprintf(stderr,"        Parameter file %s does not exist\n",filename);
          fprintf(stderr,"        Exiting to system (1)\n");
          exit(1);
        }
        parse_spline_model(fptr, bayesline[ic][ifo]);
        fclose(fptr);

        sprintf(filename,"checkpoint/bayesline_%s.dat.%i",data->ifos[ifo],ic);
        if( (fptr = fopen(filename,"r")) == NULL)
        {
          fprintf(stderr,"Error:  Could not checkpoint bayesline model\n");
          fprintf(stderr,"        Parameter file %s does not exist\n",filename);
          fprintf(stderr,"        Exiting to system (1)\n");
          exit(1);
        }
        for(i=0; i< bayesline[ic][ifo]->data->ncut; i++)
        {
          fscanf(fptr,"%lg %lg %lg",&bayesline[ic][ifo]->Snf[i],&bayesline[ic][ifo]->Sbase[i],&bayesline[ic][ifo]->Sline[i]);
        }
        fclose(fptr);

        sprintf(filename,"checkpoint/psd_prior_%s.dat.%i",data->ifos[ifo],ic);
        if( (fptr = fopen(filename,"r")) == NULL)
        {
          fprintf(stderr,"Error:  Could not checkpoint bayesline priors\n");
          fprintf(stderr,"        Parameter file %s does not exist\n",filename);
          fprintf(stderr,"        Exiting to system (1)\n");
          exit(1);
        }
        for(i=0; i<bayesline[ic][ifo]->data->ncut; i++)
        {
          fscanf(fptr,"%lg %lg %lg %lg",&bayesline[ic][ifo]->priors->sigma[i],&bayesline[ic][ifo]->priors->mean[i],&bayesline[ic][ifo]->priors->lower[i],&bayesline[ic][ifo]->priors->upper[i]);
        }
        fclose(fptr);
      }//end bayeslineFlag
    }

    // CBC model
    if(data->cbcFlag)
    {
      sprintf(filename,"checkpoint/params_cbc.dat.%i",ic);
      if( (fptr = fopen(filename,"r")) == NULL)
      {
        fprintf(stderr,"Error:  Could not checkpoint CBC model\n");
        fprintf(stderr,"        Parameter file %s does not exist\n",filename);
        fprintf(stderr,"        Exiting to system (1)\n");
        exit(1);
      }
      parse_cbc_parameters(data, model[ic], chain, bayescbc, fptr, ic);
      fclose(fptr);

      // Restore history for each CBC chain
      sprintf(filename,"checkpoint/history_cbc.dat.%i",ic);
      if( (fptr = fopen(filename,"r")) == NULL)
      {
        fprintf(stderr,"Error:  Could not checkpoint cbc history\n");
        fprintf(stderr,"        History file %s does not exist\n",filename);
        fprintf(stderr,"        Exiting to system (1)\n");
        exit(1);
      }
      printf(" parsing CBC history from %s\n", filename);
      parse_cbc_history(fptr, bayescbc, ic);
      fclose(fptr);
    }

    // Check that the model dimensions match
    for(ifo=0; ifo<NI; ifo++)
    {
      model[ic]->size += glitch[ifo]->size;
//      if(glitch[ifo]->size != NG[ifo])
//      {
//        fprintf(stderr,"Error:  Could not checkpoint glitch model\n");
//        fprintf(stderr,"        Model dimensions do not match (%s:  %i!=%i)\n",data->ifos[ifo],glitch[ifo]->size,NG[ifo]);
//        fprintf(stderr,"        Exiting to system (1)\n");
//        exit(1);
//      }
    }
    model[ic]->size += signal[0]->size;
//    if(signal[0]->size != NS)
//    {
//      fprintf(stderr,"Error:  Could not checkpoint signal model\n");
//      fprintf(stderr,"        Model dimensions do not match (%i!=%i)\n",signal[0]->size,NS);
//      fprintf(stderr,"        Exiting to system (1)\n");
//      exit(1);
//    }

    // form up residual and compute initial likelihood
    model[ic]->logL = 0.0;
    model[ic]->logLnorm = 0.0;

    for(ifo=0; ifo<NI; ifo++)
    {
      model[ic]->detLogL[ifo] = 0.0;

      for(i=0; i<data->N; i++)
      {
        data->r[ifo][i] = data->s[ifo][i];
        if(data->signalFlag) data->r[ifo][i] -= model[ic]->response[ifo][i];
        if(data->glitchFlag) data->r[ifo][i] -= glitch[ifo]->templates[i];
        if(data->cbcFlag) data->r[ifo][i] -= model[ic]->cbctemplate[ifo][i];
      }
      if(!data->constantLogLFlag)
      {
        model[ic]->detLogL[ifo] = loglike(data->imin, data->imax, data->r[ifo], model[ic]->invSnf[ifo]);
        model[ic]->logL += model[ic]->detLogL[ifo];
        for(i=0; i<data->N/2; i++)
        {
          model[ic]->logLnorm -= log(model[ic]->Snf[ifo][i]);
        }

      }
    }
    
    /****************************************************/
    /*                                                  */
    /* REOPEN CHAIN FILES                               */
    /*                                                  */
    /****************************************************/
    if(ic==0 || data->verboseFlag)
    {
      //Basic chain file with likelihood and model dimensions
      sprintf(filename,"chains/%s_model.dat.%i",modelname,ic);
      chain->intChainFile[ic] = fopen(filename,"a");

      //Parameters for reproducing glitch model
      if(data->glitchFlag)
      {
        for(ifo=0; ifo<NI; ifo++)
        {
          sprintf(filename,"chains/%s_params_%s.dat.%i",modelname,data->ifos[ifo],ic);
          chain->intGlitchChainFile[ifo][ic] = fopen(filename,"a");
        }
      }

      //Parameters for reproducing signal model
      if(data->signalFlag)
      {
        //intrinsic
        for(n=0; n<data->Npol; n++)
        {
          sprintf(filename,"chains/%s_params_h%i.dat.%i",modelname,n,ic);
          chain->intWaveChainFile[n][ic] = fopen(filename,"a");
        }
      }

      //Parameters for reproducing CBC model
      if(data->cbcFlag)
      {
        sprintf(filename,"chains/%s_params.dat.%i",modelname,ic);
        chain->cbcChainFile[ic] = fopen(filename,"a");        
      }

      //Parameters for reproducing PSD model
      if(data->bayesLineFlag)
      {
        for(ifo=0; ifo<NI; ifo++)
        {
          sprintf(filename,"chains/%s_spline_%s.dat.%i",modelname,data->ifos[ifo],ic);
          chain->splineChainFile[ic][ifo] = fopen(filename,"a");

          sprintf(filename,"chains/%s_lorentz_%s.dat.%i",modelname,data->ifos[ifo],ic);
          chain->lineChainFile[ic][ifo] = fopen(filename,"a");
        }
      }
      
    }//end chain files initialization
  }

    // Priors for GW150914 with NRTidal
    if (LALInferenceGetProcParamVal(data->commandLine, "--BBH-prior")) {
        bayescbc->lambda1min = 0.0;       // Min/max for lambda1 and lambda2
        bayescbc->lambda1max = 10.0; // 1000.0;    // Only used if NRTidal_value != NoNRT_V and
        bayescbc->lambda2min = 0.0;       // lambda_type_value = lambda 
        bayescbc->lambda2max = 10.0;

        bayescbc->lambdaTmin = 0.0;       // Min/max for lambdaT and dlambdaT
        bayescbc->lambdaTmax = 10.0;    // Only used if NRTidal_value != NoNRT_V and
        bayescbc->dlambdaTmin = -0.1;     // lambda_type_value = lambdaTilde 
        bayescbc->dlambdaTmax = 0.1;

        bayescbc->chi1min = -0.99;  // Default min, max for chi1 and chi2
        bayescbc->chi2min = -0.99;
        bayescbc->chi1max = 0.99;
        bayescbc->chi2max = 0.99;

        bayescbc->mcmax = 174.0;   // maximum chirp mass in Msun.  Mc is at most 0.435 times Mt
        bayescbc->mcmin = 0.23;    // minimum chirp mass in Msun
        bayescbc->mtmax = 400.0;   // maximum total mass in Msun (should be not more than 2.3 times mcmax)
        bayescbc->mtmin = 1.0;     // minimum total mass in Msun

        bayescbc->max[0] = log(bayescbc->mcmax*MSUN_SI);  // Mc is at most 0.435 times Mt
        bayescbc->max[1] = log(bayescbc->mtmax*MSUN_SI);  // Mt
        bayescbc->min[0] = log(bayescbc->mcmin*MSUN_SI);  // Mc
        bayescbc->min[1] = log(bayescbc->mtmin*MSUN_SI);  // Mt
        bayescbc->max[2] = bayescbc->chi1max;  // chi1
        bayescbc->max[3] = bayescbc->chi2max;  // chi2
        bayescbc->min[2] = bayescbc->chi1min;  // chi1
        bayescbc->min[3] = bayescbc->chi2min;  // chi2

        if (bayescbc->lambda_type_version == (Lambda_type) lambdaTilde) {
            bayescbc->max[7] = bayescbc->lambdaTmax; // lambdaT
            bayescbc->max[8] = bayescbc->dlambdaTmax; // dlambdaT
            bayescbc->min[7] = bayescbc->lambdaTmin; // lambdaT
            bayescbc->min[8] = bayescbc->dlambdaTmin; // dlambdaT
        } else {
            bayescbc->max[7] = bayescbc->lambda1max; // lambda1
            bayescbc->max[8] = bayescbc->lambda2max; // lambda2
            bayescbc->min[7] = bayescbc->lambda1min; // lambda1
            bayescbc->min[8] = bayescbc->lambda2min; // lambda2
        }

    }

  /****************************************************/
  /*                                                  */
  /* CLEAR OUT CHECKPOINT DIRECTORY                   */
  /*                                                  */
  /****************************************************/
  fprintf(stdout,"...resuming model %s at iteration %i\n",modelname,chain->mc);
  fflush(stdout);

  for(ifo=0; ifo<NI; ifo++)
  {
    free(g[ifo]);
    free(h[ifo]);
  }
  free(g);
  free(h);
}

static void save_sampler(struct Data *data, struct Chain *chain, struct Model **model, struct BayesLineParams ***bayesline, struct bayesCBC *bayescbc, char modelname[])
{
  int i,n;
  int ic,ifo;

  int NI = data->NI;

  struct Wavelet **signal = NULL;
  struct Wavelet **glitch = NULL;

  char filename[128];

  FILE *fptr = NULL;

  /****************************************************/
  /*                                                  */
  /* FLUSH CHAIN FILES                                */
  /*                                                  */
  /****************************************************/
  flush_chain_files(data,chain,0);


  /****************************************************/
  /*                                                  */
  /* STATE VECTOR                                     */
  /*                                                  */
  /****************************************************/
  sprintf(filename,"checkpoint/state.dat");
  fptr = fopen(filename,"w");
  fprintf(fptr,"%s ",modelname);
  fprintf(fptr,"%i ",data->cleanModelFlag);
  fprintf(fptr,"%i ",data->noiseModelFlag);
  fprintf(fptr,"%i ",data->glitchModelFlag);
  fprintf(fptr,"%i ",data->signalModelFlag);
  fprintf(fptr,"%i ",data->fullModelFlag);
  fprintf(fptr,"%i ",data->cleanOnlyFlag);
  fprintf(fptr,"%i ",data->loudGlitchFlag);
  fprintf(fptr,"\n");
  fclose(fptr);

  /****************************************************/
  /*                                                  */
  /* DATA AND PSD                                     */
  /*                                                  */
  /****************************************************/
  for(ifo=0; ifo<NI; ifo++)
  {
    sprintf(filename,"checkpoint/data.dat.%i",ifo);
    fptr = fopen(filename,"w");
    for(i=0; i<data->N/2; i++)
    {
      fprintf(fptr,"%.12g %.12g\n",data->s[ifo][2*i],data->s[ifo][2*i+1]);
    }
    fclose(fptr);
  }

  /****************************************************/
  /*                                                  */
  /* PTMCMC TEMPERATURE LADDER                        */
  /*                                                  */
  /****************************************************/
  sprintf(filename,"checkpoint/temperature.dat");
  fptr = fopen(filename,"w");

  fprintf(fptr,"%i\n",chain->mc);
  fprintf(fptr,"%i\n",chain->NC);
  fprintf(fptr,"%i\n",chain->zcount);
  
  for(ic=0; ic<chain->NC; ic++)
  {
    fprintf(fptr,"%lg ", chain->dT[ic]);
    fprintf(fptr,"%i ",  chain->index[ic]);
    fprintf(fptr,"%lg ", chain->temperature[ic]);
    fprintf(fptr,"%i ",  chain->ptprop[ic]);
    fprintf(fptr,"%i ",  chain->ptacc[ic]);
    fprintf(fptr,"%lg " , chain->A[ic]);
    fprintf(fptr,"%lg ", chain->avgLogLikelihood[ic]);
    fprintf(fptr,"%lg ", chain->varLogLikelihood[ic]);
    fprintf(fptr,"\n");
  }
  fclose(fptr);

  sprintf(filename,"checkpoint/rng.bin");
  fptr = fopen(filename,"wb");
  gsl_rng_fwrite(fptr,chain->seed);
  fclose(fptr);

  sprintf(filename,"checkpoint/logLchain.dat");
  fptr = fopen(filename,"w");
  for(i=0; i<chain->zcount; i++)
  {
    for(ic=0; ic<chain->NC; ic++)
    {
      fprintf(fptr,"%.12g ",chain->logLchain[ic][i]);
    }
    fprintf(fptr,"\n");
  }
  fclose(fptr);

  /****************************************************/
  /*                                                  */
  /* WRITE OUT CURRENT MODEL PARAMETERS               */
  /*                                                  */
  /****************************************************/
  for(ic=0; ic<chain->NC; ic++)
  {
    signal = model[ic]->signal;
    glitch = model[ic]->glitch;

    //Basic chain file with likelihood and model dimensions
    sprintf(filename,"checkpoint/model.dat.%i",ic);
    fptr = fopen(filename,"w");
    fprintf(fptr,"%i %.12g ", chain->mc, model[ic]->logL);           //iteration and logL
    fprintf(fptr,"%i ",signal[0]->size);                             //signal model dimension
    for(i=0; i<NI; i++)fprintf(fptr,"%i ",glitch[i]->size);          //glitch model dimension
    fprintf(fptr,"\n");
    fclose(fptr);

    // Glitch model
    if(data->glitchFlag)
    {
      for(ifo=0; ifo<NI; ifo++)
      {
        sprintf(filename,"checkpoint/params_%s.dat.%i",data->ifos[ifo],ic);
        fptr = fopen(filename,"w");
        print_glitch_model(fptr, glitch[ifo]);
        fclose(fptr);
      }
    }


    // Signal model
    if(data->signalFlag||data->cbcFlag)
    {
      for(n=0; n<data->Npol; n++)
      {
        sprintf(filename,"checkpoint/params_h%i.dat.%i",n,ic);
        fptr = fopen(filename,"w");
        print_signal_model(fptr, model[ic],n);
        fclose(fptr);
      }
    }

    // CBC model
    if(data->cbcFlag)
    {
      sprintf(filename,"checkpoint/params_cbc.dat.%i",ic);
      fptr = fopen(filename,"w");
      print_cbc_checkpoint_model(fptr, model[ic], data->NX);
      fclose(fptr);

      // Save history for each CBC chain
      sprintf(filename,"checkpoint/history_cbc.dat.%i",ic);
      fptr = fopen(filename,"w");
      print_cbc_history(fptr, bayescbc, ic);
      fclose(fptr);
    }
    // asd model
    if (ic == 0){
      for(ifo=0; ifo<NI; ifo++)
      {
        sprintf(filename,"checkpoint/asd_%s.dat.%i",data->ifos[ifo],ic);
        fptr = fopen(filename,"w");
        double f0 = 0;
        double df = 1.0 / data->Tobs;
        for(i=0; i<data->N/2; i++)
        {
          // frequency, asd
          fprintf(fptr,"%lg %lg\n",f0, sqrt(model[ic]->Snf[ifo][i] / (data->Tobs / 2)));
          f0 = f0 + df;
        }
        fclose(fptr); 
      }
    }

    // PSD model
    for(ifo=0; ifo<NI; ifo++)
    {
      sprintf(filename,"checkpoint/psd_%s.dat.%i",data->ifos[ifo],ic);
      fptr = fopen(filename,"w");
      for(i=0; i<data->N/2; i++)
      {
        fprintf(fptr,"%lg %lg\n",model[ic]->Snf[ifo][i],model[ic]->invSnf[ifo][i]);
      }
      fclose(fptr);

      if(data->bayesLineFlag)
      {
        sprintf(filename,"checkpoint/line_%s.dat.%i",data->ifos[ifo],ic);
        fptr = fopen(filename,"w");
        print_line_model(fptr, bayesline[ic][ifo]);
        fclose(fptr);

        sprintf(filename,"checkpoint/spline_%s.dat.%i",data->ifos[ifo],ic);
        fptr = fopen(filename,"w");
        print_spline_model(fptr, bayesline[ic][ifo]);
        fclose(fptr);

        sprintf(filename,"checkpoint/bayesline_%s.dat.%i",data->ifos[ifo],ic);
        fptr = fopen(filename,"w");
        for(i=0; i< bayesline[ic][ifo]->data->ncut; i++)
        {
          fprintf(fptr,"%lg %lg %lg\n",bayesline[ic][ifo]->Snf[i],bayesline[ic][ifo]->Sbase[i],bayesline[ic][ifo]->Sline[i]);
        }
        fclose(fptr);

        sprintf(filename,"checkpoint/psd_prior_%s.dat.%i",data->ifos[ifo],ic);
        fptr = fopen(filename,"w");
        for(i=0; i<bayesline[ic][ifo]->data->ncut; i++)
        {
          fprintf(fptr,"%lg %lg %lg %lg\n",bayesline[ic][ifo]->priors->sigma[i],bayesline[ic][ifo]->priors->mean[i],bayesline[ic][ifo]->priors->lower[i],bayesline[ic][ifo]->priors->upper[i]);
        }
        fclose(fptr);
      }//end bayeslineFlag
    }//loop over IFOs
  }//loop over chains
  
  /****************************************************/
  /*                                                  */
  /* WRITE OUT CURRENT EVIDENCE RESULTS               */
  /*                                                  */
  /****************************************************/
  sprintf(filename,"checkpoint/evidence.dat");
  fptr = fopen(filename,"w");
  fprintf(fptr,"%.12g %lg\n",data->logZnoise,data->varZnoise);
  fprintf(fptr,"%.12g %lg\n",data->logZglitch,data->varZglitch);
  fprintf(fptr,"%.12g %lg\n",data->logZsignal,data->varZsignal);
  fprintf(fptr,"%.12g %lg\n",data->logZfull,data->varZfull);
  fclose(fptr);

  fprintf(stdout,"...saved model %s at iteration %i\n",modelname,chain->mc);
}

/* ********************************************************************************** */
/*                                                                                    */
/*                                  MCMC Samplers                                     */
/*                                                                                    */
/* ********************************************************************************** */

void RJMCMC(struct Data *data, struct Model **model, struct BayesLineParams ***bayesline, struct bayesCBC *bayescbc, struct Chain *chain, struct Prior *prior, struct GlitchBuster *gb, double *logEvidence, double *varLogEvidence)
{

  print_run_stats(stdout, data, chain);
  print_run_flags(stdout, data, prior);
  if (data->cbcFlag) print_cbc_run_stats(stdout, data, bayescbc);

  int N  = data->N;
  int NI = data->NI;
  int M  = chain->count;
  int NC = chain->NC;


  int i, ic, ifo, n;

  double logZ = 0.0;
  double varZ = 0.0;

  double logPost;
  double logPostMap=-1e60;

  char filename[MAXSTRINGSIZE];
  char modelname[MAXSTRINGSIZE];

  // parameters to control how many frames are output to model animations
  int frame=0;
  int frameCount=200;

  // gnuplot animations cadence can be linear or logarithmic in time
  //TODO: linear/log frame cadence should be a command line switch
  //double frameInterval = log((double)M)/(double)frameCount;
  double frameInterval = (double)M/(double)frameCount;

  // parameters for autocorrelation length
  chain->zcount=0;

  // Store MAP model
  struct Model *model_MAP = malloc(sizeof(struct Model));
  initialize_model(model_MAP, data, prior, model[0]->Snf, chain->seed);


  /******************************************************************************/
  /*                                                                            */
  /*  Set up checkpointing                                                      */
  /*    Code taken from LALInferenceNestedSamplingAlgorithm.c                   */
  /*    courtesy of John Veitch                                                 */
  /*                                                                            */
  /******************************************************************************/
  if(data->checkpointFlag)
  {
    /* Install a periodic alarm that will trigger a checkpoint */
    int sigretcode=0;
    struct sigaction sa;
    sa.sa_sigaction=catch_alarm;
    sa.sa_flags=SA_SIGINFO;
    sigretcode=sigaction(SIGVTALRM,&sa,NULL);
    if(sigretcode!=0) fprintf(stderr,"WARNING: Cannot establish checkpoint timer!\n");

    /* Condor sends SIGUSR2 to checkpoint and continue */
    sigretcode=sigaction(SIGUSR2,&sa,NULL);
    if(sigretcode!=0) fprintf(stderr,"WARNING: Cannot establish checkpoint on SIGUSR2.\n");
    checkpoint_timer.it_interval.tv_sec=3600; /* Default timer 1 hour */
    checkpoint_timer.it_interval.tv_usec=0;
    checkpoint_timer.it_value=checkpoint_timer.it_interval;
    setitimer(ITIMER_VIRTUAL,&checkpoint_timer,NULL);

    /* Install the handler for the condor interrupt signal */
    sa.sa_sigaction=catch_interrupt;
    sigretcode=sigaction(SIGINT,&sa,NULL);
    if(sigretcode!=0) fprintf(stderr,"WARNING: Cannot establish checkpoint on SIGINT.\n");

    /* Condor sends SIGTERM to vanilla universe jobs to evict them */
    // XXX: this is not always true.  The modern recommended way to checkpoint
    // is for the code to self-exit with a recognisable exit code when
    // checkpointing.
    sigretcode=sigaction(SIGTERM,&sa,NULL);
    if(sigretcode!=0) fprintf(stderr,"WARNING: Cannot establish checkpoint on SIGTERM.\n");
    /* Condor sends SIGTSTP to standard universe jobs to evict them.
     *I think condor handles this, so didn't add a handler CHECK */
  }
  /******************************************************************************/
  /*                                                                            */
  /*  Set up data structures                                                    */
  /*                                                                            */
  /******************************************************************************/
  

  /*
   OUTPUT DATA
   */

  //give output files common name
  if(data->runPhase==0)snprintf(modelname,MAXSTRINGSIZE,"%sclean",data->runName);
  else
  {
    if(data->cbcFlag){
      snprintf(modelname,MAXSTRINGSIZE,"%scbc",data->runName);
    }
    else if(!data->glitchFlag && !data->signalFlag){
      snprintf(modelname,MAXSTRINGSIZE,"%snoise",data->runName);}

    else if(data->glitchFlag && !data->signalFlag){
      snprintf(modelname,MAXSTRINGSIZE,"%sglitch",data->runName);}

    else if(!data->glitchFlag && data->signalFlag){
      snprintf(modelname,MAXSTRINGSIZE,"%ssignal",data->runName);}

    else{
      snprintf(modelname,MAXSTRINGSIZE,"%sfull",data->runName);}
  }

  /******************************************************************************/
  /*                                                                            */
  /*  Print run stats to file in current directory                              */
  /*  so that the run is replicable                                             */
  /*                                                                            */
  /******************************************************************************/

  sprintf(filename,"%s.run",modelname);
  chain->runFile = fopen(filename,"w");

  fprintf(chain->runFile,"\n ============== BayesWave =============\n");
  fprintf(chain->runFile,"  %s\n\n",LALInferencePrintCommandLine(data->commandLine) );
  print_run_flags(chain->runFile, data, prior);

  FILE *tfile = NULL;
  if(data->verboseFlag)
  {
    //Basic chain file with likelihood and model dimensions
    sprintf(filename,"chains/%s_temperature.dat",modelname);
    tfile = fopen(filename,"w");
  }

  /******************************************************************************/
  /*                                                                            */
  /*  Set up each model's parameters and output files                           */
  /*                                                                            */
  /******************************************************************************/

  chain->intPropRate = 0.5; //split intrinsic proposals between U[prior] and Fisher

  int burnFlag   = 0;
  int searchFlag = 0;

  print_run_stats(chain->runFile, data, chain);

  if(data->checkpointFlag && data->resumeFlag)
  {
    printf("try resuming...\n");
    resume_sampler(data, chain, model, bayesline, bayescbc);
    data->resumeFlag = 0;

    /******************************************************************************/
    /*                                                                            */
    /*  Make sure run settings are correct for current sample                     */
    /*                                                                            */
    /******************************************************************************/

    if(chain->mc>chain->burn) burnFlag=1;
  }
  else
  {
    restart_sampler(data, chain, prior, model, gb, modelname, bayescbc);
    printf("starting sampler with model size %i\n",model[0]->size);
  }

  //Set MAP model to initial state
  copy_psd_model(model[chain->index[0]], model_MAP, N, NI);
  copy_ext_model(model[chain->index[0]], model_MAP, N, NI);
  copy_cbc_model(model[chain->index[0]], model_MAP, N, NI, data->NX);
  copy_int_model(model[chain->index[0]], model_MAP, N, NI,-1);
  for(ifo=0; ifo<NI; ifo++) copy_int_model(model[chain->index[0]], model_MAP, N, NI,ifo);

  FILE *logLscript = NULL;
    
  // Set tmin-tmax for plotting
  double gnutmin, gnutmax;

  if(data->gnuplotFlag)
  {

    /******************************************************************************/
    /*                                                                            */
    /*  Set up gnuplot script for <logL> movies                                   */
    /*                                                                            */
    /******************************************************************************/
    sprintf(filename,"waveforms/%s_avgLogL_animate.gpi",modelname);
    FILE *logLscript=fopen(filename,"w");
    fprintf(logLscript,"set terminal gif small size 800,600 animate optimize delay 0.5 enhanced font\n");
    fprintf(logLscript,"set output \"%s_avgLogL.gif\"          \n",modelname);
    fprintf(logLscript,"                                       \n");
    fprintf(logLscript,"set logscale x                         \n");
    fprintf(logLscript,"set xrange [1e-8:1]                    \n");
    fprintf(logLscript,"set xlabel \"1/T\"                     \n");
    fprintf(logLscript,"set ylabel \"<logL>\"                  \n");
    fclose(logLscript);
    
  }

  /******************************************************************************/
  /*                                                                            */
  /*  Set up time-frequency proposal for wavelets                               */
  /*                                                                            */
  /******************************************************************************/
  struct TimeFrequencyMap *tf = malloc(sizeof(struct TimeFrequencyMap));

  if((data->signalFlag || data->glitchFlag) && !data->fixIntrinsicFlag)
  {
    initialize_TF_proposal(data, prior, tf);

    //set up proposal for glitch model
    for(ifo=0; ifo<NI; ifo++)
    {
      TFprop_setup(data, model[chain->index[0]], prior->range, tf, ifo);
      if(data->gnuplotFlag && data->verboseFlag) TFprop_plot(prior->range, tf, (prior->range[0][1]-prior->range[0][0])/(double)(tf->nt), ifo);
    }
    if(data->signalFlag)
    {
        TFprop_signal(data, prior->range, tf, model[chain->index[0]]->projection);
    }
  }

  /******************************************************************************/
  /*                                                                            */
  /*  The BurstRJMCMC loop                                                      */
  /*                                                                            */
  /******************************************************************************/

  clock_t start, end;
  double cpu_time_used;
  start = clock();

  while(chain->mc<M)
  {

    //Checkpoint the start of the chain for insurance purposes
    if(data->checkpointFlag && chain->mc==0) save_sampler(data, chain, model, bayesline, bayescbc, modelname);

    //Reset counters after initial burn-in & switch to Markovian likelihood function
    if(burnFlag==0 && chain->mc>chain->burn)  burnFlag=1;

    //print state of cold chain to file
    print_chain_files(data, chain, model, bayesline, bayescbc, 0);

    //if(chain->mc%1000) flush_chain_files(data,chain,0);
    if(data->verboseFlag)
    {
      //only print hot chains if asked to
      for(ic=1; ic<NC; ic++)print_chain_files(data, chain, model, bayesline, bayescbc, ic);
    }
    //print status update to screen
    if(chain->mc%(M/10)==0 || data->verboseFlag)
    {
      print_chain_status(data, chain, model, searchFlag);
    }
    
    //Loop over chains doing model updates
    for(ic=0; ic<NC; ic++)
    {
        chain->beta = 1./chain->temperature[ic];

        //This function executes [cycle] intrinsic parameter updates for the geocenter signal(RJMCMC or MCMC)
        if((data->glitchFlag || data->signalFlag) && !data->fixIntrinsicFlag) EvolveIntrinsicParameters(data, prior, model, chain, tf, chain->seed, ic);

        //This function executes [cycle] extrinsic parameter updates, common to all geocenter wavelets.
        if((model[chain->index[ic]]->signal[0]->size>0 || data->cbcFlag) && !data->fixExtrinsicFlag) EvolveExtrinsicParameters(data, prior, model, chain, bayescbc, chain->seed, ic);

        //update PSD with BayesLine
        if(data->bayesLineFlag) EvolveBayesLineParameters(data, model, bayesline, chain, prior, ic);

        // Evolve BayesCBC
        if(data->cbcFlag) EvolveBayesCBCParameters(data, model, bayescbc, chain, ic);
    }


    //After so many iterations recompute the residuals and likelihood (prevent accumulation of roundoff error)
    recompute_residual(data, model, chain);
    if(burnFlag==0 && data->signalFlag && !data->fixIntrinsicFlag)
    {

      //Recalculate TFQ with new CBC subtracted data every so often in early start
      if (data->cbcFlag && chain->mc<50000 && chain->mc%5000)
      {
        for(ifo=0; ifo<NI; ifo++)
        {
          TFprop_setup(data, model[chain->index[0]], prior->range, tf, ifo);
          if(data->gnuplotFlag && data->verboseFlag) TFprop_plot(prior->range, tf, (prior->range[0][1]-prior->range[0][0])/(double)(tf->nt), ifo);
        }
      }

      TFprop_signal(data, prior->range, tf, model[chain->index[0]]->projection);
    }


    //Parallel tempering
    if(NC>1 && chain->mc>1)
    {
      PTMCMC(model, chain, chain->seed, NC); /*PTMCMC moved inside of EvolveParameters*/

      if(data->verboseFlag)
      {
        for(ic=0; ic<NC; ic++) fprintf(tfile,"%g ",chain->temperature[ic]);
        for(ic=0; ic<NC; ic++) fprintf(tfile,"%g ",chain->A[ic]);
        for(ic=0; ic<NC; ic++) fprintf(tfile,"%g ",(double)chain->ptacc[ic]/(double)chain->ptprop[ic]);
        fprintf(tfile,"\n");
        fflush(tfile);
      }

      //adapt PTMCMC spacing and reset various counters
      if(chain->mc<M/2 && data->adaptTemperatureFlag)
      {
          adapt_temperature_ladder(chain, NC);
          if(chain->mc%100000==0)
          {
              for(ic=0; ic<NC; ic++)
              {
                  chain->ptprop[ic] = 1;
                  chain->ptacc[ic]  = 1;
              }
          }
      }
    }//end NC>1 condition for parallel tempering


    // take an occasional look at the whitened reconstructed waveforms in the time domain (print ~100 total)
    if(chain->mc>=frame*frameInterval && data->gnuplotFlag)
    {
      write_gnuplot_psd_animation(data, modelname, frame);

      // Print full time domain waveforms when running with cbc model because the wavelets could
      // be confined to their own smaller timewindow (e.g. for post-merger signal runs)
      if(data->cbcFlag) 
      {
        gnutmin = 0;
        gnutmax = data->Tobs;
      } else
      {
        gnutmin = prior->range[0][0];
        gnutmax = prior->range[0][1];    
      }

      //print waveforms to file for diagnostics
      for(ifo=0; ifo<NI; ifo++)
      {
        sprintf(filename,"waveforms/%s_data_%s_%d.dat", modelname, data->ifos[ifo], frame);
        print_time_domain_waveforms(filename, data->s[ifo], N, model[chain->index[0]]->Snf[ifo], data->Tobs, data->imin, data->imax, gnutmin, gnutmax);


        if(data->bayesLineFlag)
        {
          for(i=0; i<N; i++) data->r[ifo][i] = data->s[ifo][i] - model[chain->index[0]]->glitch[ifo]->templates[i];

          sprintf(filename,"waveforms/%s_frequency_residual_%s_%d.dat",modelname,data->ifos[ifo],frame);
          print_frequency_domain_waveforms(filename, data->r[ifo], N, model[chain->index[0]]->Snf[ifo], data->Tobs, data->imin, data->imax);

          sprintf(filename,"waveforms/%s_psd_%s_%d.dat", modelname, data->ifos[ifo], frame);
          print_bayesline_spectrum(filename, bayesline[chain->index[0]][ifo]->freq, bayesline[chain->index[0]][ifo]->spow, bayesline[chain->index[0]][ifo]->Sbase, bayesline[chain->index[0]][ifo]->Sline, bayesline[chain->index[0]][ifo]->data->ncut);
        }

        if(data->glitchFlag)
        {
          sprintf(filename,"waveforms/%s_glitch_%s_%d.dat", modelname, data->ifos[ifo], frame);
          print_time_domain_waveforms(filename, model[chain->index[0]]->glitch[ifo]->templates, N, model[chain->index[0]]->Snf[ifo], data->Tobs, data->imin, data->imax, gnutmin, gnutmax);

          sprintf(filename,"waveforms/%s_colored_glitch_%s_%d.dat", modelname, data->ifos[ifo], frame);
          print_colored_time_domain_waveforms(filename, model[chain->index[0]]->glitch[ifo]->templates, N,data->Tobs, data->imin, data->imax, gnutmin, gnutmax);
        }

        if(data->signalFlag)
        {
          sprintf(filename,"waveforms/%s_wave_%s_%d.dat", modelname, data->ifos[ifo], frame);
          print_time_domain_waveforms(filename, model[chain->index[0]]->response[ifo], N, model[chain->index[0]]->Snf[ifo], data->Tobs, data->imin, data->imax, gnutmin, gnutmax);
          
          sprintf(filename,"waveforms/%s_colored_wave_%s_%d.dat", modelname, data->ifos[ifo], frame);
          print_colored_time_domain_waveforms(filename, model[chain->index[0]]->response[ifo], N, data->Tobs, data->imin, data->imax, gnutmin, gnutmax);
        }

        if(data->cbcFlag)
        {
          sprintf(filename,"waveforms/%s_cbc_%s_%d.dat", modelname, data->ifos[ifo], frame);
          print_time_domain_waveforms(filename, model[chain->index[0]]->cbctemplate[ifo], N, model[chain->index[0]]->Snf[ifo], data->Tobs, data->imin, data->imax, gnutmin, gnutmax);
          
          sprintf(filename,"waveforms/%s_colored_cbc_%s_%d.dat", modelname, data->ifos[ifo], frame);
          print_colored_time_domain_waveforms(filename, model[chain->index[0]]->cbctemplate[ifo], N, data->Tobs, data->imin, data->imax, gnutmin, gnutmax);
        }
      }
      frame++;
    }


    //Update MAP model
    if(chain->mc > M/2)
    {
      logPost = model[chain->index[0]]->logL;
      if(data->glitchFlag) for(ifo=0; ifo<NI; ifo++) logPost += model[chain->index[0]]->glitch[ifo]->logp;
      if(data->signalFlag)                           logPost += model[chain->index[0]]->signal[0]->logp;
      // TODO: Update LogP with bayesCBC  
      if(logPost > logPostMap)
      {
        logPostMap = logPost;
        copy_psd_model(model[chain->index[0]], model_MAP, N, NI);
        copy_ext_model(model[chain->index[0]], model_MAP, N, NI);
        copy_int_model(model[chain->index[0]], model_MAP, N, NI,-1);
        for(ifo=0; ifo<NI; ifo++) copy_int_model(model[chain->index[0]], model_MAP, N, NI,ifo);
        copy_cbc_model(model[chain->index[0]], model_MAP, N, NI, data->NX);
//        LaplaceApproximation(data, model_MAP, chain, prior, &logZ);
      }
    }

    // Compute Evidence/Bayes Factor using last 1/4 of samples
    if(chain->mc >= M/4)
    {
      for(ic=0; ic<NC; ic++) chain->logLchain[ic][chain->zcount] = model[chain->index[ic]]->logL + model[chain->index[ic]]->logLnorm - data->logLc;
      chain->zcount++;

//      if(chain->mc%(M/1)==0 && !data->constantLogLFlag)TrapezoidIntegration(chain, chain->zcount, modelname, &logZ, &varZ);
    }

    chain->mc+=chain->cycle;

    time_t rawtime;
    struct tm * timeinfo;

    time ( &rawtime );
    timeinfo = localtime ( &rawtime );

    // Save progress
    if(__bayeswave_saveStateFlag)
    {
      fprintf(stdout,"Interrupt/Timer message received\n");
      fprintf(stdout,"Saving state to checkpoint directory\n");//,resumefilename);
      fprintf (stdout, "Current local time and date: %s\n", asctime (timeinfo) );
      save_sampler(data, chain, model, bayesline, bayescbc, modelname);
      __bayeswave_saveStateFlag = 0;
      fflush(stdout);
    }
    // Exit gracefully if interrupted
    if(__bayeswave_exitFlag)
    {
      int checkpoint_exit_code = 77;
      fprintf(stdout,"Exiting to system with error code %d\n", checkpoint_exit_code);
      fprintf(stdout,"\n");
      fflush(stdout);
      exit(checkpoint_exit_code);
    }

  }


  end = clock();
  cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
  fprintf(stdout, "MCMC for %i iterations took %e s\n", chain->mc, cpu_time_used);


  /******************************************************************************/
  /*                                                                            */
  /*  Here we are experimenting with different ways to                          */
  /*  estimate the mean and variance of each chain                              */
  /*                                                                            */
  /******************************************************************************/
  if(!data->constantLogLFlag)
  {

    /******************************************************************************/
    /*                                                                            */
    /*  Laplace approximation to evidence                                         */
    /*                                                                            */
    /******************************************************************************/
//    LaplaceApproximation(data, model_MAP, chain, prior, &logZ);
//
//    printf("Laplace Approximation logZ = %g\n",logZ);

    /******************************************************************************/
    /*                                                                            */
    /*  Thermodynamic Integration via Trapezoid Rule                              */
    /*                                                                            */
    /******************************************************************************/
    TrapezoidIntegration(chain, chain->nPoints, modelname, &logZ, &varZ);

    printf("Trapezoid Rule logZ = %g +/- %g\n",logZ,sqrt(varZ));

    /******************************************************************************/
    /*                                                                            */
    /*  Monte Carlo integration of <logL> v beta                                  */
    /*  including chain-to-chain correlations and monotonic prior                 */
    /*                                                                            */
    /******************************************************************************/
    if(data->splineEvidenceFlag && !data->cleanFlag)
    {
      if (!(data->noiseFlag && !data->bayesLineFlag)){
        ThermodynamicIntegration(chain->temperature, chain->logLchain, chain->nPoints, chain->NC, modelname, &logZ, &varZ);

        printf("Thermodynamic Integration logZ = %g +/- %g\n",logZ,sqrt(varZ));
      }
    }
  }

  /******************************************************************************/
  /*                                                                            */
  /*  Return logZ, and exit cleanly so we can test other models                 */
  /*                                                                            */
  /******************************************************************************/

  free_model(model_MAP, data, prior);
  free(model_MAP);

  fclose(chain->runFile);

  for(ic=0; ic<NC; ic++)
  {
    if(ic==0 || data->verboseFlag)
    {
      if(data->glitchFlag || data->signalFlag)
      {
        fclose(chain->intChainFile[ic]);
        if(data->signalFlag) for(n=0; n<data->Npol; n++)
          fclose(chain->intWaveChainFile[n][ic]);
        if(data->glitchFlag) for(ifo=0; ifo<NI; ifo++)
          fclose(chain->intGlitchChainFile[ifo][ic]);
      }

      if(data->bayesLineFlag)
      {
        for(ifo=0; ifo<NI; ifo++)
        {
          fclose(chain->splineChainFile[ic][ifo]);
          fclose(chain->lineChainFile[ic][ifo]);

        }
      }

    }
  }

  if(data->signalFlag || data->glitchFlag)
  {
    free_TF_proposal(data, tf);
  }
  free(tf);

  *logEvidence    = logZ;
  *varLogEvidence = varZ;

  if(data->verboseFlag)fclose(tfile);
  fprintf(stdout,"\n");

}

void PTMCMC(struct Model **model, struct Chain *chain, gsl_rng *seed, int NC)
{
  /*
   NC is passed to PTMCMC redundendtly because the main code
   can reduce the number of chains based on the temperature
   of the hottest chain
   */
  int a, b;
  int olda, oldb;

  double heat1, heat2;
  double logL1, logL2;
  double dlogL;
  double H;
  double alpha;
  double beta;

  //b = (int)(uniform_draw(seed)*((double)(chain->NC-1)));
  for(b=NC-1; b>0; b--)
  {
    a = b - 1;
    chain->A[a]=0;

    olda = chain->index[a];
    oldb = chain->index[b];

    heat1 = chain->temperature[a];
    heat2 = chain->temperature[b];

    logL1 = model[olda]->logL + model[olda]->logLnorm;
    logL2 = model[oldb]->logL + model[oldb]->logLnorm;

    //Hot chains jump more rarely
    if(uniform_draw(seed)<1.0)///heat1)
    {
      dlogL = logL2 - logL1;
      H  = (heat2 - heat1)/(heat2*heat1);

      alpha = exp(dlogL*H);
      beta  = uniform_draw(seed);

      chain->ptprop[a]++;

      if(alpha >= beta)
      {
        chain->ptacc[a]++;
        chain->index[a] = oldb;
        chain->index[b] = olda;
        chain->A[a]=1;
        // printf("\nDEBUG: In PTMCMC() Swapping %i and %i (real index a: %i).\n\n", olda, oldb, a);
      }
    }
  }
}

void EvolveBayesLineParameters(struct Data *data, struct Model **model, struct BayesLineParams ***bayesline, struct Chain *chain, struct Prior *prior, int ic)
{
  int ifo,i;

  int N  = data->N;
  int NI = data->NI;

  struct Wavelet *wave;

  //pointers to structures
  struct Model *model_x = model[chain->index[ic]];
  struct BayesLineParams **bl_x = bayesline[chain->index[ic]];

  int priorFlag = 1;

  //update PSD for each interferometer
  for(ifo=0; ifo<NI; ifo++)
  {
    //copy over current multi-template & current residual
    for(i=0; i<N; i++)
    {
      data->r[ifo][i] = data->s[ifo][i];
      if(data->signalFlag) data->r[ifo][i] -= model_x->response[ifo][i];
      if(data->glitchFlag) data->r[ifo][i] -= model_x->glitch[ifo]->templates[i];
      if(data->cbcFlag) data->r[ifo][i] -= model_x->cbctemplate[ifo][i];
    }

    //re-run Markovian, full spectrum, full model part of BayesLine
    BayesLineRJMCMC(bl_x[ifo], data->r[ifo], model_x->Snf[ifo], model_x->invSnf[ifo], model_x->SnS[ifo], N, chain->cycle, chain->beta, priorFlag);
  }

  Shf_Geocenter_full(data, model_x->projection, model_x->Snf, model_x->SnGeo, model_x->extParams);

  //recompute likelihoods & priors of current chain
  model_x->logLnorm = 0.0;
  model_x->logL = 0.0;
  data->logLc = 0.0;

  for(ifo=0; ifo<NI; ifo++)
  {
    for(i=0; i< N; i++)
    {
      data->r[ifo][i] = data->s[ifo][i];
      if(data->signalFlag) data->r[ifo][i] -= model_x->response[ifo][i];
      if(data->glitchFlag) data->r[ifo][i] -= model_x->glitch[ifo]->templates[i];
      if(data->cbcFlag) data->r[ifo][i] -= model_x->cbctemplate[ifo][i];
    }

    model_x->detLogL[ifo] = loglike(data->imin, data->imax, data->r[ifo], model_x->invSnf[ifo]);
    if(!data->constantLogLFlag)
    {
      model_x->logL += model_x->detLogL[ifo];
      for(i=0; i<N/2; i++)
      {
        model_x->logLnorm -= log(model_x->Snf[ifo][i]);
      }
    }

    wave = model_x->glitch[ifo];
    wave->logp = 0.0;
    for(i=1; i<=wave->size; i++)
    {
      wave->logp += (data->glitch_amplitude_prior(wave->intParams[wave->index[i]],model_x->Snf[ifo], data->Tobs, prior->gSNRpeak));
    }
  }

  wave = model_x->signal[0];
  wave->logp = 0.0;
  for(i=1; i<=wave->size; i++)
  {
    wave->logp += (data->signal_amplitude_prior(wave->intParams[wave->index[i]],model_x->SnGeo, data->Tobs, prior->sSNRpeak));
  }

}

/**************************************************************/
/* Helper functions to make EvolveIntrinsicParameters cleaner */
/**************************************************************/

static void remove_wavelet(struct Wavelet *wave_x, struct Wavelet *wave_y, int kill, int dmax, int *ii)
{
  int *larrayx = wave_x->index;
  int *larrayy = wave_y->index;
  
  double **paramsx = wave_x->intParams;
  double **paramsy = wave_y->intParams;
  
  int j,jj,k,kk;
  k = 0;

  for(j=1; j<= wave_x->size; j++)
  {
    if(j != kill)
    {
      k++;
      larrayy[k] = larrayx[j];
      jj = larrayx[j];
      for(kk=0; kk < wave_x->dimension; kk++) paramsy[jj][kk] = paramsx[jj][kk];
    }
    if(j == kill)
    {
      *ii = larrayx[j];  // take note of who's demise is being proposed
    }
  }
  
  //reshift the indexing array for occupied wavelets
  if(wave_y->size == 0) for(j=1; j<=dmax; j++) larrayy[j] = j;
}

static void add_wavelet(struct Wavelet *wave_x, struct Wavelet *wave_y, int *ii)
{
  int j,k;
  int *larrayx = wave_x->index;
  int *larrayy = wave_y->index;
  
  // find a label that isn't in use
  int index = 0;
  do
  {
    index++;
    k = 0;
    for(j=1; j<= wave_x->size; j++)
    {
      if(index==larrayx[j]) k = 1;
    }
  } while(k == 1);
  
  //reshift the indexing array for occupied wavelets
  larrayy[wave_y->size] = index;
  *ii = index;
}

/**************************************************************/

void EvolveIntrinsicParameters(struct Data *data, struct Prior *prior, struct Model **model, struct Chain *chain, struct TimeFrequencyMap *tf, gsl_rng *seed, int ic)
{

  int mc;
  int i,j,k,n;
  int ii=0;
  int ifo;
  int rj;
  int typ;
  int det=0;
  int test;
  int acc;
  double alpha;
  double q,logqx,logqy;
  double logqxAp, logqyAp, logqxtfQ, logqytfQ;//proposal density for the Fisher proposal, need to split it for the relaxed polarization case
  double logH;
  
  //indicies for looping over the number of states being update (1 IFO for glitch model, Npol for signal model)
  int nmin,nmax;

  int fisherFlag;
  int phaseFlag;
  int clusterFlag;
  int uniformFlag;
  int densityFlag;
  
  int tfqFlag;

  //ratio of TF proposals to proximity proposals
  double TFtoProx = data->TFtoProx;
  double ClusterRate = 0.9;
  double FisherRate = 0.8;

    //optimize ratio for sampling prior
  if(data->constantLogLFlag || ic > 10)
  {
      FisherRate = 0.2;
      ClusterRate = 0.2;
  }

  //Unpack structures and use convenient (and familiar) names

  /* DATA */
  int N  = data->N;
  int NI = data->NI;

  /* PRIOR */
  int dmax = 1;

  double **range = prior->range;

  /* MODEL */
  struct Model *model_x = model[chain->index[ic]];

  model_x->size=model_x->signal[0]->size;
  for(ifo=0; ifo<NI; ifo++) model_x->size+=model_x->glitch[ifo]->size;

  struct Wavelet **wave_x;
  int *larrayx;
  double **paramsx;

  /* CHAIN */
  int M = chain->cycle_wavelet;//*(model_x->size+1);

  /* PROPOSED MODEL */
  struct Model *model_y = malloc(sizeof(struct Model));
  initialize_model(model_y, data, prior, model_x->Snf, seed);
  copy_ext_model(model_x, model_y, N, NI);
  copy_int_model(model_x, model_y, N, NI,-1);
  copy_psd_model(model_x, model_y, N, NI);

  model_y->size=model_x->size;

  struct Wavelet **wave_y;
  int    *larrayy;
  double **paramsy;

  if(data->signalFlag) Shf_Geocenter_full(data, model_x->projection, model_x->Snf, model_x->SnGeo, model_x->extParams);

  acc=1;
  if(data->signalFlag) copy_int_model(model_x, model_y, N, NI,-1);
  if(data->glitchFlag) for(ifo=0; ifo<NI; ifo++) copy_int_model(model_x, model_y, N, NI, ifo);
  if(data->cbcFlag) copy_cbc_model(model_x, model_y, N, NI, data->NX);
  
  for(mc=1; mc<=M; mc++)
  {
    //Flags for which proposal is being used
    uniformFlag=0;
    clusterFlag=0;
    densityFlag=0;
    fisherFlag=0;
    phaseFlag=0;

    //acc gets set to 1 if model_y gets accepted (no need to copy at next iteration)
    if(acc==0 && mc>1) copy_int_model(model_x, model_y, N, data->NI, det);

    //reset acceptence counter
    acc=0;

    //decide if we're doing a noise or signal update
    if(!data->signalFlag)
      alpha = 1.0;
    else if(data->signalFlag && !data->glitchFlag )
      alpha = 0.0;
    else
      alpha = uniform_draw(seed);

    //update geocenter model
    if(alpha<chain->modelRate)
    {
      det = -1;

      dmax = prior->smax;

      wave_x = model_x->signal;
      wave_y = model_y->signal;
      
      nmin = 0;
      nmax = model_x->Npol;

    }
    //update glitch model in one detector
    else
    {
      det = (int)floor(uniform_draw(seed)*(float)NI);

      dmax = prior->gmax[det];

      wave_x = model_x->glitch;
      wave_y = model_y->glitch;
    
      nmin = det;
      nmax = det+1;

    }

    /*
     assign helper pointers to
       signal model: h+ (nmin=0)
       glitch model: IFO (nmin=det)
     */
    larrayx = wave_x[nmin]->index;
    larrayy = wave_y[nmin]->index;

    paramsx = wave_x[nmin]->intParams;
    paramsy = wave_y[nmin]->intParams;

    //set prior and proposal terms to 0 (updated with += later on logp)
    for(n=nmin; n<nmax; n++)
    {
      wave_x[n]->logp = 0.0;
      wave_y[n]->logp = 0.0;
    }
    logqx        = 0.0;
    logqy        = 0.0;

    rj   = 0;
    test = 0;

    /*******************************************************/
    /*                                                     */
    /*              TRANS DIMENSION RJMCMC MOVE            */
    /*                                                     */
    /*******************************************************/
    if(uniform_draw(seed) < chain->rjmcmcRate)
    {
      rj = 1;

      if(uniform_draw(seed) < 0.5)  // try and add a new wavelet
      {
        //loop over Npol for signal model, single IFO (nmax=nmin+1) for glitch model
        for(n=nmin; n<nmax; n++)
        {
          wave_y[n]->size = wave_x[n]->size+1;
          if(wave_y[n]->size > prior->smax) test = 1;
        }
        model_y->size = model_x->size+1;
        
        typ = 2;
      }
      else // try and remove wavelet
      {
        //loop over Npol for signal model, single IFO (nmax=nmin+1) for glitch model
        for(n=nmin; n<nmax; n++)
        {
          wave_y[n]->size = wave_x[n]->size-1;
          if(wave_y[n]->size < 0) test = 1;
        }
        model_y->size = model_x->size-1;
        
        //extra check on minimum number of wavelets for model (signal must have 1)
        if(det==-1 && model_y->size < prior->smin) test = 1;

        typ = 3;
      }

      //check the total model dimension
      /*
      int dimcheck=0;
      if(data->signalFlag)  dimcheck += model_y->signal[0]->size;
      if(data->glitchFlag)
      {
        for(i=0; i<NI; i++) dimcheck += model_y->glitch[i]->size;
      }
      if(dimcheck>prior->smax) test = 1;
      */
      if(data->signalFlag)
      {
        if(model_y->signal[0]->size>prior->smax) test = 1;
      }
      if(data->glitchFlag)
      {
        for(i=0; i<NI; i++) if(model_y->glitch[i]->size>prior->smax) test = 1;
      }


      if(test == 0)
      {
        /*********************************

         Death Move

         *********************************/
        if(model_y->size < model_x->size)
        {
          i=(int)(uniform_draw(seed)*(double)(wave_x[nmin]->size))+1; // pick a term to try and kill

          //loop over Npol for signal model, single IFO (nmax=nmin+1) for glitch model
          for(n=nmin; n<nmax; n++) remove_wavelet(wave_x[n], wave_y[n], i, dmax, &ii);
          
          // TF density for proposal
          if(data->clusterProposalFlag)
          {
            //loop over Npol for signal model, single IFO (nmax=nmin+1) for glitch model
            for(n=nmin; n<nmax; n++)
            {
              paramsx = wave_x[n]->intParams;
              paramsy = wave_y[n]->intParams;
              larrayx = wave_x[n]->index;
              larrayy = wave_y[n]->index;

              q = 0.0;
              if(det==-1)
              {
                if(n==0)
                {
                  q += ClusterRate*TFtoProx*TFprop_density(paramsx[ii], range, tf, data->NI);
                }
              }
              else q += ClusterRate*TFtoProx*TFprop_density(paramsx[ii], range, tf, det);

              if(n==0 || det > -1)
              {
                q += ClusterRate*(1.-TFtoProx)*wavelet_proximity_density(paramsx[ii][1], paramsx[ii][0], paramsy, larrayy, wave_y[n]->size, prior);
                q += (1.0-ClusterRate)/prior->TFV;
                logqx += log(q);
              }
              logqy +=  0.0;
            }
          }
          // Uniform in TF proposal
          else
          {
            //loop over Npol for signal model, single IFO (nmax=nmin+1) for glitch model
            for(n=nmin; n<nmax; n++)
            {
              if(n==0 || det >-1) logqx += -prior->logTFV;
              logqy += 0.0;
            }
          }

          if(data->amplitudeProposalFlag)
          {
            //loop over Npol for signal model, single IFO (nmax=nmin+1) for glitch model
            for(n=nmin; n<nmax; n++)
            {
              paramsx = wave_x[n]->intParams;

              if(det==-1) logqx += ( data->signal_amplitude_prior(paramsx[ii], model_x->SnGeo, data->Tobs, prior->sSNRpeak) );
              else        logqx += ( data->glitch_amplitude_prior(paramsx[ii], model_x->Snf[det], data->Tobs, prior->gSNRpeak) );
            }
          }
        }//end death move

         /*********************************/
         /*         Birth Move           */
         /*********************************/
        if(model_y->size > model_x->size)
        {
          //loop over Npol for signal model, single IFO (nmax=nmin+1) for glitch model
          for(n=nmin; n<nmax; n++) add_wavelet(wave_x[n], wave_y[n], &ii);
          
          //Initialize with draw from prior
          for(n=nmin; n<nmax; n++)
          {
            paramsy = wave_y[n]->intParams;
            draw_wavelet_params(paramsy[ii], range, seed, wave_y[n]->dimension);
          }
          if(det==-1 && model_y->Npol > 1) constrain_hplus_hcross(wave_y, ii);

          /* TF density for proposal */
          if(data->clusterProposalFlag )
          {
            if(uniform_draw(seed)<ClusterRate)
            {
              // Draw new time and frequency from TFV density proposal
              //loop over Npol for signal model, single IFO (nmax=nmin+1) for glitch model
              for(n=nmin; n<nmax; n++)
              {
                if(det==-1)draw_time_frequency(data->NI, ii, wave_x[n], wave_y[n], range, seed, TFtoProx, tf, &k);
                else       draw_time_frequency(det, ii, wave_x[n], wave_y[n], range, seed, TFtoProx, tf, &k);

              }
              if(det==-1 && model_y->Npol > 1) constrain_hplus_hcross(wave_y, ii);

              if(ic==0)
              {
                if(k==0)
                {
                  densityFlag=1;
                  chain->dcount++;

                }
                if(k==1)
                {
                  clusterFlag=1;
                  chain->ccount++;
                }
              }
            }//end clusterRate if
            else
            {
              if(ic==0)
              {
                uniformFlag=1;
                chain->ucount++;
              }
            }

            q = 0.0;
            //loop over Npol for signal model, single IFO (nmax=nmin+1) for glitch model
            for(n=nmin; n<nmax; n++)
            {
              paramsx = wave_x[n]->intParams;
              paramsy = wave_y[n]->intParams;
              larrayx = wave_x[n]->index;
              larrayy = wave_y[n]->index;

              //signal model
              if(det==-1)
              {
                //only evaluate TFQ proposal for h+ polarization (TFQ are not free parameters for hx)
                if(n==0)
                {
                  q += ClusterRate*TFtoProx*TFprop_density(paramsy[ii], range, tf, data->NI);
                  q += ClusterRate*(1.-TFtoProx)*wavelet_proximity_density(paramsy[ii][1], paramsy[ii][0], paramsx, larrayx, wave_x[n]->size, prior);
                  q += (1.0-ClusterRate)/prior->TFV;
                  
                  logqy += log(q);
                  logqx +=  0.0;
                }
              }
              //glitch model
              else
              {
                q += ClusterRate*TFtoProx*TFprop_density(paramsy[ii], range, tf, det);
                
                q += ClusterRate*(1.-TFtoProx)*wavelet_proximity_density(paramsy[ii][1], paramsy[ii][0], paramsx, larrayx, wave_x[n]->size, prior);
                q += (1.0-ClusterRate)/prior->TFV;
                
                logqy += log(q);
                logqx +=  0.0;
              }
            }
          }//end TF density proposal
          
          /* Uniform in TF proposal */
          else
          {
            //alread drew from uniform distribution to initialize wavelet
            if(ic==0)
            {
              uniformFlag=1;
              chain->ucount++;
            }

            //loop over Npol for signal model, single IFO (nmax=nmin+1) for glitch model
            for(n=nmin; n<nmax; n++)
            {
              if(n==0 || det>-1) logqy += -prior->logTFV;
              logqx += 0.0;
            }
          }//end uniform TF density proposal

          /* Draw from SNR-based amplitude distribution */
          if(data->amplitudeProposalFlag)
          {
            
            for(n=nmin; n<nmax; n++)
            {
              if(det==-1)
              {
                
                paramsx = wave_x[n]->intParams;
                paramsy = wave_y[n]->intParams;
                larrayx = wave_x[n]->index;
                larrayy = wave_y[n]->index;
                
                data->signal_amplitude_proposal(paramsy[ii], model_x->SnGeo, seed, data->Tobs, prior->range, prior->sSNRpeak);
                logqy += ( data->signal_amplitude_prior(paramsy[ii],  model_x->SnGeo, data->Tobs, prior->sSNRpeak) );
              }
              else
              {
                data->glitch_amplitude_proposal(paramsy[ii], model_x->Snf[det], seed, data->Tobs, prior->range, prior->gSNRpeak);
                logqy += (data->glitch_amplitude_prior(paramsy[ii],  model_x->Snf[det], data->Tobs, prior->gSNRpeak) );
              }
            }
          }
          else  for(n=nmin; n<nmax; n++) logqy += -log(prior->range[3][1]-prior->range[3][0]);

          //check priors
          for(n=nmin; n<nmax; n++)
          {
            paramsy = wave_y[n]->intParams;
            test += checkrange(paramsy[ii],prior->range, wave_y[n]->dimension);
          }

        }//end birth move
      }//end test condition
    }//end trans-dimensional update

    /*******************************************************/
    /*                                                     */
    /*               FIXED DIMENSION MCMC MOVE             */
    /*                                                     */
    /*******************************************************/
    else
    {
      typ = 1;


      //update stochastic background parameters
      if(data->stochasticFlag) stochastic_background_proposal(model_x->background, model_y->background, seed, &test);


      //Now update wavelet models if they are enabled
      if( (data->signalFlag || data->glitchFlag) && test==0 && wave_x[nmin]->size > 0 )
      {
        i  = (int)(uniform_draw(seed)*(double)(wave_x[nmin]->size))+1; // pick a term to update
        ii = wave_x[nmin]->index[i];

        // replace existing wavelet (birth/death move)
        //TODO: Birth/Death move is currently disabled
        if(uniform_draw(seed) > 1.0)//chain->intPropRate/chain->temperature[ic])
        {

          // Draw all wavelet parameters from prior
          draw_wavelet_params(paramsy[ii], range, seed, wave_y[nmin]->dimension);

          // TF density for proposal
          if(data->clusterProposalFlag)
          {
            if(det==-1)
            {
              /* Draw new time and frequency from TFV density + proximity proposal */
              draw_time_frequency(data->NI, ii, wave_x[nmin], wave_y[nmin], range, seed, TFtoProx, tf, &k);
              logqx += log( TFtoProx*TFprop_density(paramsx[ii], range, tf, data->NI) + (1.-TFtoProx)*wavelet_proximity_density(paramsx[ii][1], paramsx[ii][0], paramsy, larrayy, wave_y[nmin]->size, prior));
              logqy += log( TFtoProx*TFprop_density(paramsy[ii], range, tf, data->NI) + (1.-TFtoProx)*wavelet_proximity_density(paramsy[ii][1], paramsy[ii][0], paramsx, larrayx, wave_x[nmin]->size, prior));
              
              /* copy h+ to additional polarization states */
              for(n=1; n<model_x->Npol; n++)
              {
                for(j=0; j<wave_y[nmin]->dimension; j++) model_y->signal[n]->intParams[ii][j] = model_y->signal[0]->intParams[ii][j];
              }
            }
            else
            {
              /* Draw new time and frequency from TFV density + proximity proposal */
              draw_time_frequency(det, ii, wave_x[nmin], wave_y[nmin], range, seed, TFtoProx, tf, &k);
              draw_time_frequency(data->NI, ii, wave_x[nmin], wave_y[nmin], range, seed, TFtoProx, tf, &k);
              logqx += log( TFtoProx*TFprop_density(paramsx[ii], range, tf, det) + (1.-TFtoProx)*wavelet_proximity_density(paramsx[ii][1], paramsx[ii][0], paramsy, larrayy, wave_y[nmin]->size, prior));
              logqy += log( TFtoProx*TFprop_density(paramsy[ii], range, tf, det) + (1.-TFtoProx)*wavelet_proximity_density(paramsy[ii][1], paramsy[ii][0], paramsx, larrayx, wave_x[nmin]->size, prior));
            }
          }

          // Uniform in TF proposal
          else
          {
            logqy += -prior->logTFV;
            logqx += -prior->logTFV;
          }

          if(data->amplitudeProposalFlag)
          {
            if(det==-1)
            {
              data->signal_amplitude_proposal(paramsy[ii], model_x->SnGeo, seed, data->Tobs, prior->range, prior->sSNRpeak);
              logqy += (data->signal_amplitude_prior(paramsy[ii], model_x->SnGeo, data->Tobs, prior->sSNRpeak));
              logqx += (data->signal_amplitude_prior(paramsx[ii], model_x->SnGeo, data->Tobs, prior->sSNRpeak));

            }
            else
            {
              data->glitch_amplitude_proposal(paramsy[ii], model_x->Snf[det], seed, data->Tobs, prior->range, prior->gSNRpeak);
              logqy += (data->glitch_amplitude_prior(paramsy[ii], model_x->Snf[det], data->Tobs, prior->gSNRpeak));
              logqx += (data->glitch_amplitude_prior(paramsx[ii], model_x->Snf[det], data->Tobs, prior->gSNRpeak));
            }

          }
        }

        // perturb existing wavelet & PSD model
        else
        {

          /*
           Custom mode hopper:
           */
          if(uniform_draw(seed)<0.1)
          {
            if(ic==0)
            {
              phaseFlag=1;
              chain->pcount++;
            }

            for(n=nmin; n<nmax; n++)
            {
              paramsx = wave_x[n]->intParams;
              paramsy = wave_y[n]->intParams;

              intrinsic_halfcycle_proposal(paramsx[ii],paramsy[ii],seed);
            }
            if(det==-1 && model_y->Npol > 1)
            {
              //give hx the same phase shift as h+
              wave_y[1]->intParams[ii][4] = wave_x[1]->intParams[ii][4] + (wave_y[0]->intParams[ii][4]-wave_x[0]->intParams[ii][4]);

              while(wave_y[1]->intParams[ii][4] > LAL_TWOPI) wave_y[1]->intParams[ii][4] -=LAL_TWOPI;
              while(wave_y[1]->intParams[ii][4] < 0.0) wave_y[1]->intParams[ii][4] +=LAL_TWOPI;
              
              constrain_hplus_hcross(wave_y, ii);
            }

          }

          /*
           Fisher matrix jump:
           proposal distribution uses the Fisher at x to propose y,
           then computes the Fisher at y to compute the proposal
           densities to maintain detailed balance
           */
          else if(uniform_draw(seed)<FisherRate)
          {
            if(ic==0)
            {
              fisherFlag=1;
              chain->fcount++;
            }

            tfqFlag = 0;
            for(n=nmin; n<nmax; n++)
            {
              paramsx = wave_x[n]->intParams;
              paramsy = wave_y[n]->intParams;

              if(det==-1)
              {
                intrinsic_fisher_proposal(model_x, range, paramsx[ii], paramsy[ii], model_x->SnGeo, &logqxAp, &logqyAp, &logqxtfQ, &logqytfQ, seed, data->Tobs, &test, wave_y[n]->dimension, tfqFlag);

                // t, f, Q are shared between hplus and hcross, so they should be counted only once
                if (n==0) {
                  logqx += logqxAp;
                  logqx += logqxtfQ;
                  logqy += logqyAp;
                  logqy += logqytfQ;
                }
                else {
                  logqx += logqxAp;
                  logqy += logqyAp;
                }
              }
              
              else
              {
                intrinsic_fisher_proposal(model_x, range, paramsx[ii], paramsy[ii], model_x->Snf[det], &logqxAp, &logqyAp, &logqxtfQ, &logqytfQ, seed, data->Tobs, &test, wave_y[n]->dimension, tfqFlag);
                logqx += logqxAp;
                logqx += logqxtfQ;
                logqy += logqyAp;
                logqy += logqytfQ;
              }
            }
            if(det==-1 && model_y->Npol > 1) constrain_hplus_hcross(wave_y, ii);

          }
          /*
           Draw from prior:
          */
          else
          {
            alpha=uniform_draw(seed);
            for(n=nmin; n<nmax; n++)
            {
              paramsx = wave_x[n]->intParams;
              paramsy = wave_y[n]->intParams;

              draw_wavelet_params(paramsy[ii], range, seed, wave_y[n]->dimension);
              if(det==-1 && model_y->Npol > 1) constrain_hplus_hcross(wave_y, ii);
              
              if(alpha<0.5 && data->amplitudeProposalFlag)
              {
                if(det==-1)
                {
                  data->signal_amplitude_proposal(paramsy[ii], model_x->SnGeo, seed, data->Tobs, prior->range, prior->sSNRpeak);
                  logqy += ( data->signal_amplitude_prior(paramsy[ii],  model_x->SnGeo, data->Tobs, prior->sSNRpeak) );
                  logqx += ( data->signal_amplitude_prior(paramsx[ii],  model_x->SnGeo, data->Tobs, prior->sSNRpeak) );
                }
                else
                {
                  data->glitch_amplitude_proposal(paramsy[ii], model_x->Snf[det], seed, data->Tobs, prior->range, prior->gSNRpeak);
                  logqy += ( data->glitch_amplitude_prior(paramsy[ii],  model_x->Snf[n], data->Tobs, prior->gSNRpeak) );
                  logqx += ( data->glitch_amplitude_prior(paramsx[ii],  model_x->Snf[n], data->Tobs, prior->gSNRpeak) );
                }
              }
            }
          }
        }//end perturb step
        //check priors
        for(n=nmin; n<nmax; n++)
        {
          paramsy = wave_y[n]->intParams;
          test += checkrange(paramsy[ii],prior->range, wave_y[n]->dimension);
        }
      }//end model/dimension check
    }//end fixed-dimension update
    
    // If new parameters are within prior go ahead with Hastings ratio calculation
    if(test == 0)
    {
      /**************************************************/
      /*                   LIKELIHOOD                   */
      /**************************************************/
      if(ic<chain->NC/2 || data->constantLogLFlag)
      {
        model_y->logL = data->intrinsic_likelihood(typ, ii, det, model_x, model_y, prior, chain, data);
      }
      else
        model_y->logL = EvaluateMarkovianLogLikelihood(typ, ii, det, model_x, model_y, prior, chain, data);
      
      
      /**************************************************/
      /*                     PRIORS                     */
      /**************************************************/
      if( (data->signalFlag || data->glitchFlag) && model_y->logL > -1.0e60)
      {
        /* TFQ priors */
        for(n=nmin; n<nmax; n++)
        {
          paramsx = wave_x[n]->intParams;
          paramsy = wave_y[n]->intParams;
          larrayx = wave_x[n]->index;
          larrayy = wave_y[n]->index;
          
          // clustering prior
          if(data->clusterPriorFlag)
          {
            wave_y[n]->logp += wavelet_proximity_prior(wave_y[n], prior);
            wave_x[n]->logp += wavelet_proximity_prior(wave_x[n], prior);
          }
          
          // background prior for glitch model from timeslides
          else if(data->backgroundPriorFlag && data->glitchFlag)
          {
            
            //printf("frequencies of glitch:  %g %g\n",paramsx[larrayx[ii]][1],paramsy[larrayy[ii]][1]);
            for(i=1; i<=wave_x[n]->size; i++) wave_x[n]->logp += glitch_background_prior(prior, paramsx[larrayx[i]]);
            for(i=1; i<=wave_y[n]->size; i++) wave_y[n]->logp += glitch_background_prior(prior, paramsy[larrayy[i]]);
          }
          
          // uniform prior on time-frequency volume
          else
          {
            if(n==0 || det>-1)
            {
              wave_x[n]->logp += -(double)(wave_x[n]->size)*prior->logTFV;
              wave_y[n]->logp += -(double)(wave_y[n]->size)*prior->logTFV;
            }
          }
        }//end loop over n

        
        
        /* Amplitude priors */
        for(n=nmin; n<nmax; n++)
        {
          paramsx = wave_x[n]->intParams;
          paramsy = wave_y[n]->intParams;
          larrayx = wave_x[n]->index;
          larrayy = wave_y[n]->index;

          if(data->amplitudePriorFlag)
          {
            for(i=1; i<=wave_y[n]->size; i++)
            {
              if(det==-1)
                wave_y[n]->logp += (data->signal_amplitude_prior(paramsy[larrayy[i]], model_x->SnGeo, data->Tobs, prior->sSNRpeak));
              else
                wave_y[n]->logp += (data->glitch_amplitude_prior(paramsy[larrayy[i]],model_x->Snf[det], data->Tobs, prior->gSNRpeak));
            }
            for(i=1; i<=wave_x[n]->size; i++)
            {
              if(det==-1)
                wave_x[n]->logp += (data->signal_amplitude_prior(paramsx[larrayx[i]],model_x->SnGeo, data->Tobs, prior->sSNRpeak));
              else
                wave_x[n]->logp += (data->glitch_amplitude_prior(paramsx[larrayx[i]],model_x->Snf[det], data->Tobs, prior->gSNRpeak));
            }
          }
          else
          {
            wave_y[n]->logp  += -(double)(wave_y[n]->size)*log(prior->range[3][1]-prior->range[3][0]);
            wave_x[n]->logp  += -(double)(wave_x[n]->size)*log(prior->range[3][1]-prior->range[3][0]);
          }
        }//end loop over n
        
        
        
        /* Dimension priors */
        for(n=nmin; n<nmax; n++)
        {
          if(data->waveletPriorFlag)
          {
            if(n==0 || det>-1)
            {
              wave_x[n]->logp += prior->Nwavelet[wave_x[n]->size];
              wave_y[n]->logp += prior->Nwavelet[wave_y[n]->size];
            }
          }
        }

      }

      /**************************************************/
      /*                 HASTINGS RATIO                 */
      /**************************************************/
      logH = (model_y->logL - model_x->logL)*chain->beta;                  //likelihood ratio
      logH += logqx - logqy;                                               //proposal density
      for(n=nmin; n<nmax; n++) logH += wave_y[n]->logp - wave_x[n]->logp;  //prior ratio

      
      /*
       if(model_x->size != model_y->size)
       {
         printf("test=%d\n",test);
         printf("%i: %i->%i logLy=%g logLx=%g py=%g px=%g qy=%g qx=%g, dx=%i,dy=%i,num=%g, den=%g, logH=%g\n",typ,model_x->size,model_y->size,model_y->logL,model_x->logL,wave_y->logp,wave_x->logp,logqy,logqx,wave_x->size,wave_y->size,wave_y->logp+ logqx,- wave_x->logp - logqy, logH);
         printf("    %lg, %i\n",prior->Nwavelet[wave_x->size], wave_x->size);
         printf("    %lg, %i\n",prior->Nwavelet[wave_y->size], wave_y->size);
         printf("dif=%lg\n",prior->Nwavelet[wave_y->size]-prior->Nwavelet[wave_x->size]);
      }
       */ 

      //if(ic==0 && data->stochasticFlag)printf("logLy=%g logLx=%g Ay=%g Ax=%g detC=%g logH=%g\n",model_y->logL-data->logLc,model_x->logL-data->logLc,model_x->background->logamp,model_y->background->logamp, model_y->background->detCij[data->N/4],logH);

    }// end prior test condition
    else logH = -1.0e60; // Rejection sample at prior boundaries


    // Update proposal counters for acceptance ratios
    if(ic==0)
    {
      if(rj) chain->scount++;
      else
      {
        if(wave_x[nmin]->size>0)chain->mcount++;
      }
    }

    /*******************************************************/
    /*                                                     */
    /*      Metropolis-Hastings acceptance/rejection       */
    /*                                                     */
    /*******************************************************/

    // Update current position (model_x) depending on acceptance/rejection

    alpha = log(uniform_draw(seed));

    if(logH > alpha)
    {
      acc = 1;

      copy_int_model(model_y, model_x, N, data->NI, det);

      if(data->stochasticFlag) copy_background(model_y->background, model_x->background, data->NI, data->N/2);

      if(ic==0)
      {
        if(rj) chain->sacc++;
        else
        {
          if(wave_x[nmin]->size>0)chain->macc++;
        }

        if(fisherFlag) chain->facc++;
        if(phaseFlag)  chain->pacc++;
        if(clusterFlag)chain->cacc++;
        if(densityFlag)chain->dacc++;
        if(uniformFlag)chain->uacc++;
      }
      for(n=nmin; n<nmax; n++) wave_x[n]->logp = wave_y[n]->logp;
    }

  }//end of MCMC loop over mc

  free_model(model_y, data, prior);
  free(model_y);

}

void EvolveBayesCBCParameters(struct Data *data, struct Model **model, struct bayesCBC *bayescbc, struct Chain *chain, int ic)
{

  int ifo,i;
  int imin, imax;
  double x, y;
  double ciota, epsilon, scale;
  double *dtimes;
  char filename[MAXSTRINGSIZE];

  int M =  chain->cycle;  // Nr. of local MCMC iterations
  int N  = data->N;       // sampling rate * observation time
  int NI = data->NI;      // number interferometers
  double fmin = data->fmin;
  double fmax = data->fmax;
  int icbc = chain->index[ic];
  int ncycle = (int) chain->mc*M/chain->cycle;  // Number of CBC cycles completed (for keeping track of history)

  // Pointers to structures
  struct Model *model_x = model[chain->index[ic]]; 
  struct Network *projection = model_x->projection;

  imin = fmin*data->Tobs;
  imax = fmax*data->Tobs;
  bayescbc->who[ic] = chain->index[ic];

  // Setup residuals for BayesCBC (data substracted with ex/intrinsic 
  // model template and glitch template)
  for(ifo=0; ifo<NI; ifo++)
  {
    //copy over current multi-template & current residual
    for(i=0; i<N; i++)
    {
      data->r[ifo][i] = data->s[ifo][i];
      if(data->signalFlag) data->r[ifo][i] -= model_x->response[ifo][i];
      if(data->glitchFlag) data->r[ifo][i] -= model_x->glitch[ifo]->templates[i];

      bayescbc->D[ifo][i] = data->r[ifo][i];
    }
  }

  // Pass the updated model parameters to BayesCBC structure
  setup_bayescbc_model(bayescbc, data, chain, model_x, ic, icbc, 1);

  // Reset the heterodyne everytime the psd or data changes and occassionally in case solution wanders with cbcOnly
  // if (bayescbc->heterodyneFlag && ((data->signalFlag || data->glitchFlag || data->bayesLineFlag) || (ic == 0 && chain->mc%10000==0))) {
  if (bayescbc->heterodyneFlag){ // && chain->mc==0) { // chain->mc%10000==0)

    if (bayescbc->het->initFlag == 1) freehet(bayescbc->net, bayescbc->het);

    // Compute reference heterodyne for current chain params
    heterodyne(bayescbc->net, bayescbc->het, bayescbc->D, bayescbc->pallx[chain->index[ic]], bayescbc->freq, model_x->Snf, model_x->SnS, N, data->Tobs, bayescbc, ic);
  }

  // Do updates
  BayesCBC_MCMC(M, N, data->Tobs, data->trigtime, chain->seed, ncycle, bayescbc, ic, model_x->cbcamphase);

  // Update CBC intrinsic parameters in chain model
  for(i = 0; i < bayescbc->NX; i++)
  {
     model_x->cbcParams[i] = bayescbc->pallx[icbc][i];
  }

  // Get CBC waveforms projected on the detectors
  projectCBCWaveform(model_x->cbcamphase, N, NI, fmin, data->Tobs, model_x->extParams, model_x->cbctemplate, projection->dtimes, projection->Fplus, projection->Fcross);

  // Save first 10 waveforms temporarily for checking residuals + template
  // if (ic==0 && bayescbc->debug == 1 && chain->mc<=1000) print_projected_cbc_waveform(bayescbc->SN, data->Tobs, data->trigtime, model_x->cbctemplate, bayescbc->D, N, bayescbc->mxc[0], bayescbc);

  // Recompute likelihoods of current chain
  model_x->logLnorm = 0.0;
  model_x->logL = 0.0;
  data->logLc = 0.0;

  for(ifo=0; ifo<NI; ifo++)
  {

    // Recompute residuals to calculate likelihood
    for(i=0; i< N; i++)
    {
      data->r[ifo][i] = data->s[ifo][i];
      if(data->signalFlag) data->r[ifo][i] -= model_x->response[ifo][i];
      if(data->glitchFlag) data->r[ifo][i] -= model_x->glitch[ifo]->templates[i];
      data->r[ifo][i] -= model_x->cbctemplate[ifo][i];
    }

    model_x->detLogL[ifo] = loglike(data->imin, data->imax, data->r[ifo], model_x->invSnf[ifo]);

    if(!data->constantLogLFlag)
    {
      model_x->logL += model_x->detLogL[ifo];
      for(i=0; i<N/2; i++)
      {
        model_x->logLnorm -= log(model_x->Snf[ifo][i]);
      }
    }
  }

  return;
}

void EvolveExtrinsicParameters(struct Data *data, struct Prior *prior, struct Model **model, struct Chain *chain, struct bayesCBC *bayescbc, gsl_rng *seed, int ic)
{
  int i, j, ifo, mc, n;
  int dim;
  int test, iendN, ienddim;
  double logLx, logLy, logH;
  double alpha;
  double Amp;
  double **glitch;
  double *SnGeox;
  double *SnGeoy;
  double *paramsx, *paramsy;
  double *geoshifts;
  double **intParams;
  double logpy,logpx,logJ;
  double Apx, Apy;

  //Unpack structures and use convenient (and familiar) names
  int NI = data->NI;

  /* CHAIN */
  int M = 3*chain->cycle;

  /* MODEL */
  // extrinsic parameters
  struct Model *model_x = model[chain->index[ic]];
  struct Wavelet **smodel = model_x->signal;
  paramsx = model_x->extParams;
  SnGeox  = model_x->SnGeo;

  paramsy = double_vector(NE);
  SnGeoy  = double_vector(data->N/2);

  dim = smodel[0]->size;
  ienddim = dim+1;
  iendN = data->N/2;
  intParams = double_matrix(dim,smodel[0]->dimension-1);


  //Compute reference heterodyne for likelihood
  if (data->cbcFlag && bayescbc->heterodyneFlag){
    if (bayescbc->het->initFlag == 1) freehet(bayescbc->net, bayescbc->het);

    // Pass semi-intrinsic CBC params from BW model to BayesCBC (just in case..)
    for(i = 0; i < bayescbc->NX; i++)
    {
      bayescbc->pallx[chain->index[ic]][i] = model[chain->index[ic]]->cbcParams[i];
    }

    // Pass extrinsic parameters from BW model to BayesCBC (just in case..)
    for(i = bayescbc->NX; i < bayescbc->NP; i++)
    {
        bayescbc->pallx[chain->index[ic]][i] = model[chain->index[ic]]->extParams[i-bayescbc->NX];
    }

    // Only compute fixed part when signal+cbc, as the data changes every iteration
    if (data->signalFlag)
    {
      fixedheterodyne(bayescbc->net, bayescbc->het,  bayescbc->pallx[chain->index[ic]], bayescbc->freq, model_x->Snf, model_x->SnS, data->N, data->Tobs, bayescbc, chain->index[ic]);
    }
    else 
    {
      // Otherwise, compute the full heterodyne once at the start only

      //Form up residual
      for(ifo=0; ifo<NI; ifo++)
      {
        for(i=0; i<data->N; i++)
        {
          bayescbc->D[ifo][i] = data->s[ifo][i];
          if(data->glitchFlag) bayescbc->D[ifo][i] -= model_x->glitch[ifo]->templates[i];
        }
      }

      heterodyne(bayescbc->net, bayescbc->het, bayescbc->D, bayescbc->pallx[chain->index[ic]], bayescbc->freq, model_x->Snf, model_x->SnS, data->N, data->Tobs, bayescbc, chain->index[ic]);
    }
  }
  
  
  //Set extrinsic parameter fisher matrix (numerical derivatives of response to geocenter waveform)
  extrinsic_fisher_update(data, model_x, bayescbc);

  glitch = malloc(NI*sizeof(double*));
  for(ifo=0; ifo<NI; ifo++) glitch[ifo] = model_x->glitch[ifo]->templates;

  //Initialize parameter vectors & likelihood for extrinsic parameter MCMC
  for(i=0; i<NE; i++)  paramsy[i] = paramsx[i];

  //FIXME: Fix ugly psi=0 hack for unpolarized case
  //if(!data->polarizationFlag) paramsy[2] = paramsx[2] = 0.0;

  //TODO: Why can't I use a band-limited response for xtrinsic moves?
  //Related to computeProjectionCoeffs()?
  
  //Find minimum and maximum frequencies of signal model and only compute logL over that range
  /*
   double fmin = data->fmax;
  double fmax = data->fmin;
  double fi,ff;
  for(i=1; i<ienddim; i++)
  {
    model_x->wavelet_bandwidth(smodel->intParams[smodel->index[i]],&fi,&ff);
    if(fi<fmin) fmin=fi;
    if(ff>fmax) fmax=ff;

  }
    //Make sure frequency is in range
    if(fmin < data->fmin)
        fmin = data->fmin;
    if(fmax > data->fmax)
        fmax = data->fmax;
   */
    double fmin = data->fmin;
    double fmax = data->fmax;
  
  // Ensure we start with the antenna patterns for start parameters
  // computeProjectionCoeffs(data, model_x->projection, paramsx, fmin, fmax);


  // Initialize geoshifts (dt, dphi0, and scale) for CBC waveform
  geoshifts = double_vector(4);
  geoshifts[0] = 0.0; // shift dt0
  geoshifts[1] = model_x->extParams[4]; // shift dphi0
  geoshifts[2] = model_x->extParams[5]; // shift scale
  geoshifts[3] = model_x->projection->dtimes[0]; // Holds the accepted state dtimes0
  logLx = data->extrinsic_likelihood(model_x->projection, paramsx, model_x->invSnf, smodel, glitch, model_x->cbcamphase, data, fmin, fmax, geoshifts, bayescbc);
  // logLx_old = ExtrinsicLikelihoodOld(model_x->projection, paramsx, model_x->invSnf, smodel, glitch, model_x->cbcamphase, data, fmin, fmax, geoshifts, bayescbc);

  //Compute Fisher Matrix for current sky location
  struct FisherMatrix *fisher = model_x->fisher;

  double draw;

  logpx=0.0;
  if(data->amplitudePriorFlag )
  {
    //model_x->projection has be updated by data->extrinsic_likelihood, so it stores F+ and Fx for params_y.
    Shf_Geocenter_full(data, model_x->projection, model_x->Snf, model_x->SnGeo, model_x->extParams);
    if (data->signalFlag) 
    {
      for(n=0; n<model_x->Npol; n++)
      {
        for(i=1; i<smodel[n]->size+1; i++)
        {
           logpx += (data->signal_amplitude_prior(smodel[n]->intParams[smodel[n]->index[i]],SnGeox, data->Tobs, prior->sSNRpeak));
        }
      }
    }
  }
  //logpx += dim*log(paramsx[5]);

  int sky=0;
  int cnt=0;
  int acc=0;

  int skyu=0; 
  int skyo=0;
  int fish=0;

  int cskyu=0; 
  int cskyo=0;
  int cfish=0;

  int askyu=0; 
  int askyo=0;
  int afish=0;

  // Temporary for printing a few likelihoods
  // before and after orientation proposal
  int printL=0;

  for(mc=0; mc < M; mc++)
  {
    logpy=0.;
    logJ=0.;
    sky=0;
    test=0;
    for(i=1;i<ienddim;i++) for(j=0; j<smodel[0]->dimension; j++) intParams[i][j] = smodel[0]->intParams[smodel[0]->index[i]][j];
    for(i=0; i<iendN; i++) SnGeoy[i]=SnGeox[i];

    //Initialize parameter vectors & likelihood for extrinsic parameter MCMC
    for(i=0; i<NE; i++)  paramsy[i] = paramsx[i];

    /*
     Sky-ring proposal rotates zenith to 
     point along line connecting pair of detectors,
     holds declination fixed and draws azimuth from [0,2pi]
     
     Only works if we have more than one detector.
     
     Don't bother for hot chains, which should largely be
     sampling from the prior
     */
    if(data->skyFixFlag == 0)  // standard mode
    {
      if(uniform_draw(seed)< 0.5 && chain->index[ic]<10 && NI>1)
      {
        if(ic==0)cnt++;
        if(ic==0)sky=1;
        sky_ring_proposal(paramsx,paramsy,data,seed);
        if(data->orientationProposalFlag && uniform_draw(seed)<0.5){
          network_orientation_proposal(paramsx, paramsy, data, &logJ, seed);
          if(ic==0)skyo=1;
          if(ic==0)cskyo++;
        }
        else{
          uniform_orientation_proposal(paramsy, seed);
          if(ic==0)skyu=1;
          if(ic==0)cskyu++;
        }
      }
      else
      {
        /* Uniform Draw */
        draw = uniform_draw(seed);
        if(draw<0.1)extrinsic_uniform_proposal(seed,paramsy);
        
        /* Fisher Matrix Proposal */
        else
        {
          fisher_matrix_proposal(fisher, paramsx, seed, paramsy);
          if(ic==0)fish=1;
          if(ic==0)cfish++;
        }
      }
      // if(!data->extrinsicAmplitudeFlag) paramsy[5]=1.0;
    }
    else // Fixed sky proposal
    {
      draw = uniform_draw(seed);

      if(draw > 0.8) sky_fix_proposal(paramsx,paramsy,data,seed);
      else fisher_matrix_proposal(fisher, paramsx, seed, paramsy);
      // if(!data->extrinsicAmplitudeFlag) paramsy[5]=1.0;

    }

    if(!data->extrinsicAmplitudeFlag && !data->cbcFlag) paramsy[5]=1.0;
    if(!data->extrinsicAmplitudeFlag) Amp=1.0; 
    else {
      // Amp=paramsy[5];
      
      // Convert amplitude scaling from CBC to BW convention
      Apx = (1.0+paramsx[3]*paramsx[3])/2.0;
      Apy = (1.0+paramsy[3]*paramsy[3])/2.0;
      Amp=paramsy[5]*Apy/Apx;
    }

    //FIXME: Fix ugly psi=0 hack for unpolarized case
    //if(!data->polarizationFlag) paramsy[2] = paramsx[2] = 0.0;

    //Rescale intrinsic amplitudes and phases with extrinsic parameters
    for(i=1; i<ienddim; i++)
    {
      intParams[i][3] = smodel[0]->intParams[smodel[0]->index[i]][3]*Amp;//amplitude
      if (data->signalFlag) test += checkrange(intParams[i],prior->range, smodel[0]->dimension);
    }

    //check extrinsic parameters
    test += extrinsic_checkrange(paramsy);

    //check distance is within range with new scale
    if (data->cbcFlag) {
      double DL = exp(model_x->cbcParams[6])/(paramsy[5]*LAL_PC_SI);
      if(DL < bayescbc->DLmin || DL > bayescbc->DLmax) test += 1;
    }
    if(test==0)
    {

      logLy = data->extrinsic_likelihood(model_x->projection, paramsy, model_x->invSnf, smodel, glitch, model_x->cbcamphase, data, fmin, fmax, geoshifts, bayescbc);
      // logLy_old = ExtrinsicLikelihoodOld(model_x->projection, paramsy, model_x->invSnf, smodel, glitch, model_x->cbcamphase, data, fmin, fmax, geoshifts, bayescbc);

      //amplitude prior
      if(data->amplitudePriorFlag)
      {
        //model_x->projection has be updated by data->extrinsic_likelihood, so it stores F+ and Fx for params_y.
        Shf_Geocenter_full(data, model_x->projection, model_x->Snf, SnGeoy, paramsy);

        if (data->signalFlag) 
        {
          for(n=0; n<model_x->Npol; n++)
          {
            for(i=1; i<smodel[n]->size+1; i++)
            {
              logpy += (data->signal_amplitude_prior(smodel[n]->intParams[smodel[n]->index[i]],SnGeoy, data->Tobs, prior->sSNRpeak));
            }
          }
        }
      }

      logH = (logLy - logLx)*chain->beta + logpy - logpx + logJ; 

      // Make prior flat in cos(iota) instead of epsilon
      //
      if(data->cbcFlag)
      {
        // Distance prior
        double DLx = exp(model_x->cbcParams[6])/(paramsx[5]);
        double pAx = 3.0*log(DLx) - log(paramsx[5]);

        double DLy = exp(model_x->cbcParams[6])/(paramsy[5]);
        double pAy = 3.0*log(DLy) - log(paramsy[5]);

        logH += pAy - pAx; 
      }

      // if(ic==0 && skyo==1) printf("LogLx = %f, LogLy = %f\n", logLx, logLy);

      if(paramsy[0]!=paramsy[0])
      {
        printf("NAN!\n");fflush(stdout);
        exit(0);
        //logH = (logLy - logLx)*chain->beta + logpy - logpx + logJ;
      }

      alpha = log(uniform_draw(seed));

      if(logH >= alpha)
      {

        // Update geocenter time shift due to new accepted location
        if (data->cbcFlag) {

          // Compute shift += dtimex - dtimey 
          geoshifts[0] += geoshifts[3] - model_x->projection->dtimes[0]; 
          geoshifts[3] = model_x->projection->dtimes[0]; // save new accepted state dtimex
        }

        // if(ic==0) printf("dLogL_heterodyne = %f, dLogL = %f\n", (logLy - logLx), (logLy_old - logLx_old));
        chain->xacc++;
        logLx = logLy;
        for(i=0; i< NE; i++) paramsx[i] = paramsy[i];
        for(i=0; i<iendN; i++) SnGeox[i]=SnGeoy[i];
        logpx = logpy;

        if(sky==1 && ic==0) acc++;
        if(skyu==1 && ic==0) askyu++;
        if(skyo==1 && ic==0) askyo++;
        if(fish==1 && ic==0) afish++;        
      } 
    }

    sky=0;
    skyu=0; 
    skyo=0;
    fish=0;

    chain->xcount++;

  }

  // Print proposal acceptance rates
  // if(ic==0)
  // {
  //  printf("\tRing + Uniform Acceptance     = %f\n", (double)askyu/(double)(cskyu));
  //  printf("\tRing + Orientation Acceptance = %f\n", (double)askyo/(double)(cskyo));
  //  printf("\tFisher Acceptance             = %f\n", (double)afish/(double)(cfish));
  // }

  //Now update the full model with current extrinsic parameters
  //for(i=0; i<data->N; i++) geo[0][i] *= model_x->extParams[5];

  //Rescale each wavelet amplitude
  if(!data->extrinsicAmplitudeFlag) Amp = 1.0;
  else Amp = model_x->extParams[5]; // Todo convert scaling, if this option is used for wavelets
  for(i=1;i<ienddim;i++) model_x->signal[0]->intParams[ model_x->signal[0]->index[i]][3] *= Amp; //amplitude

  computeProjectionCoeffs(data, model_x->projection, model_x->extParams, data->fmin, data->fmax);

  if(data->signalFlag) 
  {
    combinePolarizations(data, model_x->signal, model_x->h, model_x->extParams, model_x->Npol);
    waveformProject(data, model_x->projection, model_x->extParams, model_x->response, model_x->h, data->fmin, data->fmax);
    Shf_Geocenter_full(data, model_x->projection, model_x->Snf, model_x->SnGeo, model_x->extParams);
  }

  if(data->cbcFlag) 
  {

    // Recompute projection with final shifts
    double *proposed_shifts = double_vector(3);
    extrinsic_proposal_cbc_geoshifts(geoshifts, proposed_shifts, model_x->projection->dtimes[0], model_x->extParams[4], model_x->extParams[5]);
    projectCBCWaveformExtrinsic(model_x->cbcamphase, data->N, NI, data->fmin, data->Tobs,
                                model_x->extParams, model_x->cbctemplate, model_x->projection->dtimes,
                                model_x->projection->Fplus, model_x->projection->Fcross,
                                proposed_shifts);
    free_double_vector(proposed_shifts);
    
    // Update CBC phic
    model_x->cbcParams[4] += model_x->extParams[4] - geoshifts[1];
    while(model_x->cbcParams[4] > LAL_TWOPI) model_x->cbcParams[4] -= LAL_TWOPI;
    while(model_x->cbcParams[4] < 0.0) model_x->cbcParams[4] += LAL_TWOPI;

    // Update CBC distance using scale
    model_x->cbcParams[6] -= log((model_x->extParams[5]));

    // Update CBC geocenter merger time
    model_x->cbcParams[5] += geoshifts[0];

    if (!within_CBC_prior(model_x->cbcParams, bayescbc, 1)){
      fprintf(stdout, "EvolveExtrinsicParameters outside of prior! Now enforcing inside of prior\n");
      enforce_CBC_priors(model_x->cbcParams, bayescbc);
    }
  }


  //Reset amplitude scale factor to 1
  model_x->extParams[5] = 1.0;//amplitude

  free(glitch);
  free_double_vector(paramsy);
  free_double_vector(geoshifts);
  free_double_vector(SnGeoy);
  free_double_matrix(intParams,dim);
}

/*
 * Initial (optional) MCMC search to find extrinsic model parameters
 */
void burnin_cbc(struct bayesCBC *bayescbc, struct Data *data, struct Model **model, struct Chain *chain)
{
  int i, ic;

  // Temporary options to use NoNRT in burnin, and NRTidal afterwards
  if(LALInferenceGetProcParamVal(data->commandLine, "--BBH-prior"))
  {
    free_bayescbc(bayescbc, data, chain);
    bayescbc  = realloc(bayescbc, sizeof(struct bayesCBC));

    data->NX = 7;
    printf("\nSWITCHING TO USE NONRT WAVEFORM IN BURNIN\n");
    bayescbc->NRTidal_version = (NRTidal_version_type) NoNRT_V;
    // todo I THINK WE HAVE TO FREE THE STRUCTURE FIRST! NOT SURE IF THIS IS RIGHT
    initialize_bayescbc(bayescbc, data, chain, model[chain->index[0]]); // bayescbc was already initialized in main
    for(ic=0; ic<chain->NC; ic++) setup_bayescbc_model(bayescbc, data, chain, model[chain->index[ic]], ic, chain->index[ic], 0);
    printf("\nSWITCHING TO USE NONRT WAVEFORM IN BURNIN\n");
  }

  if (bayescbc->resizeSearchFlag) resize_search_chains(bayescbc->net, data->Tobs, bayescbc->D, bayescbc->SN, data->N, chain->NC, chain->seed, bayescbc);

  // Quick search to find initial signal
  fprintf(stdout, " ======= Start CBC_burnin() ======\n");
  fprintf(stdout, " Start #1 intrinsic burnin \n");
  bayescbc->chainS = fopen("chains/cbc_searchchain.dat","w");  
  bayescbc->gmst = XLALGreenwichMeanSiderealTime(&data->epoch);
  burnin_bayescbc(
    data->N, data->Tobs, bayescbc->CBC_trigtime - data->starttime, bayescbc->D, bayescbc->SN,
    chain->seed, LALInferenceGetProcParamVal(data->commandLine,"--inj")->value, 
    bayescbc->injectionStartFlag, bayescbc
  );
  fclose(bayescbc->chainS);

  // debug check for if we are within the prior
  for (int ic = 0; ic < chain->NC; ic++){
     // after we find decent initial guess, copy over parmeters to cbcParams
     if (!within_CBC_prior(bayescbc->pallx[chain->index[ic]], bayescbc, 1)){
       fprintf(stdout, "\tburnin_cbc pallx %i\n", chain->index[ic]);
     }
     for (int i = 0; i < bayescbc->NX; i++){
      model[chain->index[ic]]->cbcParams[i] = bayescbc->pallx[chain->index[ic]][i];
    }
  }


  

  // Quick search to find initial signal extrinisc parameters
  bayescbc->chainS = fopen("chains/cbc_skychain.dat","w"); 
  if (bayescbc->intrinsic_only == 0 && data->constantLogLFlag == 0)
  {
    fprintf(stdout, "\n Start #2 (optional) extrinisic burnin \n");
    extrinsic_burnin_cbc(bayescbc, data, model, chain);
  }
  fclose(bayescbc->chainS);

  if (bayescbc->resizeSearchFlag) reset_search_chains(bayescbc->net, chain->index, chain->NC, chain->seed, bayescbc); 

  // Pass updated cbc parameters to model
  for(ic=0; ic<chain->NC; ic++)
  {
    printf("Reset search chains %d, %d\n", ic, chain->index[ic]);
    for(i = 0; i < bayescbc->NX; i++)
    {
      model[chain->index[ic]]->cbcParams[i] = bayescbc->pallx[chain->index[ic]][i];
    }

    if (bayescbc->intrinsic_only == 0||bayescbc->injectionStartFlag==1)
    {
      // Pass extrinsic parameters from BW model to BayesCBC
      for(i = bayescbc->NX; i < bayescbc->NP; i++)
      {
        model[chain->index[ic]]->extParams[i-bayescbc->NX] = bayescbc->pallx[chain->index[ic]][i];
      }

      // Reset fixed sky parameters
      if(data->skyFixFlag == 1)
      {
        model[chain->index[ic]]->extParams[0] = data->FIX_RA;
        model[chain->index[ic]]->extParams[1] = sin(data->FIX_DEC);
        bayescbc->pallx[chain->index[ic]][bayescbc->NX] = data->FIX_RA;
        bayescbc->pallx[chain->index[ic]][bayescbc->NX+1] = sin(data->FIX_DEC);
      }

      // Update projection based on extrinsic params from burnin
      computeProjectionCoeffs(data, model[chain->index[ic]]->projection, model[chain->index[ic]]->extParams, data->fmin, data->fmax);
      Shf_Geocenter_full(data, model[chain->index[ic]]->projection, model[chain->index[ic]]->Snf, model[chain->index[ic]]->SnGeo, model[chain->index[ic]]->extParams);
    }
    // Store CBC templates in model after burnin
    geowave(model[chain->index[ic]]->cbcamphase, bayescbc->freq, model[chain->index[ic]]->cbcParams, data->N, bayescbc);
    projectCBCWaveform(model[chain->index[ic]]->cbcamphase, data->N, data->NI, data->fmin, data->Tobs, model[chain->index[ic]]->extParams, model[chain->index[ic]]->cbctemplate, model[chain->index[ic]]->projection->dtimes, model[chain->index[ic]]->projection->Fplus, model[chain->index[ic]]->projection->Fcross);
  }
  fprintf(stdout, " ======= End of CBC_burnin() ======\n");

  // Temporary options to use NoNRT in burnin, and switch to NRTidal afterwards
  if(LALInferenceGetProcParamVal(data->commandLine, "--BBH-prior"))
  {
    // TODO !! using realloc here is undefined behavior? cant free a structure and then realloc it
    free_bayescbc(bayescbc, data, chain);
    bayescbc  = realloc(bayescbc, sizeof(struct bayesCBC));

    data->NX = 9;
    bayescbc->NRTidal_version = (NRTidal_version_type) NRTidal_V;
    initialize_bayescbc(bayescbc, data, chain, model[chain->index[0]]);

    bayescbc->lambda1min = 0.0;       // Min/max for lambda1 and lambda2
    bayescbc->lambda1max = 10.0; // 1000.0;    // Only used if NRTidal_value != NoNRT_V and
    bayescbc->lambda2min = 0.0;       // lambda_type_value = lambda 
    bayescbc->lambda2max = 10.0;

    bayescbc->lambdaTmin = 0.0;       // Min/max for lambdaT and dlambdaT
    bayescbc->lambdaTmax = 10.0;    // Only used if NRTidal_value != NoNRT_V and
    bayescbc->dlambdaTmin = -0.1;     // lambda_type_value = lambdaTilde 
    bayescbc->dlambdaTmax = 0.1;     // lambda_type_value = lambdaTilde 

    bayescbc->chi1min = -0.99;  // Default min, max for chi1 and chi2
    bayescbc->chi2min = -0.99;
    bayescbc->chi1max = 0.99;
    bayescbc->chi2max = 0.99;

    bayescbc->mcmax = 174.0;   // maximum chirp mass in Msun.  Mc is at most 0.435 times Mt
    bayescbc->mcmin = 0.23;    // minimum chirp mass in Msun
    bayescbc->mtmax = 400.0;   // maximum total mass in Msun (should be not more than 2.3 times mcmax)
    bayescbc->mtmin = 1.0;     // minimum total mass in Msun

    bayescbc->max[0] = log(bayescbc->mcmax*MSUN_SI);  // Mc is at most 0.435 times Mt
    bayescbc->max[1] = log(bayescbc->mtmax*MSUN_SI);  // Mt
    bayescbc->min[0] = log(bayescbc->mcmin*MSUN_SI);  // Mc
    bayescbc->min[1] = log(bayescbc->mtmin*MSUN_SI);  // Mt
    bayescbc->max[2] = bayescbc->chi1max;  // chi1
    bayescbc->max[3] = bayescbc->chi2max;  // chi2
    bayescbc->min[2] = bayescbc->chi1min;  // chi1
    bayescbc->min[3] = bayescbc->chi2min;  // chi2

    if (bayescbc->lambda_type_version == (Lambda_type) lambdaTilde) {
        bayescbc->max[7] = bayescbc->lambdaTmax; // lambdaT
        bayescbc->max[8] = bayescbc->dlambdaTmax; // dlambdaT
        bayescbc->min[7] = bayescbc->lambdaTmin; // lambdaT
        bayescbc->min[8] = bayescbc->dlambdaTmin; // dlambdaT
    } else {
        bayescbc->max[7] = bayescbc->lambda1max; // lambda1
        bayescbc->max[8] = bayescbc->lambda2max; // lambda2
        bayescbc->min[7] = bayescbc->lambda1min; // lambda1
        bayescbc->min[8] = bayescbc->lambda2min; // lambda2
    }

    for(ic=0; ic<chain->NC; ic++) {
      
      model[chain->index[ic]]->cbcParams[7] = 0.0000001;    // Initialize value for lambdas
      model[chain->index[ic]]->cbcParams[8] = 0.0000001;

      printf("bayescbc->NX: %i\n", bayescbc->NX);
      setup_bayescbc_model(bayescbc, data, chain, model[chain->index[ic]], ic, chain->index[ic], 1);
    }
    printf("\nSWITCHED BACK TO USE NRTidal WAVEFORM\n");
  }
}

/*
 * Optional part of initial CBC MCMC burnin search for extrinsic model parameters
 */
void extrinsic_burnin_cbc(struct bayesCBC *bayescbc, struct Data *data, struct Model **model, struct Chain *chain)
{
  int i, j, k, id;
  int nt, bn;
  int imin, imax;
  double x, dtx;
  int mty, mtid, mti;
  double *logL, *logLsky, *logLfull, *logLstart;
  double **ext_data, ***wave;
  double *DD, **WW;
  double *rho;
  double ***DHc, ***DHs, ***HH;
  double *heat; 
  double Lmax;
  FILE *chainFile;

  // local pointers
  struct Net *net = bayescbc->net;
  int N = data->N;
  int NX = bayescbc->NX;
  int NP = bayescbc->NP;
  int NS = bayescbc->NS; // number quasi-intrinsic sky parameters (7)
  int NC = bayescbc->NC; // number of chains
  int NCC = bayescbc->NCC; // number of cold chains 
  int NH = bayescbc->NH; // length of history
  int NQ = bayescbc->NQ; // number of mass ratios in global proposal
  int NM = bayescbc->NM; // number of chirp masses in global proposal
  double **paramx = bayescbc->paramx; // intrinsic burnin parameters
  // skyx: quasi-intrinsic skyparameters :[0] alpha, [1] sin(delta) [2] psi [3] ciota [4] scale [5] phi0 [6] dt
  double **skyx = bayescbc->skyx;
  double **pallx = bayescbc->pallx; // intrinsic burnin parameters
  double **D = bayescbc->D;
  double **SN = bayescbc->SN;

  logL = double_vector(NC);
  logLsky = double_vector(NC);
  logLstart = double_vector(NC);
  logLfull = double_vector(NC);

  // whitened signal in each detector
  wave = double_tensor(NC,net->Nifo,N);

  // whitened data in each detector
  ext_data = double_matrix(net->Nifo,N);

  rho = double_vector(net->Nifo);

  // convert data to local format (needed for local fourier transforms)
  for (id = 0; id < net->Nifo; ++id)
  {
      ext_data[id][0] = 0.0;
      ext_data[id][N/2] = 0.0;
  }
  
  for (i = 1; i < N/2; ++i)
  {
      for (id = 0; id < net->Nifo; ++id)
      {
          x = 1.0/sqrt(SN[id][i]);
          ext_data[id][i] = D[id][2*i]*x;
          ext_data[id][N-i] = D[id][2*i+1]*x;
      }
  }

  Lmax = 0.0;

  upsample(N, data->Tobs, &nt, &bn);
    
  dtx = data->Tobs/(double)(bn);

  imin = (int)((data->fmin*data->Tobs));
  imax = (int)((data->fmax*data->Tobs));
    

  DD = double_vector(net->Nifo);
  WW = double_matrix(NC,net->Nifo);
  
  DHc = double_tensor(NC,net->Nifo,nt+1);
  DHs = double_tensor(NC,net->Nifo,nt+1);
  HH = double_tensor(NC,net->Nifo,3);


  if(bayescbc->debug == 1) printf("\nFinding sky location\n");
  
  // // Find sky location and set up whitend signal arrays
  // #pragma omp parallel for
  for(j = 0; j < NC; j++)
  {
      int mtid, mti;
      double mty, Scale;
      double *SNRsq;
      double **hwave;
      
      SNRsq = double_vector(net->Nifo);
      hwave = double_matrix(net->Nifo,N);            

      // hwave in [0,N/2] real and [N/2,N] imag
      templates(net, hwave, bayescbc->freq, paramx[j], N, bayescbc);

      // might want to put a catch here for any chains that didn't reach a decent logL and re-run the rearch phase
      logLstart[j] = log_likelihood(net, D, paramx[j], bayescbc->freq, SN, N, data->Tobs, bayescbc);
      
      mty = 0.0;
      for (mtid = 0; mtid < net->Nifo; ++mtid)
      {
          SNRsq[mtid] = 0.0;
         for (mti = 1; mti < N/2; ++mti)
          {
          // The sky mapping code puts in the amplitude and phase shifts. The signals only differ due to the whitening
           SNRsq[mtid] += 4.0*(hwave[0][mti]*hwave[0][mti]+hwave[0][N-mti]*hwave[0][N-mti])/SN[mtid][mti];
          }
          Scale = 1.0;
          if(mtid > 0) Scale = paramx[j][(mtid-1)*3+NX+2]*paramx[j][(mtid-1)*3+NX+2];
          SNRsq[mtid] *= Scale;
          mty += SNRsq[mtid];
      }
      
      // if(bayescbc->debug == 1) printf("%f %f\n", SNRsq[0], SNRsq[1]);

      if (data->skyFixFlag == 0)
      {
        // find a sky location roughly consistent with the time delays
        skyring(net, paramx[j], skyx[j], pallx[j], SNRsq, bayescbc->freq, D, SN, N, data->Tobs, chain->seed, NX, NS, bayescbc->gmst);
      }

      dshifts(net, skyx[j], paramx[j], NX, bayescbc);

      // map to the geocenter params
      pmap_all(net, pallx[j], paramx[j], skyx[j], NX, bayescbc->gmst);

      // get the geocenter reference template. This does depend on the assumed sky location, hence follows skystart
      geotemplate(hwave[0], bayescbc->freq,  pallx[j], N, bayescbc);

      for (mtid = 0; mtid < net->Nifo; ++mtid)
      {
          wave[j][mtid][0] = 0.0;
          wave[j][mtid][N/2] = 0.0;
          for (mti = 1; mti < N/2; ++mti)
          {
              // The sky mapping code puts in the amplitude and phase shifts. The signals only differ due to the whitening
              mty = 1.0/sqrt(SN[mtid][mti]);
              wave[j][mtid][mti] = hwave[0][mti]*mty;
              wave[j][mtid][N-mti] = hwave[0][N-mti]*mty;
          }

      }

      skylikesetup(net, ext_data, wave[j], DD, WW[j], DHc[j], DHs[j], data->Tobs, N, bn, nt, imin, imax);
      fisherskysetup(net, wave[j], HH[j], data->Tobs, N);

      logLsky[j] = skylike(net, skyx[j], DD, WW[j], DHc[j], DHs[j], dtx, nt, 0, bayescbc);
      logL[j] = log_likelihood(net, D, paramx[j], bayescbc->freq, SN, N, data->Tobs, bayescbc);
      logLfull[j] = log_likelihood_full(net, D, pallx[j], bayescbc->freq, SN, rho, N, data->Tobs, bayescbc, 1);      
      if(bayescbc->debug == 1) printf("%d logL start %f intrinsic %f full %f sky %f\n", j, logLstart[j], logL[j], logLfull[j], logLsky[j]);

      free_double_vector(SNRsq);
      free_double_matrix(hwave,net->Nifo);

  }


  chainFile = fopen("sky.dat","w");
  skymcmc(net, 1000000, bayescbc->mxc, chainFile, paramx, skyx, pallx, bayescbc->who, bayescbc->heat, dtx, nt, DD, WW,
          DHc, DHs, HH, data->Tobs, chain->seed, bayescbc, data->skyFixFlag);
  fclose(chainFile);

  for(j = 0; j < NC; j++)
  {   
    pmap_all(net, pallx[j], paramx[j], skyx[j], NX, bayescbc->gmst);
    logLfull[j] = log_likelihood_full(net, D, pallx[j], bayescbc->freq, SN, rho, N, data->Tobs, bayescbc, 1);
    if(bayescbc->debug) printf("   %d logL full %f\n", j, logLfull[j]);
  }

  
  x = -1e4;
  k = bayescbc->who[0];
  for(j = 0; j < NC; j++)
  {
      if(logLfull[j] > x)
      {
          x = logLfull[j];
          k = j;
      }
  }
  
  // set threshold that all chains start withing 20% of highest likelihood
  x *= 0.8;
  
  if (bayescbc->debug) printf("  Chains CBC logL before starting main MCMC:\n");

  for(j = 0; j < NC; j++)
  {
      
      // printf("%d %f  ", jj, logLfull[jj]);
      
      if(logLfull[j] < x)
      {
          for(i = 0; i < NP; i++)
          {
              pallx[j][i] = pallx[k][i];
          }
      }

      logLfull[j] = log_likelihood_full(net, D, pallx[j], bayescbc->freq, SN, rho, N, data->Tobs, bayescbc, 1);
      bayescbc->logLx[j] = logLfull[j]; // BW shared array
      if(bayescbc->debug) printf("   %d logL full %f\n", j, logLfull[j]);
  }

  free_double_vector(rho);
  free_double_vector(logL);
  free_double_vector(logLstart);
  free_double_vector(logLsky);
  free_double_vector(logLfull);
  free_double_tensor(wave,NC,net->Nifo);
  free_double_vector(DD);
  free_double_matrix(WW,NC);
  free_double_tensor(DHc,NC,net->Nifo);
  free_double_tensor(DHs,NC,net->Nifo);
  free_double_tensor(HH,NC,net->Nifo);
  free_double_matrix(ext_data,net->Nifo);

}

/* ********************************************************************************** */
/*                                                                                    */
/*                                    MCMC tools                                      */
/*                                                                                    */
/* ********************************************************************************** */


void adapt_temperature_ladder(struct Chain *chain, int NC)
{
  int ic;//,i,ifo;
  //  double x;

  double S[NC];
  double A[NC][2];

  double nu=10;
  double t0=10000;

  for(ic=1; ic<NC-1; ic++)
  {
    S[ic] = log(chain->temperature[ic] - chain->temperature[ic-1]);
    A[ic][0] = chain->A[ic-1];
    A[ic][1] = chain->A[ic];
  }

  ic=0;
  for(ic=1; ic<NC-1; ic++)
  {
    S[ic] += (A[ic][0] - A[ic][1])*(t0/((double)chain->mc+t0))/nu;
    //S[ic] += (A[ic][0] - A[ic][1])*(mc)/nu;
    chain->temperature[ic] = chain->temperature[ic-1] + exp(S[ic]);

  }//end loop over ic
}//end adapt function

