/*
 *  Copyright (C) 2018 Neil J. Cornish, Tyson B. Littenberg, Francesco Pannarale,
 *    Margaret Millhouse, Katerina Chatziioannou, James A. Clark, Sudarshan Ghonge,
 *    Deborah L. Ferguson
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with with program; see the file COPYING. If not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 *  MA  02111-1307  USA
 */


/***************************  REQUIRED LIBRARIES  ***************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <math.h>
#include <fftw3.h>
#include <sys/stat.h>
#include <gsl/gsl_cdf.h>


#include <lal/LALInferenceTemplate.h>
#include <lal/LALSimIMR.h>
#include <lal/LALSimulation.h>
#include <lal/RingUtils.h>

#include "BayesCBC.h"
#include "BayesLine.h"
#include "BayesWave.h"
#include "BayesWaveIO.h"
#include "BayesWaveMCMC.h"
#include "BayesWaveMath.h"
#include "BayesWavePrior.h"
#include "BayesWaveModel.h"
#include "BayesWaveWavelet.h"
#include "BayesWaveProposal.h"
#include "BayesWaveEvidence.h"
#include "BayesWaveLikelihood.h"

/*************  PROTOTYPE DECLARATIONS FOR INTERNAL FUNCTIONS  **************/

#define GLOBAL_F_RESOLUTION 8.0 // Frequency resolution for Qscans (Hz)

struct Moments
{
  double *overlap;
  double *energy;
  double *frequency;
  double *time;
  double *bandwidth;
  double *duration;
  double *snr;
  double networkOverlap;
  double *hmax;
  double *t_at_hmax;
  double *f_at_max_amp;
  double *medianfrequency;
  double *mediantime;
  double *bandwidth90percentinterval;
  double *duration90percentinterval;
  double *tl;
  double *th;
};

void print_help_message(void);
void ComputeWaveformOverlap(struct Data *data, double **hinj, double **hrec, double **psd, struct Moments *m);
void PrintRhof(struct Data *data, double deltaF, double **h, double **psd, struct Moments *m);
void ComputeWaveformMoments(struct Data *data, double **h, double **psd, struct Moments *m);
void ComputeGeocenterMoments(struct Data *data, double deltaF, double deltaT, double *h, double *psd, struct Moments *m);

void free_moments(struct Moments *m);
void initialize_moments(struct Moments *m, int NI);
void setup_output_files(int NI, int injectionFlag, char *outdir, char *model, char *weighting, FILE **momentsOut, struct Data *data);
void discard_burnin_samples(FILE *params, int *COUNT, int *BURNIN);
void print_moments(int NI, int injectionFlag, FILE **outfile, struct Moments *m);
void print_waveforms(struct Data *data, double **h, double **psd, FILE **ofile);
void print_spectrum(struct Data *data, double **h, FILE **ofile);
void print_hdot(struct Data *data, double **h, double **psd, FILE **ofile, double deltaF);
void print_spectrograms(struct Data *data, char outdir[], char type[], char *ifo, double *h, double *psd);

void get_time_domain_hdot(double *hdot, double *h, int N, int imin, int imax, double deltaF);

void print_stats(struct Data *data, LIGOTimeGPS GPS, int injectionFlag, char *outdir, char model[], int **dimension);

double get_energy(int imin, int imax, double *a, double deltaF);
double get_rhof(int imin, int imax, double *a, double deltaF, double *arg);
double get_rhof_net(int imin, int imax, double **a, double deltaF, double *arg, int NI);
double rhonorm(int imax, double *a, double deltaF);
double get_f0(int imax, double *a, double deltaF);

void get_f_quantiles(int imin, int imax, double *rhof, double deltaF, double *fm, double *bw);
void get_t_quantiles(int N, double *rhot, double deltaT, double *tm, double *dur);
double get_rhof_f2weighted(int imin, int imax, double *a, double deltaF, double *arg);

double get_band(int imax, double *a, double deltaF, double f0);
void get_hmax_and_t(int imax, double *a, double deltaT, double *hmax, double *t_at_hmax);
void get_time_domain_waveforms(double *hoft, double *h, int N, int imin, int imax);
double get_rhot(int N, double *a, double deltaT, double *arg);
double get_rhot_net(int N, double **a, double deltaT, double *arg, int NI);
void whiten_data(int imin, int imax, double *a, double *Snf);

void write_time_freq_samples(struct Data *data);

void print_single_waveform(struct Data *data, double **h, double **psd, FILE *ofile);

void tf_tracks(double *hoft, double *frequency_out, double deltaT, int N);
void interp_tf(double **tf, double **pts, int tlength, int Nsample);
void median_and_CIs(double **waveforminfo, int N, int Nwaveforms, double deltaT, FILE *outfile, double *median_waveform);
void median_and_CIs_fourierdom(double **waveforminfo, int N, int Nwaveforms, double deltaT, FILE *outfile, double *median_waveform);
void Q_scan(struct Data *data, double *median_waveform, double Q, double *invPSD, FILE *outfile);
void Transform(double *hoff, double Q, double dt, double fmin, double *invPSD, int n, int m, FILE *outfile);

void print_anderson_darling_p_values(char *outDir, int Nsamp);
void anderson_darling_test(char *outdir, int ifo, double fmin, double Tobs, char *model, char **ifos);


int post_process_model(char model_name[], char model_type[], struct Data *data, struct Model *model, struct Chain *chain, double **hdata, double **hoft, double **psd, double **one, double **hinj, int **dimension, int injectionFlag, LIGOTimeGPS GPStrig, bool lite);

void get_time_domain_envelope(double *envelope, double *hdata_sample, int N, int imin, int imax);
double get_time_at_max(double *hdata_sample, int N, int imin, int imax, double deltaT);
void get_freq_at_max(double *hdata_sample, int N, int imin, int imax, double deltaT, double *f_at_max_amp);

/* ============================  MAIN PROGRAM  ============================ */

int main(int argc, char *argv[])
{

  /*   Variable declaration   */
  int i, ifo, imin, ii;//, imax;

  char filename[MAXSTRINGSIZE];

  /* LAL data structure that will contain all of the data from the frame files */
  LALInferenceRunState *runState = XLALCalloc(1,sizeof(LALInferenceRunState));

  runState->commandLine = LALInferenceParseCommandLine(argc,argv);

  print_version(stdout);

  /******************************************************************************/
  /*                                                                            */
  /*  Usage statment if --help                                                  */
  /*                                                                            */
  /******************************************************************************/

  if(LALInferenceGetProcParamVal(runState->commandLine, "--help") || argc==1)
  {
    print_help_message();
    return 0;
  }

  /******************************************************************************/
  /*                                                                            */
  /*  Read/store  data                                                          */
  /*                                                                            */
  /******************************************************************************/

  /*
   Unlike in BayesWaveBurst, we DO NOT want to read and store the IFO data,
   but we have to use LALInferenceReadData to properly setup the runState
   structure and use the injection software.

   This is a hack to fill the runState detector with zeros so that all we store
   in runState is the injected waveform.
   */

  if( LALInferenceGetProcParamVal(runState->commandLine, "--0noise") )
  {
    fprintf(stdout,"Found --0noise in the command line\n");
  } else {
    fprintf(stdout," \n");
    fprintf(stdout,"Error:  You must include --0noise in the command line\n");
    return 0;
  }

  fprintf(stdout, "\n");
  fprintf(stdout, " ======= LALInferenceReadData() ======\n");
  runState->data = LALInferenceReadData(runState->commandLine);
  fprintf(stdout, "\n");

  // Check if this is a "lite" version (less ouput)                                                                                                    
  bool lite = False;
  if (LALInferenceGetProcParamVal(runState->commandLine, "--lite")) {
    lite = True;
  }

  /******************************************************************************/
  /*                                                                            */
  /*  read/store data                                                           */
  /*                                                                            */
  /******************************************************************************/
  LALInferenceIFOData *dataPtr = NULL;

  dataPtr = runState->data;

  double *SNRinj = NULL;

  int NI;
  int N = dataPtr->timeData->data->length;
  double Tobs = (double)N*dataPtr->timeData->deltaT;

  ifo=0;
  while(dataPtr!=NULL)
  {
    dataPtr = dataPtr->next;
    ifo++;
  }
  NI=ifo;

  double **timeData = double_matrix(NI-1,N-1);
  double **freqData = double_matrix(NI-1,N-1);
  double **psd      = double_matrix(NI-1,N/2-1);
  double **one      = double_matrix(NI-1,N/2-1);

  struct Data *data = malloc(sizeof(struct Data));

  /*
   Unused, but needed for command line and initialization functions
   */
  struct Prior *prior = malloc(sizeof(struct Prior));
  struct Chain *chain = malloc(sizeof(struct Chain));

  const gsl_rng_type *T = gsl_rng_default;
  chain->seed = gsl_rng_alloc(T);
  gsl_rng_env_setup();
  
  parse_command_line(data, chain, prior, runState->commandLine);
    
    
  // Median stuff
  double norm;

  /*
   Move to the directory specified by the user, or stay in ./
   */
  chdir(data->outputDirectory);
  fprintf(stdout,"\nOutputting all data to: %s \n\n", data->outputDirectory);

  /*
   OUTPUT DIRECTORY
   */
  char outdir[MAXSTRINGSIZE];
  //sprintf(outdir,"%spost",data->runName);
  sprintf(outdir,"%spost",data->runName);
  mkdir(outdir,S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);

  char wavedir[MAXSTRINGSIZE];
  sprintf(wavedir,"%s/waveforms",outdir);
  mkdir(wavedir,S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);

  /*
   Get trigger time in GPS seconds
   */
  LIGOTimeGPS GPStrig;
  ProcessParamsTable *ppt = NULL;

  ppt = LALInferenceGetProcParamVal(runState->commandLine,"--trigtime");
  XLALStrToGPS(&GPStrig,ppt->value,NULL);


  // burstMDC injection
  int injectionFlag = 0;
  int xmlInjectionFlag = 0;
  if(LALInferenceGetProcParamVal(runState->commandLine, "--MDC-cache"))
  {
    SNRinj = malloc((NI+1)*sizeof(double));
    fprintf(stdout, " ==== LALInferenceInjectFromMDC(): started. ====\n");
    InjectFromMDC(runState->commandLine, runState->data, SNRinj);
    fprintf(stdout, " ==== LALInferenceInjectFromMDC(): finished. ====\n\n");
    free(SNRinj);

    injectionFlag = 1;
  }

  //CBC injection
  if(LALInferenceGetProcParamVal(runState->commandLine, "--inj"))
  {
    fprintf(stdout, " ==== LALInferenceInjectInspiralSignal(): started. ====\n");
    LALInferenceInjectInspiralSignal(runState->data,runState->commandLine);
    fprintf(stdout, " ==== LALInferenceInjectInspiralSignal(): finished. ====\n\n");

    injectionFlag = 1;
    //if(!LALInferenceGetProcParamVal(runState->commandLine, "--lalinspiralinjection"))xmlInjectionFlag = 1;
  }

  
  // Check chirplet flag
  int chirpletFlag = 0;
  if (LALInferenceGetProcParamVal(runState->commandLine, "--chirplets")) {
    chirpletFlag = 1;
  }
  
  
  //Output Fourier domain data for BayesLine
  dataPtr = runState->data;

  double fmin   = dataPtr->fLow;
  double fmax   = dataPtr->fHigh;
  double deltaT = dataPtr->timeData->deltaT;

  int tsize = dataPtr->timeData->data->length/4;

  //Copy Fourier domain data & PSD from LAL data types into simple arrays
  ifo=0;
  imin = (int)(fmin*Tobs);
  while(dataPtr!=NULL)
  {
    for(i=0; i<N; i++) timeData[ifo][i] = dataPtr->timeData->data->data[i];
    for(i=0; i<N/2; i++)
    {
      if(i<imin)
      {
        freqData[ifo][2*i]   = 0.0;
        freqData[ifo][2*i+1] = 0.0;
        psd[ifo][i] = 1.0;
        one[ifo][i] = 1.0;
      }
      else
      {
        freqData[ifo][2*i]   = creal(dataPtr->freqData->data->data[i]);
        freqData[ifo][2*i+1] = cimag(dataPtr->freqData->data->data[i]);

        /*
         Innerproducts are expecting <n_i^2> instead of Sn(f):
         So psd arrays == <n_i^2> = T/2 * Sn(f)
         */
        psd[ifo][i] = dataPtr->oneSidedNoisePowerSpectrum->data->data[i]*Tobs/2.0;
        one[ifo][i] = 1.0;
      }
    }
    dataPtr = dataPtr->next;
    ifo++;
  }

  //Storage for h+(f) and hx(f) of all IFOs
  double **freqDataPlus  = double_matrix(NI-1,N-1);
  double **freqDataCross = double_matrix(NI-1,N-1);


  /******************************************************************************/
  /*                                                                            */
  /*  Setup DATA, CHAIN, PRIOR, MODEL structures                                */
  /*                                                                            */
  /******************************************************************************/

  /*
   DATA
   */
  initialize_data(data,freqData,N,tsize,Tobs,NI,fmin,fmax);
  
  // Check chirplet flag
  if (chirpletFlag) data->NW = 6;
  else data->NW = 5;
  
  //Write out time and freq. samples
  write_time_freq_samples(data);

  data->detector  = malloc(NI*sizeof(LALDetector*));
  data->epoch     = runState->data->epoch;

  ifo=0;
  dataPtr = runState->data;
  while(dataPtr!=NULL)
  {
    sprintf(data->ifos[ifo],"%s",dataPtr->name);
    data->detector[ifo] = dataPtr->detector;
    ifo++;
    dataPtr = dataPtr->next;
  }

  /*
   CHAIN
   */
  //chain->NC = chain->NCmin;
  allocate_chain(chain,NI,data->Npol);

  // initialize chain index 
  for(int ic=0; ic < chain->NC; ic++) chain->index[ic]=ic;
 

  /*
   PRIOR
   */
  data->Dmax = 400;

  int omax = 10;
  if(data->Dmax < omax) omax = data->Dmax;

  //Maximum number of sineGaussians
  prior->gmin = int_vector(data->NI-1);
  prior->gmax = int_vector(data->NI-1);
  //Wavelet parameters
  prior->range = double_matrix(4,1);

  initialize_priors(data, prior, omax);

  /*
   MODEL
   */
  struct Model *model = malloc(sizeof(struct Model));
  initialize_model(model, data, prior, psd, chain->seed);

  //Use Welch averaged PSD from LALInferenceReadData
  for(ifo=0; ifo<data->NI; ifo++)
  {
    for(i=0; i<N/2; i++)
    {
      model->SnS[ifo][i] = psd[ifo][i];
      model->Snf[ifo][i] = psd[ifo][i];
    }
  }

  // BayesWave internal injection
  if(LALInferenceGetProcParamVal(runState->commandLine,"--BW-inject"))
  {
    fprintf(stdout, " ==== BayesWaveInjection(): started. ====\n");
    BayesWaveInjection(runState->commandLine, data, chain, prior, psd, &chain->NC);
    fprintf(stdout, " ==== BayesWaveInjection(): finished. ====\n\n");
    injectionFlag = 1;
  }


  /*
   BAYESLINE PSD
   */
  int PSDimin = 0;
  int PSDimax = data->N/2;


  /******************************************************************************/
  /*                                                                            */
  /*  Declare the list of metrics to compute as part                            */
  /*  of the post-processing step.                                              */
  /*                                                                            */
  /******************************************************************************/

  fprintf(stdout, "\n");
  fprintf(stdout, " ===== BayesWave Post Processing =====\n");

  //print p-values for reference
  print_anderson_darling_p_values(outdir,N);

  //User-friendly arrays for PSD, signal, and injected waveform
  double **hinj      = malloc(NI*sizeof(double*));
  double **hinjPlus  = malloc(NI*sizeof(double*));
  double **hinjCross = malloc(NI*sizeof(double*));
  double **hoft      = malloc(NI*sizeof(double*));
  double **roft      = malloc(NI*sizeof(double*));
  double **hinj_white= malloc(NI*sizeof(double*));

  for(ifo=0; ifo<NI; ifo++)
  {
    hinj[ifo]      = malloc(N*sizeof(double));
    hinjPlus[ifo]  = malloc(N*sizeof(double));
    hinjCross[ifo] = malloc(N*sizeof(double));
    hoft[ifo]      = malloc(N*sizeof(double));
    roft[ifo]      = malloc(N*sizeof(double));
    hinj_white[ifo]= malloc(N*sizeof(double));
  }
  
  //Fill hinj, hinjPlus, and hCrossPlus with the injected waveform (held in data because did a 0-noise simulation) and its + and x polarizations
  for(ifo=0; ifo<NI; ifo++) for(i=0; i<N; i++) hinj[ifo][i] = data->s[ifo][i];
  if(xmlInjectionFlag)
  {
    for(ifo=0; ifo<NI; ifo++) for(i=0; i<N; i++) hinjPlus[ifo][i]  = freqDataPlus[ifo][i];
    for(ifo=0; ifo<NI; ifo++) for(i=0; i<N; i++) hinjCross[ifo][i] = freqDataCross[ifo][i];
  }

  if(injectionFlag && !data->polarizationFlag)
  {
    fprintf(stdout, "Computing Stokes parameters for injected waveform\n");


    FILE *FDPolsInjFile;
    char FDPolsInjFileName[MAXSTRINGSIZE];
    sprintf(FDPolsInjFileName,"%s_FD_geocenter_pols.dat",data->ifos[0]);
    fprintf(stdout, "%s\n",FDPolsInjFileName);
    if( (FDPolsInjFile = fopen(FDPolsInjFileName, "r")) == NULL )
    {
      fprintf(stdout,"Could not find %s. Skipping computation of injected Stokes parameters\n",FDPolsInjFileName);
    }
    else
    {
      fprintf(stdout, "FDPolsInjFile found! -- Reading in data from it!\n");
      
      double junk;
      char fuckstring[MAXSTRINGSIZE];
      for(i=0; i<data->N/2; i++) 
      {
        //int readInNumber = fscanf(FDPolsInjFile,"%s",&fuckstring);
        //fprintf(stdout, "%s\n-----------------------------\n", fuckstring);
        //int readInNumber = fscanf(FDPolsInjFile,"%10.10g",&junk);
        //fscanf(FDPolsInjFile,"%10.10g",&hinjPlus[0][2*i]);
        //fscanf(FDPolsInjFile,"%10.10g",&hinjPlus[0][2*i+1]);
        //fscanf(FDPolsInjFile,"%10.10g",&hinjCross[0][2*i]);
        //fscanf(FDPolsInjFile,"%10.10g",&hinjCross[0][2*i+1]);
        int readInNumber = fscanf(FDPolsInjFile,"%lg %lg %lg %lg %lg",&junk,&hinjPlus[0][2*i],&hinjPlus[0][2*i+1],&hinjCross[0][2*i],&hinjCross[0][2*i+1]);
        //fprintf(stdout, "%d\n", readInNumber);
        //fprintf(stdout, "%e\n", junk);
        //fprintf(stdout, "%e\n", hinjPlus[0][2*i]);
        //fprintf(stdout, "%e\n", hinjPlus[0][2*i+1]);
        //fprintf(stdout, "%e\n", hinjCross[0][2*i]);
        //fprintf(stdout, "%e\n", hinjCross[0][2*i+1]);
      }

      //fprintf(stdout,"Test (line 150): %e %e %e %e\n",hinjPlus[0][2*150],hinjPlus[0][2*150+1],hinjCross[0][2*150],hinjCross[0][2*150+1]);

      double **stokes_injected = double_matrix(3, N/2-1);

      for (ii = 0; ii < data->N/2; ii++)
      {
        stokes_injected[0][ii] = 2 * (hinjPlus[0][2*ii]*hinjCross[0][2*ii] + hinjPlus[0][2*ii+1]*hinjCross[0][2*ii+1]);                           // U
        stokes_injected[1][ii] = (hinjCross[0][2*ii]*hinjCross[0][2*ii] + hinjCross[0][2*ii+1]*hinjCross[0][2*ii+1]) / (hinjPlus[0][2*ii]*hinjPlus[0][2*ii] + hinjPlus[0][2*ii+1]*hinjPlus[0][2*ii+1]); // (I-Q)/(I+Q)
        stokes_injected[2][ii] = (hinjPlus[0][2*ii]*hinjCross[0][2*ii+1] - hinjPlus[0][2*ii+1]*hinjCross[0][2*ii]) / (hinjPlus[0][2*ii]*hinjPlus[0][2*ii] + hinjPlus[0][2*ii+1]*hinjPlus[0][2*ii+1]); // V/(I+Q)
        stokes_injected[3][ii] = (hinjCross[0][2*ii]*hinjCross[0][2*ii] + hinjCross[0][2*ii+1]*hinjCross[0][2*ii+1]) / (hinjPlus[0][2*ii]*hinjCross[0][2*ii+1] - hinjPlus[0][2*ii+1]*hinjCross[0][2*ii]);  // (I-Q)/V
      }

      FILE **injectedStokesFile = malloc(4*sizeof(FILE*));

      sprintf(filename,"%s/injected_frequency_domain_stokes_U.dat",outdir);
      injectedStokesFile[0] = fopen(filename,"w");

      sprintf(filename,"%s/injected_frequency_domain_stokes_eps_sqr.dat",outdir);                    
      injectedStokesFile[1] = fopen(filename,"w");

      sprintf(filename,"%s/injected_frequency_domain_stokes_eps1.dat",outdir);                    
      injectedStokesFile[2] = fopen(filename,"w");

      sprintf(filename,"%s/injected_frequency_domain_stokes_eps2.dat",outdir);                    
      injectedStokesFile[3] = fopen(filename,"w");

      double deltaF = 1./data->Tobs;
      
      for (ii = 0; ii < data->N/2; ii++)
      {
      fprintf(injectedStokesFile[0],"%g %g\n", ii*deltaF, stokes_injected[0][ii]);
      fprintf(injectedStokesFile[1],"%g %g\n", ii*deltaF, stokes_injected[1][ii]);
      fprintf(injectedStokesFile[2],"%g %g\n", ii*deltaF, stokes_injected[2][ii]);
      fprintf(injectedStokesFile[3],"%g %g\n", ii*deltaF, stokes_injected[3][ii]);
      }

      fclose(injectedStokesFile[0]);
      fclose(injectedStokesFile[1]);
      fclose(injectedStokesFile[2]);
      fclose(injectedStokesFile[3]);

      free_double_matrix(stokes_injected,3);

      fclose(FDPolsInjFile);
    }
 
  }
  
  // Different structures for whitened and "colored" moments
  struct Moments *whitenedInjectionMoments      = malloc(sizeof(struct Moments));
  struct Moments *whitenedPlusInjectionMoments  = malloc(sizeof(struct Moments));
  struct Moments *whitenedCrossInjectionMoments = malloc(sizeof(struct Moments));
  struct Moments *whitenedPlusSignalMoments     = malloc(sizeof(struct Moments));
  struct Moments *whitenedCrossSignalMoments    = malloc(sizeof(struct Moments));
  struct Moments *geoInjectionMoments           = malloc(sizeof(struct Moments));

  initialize_moments(whitenedInjectionMoments, NI);
  if(xmlInjectionFlag)
  {
    initialize_moments(whitenedPlusInjectionMoments,  NI);
    initialize_moments(whitenedCrossInjectionMoments, NI);
  }
  initialize_moments(whitenedPlusSignalMoments,  NI);
  initialize_moments(whitenedCrossSignalMoments, NI);
  initialize_moments(geoInjectionMoments,        NI);

  struct Moments *coloredInjectionMoments      = malloc(sizeof(struct Moments));
  struct Moments *coloredPlusInjectionMoments  = malloc(sizeof(struct Moments));
  struct Moments *coloredCrossInjectionMoments = malloc(sizeof(struct Moments));

  initialize_moments(coloredInjectionMoments, NI);
  if(xmlInjectionFlag)
  {
    initialize_moments(coloredPlusInjectionMoments,  NI);
    initialize_moments(coloredCrossInjectionMoments, NI);
  }

  // Keep track of dimension distribution for output stats
  int **dimension = malloc(NI*sizeof(int *));
  for(ifo=0; ifo<NI; ifo++) dimension[ifo] = malloc(data->Dmax*sizeof(int));

  // Output injected moments separately
  FILE **whitenedInjFile      = malloc((NI+1)*sizeof(FILE*));
  FILE **coloredInjFile       = malloc((NI+1)*sizeof(FILE*));
  FILE **whitenedPlusInjFile  = malloc((NI+1)*sizeof(FILE*));
  FILE **coloredPlusInjFile   = malloc((NI+1)*sizeof(FILE*));
  FILE **whitenedCrossInjFile = malloc((NI+1)*sizeof(FILE*));
  FILE **coloredCrossInjFile  = malloc((NI+1)*sizeof(FILE*));
  FILE **geoInjFile           = malloc(1*sizeof(FILE*));


  // Output a subsample of all whitened waveforms to a single (gigantic) file

  FILE **injWhitenedSpectrumFile      = malloc(NI*sizeof(FILE*));
  FILE **injWhitenedWaveformFile      = malloc(NI*sizeof(FILE*));
  FILE **injPlusWhitenedWaveformFile  = malloc(NI*sizeof(FILE*));
  FILE **injCrossWhitenedWaveformFile = malloc(NI*sizeof(FILE*));
  FILE **injColoredWaveformFile       = malloc(NI*sizeof(FILE*));
  FILE **injPlusColoredWaveformFile   = malloc(NI*sizeof(FILE*));
  FILE **injCrossColoredWaveformFile  = malloc(NI*sizeof(FILE*));
  FILE **whitenedInjQscanFile         = malloc(NI*sizeof(FILE*));
  FILE **injectedTfFile               = malloc(NI*sizeof(FILE*));
  
  /********************************/
  /*                              */
  /*  Post process data           */
  /*                              */
  /********************************/

  // read data from file made by BayesWaveBurst
  FILE *FDdata = NULL;

  // Output a subsample of all whitened waveforms to a single (gigantic) file
  FILE **whitenedDataFile      = malloc(NI*sizeof(FILE*));
  FILE **coloredDataFile       = malloc(NI*sizeof(FILE*));
  FILE **whitenedDataQscanFile = malloc(NI*sizeof(FILE*));

  double junk;
  
  for(ifo=0; ifo<NI; ifo++)
  {
    sprintf(filename,"waveforms/%sfourier_domain_data_%s.dat",data->runName,data->ifos[ifo]);

    if( (FDdata = fopen(filename,"r")) == NULL)
    {
      fprintf(stdout,"Could not find %s.  Filling data array with 0's\n",filename);
      for(i=0; i<data->N; i++) hoft[ifo][i] = 0.0;

      fclose(FDdata);
    }
    else
    {
      for(i=0; i<data->N/2; i++) fscanf(FDdata,"%lg %lg %lg",&junk,&hoft[ifo][2*i],&hoft[ifo][2*i+1]);
      fclose(FDdata);

      sprintf(filename,"waveforms/%sfourier_domain_residual_%s.dat",data->runName,data->ifos[ifo]);
      if( (FDdata = fopen(filename,"r")) == NULL)
      {
        for(i=0; i<data->N/2; i++)
        {
          roft[ifo][2*i]   = hoft[ifo][2*i];
          roft[ifo][2*i+1] = hoft[ifo][2*i+1];
        }
      }
      else
      {
        for(i=0; i<data->N/2; i++) fscanf(FDdata,"%lg %lg %lg",&junk,&roft[ifo][2*i],&roft[ifo][2*i+1]);
        fclose(FDdata);
      }
      fprintf(stdout, "Getting data into same format as waveforms\n");

    }
  }
  
  //print fourier-domain data and model PSD
  FILE *PSDfile=NULL;
  double dpower;
  double rpower;
  for(ifo=0; ifo<NI; ifo++)
  {
    sprintf(filename,"%s/gaussian_noise_model_%s.dat",outdir,data->ifos[ifo]);
    PSDfile = fopen(filename,"w");
    for(i=PSDimin; i<PSDimax; i++)
    {
      dpower = hoft[ifo][2*i]*hoft[ifo][2*i]+hoft[ifo][2*i+1]*hoft[ifo][2*i+1];
      rpower = roft[ifo][2*i]*roft[ifo][2*i]+roft[ifo][2*i+1]*roft[ifo][2*i+1];
      fprintf(PSDfile,"%g %g %g %g\n",(double)i/data->Tobs,dpower,rpower,psd[ifo][i]);
    }
    fclose(PSDfile);

    sprintf(filename,"%s/colored_data_%s.dat",outdir,data->ifos[ifo]);
    coloredDataFile[ifo] = fopen(filename,"w");

    sprintf(filename,"%s/whitened_data_%s.dat",outdir,data->ifos[ifo]);
    whitenedDataFile[ifo] = fopen(filename,"w");
      

  }
  
  //print time-domain data
  print_waveforms(data, hoft, psd, whitenedDataFile);
  print_waveforms(data, hoft, one, coloredDataFile);
  
  double **hdata = double_matrix(NI-1,data->N-1);
  double **htemp = double_matrix(NI-1,data->N-1);
  double **invPSD = double_matrix(NI-1,data->N/2-1);
  
  
  char datadir[MAXSTRINGSIZE];
  sprintf(datadir,"%spost/data",data->runName);
  mkdir(datadir,S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);

  for(ifo=0; ifo<NI; ifo++)
  {
    whiten_data(data->imin, data->imax, hoft[ifo], psd[ifo]);
   
    get_time_domain_waveforms(hdata[ifo], hoft[ifo], data->N, data->imin, data->imax);
    
    norm = sqrt((double)data->N);
    for (ii = 0; ii < data->N; ii++)  hdata[ifo][ii] /= norm;
    
    if (!lite){
      print_spectrograms(data,datadir,"data",data->ifos[ifo], hdata[ifo], one[ifo]);
    }
    for (ii = 0; ii < data->N/2; ii++) invPSD[ifo][ii] = 1.0/psd[ifo][ii];
    
    //recolor h(t)-- need hoft to check fourier domain residuals
    whiten_data(data->imin, data->imax, hoft[ifo], invPSD[ifo]);
  }
  
  
  for(ifo=0; ifo<NI; ifo++)
  {
    fclose(whitenedDataFile[ifo]);
    fclose(coloredDataFile[ifo]);
  }
  
  free(whitenedDataFile);
  free(coloredDataFile);
  free(whitenedDataQscanFile);
  
  

  /********************************/
  /*                              */
  /*  Post process injection      */
  /*                              */
  /********************************/
  if(injectionFlag)
  {
    
    fprintf(stdout, "Computing moments for injected waveform\n");

    for(ifo=0; ifo<NI; ifo++)
    {
      sprintf(filename,"%s/injected_whitened_waveform_%s.dat",outdir,data->ifos[ifo]);
      injWhitenedWaveformFile[ifo] = fopen(filename,"w");
      sprintf(filename,"%s/injected_whitened_spectrum_%s.dat",outdir,data->ifos[ifo]);
      injWhitenedSpectrumFile[ifo] = fopen(filename,"w");
      if(xmlInjectionFlag)
      {
        fprintf(stdout, "Outputting plus and cross injected...\n");
      
        sprintf(filename,"%s/injected_plus_whitened_waveform_%s.dat",outdir,data->ifos[ifo]);
        injPlusWhitenedWaveformFile[ifo] = fopen(filename,"w");
        sprintf(filename,"%s/injected_cross_whitened_waveform_%s.dat",outdir,data->ifos[ifo]);
        injCrossWhitenedWaveformFile[ifo] = fopen(filename,"w");
      }
      
      sprintf(filename,"%s/injected_colored_waveform_%s.dat",outdir,data->ifos[ifo]);
      injColoredWaveformFile[ifo] = fopen(filename,"w");
      if(xmlInjectionFlag)
      {
        sprintf(filename,"%s/injected_plus_colored_waveform_%s.dat",outdir,data->ifos[ifo]);
        injPlusColoredWaveformFile[ifo] = fopen(filename,"w");
        sprintf(filename,"%s/injected_cross_colored_waveform_%s.dat",outdir,data->ifos[ifo]);
        injCrossColoredWaveformFile[ifo] = fopen(filename,"w");
      }
      
      sprintf(filename,"%s/injected_tf_%s.dat",outdir,data->ifos[ifo]);
      injectedTfFile[ifo] = fopen(filename,"w");
    }

    setup_output_files(NI, injectionFlag, outdir, "injection", "whitened", whitenedInjFile, data);
    setup_output_files(NI, injectionFlag, outdir, "injection", "colored", coloredInjFile, data);

    setup_output_files(-1, injectionFlag, outdir, "injection", "geocenter", geoInjFile, data);

    if(xmlInjectionFlag)
    {
      setup_output_files(NI, injectionFlag, outdir, "injectionPlus", "whitened", whitenedPlusInjFile,data);
      setup_output_files(NI, injectionFlag, outdir, "injectionPlus", "colored", coloredPlusInjFile, data);
      setup_output_files(NI, injectionFlag, outdir, "injectionCross", "whitened", whitenedCrossInjFile, data);
      setup_output_files(NI, injectionFlag, outdir, "injectionCross", "colored", coloredCrossInjFile,data);
    }

    for(ifo=0; ifo<NI; ifo++)
    {
      whitenedInjectionMoments->overlap[ifo] = 1.0;
      coloredInjectionMoments->overlap[ifo]  = 1.0;
      if(xmlInjectionFlag)
      {
        whitenedPlusInjectionMoments->overlap[ifo]  = 1.0;
        coloredPlusInjectionMoments->overlap[ifo]   = 1.0;
        whitenedCrossInjectionMoments->overlap[ifo] = 1.0;
        coloredCrossInjectionMoments->overlap[ifo]  = 1.0;
      }
    }
    whitenedInjectionMoments->networkOverlap = 1.0;
    coloredInjectionMoments->networkOverlap  = 1.0;
    geoInjectionMoments->networkOverlap = 1.0;

    ComputeWaveformMoments(data, hinj, psd, whitenedInjectionMoments); // INJECTION
    ComputeWaveformMoments(data, hinj, one, coloredInjectionMoments);

    /* Calculate "geocenter" */
    //ComputeGeocenterMoments(data, deltaF, deltaT, hinj[0], one[0], geoInjectionMoments);
    //print_moments(0, injectionFlag, geoInjFile, geoInjectionMoments);

    if(xmlInjectionFlag)
    {
      ComputeWaveformMoments(data, hinjPlus, psd, whitenedPlusInjectionMoments);
      ComputeWaveformMoments(data, hinjPlus, one, coloredPlusInjectionMoments);
      ComputeWaveformMoments(data, hinjCross, psd, whitenedCrossInjectionMoments);
      ComputeWaveformMoments(data, hinjCross, one, coloredCrossInjectionMoments);
    }

    
    // Whitened hrss is meaningless and colored SNR are meaningless
     //Swap their values in the structures so the moments files are self-contained

     for(ifo=0; ifo<NI; ifo++)
     {
     whitenedInjectionMoments->energy[ifo] = coloredInjectionMoments->energy[ifo];
     coloredInjectionMoments->snr[ifo]     = whitenedInjectionMoments->snr[ifo];
     }


    
    //Detector-by-detector moments
    for(ifo=0; ifo<NI; ifo++)
    {
      print_moments(ifo, injectionFlag, whitenedInjFile, whitenedInjectionMoments);
      print_moments(ifo, injectionFlag, coloredInjFile, coloredInjectionMoments);
      if(xmlInjectionFlag)
      {
        print_moments(ifo, injectionFlag, whitenedPlusInjFile, whitenedPlusInjectionMoments);
        print_moments(ifo, injectionFlag, coloredPlusInjFile, coloredPlusInjectionMoments);
        print_moments(ifo, injectionFlag, whitenedCrossInjFile, whitenedCrossInjectionMoments);
        print_moments(ifo, injectionFlag, coloredCrossInjFile, coloredCrossInjectionMoments);
      }
    }

    //Print injected time-domain waveforms (and their + and x polarizations if possible)
    if(xmlInjectionFlag)
    {
      
      //LALinference injections are windowed to improve FFTs to the time domain
      double **windowed_injection = malloc(NI*sizeof(double*));
      for(ifo=0; ifo<NI; ifo++) windowed_injection[ifo] = malloc(N*sizeof(double));
      //Window and output hinj in the time domain
      print_waveforms(data, windowed_injection, psd, injWhitenedWaveformFile);
      print_waveforms(data, windowed_injection, one, injColoredWaveformFile);
      print_spectrum(data, windowed_injection, injWhitenedSpectrumFile);
      //Window and output hinjPlus in the time domain
      print_waveforms(data, windowed_injection, psd, injPlusWhitenedWaveformFile);
      print_waveforms(data, windowed_injection, one, injPlusColoredWaveformFile);
      //Window and output hinjCross in the time domain
      print_waveforms(data, windowed_injection, psd, injCrossWhitenedWaveformFile);
      print_waveforms(data, windowed_injection, one, injCrossColoredWaveformFile);
        
      for (ifo = 0; ifo < NI; ifo++)
      {
        for (i = 0; i<data->N; i++)  hinj_white[ifo][i] = hinj[ifo][i];
        
        whiten_data(data->imin, data->imax, hinj_white[ifo], psd[ifo]);
        
        get_time_domain_waveforms(htemp[ifo], hinj_white[ifo], data->N, data->imin, data->imax);
        
        print_spectrograms(data,outdir,"injected",data->ifos[ifo], htemp[ifo], one[ifo]);
       }
    }
    else
    {
      print_waveforms(data, hinj, psd, injWhitenedWaveformFile);
      print_waveforms(data, hinj, one, injColoredWaveformFile);
      
      print_spectrum(data, hinj, injWhitenedSpectrumFile);
      
      
      for (ifo = 0; ifo < NI; ifo++)
      {
        
        for (i = 0; i<data->N; i++) hinj_white[ifo][i] = hinj[ifo][i];
        
        whiten_data(data->imin, data->imax, hinj_white[ifo], psd[ifo]);
        
        get_time_domain_waveforms(htemp[ifo], hinj_white[ifo], data->N, data->imin, data->imax);
        
        print_spectrograms(data,outdir,"injected",data->ifos[ifo], htemp[ifo], one[ifo]);

        // injected tf
        tf_tracks(htemp[ifo], hinj_white[ifo], deltaT, data->N);
        for (i = 0; i < data->N; i++)   fprintf(injectedTfFile[ifo],"%g %g\n",i*deltaT,hinj_white[ifo][i]);
        
      }
    }

    for(ifo=0; ifo<NI; ifo++)
    {
      fclose(injWhitenedWaveformFile[ifo]);
      fclose(injColoredWaveformFile[ifo]);
      fclose(whitenedInjFile[ifo]);
      fclose(coloredInjFile[ifo]);
      if(xmlInjectionFlag)
      {
        fclose(injPlusWhitenedWaveformFile[ifo]);
        fclose(injCrossWhitenedWaveformFile[ifo]);
        fclose(injPlusColoredWaveformFile[ifo]);
        fclose(injCrossColoredWaveformFile[ifo]);
        fclose(whitenedPlusInjFile[ifo]);
        fclose(whitenedCrossInjFile[ifo]);
        fclose(coloredPlusInjFile[ifo]);
        fclose(coloredCrossInjFile[ifo]);
          fclose(injectedTfFile[ifo]);
      }
    }

    fclose(geoInjFile[0]);
    free(geoInjFile);

    free(injWhitenedWaveformFile);
    free(injColoredWaveformFile);
    free(whitenedInjFile);
    free(coloredInjFile);
    if(xmlInjectionFlag)
    {
      free(injPlusWhitenedWaveformFile);
      free(injCrossWhitenedWaveformFile);
      free(injPlusColoredWaveformFile);
      free(injCrossColoredWaveformFile);
      free(whitenedPlusInjFile);
      free(whitenedCrossInjFile);
      free(coloredPlusInjFile);
      free(coloredCrossInjFile);
        free(injectedTfFile);
        
        
    }
      free(whitenedInjQscanFile);
      free_double_matrix(htemp,NI-1);
      

  }

  /***************************************/
  /*                                     */
  /*  Post process signal+glitch model   */
  /*                                     */
  /***************************************/
  if(data->fullModelFlag)
  {
    post_process_model("full", "signal", data, model, chain, hdata, hoft, psd, one, hinj, dimension, injectionFlag, GPStrig, lite);
    post_process_model("full", "glitch", data, model, chain, hdata, hoft, psd, one, hinj, dimension, injectionFlag, GPStrig, lite);
    post_process_model("full", "full",   data, model, chain, hdata, hoft, psd, one, hinj, dimension, injectionFlag, GPStrig, lite);
  }

  /***************************************/
  /*                                     */
  /*  Post process cbc+glitch model      */
  /*                                     */
  /***************************************/
  if(data->GlitchCBCFlag)
  {
    post_process_model("cbcglitch", "cbc", data, model, chain, hdata, hoft, psd, one, hinj, dimension, injectionFlag, GPStrig, lite);
    post_process_model("cbcglitch", "glitch", data, model, chain, hdata, hoft, psd, one, hinj, dimension, injectionFlag, GPStrig, lite);
    post_process_model("cbcglitch", "cbcglitch", data, model, chain, hdata, hoft, psd, one, hinj, dimension, injectionFlag, GPStrig, lite);
  }

  /***************************************/
  /*                                     */
  /*  Post process cbc+signal model      */
  /*                                     */
  /***************************************/
  if(data->SignalCBCFlag)
  {
    fprintf(stdout, "running post_process_model on CBC signal\n");
    post_process_model("cbcsignal", "cbc", data, model, chain, hdata, hoft, psd, one, hinj, dimension, injectionFlag, GPStrig, lite);
    post_process_model("cbcsignal", "signal", data, model, chain, hdata, hoft, psd, one, hinj, dimension, injectionFlag, GPStrig, lite);
    post_process_model("cbcsignal", "cbcsignal", data, model, chain, hdata, hoft, psd, one, hinj, dimension, injectionFlag, GPStrig, lite);
  }


  /***************************************/
  /*                                     */
  /*  Post process cbc model             */
  /*                                     */
  /***************************************/
  if(data->cbcModelFlag)
  {
    post_process_model("cbc", "cbc", data, model, chain, hdata, hoft, psd, one, hinj, dimension, injectionFlag, GPStrig, lite);
  }
  
  /***************************************/
  /*                                     */
  /*  Post process signal model          */
  /*                                     */
  /***************************************/
  if(data->signalModelFlag) post_process_model("signal", "signal", data, model, chain, hdata, hoft, psd, one, hinj, dimension, injectionFlag, GPStrig, lite);
 

  /***************************************/
  /*                                     */
  /*  Post process glitch model          */
  /*                                     */
  /***************************************/
  if(data->glitchModelFlag) post_process_model("glitch", "glitch", data, model, chain, hdata, hoft, psd, one, hinj, dimension, injectionFlag, GPStrig, lite);

  
  /*****************************************/
  /*                                       */
  /*  Post process cleaning phase          */
  /*                                       */
  /*****************************************/
  if(data->cleanModelFlag) post_process_model("clean", "glitch", data, model, chain, hdata, hoft, psd, one, hinj, dimension, injectionFlag, GPStrig, lite);

  
  /*****************************************/
  /*                                       */
  /*  Post process noise phase             */
  /*                                       */
  /*****************************************/
  if(data->noiseModelFlag) post_process_model("noise", "noise", data, model, chain, hdata, hoft, psd, one, hinj, dimension, injectionFlag, GPStrig, lite);
  
  
  /*****************************************/
  /*                                       */
  /*  Free memory, exit cleanly            */
  /*                                       */
  /*****************************************/
  
  free_double_matrix(hdata,NI-1);
  
  if(data->nukeFlag)
  {
    fprintf(stdout, " ======= rm'ing chains/ and checkpoint/ ======\n");
    char command[MAXSTRINGSIZE];
    sprintf(command,"rm -rvf chains checkpoint");
    system(command);
  }
  
  fprintf(stdout, " ======= DONE! ======\n");
  
  return 0;
  
}


void print_help_message(void)
{
  fprintf(stdout,"\n ======== BayesWavePost USAGE: =======\n\n");
  fprintf(stdout,"REQUIRED:\n");
  fprintf(stdout,"  --ifo IFO              interferometer (H1,L1,V1)\n");
  fprintf(stdout,"  --IFO-flow             minimum frequency (Hz)\n");
  fprintf(stdout,"  --IFO-cache LALAdLIGO  lie about the PSD\n");
  fprintf(stdout,"  --trigtime             GPS trigger time\n");
  fprintf(stdout,"  --srate                sampling rate (Hz)\n");
  fprintf(stdout,"  --seglen               duration of data (s)\n");
  fprintf(stdout,"  --PSDstart             GPS start time for PSD estimation\n");
  fprintf(stdout,"  --PSDlength            duration of PSD estimation length\n");
  fprintf(stdout,"  --dataseed 1234        Lie about random seed for data\n");
  fprintf(stdout,"  --0noise               no noise realization\n");
  fprintf(stdout,"\n");
  fprintf(stdout,"OPTIONAL:\n");
  fprintf(stdout,"  ----------------------------------------------------------------------------------\n");
  fprintf(stdout,"  --- Run parameters   -------------------------------------------------------------\n");
  fprintf(stdout,"  ----------------------------------------------------------------------------------\n");
  fprintf(stdout,"  --runName              run name for output files\n");
  fprintf(stdout,"  --nuke                 delete chains/ and checkpoint/ when done [NOT RECOMMENDED]\n");
  fprintf(stdout,"\n");
  fprintf(stdout,"  ----------------------------------------------------------------------------------\n");
  fprintf(stdout,"  --- Model parameters   -----------------------------------------------------------\n");
  fprintf(stdout,"  ----------------------------------------------------------------------------------\n");
  fprintf(stdout,"  --bayesLine            use BayesLine for PSD model\n");
  fprintf(stdout,"\n");
  fprintf(stdout,"  ----------------------------------------------------------------------------------\n");
  fprintf(stdout,"  --- LALInference injection  options  ---------------------------------------------\n");
  fprintf(stdout,"  ----------------------------------------------------------------------------------\n");
  fprintf(stdout,"  --inj injfile.xml      Injection XML file to use\n");
  fprintf(stdout,"  --event N              Event number from Injection XML file to use\n");
  fprintf(stdout,"\n");
  fprintf(stdout,"  ----------------------------------------------------------------------------------\n");
  fprintf(stdout,"  --- Burst MDC injection  options  ------------------------------------------------\n");
  fprintf(stdout,"  ----------------------------------------------------------------------------------\n");
  fprintf(stdout,"  --MDC-channel          IFO1-chan, IFO2-chan, etc\n");
  fprintf(stdout,"  --MDC-cache            IFO1-mdcframe, IFO2-mdcframe, etc\n");
  fprintf(stdout,"  --MDC-prefactor        Rescale injection amplitude (1.0)\n");
  fprintf(stdout,"\n");
  fprintf(stdout,"  ----------------------------------------------------------------------------------\n");
  fprintf(stdout,"  --- BayesWave internal injection options  ----------------------------------------\n");
  fprintf(stdout,"  ----------------------------------------------------------------------------------\n");
  fprintf(stdout,"  --BW-inject            (signal/glitch)\n");
  fprintf(stdout,"  --BW-injName           runName that produced the chain file \n");
  fprintf(stdout,"  --BW-path              Path to BW chain file for injection (./chains) \n");
  fprintf(stdout,"  --BW-event             Which sample from BayesWave chain (200000)\n");
  fprintf(stdout,"\n");
  fprintf(stdout,"EXAMPLE:\n");
  fprintf(stdout,"BayesWavePost --ifo H1 --H1-flow 32 --H1-cache LALSimAdLIGO --H1-channel LALSimAdLIGO --trigtime 900000000.00 --srate 512 --seglen 4 --PSDstart 900000000 --PSDlength 1024 --dataseed 1234 --0noise\n");
  fprintf(stdout,"\n");
}



void ComputeWaveformOverlap(struct Data *data, double **hinj, double **hrec, double **psd, struct Moments *m)
{
  int N = data->N;
  int NI = data->NI;
  int imin = data->imin;

  int ifo,n;

  double **invpsd = malloc(NI*sizeof(double *));
  for(ifo=0; ifo<NI; ifo++) invpsd[ifo] = malloc(N/2*sizeof(double));

  //get input waveform into right format (either colored or whitened)
  for(ifo=0; ifo<NI; ifo++)
  {
    for(n=0; n<N/2; n++)
    {
      invpsd[ifo][n] = 1/psd[ifo][n];
    }
  }

  double hihi; //Detector (hinj|hinj)
  double hrhr; //Detector (hrec|hrec)
  double hrhi; //Detector (hrec|hinj)
  double ii=0; //Network (hinj|hinj)
  double rr=0; //Network (hrec|hrec)
  double ri=0; //Network (hrec|hinj)

  for(ifo=0; ifo<NI; ifo++)
  {
    hihi = fourier_nwip(imin, N/2, hinj[ifo], hinj[ifo], invpsd[ifo]);
    hrhr = fourier_nwip(imin, N/2, hrec[ifo], hrec[ifo], invpsd[ifo]);
    hrhi = fourier_nwip(imin, N/2, hrec[ifo], hinj[ifo], invpsd[ifo]);

    ii += hihi;
    rr += hrhr;
    ri += hrhi;

    //Store inj-rec overlap of each detector to file
    m->overlap[ifo] =  hrhi/sqrt(hihi*hrhr); // (hrec|hinj)/sqrt((hrec|hrec)(hinj|hinj))
  }

  //Store inj-rec networ overlap to file
  m->networkOverlap = ri/sqrt(ii*rr); // (hrec|hinj)/sqrt((hrec|hrec)(hinj|hinj))

  for(ifo=0; ifo<NI; ifo++) free(invpsd[ifo]);
  free(invpsd);

}

void PrintRhof(struct Data *data, double deltaF, double **h, double **psd, struct Moments *m){
  int N = data->N;
  int NI = data->NI;
  int imin = data->imin;
  int imax = data->imax;
  int i;

  int ifo,n;
  double *rho = malloc(N*sizeof(double));
  double **wf = malloc(NI*sizeof(double *));

  double **invPSD = malloc(NI*sizeof(double *));

  for(ifo=0; ifo<NI; ifo++)
  {
    wf[ifo]=malloc(N*sizeof(double));

    invPSD[ifo] = malloc(N/2*sizeof(double));
  }

  //get input waveform into right format (either colored or whitened)
  for(ifo=0; ifo<NI; ifo++)
  {
    for(n=0; n<N/2; n++)
    {
      invPSD[ifo][n] = 1/psd[ifo][n];
      wf[ifo][2*n]   = h[ifo][2*n]/sqrt(psd[ifo][n]);
      wf[ifo][2*n+1] = h[ifo][2*n+1]/sqrt(psd[ifo][n]);
    }
  }

  for(ifo=0; ifo<=NI; ifo++)
  {
    /*
     RECOVERED SIGNAL ENERGY
     */
    if(ifo<NI) m->energy[ifo] = get_rhof(imin, imax, wf[ifo], deltaF, rho);
    else m->energy[ifo] = get_rhof_net(imin, imax, wf, deltaF, rho, NI);

    // *********** print rho **************** //


    if(ifo==0){ // do only for H1
      printf("\n\n\n");
      for (i=0; i<imax; i+=10) {
        printf("%g %g\n",(double)i*deltaF,rho[i]);
      }
      printf("\n\n\n");
    }

  }

  for(ifo=0; ifo<NI; ifo++)
  {
    free(wf[ifo]);

    free(invPSD[ifo]);
  }
  free(rho);
  free(wf);

  free(invPSD);

}


void ComputeWaveformMoments(struct Data *data, double **h, double **psd, struct Moments *m)
{
  int N = data->N;
  int NI = data->NI;
  int imin = data->imin;
  int imax = data->imax;

  double deltaT = data->Tobs/(double)N;
  double deltaF = 1./data->Tobs;
  
  int ifo,n;
  double *rho = malloc(N*sizeof(double));
  double **wt = malloc(NI*sizeof(double *));
  double **wf = malloc(NI*sizeof(double *));

  double **invPSD = malloc(NI*sizeof(double *));

  for(ifo=0; ifo<NI; ifo++)
  {
    wt[ifo]=malloc(N*sizeof(double));
    wf[ifo]=malloc(N*sizeof(double));

    invPSD[ifo] = malloc(N/2*sizeof(double));
  }

  //get input waveform into right format (either colored or whitened)
  for(ifo=0; ifo<NI; ifo++)
  {
    for(n=0; n<N/2; n++)
    {
      invPSD[ifo][n] = 1/psd[ifo][n];
      wf[ifo][2*n]   = h[ifo][2*n]/sqrt(psd[ifo][n]);
      wf[ifo][2*n+1] = h[ifo][2*n+1]/sqrt(psd[ifo][n]);
        
    }

    //transform waveform into time-domain
    get_time_domain_waveforms(wt[ifo], wf[ifo], N, imin, imax);
  }

  //SNR
  for(ifo=0; ifo<NI; ifo++) m->snr[ifo] = detector_snr(imin, imax, h[ifo], invPSD[ifo], 1.0);

  /******************************/
  /*                            */
  /*  Frequency domain moments  */
  /*                            */
  /******************************/

  for(ifo=0; ifo<=NI; ifo++)
  {
    /*
     RECOVERED SIGNAL ENERGY
     */
    if(ifo<NI)
      m->energy[ifo] = get_rhof(imin, imax, wf[ifo], deltaF, rho);
    else
      m->energy[ifo] = get_rhof_net(imin, imax, wf, deltaF, rho, NI);

      
      
      
    /*
     RECOVERED CENTRAL FREQUENCY
     */
    m->frequency[ifo] = get_f0(imax, rho, deltaF);


    /*
     RECOVERED BANDWIDTH
     */
    m->bandwidth[ifo] = get_band(imax, rho, deltaF, m->frequency[ifo]);

    /* Median f and 90% interval bw */
    //get_f_quantiles(imin, imax, rho, deltaF, &fmedtemp, &bwtemp);

    //m->medianfrequency[ifo] = fmedtemp;
    //m->bandwidth90percentinterval[ifo] = bwtemp;


  }



  /******************************/
  /*                            */
  /*     Time domain moments    */
  /*                            */
  /******************************/

  for(ifo=0; ifo<=NI; ifo++)
  {
    /*
     RECOVERED SIGNAL ENERGY
     */
    if(ifo<NI)
      m->energy[ifo] = get_rhot(N, wt[ifo], deltaT, rho);
    else
      m->energy[ifo] = get_rhot_net(N, wt, deltaT, rho, NI);
      
      

    /*
     RECOVERED CENTRAL TIME
     */
    m->time[ifo] = get_f0(N, rho, deltaT);

    /*
     RECOVERED WAVEFORM DURATION
     */
    m->duration[ifo] = get_band(N, rho, deltaT, m->time[ifo]);

    /*
     Median t and quantiles
     */

    //get_t_quantiles(N, rho, deltaT, &tmedtemp, &durtemp);
    //m->mediantime[ifo] = tmedtemp;
    //m->duration90percentinterval[ifo] = durtemp;



    /*
     WAVEFORM MAXIMUM AND TIME AT MAXIMUM
     */
    if(ifo<NI)
    {
      get_hmax_and_t(N, wt[ifo], deltaT, &m->hmax[ifo], &m->t_at_hmax[ifo]);
    }

    /*
      Frquency at maximum amplitude                                                                                                                                    
    */
    if(ifo<NI)
    {
      get_freq_at_max(wf[ifo], N, imin, imax, deltaT, &m->f_at_max_amp[ifo]);
    }


  }


  for(ifo=0; ifo<NI; ifo++)
  {
    free(wf[ifo]);
    free(wt[ifo]);

    free(invPSD[ifo]);
  }
  free(rho);
  free(wt);
  free(wf);

  free(invPSD);

}

void ComputeGeocenterMoments(struct Data *data, double deltaF, double deltaT, double *h, double *psd, struct Moments *m)
{
  int N = data->N;
  int imin = data->imin;
  int imax = data->imax;
  double tmedtemp, durtemp, fmedtemp, bwtemp;

  int n;
  double *rho = malloc(N*sizeof(double));
  double *rhop = malloc(N*sizeof(double));
  double *wt  = malloc(N*sizeof(double));
  double *wf  = malloc(N*sizeof(double));
  double *hdot  = malloc(N*sizeof(double));

  double *invPSD = malloc(N/2*sizeof(double));


  //get input waveform into right format (either colored or whitened)
  for(n=0; n<N/2; n++)
  {
    wf[2*n]   = h[2*n]/sqrt(psd[n]);
    wf[2*n+1] = h[2*n+1]/sqrt(psd[n]);

    invPSD[n] = 1./psd[n];

  }

  //transform waveform into time-domain
  get_time_domain_waveforms(wt, wf, N, imin, imax);

  //SNR
  m->snr[0] = detector_snr(imin, imax, h, invPSD, 1.0);

  /******************************/
  /*                            */
  /*  Frequency domain moments  */
  /*                            */
  /******************************/

  /*
   RECOVERED SIGNAL ENERGY
   */
  m->energy[0] = get_rhof(imin, imax, wf, deltaF, rho);




  /*     New energy weighted rho    */
  //temp = get_rhof_f2weighted(imin, imax, wf, deltaF, rhop);

  /*
   RECOVERED CENTRAL FREQUENCY
   */
  m->frequency[0] = get_f0(imax, rhop, deltaF);


  /*
   RECOVERED BANDWIDTH
   */
  m->bandwidth[0] = get_band(imax, rhop, deltaF, m->frequency[0]);


  /* Median f and bw by quantiles*/

  get_f_quantiles(imin, imax, rhop, deltaF, &fmedtemp, &bwtemp);

  m->medianfrequency[0] = fmedtemp;
  m->bandwidth90percentinterval[0] = bwtemp;


  /******************************/
  /*                            */
  /*     Time domain moments    */
  /*                            */
  /******************************/


  /*
   RECOVERED SIGNAL ENERGY
   */
  m->energy[0] = get_rhot(N, wt, deltaT, rho);



  /*
   WAVEFORM MAXIMUM AND TIME AT MAXIMUM
   */
  get_hmax_and_t(N, wt, deltaT, &m->hmax[0], &m->t_at_hmax[0]);


  /* Get energy weighted rho (use hdot) */

  get_time_domain_hdot(hdot, wf, N, imin, imax, deltaF);

  get_rhot(N, hdot, deltaT, rho);

  /*
   RECOVERED CENTRAL TIME
   */
  m->time[0] = get_f0(N, rho, deltaT);

  /*
   RECOVERED WAVEFORM DURATION
   */
  m->duration[0] = get_band(N, rho, deltaT, m->time[0]);


  /* Median t and duration by quantiles */


  get_t_quantiles(N, rho, deltaT, &tmedtemp, &durtemp);
  m->mediantime[0] = tmedtemp;
  m->duration90percentinterval[0] = durtemp;


  free(invPSD);
  free(rho);
  free(wt);
  free(wf);

}


void initialize_moments(struct Moments *m, int NI)
{
  m->overlap   = malloc((NI+1)*sizeof(double));
  m->energy    = malloc((NI+1)*sizeof(double));
  m->frequency = malloc((NI+1)*sizeof(double));
  m->time      = malloc((NI+1)*sizeof(double));
  m->bandwidth = malloc((NI+1)*sizeof(double));
  m->duration  = malloc((NI+1)*sizeof(double));
  m->snr       = malloc((NI+1)*sizeof(double));
  m->hmax      = malloc((NI+1)*sizeof(double));
  m->t_at_hmax = malloc((NI+1)*sizeof(double));
  m->f_at_max_amp = malloc((NI+1)*sizeof(double));
  m->medianfrequency = malloc((NI+1)*sizeof(double));
  m->bandwidth90percentinterval = malloc((NI+1)*sizeof(double));
  m->mediantime = malloc((NI+1)*sizeof(double));
  m->duration90percentinterval = malloc((NI+1)*sizeof(double));
  m->tl = malloc((NI+1)*sizeof(double));
  m->th = malloc((NI+1)*sizeof(double));
}

void free_moments(struct Moments *m)
{
  free(m->overlap);
  free(m->energy);
  free(m->frequency);
  free(m->time);
  free(m->bandwidth);
  free(m->duration);
  free(m->snr);
  free(m->hmax);
  free(m->t_at_hmax);
  free(m->f_at_max_amp);
  free(m->medianfrequency);
  free(m->bandwidth90percentinterval);
  free(m->mediantime );
  free(m->duration90percentinterval);
  free(m->tl);
  free(m->th);
  free(m);
}


void setup_output_files(int NI, int injectionFlag, char *outdir, char *model, char *weighting, FILE **momentsOut, struct Data *data)
{
  int ifo;
  char filename[MAXSTRINGSIZE];

  //Open files
  if(NI<0)
  {
    sprintf(filename,"%s/%s_%s_moments.dat.geo", outdir, model, weighting);
    momentsOut[0] = fopen(filename,"w");

    //Write headers
    fprintf(momentsOut[0], "# ");
    fprintf(momentsOut[0], "snr ");
    fprintf(momentsOut[0], "t_energy_rec ");
    fprintf(momentsOut[0], "hrss ");
    fprintf(momentsOut[0], "t0_rec ");
    fprintf(momentsOut[0], "dur_rec ");
    fprintf(momentsOut[0], "f0_rec ");
    fprintf(momentsOut[0], "band_rec ");
//    fprintf(momentsOut[0], "median_t0 ");
//    fprintf(momentsOut[0], "dur_quantile ");
//    fprintf(momentsOut[0], "median_f0 ");
//    fprintf(momentsOut[0], "band_quantile ");
    if(injectionFlag)
    {
      fprintf(momentsOut[0], "overlap ");
      fprintf(momentsOut[0], "network_overlap ");
    }
    fprintf(momentsOut[0], "h_max ");
    fprintf(momentsOut[0], "t_at_h_max ");
    fprintf(momentsOut[0], "f_at_max_amp");
    fprintf(momentsOut[0], "\n");
  }

  else
  {
    for(ifo=0; ifo<NI; ifo++)
    {
      sprintf(filename,"%s/%s_%s_moments_%s.dat", outdir, model, weighting, data->ifos[ifo]);
      momentsOut[ifo] = fopen(filename,"w");
    }
    sprintf(filename,"%s/%s_%s_moments.dat.net", outdir, model, weighting);
    momentsOut[NI] = fopen(filename,"w");

    //Write headers
    for(ifo=0; ifo<=NI; ifo++)
    {
      fprintf(momentsOut[ifo], "# ");
      fprintf(momentsOut[ifo], "snr ");
      fprintf(momentsOut[ifo], "t_energy_rec ");
      fprintf(momentsOut[ifo], "hrss ");
      fprintf(momentsOut[ifo], "t0_rec ");
      fprintf(momentsOut[ifo], "dur_rec ");
      fprintf(momentsOut[ifo], "f0_rec ");
      fprintf(momentsOut[ifo], "band_rec ");
//      fprintf(momentsOut[ifo], "median_t0 ");
//      fprintf(momentsOut[ifo], "dur_quantile ");
//      fprintf(momentsOut[ifo], "median_f0 ");
//      fprintf(momentsOut[ifo], "band_quantile ");
      if(injectionFlag)
      {
        fprintf(momentsOut[ifo], "overlap ");
        fprintf(momentsOut[ifo], "network_overlap ");
      }
      fprintf(momentsOut[ifo], "h_max ");
      fprintf(momentsOut[ifo], "t_at_h_max ");
      fprintf(momentsOut[ifo], "f_at_max_amp");
      fprintf(momentsOut[ifo], "\n");
    }
  }
}


void discard_burnin_samples(FILE *params, int *COUNT, int *BURNIN)
{
  int n;
  char burnrows[100000];
  (*COUNT) = 0;
  while(!feof(params))
  {
    fgets(burnrows,100000,params);
    (*COUNT)++;
  }
  (*COUNT)--;
  //(*BURNIN) = 9990; //(*COUNT)/2; // 9990
  (*BURNIN) = (*COUNT)/2; // 9990
  rewind(params);

  for(n=0; n<(*BURNIN); n++) fgets(burnrows,100000,params);
}

void print_moments(int ifo, int injectionFlag, FILE **outfile, struct Moments *m)
{
  fprintf(outfile[ifo], "%e ",m->snr[ifo]);
  fprintf(outfile[ifo], "%e ",m->energy[ifo]);
  fprintf(outfile[ifo], "%e ",log10(sqrt(m->energy[ifo]/2.)));
  fprintf(outfile[ifo], "%e ",m->time[ifo]);
  fprintf(outfile[ifo], "%e ",m->duration[ifo]);
  fprintf(outfile[ifo], "%e ",m->frequency[ifo]);
  fprintf(outfile[ifo], "%e ",m->bandwidth[ifo]);
  // New moments
//  fprintf(outfile[ifo], "%e ",m->mediantime[ifo]);
//  fprintf(outfile[ifo], "%e ",m->duration90percentinterval[ifo]);
//  fprintf(outfile[ifo], "%e ",m->medianfrequency[ifo]);
//  fprintf(outfile[ifo], "%e ",m->bandwidth90percentinterval[ifo]);
  // end new moments
  if(injectionFlag)
  {
    fprintf(outfile[ifo], "%e ",m->overlap[ifo]);
    //if(injectionFlag)fprintf(outfile[ifo], "%e\n",m->overlap[ifo]);
    fprintf(outfile[ifo], "%e ",m->networkOverlap);
  }
  fprintf(outfile[ifo], "%e ",m->hmax[ifo]);
  fprintf(outfile[ifo], "%e ",m->t_at_hmax[ifo]);
  fprintf(outfile[ifo], "%e ",m->f_at_max_amp[ifo]);
  fprintf(outfile[ifo], "\n");
}

void print_waveforms(struct Data *data, double **h, double **psd, FILE **ofile)
{
  int i;
  int ifo;

  int N = data->N;
  int NI = data->NI;
  int imin = data->imin;
  int imax = data->imax;

  double *hoft = malloc(N*sizeof(double));
  double *w    = malloc(N*sizeof(double));

  double norm = sqrt((double)N);

  for(ifo=0; ifo<NI; ifo++)
  {
    //whiten waveforms
    for(i=0; i<N/2; i++)
    {
      w[2*i]   =   h[ifo][2*i] / sqrt(psd[ifo][i]);
      w[2*i+1] = h[ifo][2*i+1] / sqrt(psd[ifo][i]);
    }

    get_time_domain_waveforms(hoft, w, N, imin, imax);

    for(i=0; i<N; i++)
    {
      hoft[i]/=norm;
    }

    for(i=0; i<N; i++)
      fprintf(ofile[ifo], "%e ", hoft[i]);
    //fprintf(ofile[ifo], "%e\n", hoft[i]);

    fprintf(ofile[ifo], "\n");
  }

  free(hoft);
  free(w);
}

void tf_tracks(double *hoft, double *frequency_out, double deltaT, int N){
    int i, ii, jj;
    
    double slope, intercept, zero, freq;
    
    double *w    = malloc(N*sizeof(double));
    double **tf_raw = malloc(2*sizeof(double *));
    double **tf_track = malloc(2*sizeof(double *));
    double *zerotimes = malloc(N*sizeof(double));
    
    for (i = 0; i < 2; i++) {
        tf_raw[i] = malloc(N*sizeof(double));
        tf_track[i] = malloc(N*sizeof(double));
    }
    
    for (ii = 0; ii < N; ii++) {
        tf_track[0][ii] = (double)ii*deltaT;
    }
    
    jj = 0;
  
    // Get f of t from zero crossings
    for (ii = 0; ii <= 2; ii++) {
        zerotimes[ii] = 0.0;
    }
    
    
    for (ii = 0; ii < N-1; ii++) {
        // find where it crosses
        if (hoft[ii]*hoft[ii+1] < 0.0) {
            
            slope = (hoft[ii+1] - hoft[ii])/deltaT; // rise over run
            intercept = hoft[ii] - slope*(double)ii*deltaT; // y = mx + b;
            zero = -intercept/slope; // gives t value of zero;
            
            
            // update crossing points (ie "current" "previous" and "next")
            zerotimes[2] = zerotimes[1];
            zerotimes[1] = zerotimes[0];
            zerotimes[0] = zero;
            
            // Find the frequency at each zero crossing
            if (jj > 3) {
                freq = (1./(zerotimes[0]-zerotimes[2]));
                //printf("%g %g\n",hoft[ii],hoft[ii+1]);
                //printf("%g %g %g\n",zerotimes[0],zerotimes[2],freq);
                tf_raw[0][jj] = zerotimes[1];
                
                //printf("%g\n",tf[1][jj]);
                tf_raw[1][jj] = freq;
                
                
            }
            else{
                tf_raw[0][jj] = zerotimes[1];
                tf_raw[1][jj] = 0.0;
            }
            jj++; // this gives the number of tf points we have
        }
    }
    
    
    
    // Interpolate
    interp_tf(tf_track, tf_raw, jj, N);
    
    for (ii = 0; ii < N; ii++) {
        //fprintf(ofile[ifo], "%e ", tf_track[1][ii]);
        frequency_out[ii] = tf_track[1][ii];
    }
    
    
    for (ii=0; ii<2; ii++) {
        free(tf_raw[ii]);
        free(tf_track[ii]);
    
    }
    
    free(w);
    free(zerotimes);
    free(tf_raw);
    free(tf_track);
    
    
}


void interp_tf(double **tf, double **pts, int tlength, int Nsample){
    int ii, jj;
    double slope, intercept;
    
    
    ii = 2;
    for (jj = 0; jj < Nsample; jj++) {
        
        while ((pts[0][ii] < tf[0][jj] || fabs(pts[0][ii-1]-pts[0][ii]) < 1e-7 /* sometimes points too close together gets fucked up*/) && ii < tlength) {
            ii++; // this just figures out which two points we're interpolating between
        }
        
        slope = (pts[1][ii]-pts[1][ii-1])/(pts[0][ii]-pts[0][ii-1]);
        intercept = pts[1][ii] - slope*pts[0][ii]; // y = mx + b;
        tf[1][jj] = slope*tf[0][jj]+intercept; // assign frequency
        
        
        
    }
    
}


void median_and_CIs(double **waveforminfo, int N, int Nwaveforms, double deltaT, FILE *outfile, double *median_waveform){
    
    int ii, n;
    double median, upper50, lower50, upper90, lower90;
    

    double *dataslice;
    dataslice = double_vector(Nwaveforms-1);
    
    
    for (ii = 0; ii < N; ii++) {
        
        for (n = 0; n < Nwaveforms; n++) {
            dataslice[n] = waveforminfo[n][ii];
        }
        
        gsl_sort(dataslice, 1, Nwaveforms);
        
        median = gsl_stats_median_from_sorted_data (dataslice,
                                                    1, Nwaveforms);
        upper90 = gsl_stats_quantile_from_sorted_data (dataslice,
                                                      1, Nwaveforms,
                                                      0.95);
        lower90 = gsl_stats_quantile_from_sorted_data (dataslice,
                                                      1, Nwaveforms,
                                                      0.05);
        upper50 = gsl_stats_quantile_from_sorted_data (dataslice,
                                                       1, Nwaveforms,
                                                       0.75);
        lower50 = gsl_stats_quantile_from_sorted_data (dataslice,
                                                       1, Nwaveforms,
                                                       0.25);
        
        median_waveform[ii] = median;
        
        
        fprintf(outfile,"%g %g %g %g %g %g\n", ii*deltaT, median, lower50, upper50, lower90, upper90);
    }
 
    
    free_double_vector(dataslice);
    
    //fclose(outfile);
    
}




void median_and_CIs_fourierdom(double **waveforminfo, int N, int Nwaveforms, double deltaT, FILE *outfile, double *median_waveform){
    
    int ii, n;
    double median, upper50, lower50, upper90, lower90;
    double medianre, medianim, upper50re, upper50im, lower50re, lower50im, upper90re, upper90im, lower90re, lower90im;
    
    
    double *dataslicere, *datasliceim;
    dataslicere = double_vector(Nwaveforms-1);
    datasliceim = double_vector(Nwaveforms-1);
    
    
    for (ii = 0; ii < N; ii++) {
        
        for (n = 0; n < Nwaveforms; n++) {
            dataslicere[n] = waveforminfo[n][2*ii];
            datasliceim[n] = waveforminfo[n][2*ii+1];
        }
        
        gsl_sort(dataslicere, 1, Nwaveforms);
        gsl_sort(datasliceim, 1, Nwaveforms);
        
        medianre = gsl_stats_median_from_sorted_data (dataslicere,
                                                    1, Nwaveforms);
        medianim = gsl_stats_median_from_sorted_data (datasliceim,
                                                    1, Nwaveforms);
        median = medianre*medianre+medianim*medianim;
        
        upper90re = gsl_stats_quantile_from_sorted_data (dataslicere,
                                                       1, Nwaveforms,
                                                       0.95);
        upper90im = gsl_stats_quantile_from_sorted_data (datasliceim,
                                                       1, Nwaveforms,
                                                       0.95);
        upper90 = upper90re*upper90re+upper90im*upper90im;
        
        lower90re = gsl_stats_quantile_from_sorted_data (dataslicere,
                                                       1, Nwaveforms,
                                                       0.05);
        lower90im = gsl_stats_quantile_from_sorted_data (datasliceim,
                                                       1, Nwaveforms,
                                                       0.05);
        lower90 = lower90re*lower90re+lower90im*lower90im;
        
        upper50re = gsl_stats_quantile_from_sorted_data (dataslicere,
                                                       1, Nwaveforms,
                                                       0.75);
        upper50im = gsl_stats_quantile_from_sorted_data (datasliceim,
                                                       1, Nwaveforms,
                                                       0.75);
        upper50 = upper50re*upper50re+upper50im*upper50im;
        
        lower50re = gsl_stats_quantile_from_sorted_data (dataslicere,
                                                       1, Nwaveforms,
                                                       0.25);
        lower50im = gsl_stats_quantile_from_sorted_data (datasliceim,
                                                       1, Nwaveforms,
                                                       0.25);
        lower50 = lower50re*lower50re+lower50im*lower50im;
        
        median_waveform[ii] = median;
        
        
        fprintf(outfile,"%g %g %g %g %g %g\n", ii*deltaT, median, lower50, upper50, lower90, upper90);
        
//        fprintf(outfile,"%g %g %g %g %g %g %g %g %g %g %g\n", ii*deltaT, medianre, medianim, lower50re, lower50im, upper50re, upper50im, lower90re, lower90im, upper90re, upper90im);
        
    }
    
    
    free_double_vector(dataslicere);
    free_double_vector(datasliceim);
    
    //fclose(outfile);
    
}

void Q_scan(struct Data *data, double *median_waveform, double Q, double *invPSD, FILE *outfile)
{
  int ii, M, N, imax, imin;
  
  double fmax, fmin;
  double *hoff;
  
  double deltaT = data->Tobs/(double)data->N;
  double deltaF = 1./data->Tobs;
  
  imin = data->imin;
  imax = data->imax;
  fmin = (double)imin*deltaF;
  fmax = (double)imax*deltaF;
  
  M = (int)((fmax-fmin)/GLOBAL_F_RESOLUTION);
  N = data->N;
  
  //tfmap = double_matrix(M-1,N-1);
  hoff = double_vector(N-1);
  
  for(ii = 0; ii < N; ii++) hoff[ii] = median_waveform[ii];
  
  Transform(hoff, Q, deltaT, fmin, invPSD,N, M, outfile);
  
  free_double_vector(hoff);
}


void Transform(double *hoff, double Q, double dt, double fmin, double *invPSD, int n, int m, FILE *outfile)
{
  int i, j;
  double f;
  double *AC, *AF;
  double *corr, *b;
  double *params;
  double bmag;
  double Tobs;
  double fix;
  
  Tobs = (double)n*dt;
  
  fix = sqrt(Tobs)/((double)n);
  
  // [0] t0 [1] f0 [2] Q [3] Amp [4] phi
  
  params = double_vector(4);
  
  params[0] = 0.0;
  params[2] = Q;
  params[3] = 1.0;
  params[4] = 0.0;
  
  double *timeData = (double *)malloc(sizeof(double)*n);
  fftw_complex *freqData = (fftw_complex *) fftw_malloc(sizeof(fftw_complex)*n);
  fftw_plan forward = fftw_plan_dft_r2c_1d(n, timeData, freqData, FFTW_MEASURE);
  for(i=0; i<n; i++) timeData[i] = hoff[i];
  
  fftw_execute(forward);
  
  for(i=0; i<n/2; i++)
  {
    hoff[2*i]   = freqData[i][0];
    hoff[2*i+1] = freqData[i][1];
  }
  //get DC and Nyquist where FFTW wants them
  hoff[1] = freqData[n/2][0];

  
  free(timeData);
  fftw_destroy_plan(forward);
  fftw_free(freqData);

  for (i = 0; i < n; i++) {
    hoff[i] *= fix;
  }
  
  AC   = double_vector(n-1);
  AF   = double_vector(n-1);
  corr = double_vector(n-1);
  b    = double_vector(n-1);
  
  
  for(j = 0; j < m; j++)
  {
    
    f = fmin + GLOBAL_F_RESOLUTION*(double)(j);
        
    params[1] = f;
    
    SineGaussianFourier(b, params, n, 0, Tobs);
    
    bmag = sqrt(fourier_nwip(0,n/2, b, b, invPSD));
    
    for(i = 0; i < n; i++) corr[i] = 0.0;
    
    phase_blind_time_shift(AC, AF, hoff, b, invPSD, n);
    
    for(i = 0; i < n; i++) corr[i] += sqrt(AC[i]*AC[i]+AF[i]*AF[i])/bmag;
    
    for(i = 0; i < n; i++)
    {
      //tfmap[j][i] = corr[i];
      fprintf(outfile,"%e ",corr[i]);
    }
    
    fprintf(outfile,"\n");
    
    
  }
  
  free_double_vector(AC);
  free_double_vector(AF);
  free_double_vector(corr);
  free_double_vector(b);
  
}

void print_spectrum(struct Data *data, double **h, FILE **ofile)
{
  int i;
  int ifo;
  int re,im;
  int NI = data->NI;
  int imin = data->imin;
  int imax = data->imax;
  double f;

  for(ifo=0; ifo<NI; ifo++)
  {
    for(i=imin; i<imax; i++)
    {
      f=(double)i/data->Tobs;
      re = 2*i;
      im = re+1;
      fprintf(ofile[ifo], "%lg %e ", f,h[ifo][re]*h[ifo][re]+h[ifo][im]*h[ifo][im]);
      fprintf(ofile[ifo], "\n");
    }
  }
}

void print_hdot(struct Data *data, double **h, double **psd, FILE **ofile, double deltaF)
{
  int i;
  int ifo;

  int N = data->N;
  int NI = data->NI;
  int imin = data->imin;
  int imax = data->imax;

  double *hoft = malloc(N*sizeof(double));
  double *w    = malloc(N*sizeof(double));

  for(ifo=0; ifo<NI; ifo++)
  {
    //whiten waveforms
    for(i=0; i<N/2; i++)
    {
      w[2*i]   =   h[ifo][2*i] / sqrt(psd[ifo][i]);
      w[2*i+1] = h[ifo][2*i+1] / sqrt(psd[ifo][i]);
    }

    get_time_domain_hdot(hoft, w, N, imin, imax, deltaF);

    for(i=0; i<N; i++)
      fprintf(ofile[ifo], "%e ", hoft[i]);
    //fprintf(ofile[ifo], "%e\n", hoft[i]);

    fprintf(ofile[ifo], "\n");
  }

  free(hoft);
  free(w);
}

void print_spectrograms(struct Data *data, char outdir[], char type[], char *ifo, double *h, double *psd)
{
  int n;
  int NQ = 3;
  double Qlist[3] = {4,8,16};
  
  FILE *QscanFile;
  char filename[MAXSTRINGSIZE];
  
  for(n=0; n<NQ; n++)
  {
    sprintf(filename,"%s/%s_spectrogram_%g_%s.dat",outdir,type,Qlist[n],ifo);
    QscanFile = fopen(filename,"w");
    Q_scan(data, h, Qlist[n], psd, QscanFile);
    fclose(QscanFile);
  }
}

void print_single_waveform(struct Data *data, double **h, double **psd, FILE *ofile)
{
  int i;
  int ifo;

  int N = data->N;
  int imin = data->imin;
  int imax = data->imax;

  double *hoft = malloc(N*sizeof(double));
  double *w    = malloc(N*sizeof(double));

  double maxh = 0.0;
  double maxt = 0.0;
  double samp = data->Tobs/(double)data->N;

  ifo=0;
  //whiten waveforms
  for(i=0; i<N/2; i++)
  {
    w[2*i]   =   h[ifo][2*i] / sqrt(psd[ifo][i]);
    w[2*i+1] = h[ifo][2*i+1] / sqrt(psd[ifo][i]);
  }

  //Obtain waveform in the time domain
  get_time_domain_waveforms(hoft, w, N, imin, imax);

  for(i=0; i<N; i++) fprintf(ofile, "%e\n", hoft[i]);

  for(i=0; i<N; i++)
  {
    if(sqrt(hoft[i]*hoft[i])>maxh )
    {
      maxh = sqrt(hoft[i]*hoft[i]);
      maxt = (double)i*samp;
    }
  }

  printf("max waveform %g at %g s \n",maxh,maxt);

  free(hoft);
  free(w);
}

void print_stats(struct Data *data, LIGOTimeGPS GPS, int injectionFlag, char *outdir, char model[], int **dimension)
{
  int NI = data->NI;

  int map,max;
  double bayesfactor;
  int Zs,Zn;

  FILE *momentsfile = NULL;
  FILE *statsfile   = NULL;

  char filename[MAXSTRINGSIZE];
  char filerow[10000];
  int count;

  double *snr       = NULL;
  double *time      = NULL;
  double *duration  = NULL;
  double *frequency = NULL;
  double *bandwidth = NULL;
  double *hmax      = NULL;
  double *t_at_hmax = NULL;
  double *f_at_max_amp = NULL;

  int seconds = GPS.gpsSeconds;
  int nanoseconds = GPS.gpsNanoSeconds;
  double GPStime = (double)seconds + 1.0e-9*nanoseconds;

  if(!strcmp(model,"signal")) NI++;

  for(int ifo=0; ifo<NI; ifo++)
  {

    //find model with most posterior support

    map=max=-1;
    bayesfactor=0.0;
    Zn = 0;
    Zs = 0;

    if(!strcmp(model,"signal"))
    {
      for(int i=0; i<data->Dmax; i++)
      {
        //evidence for each model
        if(i==0) Zn += dimension[0][i];
        else     Zs += dimension[0][i];

        //map model
        if(dimension[0][i]>max)
        {
          max = dimension[0][i];
          map = i;
        }
      }
      if(ifo==0)printf("map dimension for channel \"%s\" = %i\n",model,map);
    }
    else
    {
      for(int i=0; i<data->Dmax; i++)
      {
        //evidence for each model
        if(i==0) Zn += dimension[ifo][i];
        else     Zs += dimension[ifo][i];

        //map model
        if(dimension[ifo][i]>max)
        {
          max = dimension[ifo][i];
          map = i;
        }
      }
      printf("map dimension for channel \"%s[%i]\" = %i\n",model,ifo,map);
    }

    if(Zn==0) bayesfactor = 1000.0;
    else      bayesfactor = (double)Zs/(double)Zn;


    if( !strcmp(model,"signal") && ifo==(NI-1) )
    {
      sprintf(filename,"%s/%s_whitened_moments.dat.geo",outdir,model);
      momentsfile = fopen(filename,"r");

      sprintf(filename,"%s/%s_stats.dat.geo",outdir,model);
      statsfile = fopen(filename,"w");
    }
    else
    {
      sprintf(filename,"%s/%s_whitened_moments_%s.dat",outdir,model,data->ifos[ifo]);
      momentsfile = fopen(filename,"r");

      sprintf(filename,"%s/%s_stats_%s.dat",outdir,model,data->ifos[ifo]);
      statsfile = fopen(filename,"w");
    }

    // print header for stats file
    fprintf(statsfile, "# ");
    fprintf(statsfile, "map_D ");
    fprintf(statsfile, "bayesfactor ");
    fprintf(statsfile, "snr ");
    fprintf(statsfile, "time ");
    fprintf(statsfile, "duration ");
    fprintf(statsfile, "frequency ");
    fprintf(statsfile, "bandwidth ");
    fprintf(statsfile, "h_max ");
    fprintf(statsfile, "t_at_h_max ");
    fprintf(statsfile, "f_at_max_amp ");
    fprintf(statsfile, "\n");

    count=0;

    //ignore header
    fgets(filerow,10000,momentsfile);

    //get size of file
    while(!feof(momentsfile))
    {
      fgets(filerow,10000,momentsfile);
      count++;
    }
    rewind(momentsfile);
    count--;

    //allocate memory, store burst table moments
    double junk;
    snr       = malloc(count*sizeof(double));
    time      = malloc(count*sizeof(double));
    duration  = malloc(count*sizeof(double));
    frequency = malloc(count*sizeof(double));
    bandwidth = malloc(count*sizeof(double));
    hmax = malloc(count*sizeof(double));
    t_at_hmax = malloc(count*sizeof(double));
    f_at_max_amp = malloc(count*sizeof(double));

    //ignore header again
    fgets(filerow,10000,momentsfile);

    for(int i=0; i<count; i++)
    {
      fscanf(momentsfile, "%lg",&snr[i]);
      fscanf(momentsfile, "%lg",&junk);
      fscanf(momentsfile, "%lg",&junk); //FP: bug? There are two values between snr and t0_rec but only one was going to &junk
      fscanf(momentsfile, "%lg",&time[i]);
      fscanf(momentsfile, "%lg",&duration[i]);
      fscanf(momentsfile, "%lg",&frequency[i]);
      fscanf(momentsfile, "%lg",&bandwidth[i]);
      if(injectionFlag)
      {
        fscanf(momentsfile, "%lg",&junk);
        fscanf(momentsfile, "%lg",&junk);
      }
      fscanf(momentsfile, "%lg",&hmax[i]);
      fscanf(momentsfile, "%lg",&t_at_hmax[i]);
      fscanf(momentsfile, "%lg",&f_at_max_amp[i]);
    }

    //sort each moment array
    gsl_sort(snr,1,count);
    gsl_sort(time,1,count);
    gsl_sort(duration,1,count);
    gsl_sort(frequency,1,count);
    gsl_sort(bandwidth,1,count);
    gsl_sort(hmax,1,count);
    gsl_sort(t_at_hmax,1,count);
    gsl_sort(f_at_max_amp,1,count);

    //get and print medians for stats file
    fprintf(statsfile, "%i ",    map );
    fprintf(statsfile, "%g ",    bayesfactor);
    fprintf(statsfile, "%g ",    gsl_stats_median_from_sorted_data (snr,1,count) );
    fprintf(statsfile, "%.16g ", gsl_stats_median_from_sorted_data (time,1,count)  + (GPStime + 2.0 - data->Tobs) );
    fprintf(statsfile, "%g ",    gsl_stats_median_from_sorted_data (duration,1,count) );
    fprintf(statsfile, "%g ",    gsl_stats_median_from_sorted_data (frequency,1,count) );
    fprintf(statsfile, "%g ",    gsl_stats_median_from_sorted_data (bandwidth,1,count) );
    fprintf(statsfile, "%g ",    gsl_stats_median_from_sorted_data (hmax,1,count));
    fprintf(statsfile, "%g ",    gsl_stats_median_from_sorted_data (t_at_hmax,1,count) + (GPStime + 2.0 - data->Tobs));
    fprintf(statsfile, "%g ",    gsl_stats_median_from_sorted_data (f_at_max_amp,1,count) );
    fprintf(statsfile, "\n");

    fclose(momentsfile);
    fclose(statsfile);

    free(snr);
    free(time);
    free(duration);
    free(frequency);
    free(bandwidth);

  }
}


double get_energy(int imin, int imax, double *a, double deltaF)
{
  int i;
  double result;

  // Calculate total signal energy
  result = 0.0;
  for(i=imin; i<imax; i++){
    result += (a[i*2]*a[i*2] + a[i*2+1]*a[i*2+1]);
  }
  result *= 2*deltaF;
  return(result);
}

double get_energy_f2weighted(int imin, int imax, double *a, double deltaF)
{
  int i;
  double result;

  // Calculate total signal energy
  result = 0.0;
  for(i=imin; i<imax; i++){
    result += (a[i*2]*a[i*2] + a[i*2+1]*a[i*2+1])*i*i;
  }
  result *= 2*deltaF*deltaF*deltaF;
  return(result);
}

double get_rhof(int imin, int imax, double *a, double deltaF, double *arg)
{
  // Calculate rho_f (Normalized signal energy frequency distribution.  See moments.pdf eqn 1.5)

  int i;
  double norm;

  // We'll normalize by the total signal energy
  norm = get_energy(imin, imax, a, deltaF);
  for(i=0; i<imax; i++) {
    if (i<imin) {
      arg[i] = 0;
    } else {
      arg[i] = 2*(a[i*2]*a[i*2] + a[i*2+1]*a[i*2+1])/norm;
    }
  }
  // Return value is the total signal energy
  return(norm);
}

double get_rhof_f2weighted(int imin, int imax, double *a, double deltaF, double *arg)
{
  // Calculate rho_f (Normalized signal energy frequency distribution.  See moments.pdf eqn 1.5)

  int i;
  double norm;

  // We'll normalize by the total signal energy
  norm = get_energy_f2weighted(imin, imax, a, deltaF);
  for(i=0; i<imax; i++) {
    if (i<imin) {
      arg[i] = 0;
    } else {
      arg[i] = 2*(a[i*2]*a[i*2] + a[i*2+1]*a[i*2+1])*i*deltaF*i*deltaF/norm;
    }
  }
  // Return value is the total signal energy
  return(norm);
}

double get_rhof_net(int imin, int imax, double **a, double deltaF, double *arg, int NI)
{
  // Calculate rho_f (Normalized signal energy frequency distribution.  See moments.pdf eqn 1.5)

  int i,ifo;
  double norm;

  // We'll normalize by the total signal energy
  norm=0.0;
  for(ifo=0; ifo<NI; ifo++) norm += get_energy(imin, imax, a[ifo], deltaF);

  for(i=0; i<imax; i++)
  {
    arg[i]=0.0;
    if(i>=imin) for(ifo=0; ifo<NI; ifo++) arg[i] += 2*(a[ifo][i*2]*a[ifo][i*2] + a[ifo][i*2+1]*a[ifo][i*2+1])*i*deltaF*i*deltaF/norm;
  }

  // Return value is the total signal energy
  return(norm);
}

double get_rhot(int N, double *a, double deltaT, double *arg)
{
  // Calculate rho_t (Normalized signal energy frequency distribution.  See moments.pdf eqn 1.5)
  int i;
  double norm;

  // We'll normalize by the total signal energy
  norm = 0;
  for(i=0;i<N; i++) {
    norm += a[i]*a[i];
  }
  norm *= deltaT;

  for(i=0; i<N; i++) {
    arg[i] = a[i]*a[i]/norm;
  }
  // Return value is the total signal energy
  return(norm);
}

double get_rhot_net(int N, double **a, double deltaT, double *arg, int NI)
{
  // Calculate rho_t (Normalized signal energy frequency distribution.  See moments.pdf eqn 1.5)
  int i,ifo;
  double norm;

  // We'll normalize by the total signal energy
  norm = 0;
  for(ifo=0; ifo<NI; ifo++)
  {
    for(i=0;i<N; i++) {
      norm += a[ifo][i]*a[ifo][i];
    }
  }
  norm *= deltaT;

  for(i=0; i<N; i++) {
    arg[i]=0.0;
    for(ifo=0; ifo<NI; ifo++) arg[i] += a[ifo][i]*a[ifo][i]/norm;
  }
  // Return value is the total signal energy
  return(norm);
}


double rhonorm(int imax, double *a, double deltaF)
{
  // Calculate normalization.  This is a sanity check, since rho is supposed to have a normalization of 1.
  int i;
  double norm;

  norm = 0.0;
  for(i=0; i<imax; i++) {
    norm += (a[i]);
  }
  norm *= deltaF;
  return(norm);
}




void get_f_quantiles(int imin, int imax, double *rhof, double deltaF, double *fm, double *bw){
  double f, flow=imin*deltaF, fhigh=imax*deltaF, fmed=0.5*(flow+fhigh);
  double x;
  double quantile_range = .68;

  int j, flag1, flag2, flag3;

  flag1 = 0;
  flag2 = 0;
  flag3 = 0;

  x = 0.0;

  for(j=0;j<imax;j++){
    f = (double)(j)*deltaF;
    x += rhof[j]*deltaF;

    if(x > 0.5*(1.0-quantile_range) && flag1==0){
      flag1 = 1; // low end of quantile
      flow = f;
    }

    if (x > 0.5 && flag2==0) {
      flag2 = 1; // median
      fmed = f;
    }

    if(x > 0.5*(1.0+quantile_range) && flag3==0){
      flag3 = 1; // high end of quantile
      fhigh = f;
    }

  }


  *fm = fmed;

  *bw = fhigh-flow; // 90% bandwidth


}


void get_t_quantiles(int N, double *rhot, double deltaT, double *tm, double *dur){
  double tlow=0.0, thigh=N*deltaT, tmed=0.5*(tlow+thigh), t;
  double x;
  double quantile_range = .68;


  int flag1, flag2, flag3, j;

  flag1 = 0;
  flag2 = 0;
  flag3 = 0;

  x = 0.0;

  for(j=0;j<N;j++){
    t = (double)(j)*deltaT;
    x += rhot[j]*deltaT;

    if(x > 0.5*(1.0-quantile_range) && flag1==0){
      flag1 = 1;
      tlow = t; // low end of quantile
    }

    if (x > 0.5 && flag2==0) {
      flag2 = 1;
      tmed = t; // median
    }

    if(x > 0.5*(1.0+quantile_range) && flag3==0){
      flag3 = 1;
      thigh = t; // high end of quantile
    }


  }

  *tm = tmed;

  *dur = thigh-tlow; // 90% duration

}

double get_f0(int imax, double *a, double deltaF)
{
  // Calculate the central frequency (aka first moment).
  int i;
  double result;

  result = 0.0;
  for(i=0; i<imax; i++) {
    result += (a[i]*i);
  }
  result *= deltaF*deltaF;
  return(result);
}

double get_band(int imax, double *a, double deltaF, double f0)
{
  // Calculate the bandwidth (aka 2nd moment).
  int i;
  double result;

  result = 0.0;
  for(i=0; i<imax; i++) {
    result += ( a[i]*(i*deltaF - f0)*(i*deltaF - f0) );
  }
  result *= deltaF;
  result = sqrt(result);
  return(result);
}

void get_hmax_and_t(int imax, double *a, double deltaT, double *hmax, double *t_at_hmax)
{
  //Find the maximum value of the vector and the time there
  int i;
  double a_abs;

  *hmax = 0.0;
  *t_at_hmax = 0.0;

  for(i=0; i<imax; i++)
  {
    a_abs = sqrt(a[i]*a[i]);
    if(a_abs>*hmax)
    {
      *hmax = a_abs;
      *t_at_hmax = (double)i*deltaT;
    }
  }
}

void whiten_data(int imin, int imax, double *a, double *Snf)
{
  int i;
  for(i=imin; i<imax; i++) {
    a[i*2] = a[i*2]/sqrt(Snf[i]);
    a[i*2+1] = a[i*2+1]/sqrt(Snf[i]);
  }
}

void get_time_domain_waveforms(double *hoft, double *h, int N, int imin, int imax)
{
  int i;
  
  fftw_complex *hoff = (fftw_complex *) fftw_malloc(sizeof(fftw_complex)*N);
  fftw_plan reverse = fftw_plan_dft_c2r_1d(N, hoff, hoft, FFTW_MEASURE);
  
  hoff[0][0] = 0.0;
  hoff[0][1] = 0.0;
  for(i=1; i<N/2; i++)
  {
    if(i>imin && i<imax)
    {
      hoff[i][0] = h[2*i];
      hoff[i][1] = h[2*i+1];
    }
    else
    {
      hoff[i][0] = 0.0;
      hoff[i][1] = 0.0;
    }
  }
  
  //get DCand Nyquist where FFTW wants them
  hoff[N/2][0] = hoff[0][1];
  hoff[N/2][1] = hoff[0][1] = 0.0;

  fftw_execute(reverse);
  
  fftw_destroy_plan(reverse);
  fftw_free(hoff);
}

void get_time_domain_hdot(double *hdot, double *h, int N, int imin, int imax, double deltaF)
{
  int i;
  double xx, yy;

  fftw_complex *hoff = (fftw_complex *) fftw_malloc(sizeof(fftw_complex)*N);
  fftw_plan reverse = fftw_plan_dft_c2r_1d(N, hoff, hdot, FFTW_MEASURE);

  for(i=0; i<N/2; i++)
  {
    hoff[0][i] = -LAL_TWOPI*h[2*i]; // Take care of the -2pi part
    hoff[1][i] = -LAL_TWOPI*h[2*i+1];
  }

  hoff[0][0] = 0.0;
  hoff[0][1] = 0.0;
  for(i=1; i< N/2; i++)
  {
    if(i>imin && i<imax)
    {

      xx = hoff[i][0]; // Real part
      yy = hoff[i][1]; // Imaginary part

      // switch real and imaginary parts i*(x+iy) = ix-y

      hoff[i][0] =  yy*(double)(i)*deltaF;  // f = i*deltaf
      hoff[i][1] = -xx*(double)(i)*deltaF;  // Change sign of new imaginary part so it doesn't come out time reversed
    }
    else
    {
      hoff[i][0] = 0.0;
      hoff[i][1] = 0.0;
    }
  }
  
  //get DCand Nyquist where FFTW wants them
  hoff[N/2][0] = hoff[0][1];
  hoff[N/2][1] = hoff[0][1] = 0.0;

  fftw_execute(reverse);

  fftw_destroy_plan(reverse);
  fftw_free(hoff);

}

void write_time_freq_samples(struct Data *data)
{

  int N    = data->N;
  int imax = data->imax;

  double Tobs   = data->Tobs;
  double deltaF = 1./data->Tobs;
  double offset;

  offset = data->trigtime - (data->starttime + data->Tobs/2.0);

  char filename[MAXSTRINGSIZE];
  sprintf(filename,"%spost/timesamp.dat",data->runName);
  FILE *timesamp = fopen(filename, "w");
  sprintf(filename,"%spost/freqsamp.dat",data->runName);
  FILE *freqsamp = fopen(filename, "w");
  double t0 = Tobs/2.0 + offset;
  double f;
  double t;
  int i;
  for(i=0; i<N; i++) {
    t = (i*Tobs)/N;
    fprintf(timesamp, "%e \n", t-t0);
  }
  fclose(timesamp);
  for (i=0;i<imax;i++) {
    f = i*deltaF;
    fprintf(freqsamp, "%e \n", f);
  }
  fclose(freqsamp);
}

int post_process_model(char model_name[], char model_type[], struct Data *data, struct Model *model, struct Chain *chain, double **hdata, double **hoft, double **psd, double **one, double **hinj, int **dimension, int injectionFlag, LIGOTimeGPS GPStrig, bool lite)
{
  // model name holds what kind of run this was in the 1st place? 
  // model type holds what kind of run this is right now 
  
  char outdir[MAXSTRINGSIZE];
  // name post directory 
  sprintf(outdir,"%spost/%s",data->runName,model_name);
  mkdir(outdir,S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
  
  int COUNT,BURNIN;
  char filename[MAXSTRINGSIZE];
  double norm;
  
  // Shortcut pointers to different members of the Model structre
  struct Wavelet **glitch = NULL;
  struct Wavelet **signal = NULL;
  int NI = data->NI;
  int N  = data->N; // number of time samples in segment 
  
  double deltaT = data->Tobs/(double)N;
  double deltaF = 1./data->Tobs;
  
  //Input files
  FILE **splinechain = malloc((NI)*sizeof(FILE*));
  FILE **linechain   = malloc((NI)*sizeof(FILE*));
  
  //Output files
  FILE **recWhitenedWaveformFile     = malloc(NI*sizeof(FILE*));
  FILE **recColoredWaveformFile      = malloc(NI*sizeof(FILE*));
  FILE **medianWaveformFile          = malloc(NI*sizeof(FILE*));
  FILE **medianFrequencyWaveformFile = malloc(NI*sizeof(FILE*));
  FILE **medianFrequencyFile = malloc(NI*sizeof(FILE*));
  FILE **medianStokesFile      = malloc(4*sizeof(FILE*));
  FILE **medianFrequencyPolFile = malloc(data->Npol*sizeof(FILE*));
  FILE **medianTimeFrequencyFile = malloc(NI*sizeof(FILE*));
  FILE **medianPSDFile           = malloc(NI*sizeof(FILE*));
  FILE **medianPSDFileLI         = malloc(NI*sizeof(FILE*));

  FILE **whitenedGeocenterFile = malloc(1*sizeof(FILE*));
  FILE **coloredGeocenterFile  = malloc(1*sizeof(FILE*));

  FILE **whitenedFile = malloc((NI+1)*sizeof(FILE*));
  FILE **coloredFile  = malloc((NI+1)*sizeof(FILE*));
  
  //Storage for model parameters and reconstructions
  FILE **glitchParams = malloc(NI*sizeof(FILE *));
  FILE **signalParams = malloc(data->Npol*sizeof(FILE *));
  FILE *cbcParams = malloc(sizeof(FILE *));

  double *Sgeo   = double_vector(N/2-1);
  double **grec  = double_matrix(NI-1,N-1);
  double **hrec  = double_matrix(NI-1,N-1); // waveform reconstructed, NI by N points in time segment
  double **hcbcrec  = double_matrix(NI-1,N-1); // cbc waveform reconstructed
  double **hgrec = double_matrix(NI-1,N-1); // combination of whatever reconstructed 
  //double **hgeo  = double_matrix(data->Npol-1,N-1);
  double **hpol  = double_matrix(data->Npol-1,N-1);

  // Different structures for whitened and colored moments
  struct Moments *whitenedGlitchMoments = malloc(sizeof(struct Moments));
  struct Moments *coloredGlitchMoments  = malloc(sizeof(struct Moments));
  initialize_moments(whitenedGlitchMoments, NI);
  initialize_moments(coloredGlitchMoments,  NI);
  
  struct Moments *whitenedSignalMoments = malloc(sizeof(struct Moments));
  struct Moments *coloredSignalMoments  = malloc(sizeof(struct Moments));
  initialize_moments(whitenedSignalMoments, NI);
  initialize_moments(coloredSignalMoments,  NI);

  struct Moments *whitenedCBCMoments = malloc(sizeof(struct Moments));
  struct Moments *coloredCBCMoments  = malloc(sizeof(struct Moments));
  initialize_moments(whitenedCBCMoments, NI);
  initialize_moments(coloredCBCMoments,  NI);

  // note, this struct is used whenever there are multiple models (full, cbcglitch, cbcsignal)
  struct Moments *whitenedFullMoments = malloc(sizeof(struct Moments));
  struct Moments *coloredFullMoments  = malloc(sizeof(struct Moments));
  initialize_moments(whitenedFullMoments, NI);
  initialize_moments(coloredFullMoments,  NI);

  int fullFlag      = 0;
  int glitchFlag    = 0;
  int signalFlag    = 0;
  int cleanFlag     = 0;
  int cbcFlag       = 0; 
  int cbcGlitchFlag = 0; 
  int cbcSignalFlag = 0; 

  // cbc files have a bit different naming convention
  int cbcNames = 0; 

  // ----- SET FLAGS -----
  // If cleaning phase
  if(!strcmp(model_name,"clean")){ cleanFlag=1;}
  if(!strcmp(model_type,"glitch")){ glitchFlag = 1;}
  if(!strcmp(model_type,"signal")){ signalFlag = 1;}
  if(!strcmp(model_type,"cbc")){ cbcFlag = 1;}
  if(!strcmp(model_type,"full")){ fullFlag = 1;} 
  if(!strcmp(model_type,"cbcglitch")){cbcGlitchFlag = 1;}
  if(!strcmp(model_type,"cbcsignal")){cbcSignalFlag = 1;}

  // check if have cbc naming convention (ie were run using cbc runs) 
  if(!strcmp(model_name, "cbcglitch") || !strcmp(model_name, "cbcsignal") || !strcmp(model_name, "cbc")){
      cbcNames = 1;
  }

  // ----- CHECK FOR FILES AND READ IN IF THEY EXIST ----- // 


  // If noise-only model is in use, check for files
  if(!strcmp(model_type,"noise"))
  {
    //File containing noise-only PSD parameters
    sprintf(filename,"chains/%s%s_spline_%s.dat.0",data->runName, model_name,data->ifos[0]);
    if( fopen(filename,"r") == NULL)
    {
      fprintf(stdout,"post_process_model: Could not find %s.  Finishing...\n",filename);
      return 1;
    }
  }

  // If glitch model is in use, check for files
  if(glitchFlag || fullFlag || cbcGlitchFlag)
  {
    //File containing glitch-model waveform parameters
    if(cbcNames){
        sprintf(filename,"chains/cbc_params_%s.dat.0", data->ifos[0]);
    }
    else{
        sprintf(filename,"chains/%s%s_params_%s.dat.0",data->runName,model_name,data->ifos[0]);
    }
    //check to see if it exists...
    if( (glitchParams[0] = fopen(filename,"r")) == NULL)
    {
      fprintf(stdout,"Could not find %s.  Skipping to next model\n",filename);
      return 1;
    }
  }

  // If signal model is in use, check for files
  if(signalFlag || fullFlag || cbcSignalFlag){
    //File containing signal-model waveform parameters
    if (cbcNames){
        sprintf(filename,"chains/cbc_params_h0.dat.0");
    }else{
        sprintf(filename,"chains/%s%s_params_h0.dat.0",data->runName,model_name);
    }
    //check to see if it exists...
    if( (signalParams[0] = fopen(filename,"r")) == NULL)
    {
      fprintf(stdout,"Could not find %s.  Skipping to next model\n",filename);
      return 1;
    } 
  }
  // If cbc model is in use, check for files
  if(cbcFlag || cbcGlitchFlag || cbcSignalFlag)
  {
    //File containing cbc waveform parameters
    sprintf(filename,"chains/cbc_params.dat.0");
    
    //check to see if it exists...
    if( (cbcParams = fopen(filename,"r")) == NULL)
    {
      fprintf(stdout,"Could not find %s.  Skipping to next model\n",filename);
      return 1;
    }
  }

  //...carry on
  
  // -------- READ IN DATA --------- 
  fprintf(stdout, "Starting loop over %s  ...\n", filename);
  
  glitch = model->glitch;
  signal = model->signal;
  glitch[0] = model->glitch[0];
  signal[0] = model->signal[0];

  // initialize glitch
  if(glitchFlag || fullFlag || cbcGlitchFlag)
  {
    for(int ifo=1; ifo<NI; ifo++)
    {
      if(cbcNames){
        sprintf(filename,"chains/cbc_params_%s.dat.0", data->ifos[ifo]);
      }else{
        sprintf(filename,"chains/%s%s_params_%s.dat.0",data->runName,model_name,data->ifos[ifo]);
      }
      glitchParams[ifo] = fopen(filename,"r");
      
      glitch[ifo] = model->glitch[ifo];
    }
    for(int ifo=0; ifo<NI; ifo++){
        discard_burnin_samples(glitchParams[ifo], &COUNT, &BURNIN);
    }
  }

  // initialize signal 
  if(signalFlag || fullFlag || cbcSignalFlag)
  {
    for(int ifo=1; ifo<data->Npol; ifo++)
    {
      if(cbcNames){
        sprintf(filename,"chains/cbc_params_h%i.dat.0", ifo);
      }else{
        sprintf(filename,"chains/%s%s_params_h%i.dat.0",data->runName, model_name, ifo);
      }
      signalParams[ifo] = fopen(filename,"r");
      signal[ifo] = model->signal[ifo];
    }
    
    for(int ifo=0; ifo<data->Npol; ifo++)
      discard_burnin_samples(signalParams[ifo], &COUNT, &BURNIN);
  }

  // initialize cbc signal
  if(cbcFlag || cbcGlitchFlag || cbcSignalFlag){
    // reads in cbc parameters 
    sprintf(filename,"chains/cbc_params.dat.0");
    cbcParams = fopen(filename,"r");
    discard_burnin_samples(cbcParams, &COUNT, &BURNIN);
  }


  // Open files for Waveform reconstructions
  bool waveform_flags = (glitchFlag || signalFlag || fullFlag || cbcFlag || cbcGlitchFlag || cbcSignalFlag);
  if(waveform_flags)
  {
    for(int ifo=0; ifo<NI; ifo++)
    {
      //Time domain reconstructions
      sprintf(filename,"%s/%s_recovered_whitened_waveform_%s.dat",outdir,model_type,data->ifos[ifo]);
      recWhitenedWaveformFile[ifo] = fopen(filename,"w");
      
      sprintf(filename,"%s/%s_recovered_colored_waveform_%s.dat",outdir,model_type,data->ifos[ifo]);
      recColoredWaveformFile[ifo] = fopen(filename,"w");
      
      sprintf(filename,"%s/%s_median_time_domain_waveform_%s.dat",outdir,model_type,data->ifos[ifo]);
      medianWaveformFile[ifo] = fopen(filename,"w");
      
      //Frequency domain spectrum reconstructions
      sprintf(filename,"%s/%s_median_frequency_domain_waveform_spectrum_%s.dat",outdir,model_type,data->ifos[ifo]);
      medianFrequencyWaveformFile[ifo] = fopen(filename,"w");
      
      //Frequency domain reconstructions                                                                                                             
      sprintf(filename,"%s/%s_median_frequency_domain_waveform_%s.dat",outdir,model_type,data->ifos[ifo]);
      medianFrequencyFile[ifo] = fopen(filename,"w");

      //Time-frequency reconstructions
      sprintf(filename,"%s/%s_median_tf_%s.dat",outdir,model_type,data->ifos[ifo]);
      medianTimeFrequencyFile[ifo] = fopen(filename,"w");
      
    }

  if(!data->polarizationFlag)
    {
      for(int pol=0; pol<4; pol++)
      {
        //Polarization waveform reconstruction
        if(pol==0)
        {
          //sprintf(filename,"%s/%s_median_frequency_domain_stokes_I.dat",outdir,model_type);
          sprintf(filename,"%s/%s_median_frequency_domain_stokes_U.dat",outdir,model_type);
        }
        if(pol==1)
        {
          //sprintf(filename,"%s/%s_median_frequency_domain_stokes_Q.dat",outdir,model_type);
          sprintf(filename,"%s/%s_median_frequency_domain_stokes_eps_sqr.dat",outdir,model_type);
        }
        if(pol==2)
        {
          //sprintf(filename,"%s/%s_median_frequency_domain_stokes_U.dat",outdir,model_type);
          sprintf(filename,"%s/%s_median_frequency_domain_stokes_eps1.dat",outdir,model_type);
        }
        if(pol==3)
        {
          //sprintf(filename,"%s/%s_median_frequency_domain_stokes_V.dat",outdir,model_type);
          sprintf(filename,"%s/%s_median_frequency_domain_stokes_eps2.dat",outdir,model_type);
        }
        medianStokesFile[pol] = fopen(filename,"w");
      }
    }
    
    for(int pol=0; pol<data->Npol; pol++)
    {
      if(pol==0)
      {
        sprintf(filename,"%s/%s_median_frequency_domain_waveform_p.dat",outdir,model_type);
      }
      if(pol==1)
      {
        sprintf(filename,"%s/%s_median_frequency_domain_waveform_c.dat",outdir,model_type);
      }
      medianFrequencyPolFile[pol] = fopen(filename,"w");
    }

    // sets up moment files  
    setup_output_files(NI, injectionFlag, outdir, model_type, "whitened", whitenedFile,data);
    setup_output_files(NI, injectionFlag, outdir, model_type, "colored", coloredFile,data);
    
    if(signalFlag)
    {
      setup_output_files(-1, injectionFlag, outdir, model_type, "whitened", whitenedGeocenterFile,data);
      setup_output_files(-1, injectionFlag, outdir, model_type, "colored", coloredGeocenterFile,data);
    }
    
    for(int ifo=0; ifo<NI; ifo++) for(int n=0; n<data->Dmax; n++) dimension[ifo][n] = 0;
  }
  
  //Noise-model reconstructions
  for(int ifo=0; ifo<NI; ifo++)
  {
    sprintf(filename,"%s/%s_median_PSD_%s.dat",outdir,model_type,data->ifos[ifo]);
    medianPSDFile[ifo] = fopen(filename,"w");

    sprintf(filename,"%s/%s_median_PSD_forLI_%s.dat",outdir,model_type,data->ifos[ifo]);
    medianPSDFileLI[ifo] = fopen(filename,"w");

  }
  
  //BayesLine
  if(data->bayesLineFlag)
  {
    for(int ifo=0; ifo<NI; ifo++)
    {
      // cbc naming conventions are a bit different than the others, so gotta do this 
      if(cbcNames){
        sprintf(filename,"chains/cbc_spline_%s.dat.0", data->ifos[ifo]);
        splinechain[ifo] = fopen(filename,"r");
        
        sprintf(filename,"chains/cbc_lorentz_%s.dat.0", data->ifos[ifo]);
        linechain[ifo] = fopen(filename,"r");
      }
      else{
        sprintf(filename,"chains/%s%s_spline_%s.dat.0",data->runName,model_name,data->ifos[ifo]);
        splinechain[ifo] = fopen(filename,"r");
        
        sprintf(filename,"chains/%s%s_lorentz_%s.dat.0",data->runName,model_name,data->ifos[ifo]);
        linechain[ifo] = fopen(filename,"r");
      }
      discard_burnin_samples(splinechain[ifo], &COUNT, &BURNIN);
      discard_burnin_samples(linechain[ifo], &COUNT, &BURNIN);
    } 
  }
  
  int Nwaveform = COUNT-BURNIN;
  printf("Nsamples = %i, Count = %i, Burnin = %i\n",Nwaveform, COUNT, BURNIN);
  
  double **hoft_median    = double_matrix(NI-1, N-1); // time domain waveform (h of t)
  double **hoff_median    = double_matrix(NI-1, N/2-1);
  double **stokes_median = double_matrix(3, N/2-1);
  double **waveform_pol_median = double_matrix(data->Npol-1, N/2-1);
  double **psd_median     = double_matrix(NI-1, N/2-1);
  double **tftrack_median = double_matrix(NI-1, N-1);
  
  double ***waveform_mega = double_tensor(NI-1, Nwaveform-1, N-1);
  double ***stokes_mega = double_tensor(3, Nwaveform-1, N/2-1);
  double ***waveform_mega_pol = double_tensor(data->Npol-1, Nwaveform-1, N-1);
  double ***psd_mega      = double_tensor(NI-1, Nwaveform-1, N/2-1);

  FILE **residualFile = malloc(NI*sizeof(FILE *));

  // initialize bayesCBC struct 
  struct bayesCBC *bayescbc = malloc(sizeof(struct bayesCBC)); 
  if (cbcFlag || cbcGlitchFlag || cbcSignalFlag){
    initialize_bayescbc(bayescbc, data, chain, model);
  }
  
  
  for(int n=0; n<COUNT-BURNIN; n++)
  {
    
    /******************************************************************************/
    /*                                                                            */
    /*  Get current state of MCMC output into format for BayesWave code library   */
    /*                                                                            */
    /******************************************************************************/

    /*
     PSD MODEL
     */
    if(data->bayesLineFlag){
      parse_bayesline_parameters(data, model, splinechain, linechain, psd);
    }
    if ((n % 1000) == 0){
        fprintf(stdout, "%g \n", psd[0][200]);
    }
    
    
    /*
     GLITCH MODEL
     */
    if(glitchFlag || cbcGlitchFlag || fullFlag)
    {
      parse_glitch_parameters(data, model, glitchParams, grec);
      // glitch[ifo]->size holds the number of wavelets that describe that glitch 
      for(int ifo=0; ifo<NI; ifo++) dimension[ifo][glitch[ifo]->size]++;
    }
    
    /*
     SIGNAL MODEL
     */
    if(signalFlag || fullFlag || cbcSignalFlag)
    {
      parse_signal_parameters(data, model, signalParams, hrec);
     // for(int ifo=0; ifo<data->Npol; ifo++) for(int i=0; i<N; i++) hgeo[ifo][i] = model->signal[ifo]->templates[i];
      for(int pol=0; pol<data->Npol; pol++) for(int i=0; i<N; i++) hpol[pol][i] = model->signal[pol]->templates[i];
      dimension[0][signal[0]->size]++;
      
      Shf_Geocenter_full(data, model->projection, model->Snf, Sgeo, model->extParams);
    }

    /*
     CBC MODEL
     */
    if(cbcFlag || cbcGlitchFlag || cbcSignalFlag){
      // TODO set runPhase bool to true, I am NOT sure if this is valid or what runphase does
      // but in parse_cbc_parameters we need it so that bayescbc gets set correctly 
      data->runPhase = 1;
      // sets external parameters, not set otherwise
      parse_cbc_parameters_chain_model(data, model, chain, bayescbc, cbcParams, 0);
      // initialze reconstructed waveform
      for(int ifo=0; ifo<data->NI; ifo++) for(int i=0; i<data->N; i++) {hcbcrec[ifo][i] = model->cbctemplate[ifo][i];}

      //if (n < 8){
      //  fprintf(stdout, "Printing cbc template \n"); 
      //  for(ifo=0; ifo<data->NI; ifo++) for(i=0; i<data->N; i++) if(i%100 ==0) {fprintf(stdout, "%lg, ",model->cbctemplate[ifo][i]);}
      //}
      //fprintf(stdout, "\n");
    }

    /*
     CBC+GLITCH MODEL
     */
    if(cbcGlitchFlag){
      // add cbc and glitch waveforms together
      for(int ifo=0; ifo<data->NI; ifo++) for(int i=0; i<N; i++) hgrec[ifo][i] = hcbcrec[ifo][i] + grec[ifo][i];
    }

    /*
     CBC+SIGNAL MODEL
     */
    if(cbcSignalFlag){
      // add cbc and glitch waveforms together
      for(int ifo=0; ifo<data->NI; ifo++) for(int i=0; i<N; i++) hgrec[ifo][i] = hrec[ifo][i] + hcbcrec[ifo][i];
    }

    /*
     SIGNAL+GLITCH MODEL
     */
    if(fullFlag)
    {
      //Shf_Geocenter_full(data, model->projection, model->Snf, Sgeo, model->extParams);
      for(int ifo=0; ifo<data->NI; ifo++) for(int i=0; i<N; i++) hgrec[ifo][i] = hrec[ifo][i] + grec[ifo][i];
    }
    
    /******************************************************************************/
    /*                                                                            */
    /*  Compute various moments/overlaps to quantify waveform reconstruction      */
    /*                                                                            */
    /******************************************************************************/
    
    /*
     MATCH BETWEEN INJECTED AND RECOVERED WAVEFORM
     */
    
    if(injectionFlag)
    {
      if(glitchFlag)
      {
        ComputeWaveformOverlap(data, hinj, grec, psd, whitenedGlitchMoments);
        ComputeWaveformOverlap(data, hinj, grec, one, coloredGlitchMoments);
      }
      if(signalFlag)
      {
        ComputeWaveformOverlap(data, hinj, hrec, psd, whitenedSignalMoments);
        ComputeWaveformOverlap(data, hinj, hrec, one, coloredSignalMoments);
      }
      if(cbcFlag)
      {
        ComputeWaveformOverlap(data, hinj, hcbcrec, psd, whitenedCBCMoments);
        ComputeWaveformOverlap(data, hinj, hcbcrec, one, coloredCBCMoments);
      }
      if(fullFlag || cbcGlitchFlag || cbcSignalFlag)
      {
        ComputeWaveformOverlap(data, hinj, hgrec, psd, whitenedFullMoments);
        ComputeWaveformOverlap(data, hinj, hgrec, one, coloredFullMoments);
      }
    }
    
    /*
     WAVEFORM MOMENTS
     */
    if(glitchFlag)
    {
      ComputeWaveformMoments(data, grec, psd, whitenedGlitchMoments);
      ComputeWaveformMoments(data, grec, one, coloredGlitchMoments);
      /*
       Whitened hrss is meaningless and colored SNR are meaningless
       Swap their values in the structures so the moments files are self-contained
       */
      for(int ifo=0; ifo<NI; ifo++)
      {
        whitenedGlitchMoments->energy[ifo] = coloredGlitchMoments->energy[ifo];
        coloredGlitchMoments->snr[ifo]     = whitenedGlitchMoments->snr[ifo];
      }
    }
    if(signalFlag)
    {
      ComputeWaveformMoments(data, hrec, psd, whitenedSignalMoments);
      ComputeWaveformMoments(data, hrec, one, coloredSignalMoments);
      
      /*
       Whitened hrss is meaningless and colored SNR are meaningless
       Swap their values in the structures so the moments files are self-contained
       */
      for(int ifo=0; ifo<NI; ifo++)
      {
        whitenedSignalMoments->energy[ifo] = coloredSignalMoments->energy[ifo];
        coloredSignalMoments->snr[ifo]     = whitenedSignalMoments->snr[ifo];
      }
    }
    if(cbcFlag)
    {
      ComputeWaveformMoments(data, hcbcrec, psd, whitenedCBCMoments);
      ComputeWaveformMoments(data, hcbcrec, one, coloredCBCMoments);
      /*
       Whitened hrss is meaningless and colored SNR are meaningless
       Swap their values in the structures so the moments files are self-contained
       */
      for(int ifo=0; ifo<NI; ifo++)
      {
        whitenedCBCMoments->energy[ifo] = coloredCBCMoments->energy[ifo];
        coloredCBCMoments->snr[ifo]     = whitenedCBCMoments->snr[ifo];
      }
    }
    if(fullFlag || cbcGlitchFlag || cbcSignalFlag)
    {
      ComputeWaveformMoments(data, hgrec, psd, whitenedFullMoments);
      ComputeWaveformMoments(data, hgrec, one, coloredFullMoments);
    }
    
    for(int ifo=0; ifo<NI; ifo++)
    {
      if(glitchFlag && glitch[ifo]->size>0)
      {
        print_moments(ifo, injectionFlag, whitenedFile, whitenedGlitchMoments);
        print_moments(ifo, injectionFlag, coloredFile, coloredGlitchMoments);
      }
      if(signalFlag && signal[0]->size>0)
      {
        print_moments(ifo, injectionFlag, whitenedFile, whitenedSignalMoments);
        print_moments(ifo, injectionFlag, coloredFile, coloredSignalMoments);
      }
      if(cbcFlag)
      {
        print_moments(ifo, injectionFlag, whitenedFile, whitenedCBCMoments);
        print_moments(ifo, injectionFlag, coloredFile, coloredCBCMoments);
      }
      if(cbcGlitchFlag && glitch[ifo]->size>0)
      {
        print_moments(ifo, injectionFlag, whitenedFile, whitenedFullMoments);
        print_moments(ifo, injectionFlag, coloredFile, coloredFullMoments);
      }
      if(cbcGlitchFlag && glitch[ifo]->size>0)
      {
        print_moments(ifo, injectionFlag, whitenedFile, whitenedFullMoments);
        print_moments(ifo, injectionFlag, coloredFile, coloredFullMoments);
      }
      if(fullFlag && (signal[0]->size>0 || glitch[ifo]->size>0) )
      {
        print_moments(ifo, injectionFlag, whitenedFile, whitenedFullMoments);
        print_moments(ifo, injectionFlag, coloredFile, coloredFullMoments);
      }

    }

    /*
     TIME DOMAIN WAVEFORMS
     */
    //TODO: Are these still needed?, not used for CBC or GlitchCBC, files 2 big
    if(n%100 == 0 && !lite)
    {
      //fprintf(stdout, "printing waveforms \n"); 
      if(glitchFlag)
      {
        print_waveforms(data, grec, psd, recWhitenedWaveformFile);
        print_waveforms(data, grec, one, recColoredWaveformFile);
      }
      if(signalFlag)
      {
        print_waveforms(data, hrec, psd, recWhitenedWaveformFile);
        print_waveforms(data, hrec, one, recColoredWaveformFile);
      }
      /*
      if(cbcFlag)
      {
        print_waveforms(data, hcbcrec, psd, recWhitenedWaveformFile);
        print_waveforms(data, hcbcrec, one, recColoredWaveformFile);
      }
      if(fullFlag || cbcGlitchFlag || cbcSignalFlag)
      {
        print_waveforms(data, hgrec, psd, recWhitenedWaveformFile);
        print_waveforms(data, hgrec, one, recColoredWaveformFile);
      }
      */
    }
    
    
    /*
     RECONSTRUCTION POSTERIORS
     */
    for(int ifo = 0; ifo<NI; ifo++)
    {
      for (int ii = 0; ii < data->N/2; ii++)
      {
        if(glitchFlag)
        {
          waveform_mega[ifo][n][2*ii]   = grec[ifo][2*ii];
          waveform_mega[ifo][n][2*ii+1] = grec[ifo][2*ii+1];
        }
        if(signalFlag)
        {
          //reconstructed waveforms real and imaginary parts
          waveform_mega[ifo][n][2*ii]   = hrec[ifo][2*ii];
          waveform_mega[ifo][n][2*ii+1] = hrec[ifo][2*ii+1];
        }
        if(cbcFlag)
        {
          //reconstructed waveforms real and imaginary parts
          waveform_mega[ifo][n][2*ii]   = hcbcrec[ifo][2*ii];
          waveform_mega[ifo][n][2*ii+1] = hcbcrec[ifo][2*ii+1];
        }
        if(fullFlag || cbcGlitchFlag || cbcSignalFlag)
        {
          waveform_mega[ifo][n][2*ii]   = hgrec[ifo][2*ii];
          waveform_mega[ifo][n][2*ii+1] = hgrec[ifo][2*ii+1];
        }
        psd_mega[ifo][n][ii] = psd[ifo][ii];
      }
    }

    /*
     RELAXED POLARIZATION RECONSTRUCTION POSTERIORS
     */
    if(signalFlag)
    {
      //h_plus and h_cross mega structure
      for(int pol = 0; pol<data->Npol; pol++)
      {
        for (int ii = 0; ii < data->N/2; ii++)
        {
          waveform_mega_pol[pol][n][2*ii]   = hpol[pol][2*ii];
          waveform_mega_pol[pol][n][2*ii+1] = hpol[pol][2*ii+1];
        }
      }

      if(!data->polarizationFlag)
      {
        //Stokes parameters
        for (int ii = 0; ii < data->N/2; ii++)
        {
          //stokes_mega[0][n][ii] = hpol[0][2*ii]*hpol[0][2*ii] + hpol[0][2*ii+1]*hpol[0][2*ii+1] + hpol[1][2*ii]*hpol[1][2*ii] + hpol[1][2*ii+1]*hpol[1][2*ii+1];       // I, see appendix A in Romano&Cornish LRR (2017) 20:2
          //stokes_mega[1][n][ii] = hpol[0][2*ii]*hpol[0][2*ii] + hpol[0][2*ii+1]*hpol[0][2*ii+1] - hpol[1][2*ii]*hpol[1][2*ii] - hpol[1][2*ii+1]*hpol[1][2*ii+1];   // Q
          //stokes_mega[2][n][ii] = 2 * (hpol[0][2*ii]*hpol[1][2*ii] + hpol[0][2*ii+1]*hpol[1][2*ii+1]);                           // U
          //stokes_mega[3][n][ii] = 2 * (hpol[0][2*ii]*hpol[1][2*ii+1] - hpol[0][2*ii+1]*hpol[1][2*ii]);                           // V
          //different combinations of Stokes parameters
          stokes_mega[0][n][ii] = 2 * (hpol[0][2*ii]*hpol[1][2*ii] + hpol[0][2*ii+1]*hpol[1][2*ii+1]);                           // U
          stokes_mega[1][n][ii] = (hpol[1][2*ii]*hpol[1][2*ii] + hpol[1][2*ii+1]*hpol[1][2*ii+1]) / (hpol[0][2*ii]*hpol[0][2*ii] + hpol[0][2*ii+1]*hpol[0][2*ii+1]); // (I-Q)/(I+Q)
          stokes_mega[2][n][ii] = (hpol[0][2*ii]*hpol[1][2*ii+1] - hpol[0][2*ii+1]*hpol[1][2*ii]) / (hpol[0][2*ii]*hpol[0][2*ii] + hpol[0][2*ii+1]*hpol[0][2*ii+1]); // V/(I+Q)
          stokes_mega[3][n][ii] = (hpol[1][2*ii]*hpol[1][2*ii] + hpol[1][2*ii+1]*hpol[1][2*ii+1]) / (hpol[0][2*ii]*hpol[1][2*ii+1] - hpol[0][2*ii+1]*hpol[1][2*ii]);  // (I-Q)/V
        }
      }

    }
    
  } // End loop over chains
  
 double **residual = double_matrix(NI-1,N-1);
for (int ifo=0; ifo<NI; ifo++)
  {
    for(int ii=0; ii<data->imin; ii++)
    {
      residual[ifo][2*ii]   = 0.0;
      residual[ifo][2*ii+1] = 0.0;
    }
    for(int ii=data->imin; ii<data->N/2; ii++)
    {
      residual[ifo][2*ii]   = hoft[ifo][2*ii];
      residual[ifo][2*ii+1] = hoft[ifo][2*ii+1];
    }
  }
  
  int random_draw = (int)floor(Nwaveform * uniform_draw(chain->seed));
  printf("random_draw = %i\n",random_draw);

// Calculate median for relaxed polarization
  if(signalFlag)
  {
    for(int pol = 0; pol<data->Npol; pol++)
    {
      median_and_CIs_fourierdom(waveform_mega_pol[pol], N/2, Nwaveform, deltaF, medianFrequencyPolFile[pol], waveform_pol_median[pol]);
    }
    if(!data->polarizationFlag)
    {
      for(int pol = 0; pol<4; pol++)
      {
        median_and_CIs(stokes_mega[pol], N/2, Nwaveform, deltaF, medianStokesFile[pol], stokes_median[pol]);
      }
    }
  }
  // Calculate medians
  for (int ifo = 0; ifo < NI; ifo++) {
    if (waveform_flags){
      //Compute the median hoff waveform
      median_and_CIs(waveform_mega[ifo], N, Nwaveform, deltaF, medianFrequencyFile[ifo], hoft_median[ifo]);

      // Print fourier domain residuals from the median waveform reconstruction
      sprintf(filename, "%s/fourier_domain_%s_median_residual_%s.dat", outdir, model_type, data->ifos[ifo]);
      residualFile[ifo] = fopen(filename, "w");

      for (int ii = data->imin; ii < data->N / 2; ii++) {
        residual[ifo][2 * ii] -= hoft_median[ifo][2 * ii];
        residual[ifo][2 * ii + 1] -= hoft_median[ifo][2 * ii + 1];
      }

      for (int ii = 0; ii < data->N / 2; ii++) {
        fprintf(residualFile[ifo], "%g %g %g\n", (double) ii * deltaF, residual[ifo][2 * ii],
                residual[ifo][2 * ii + 1]);
      }
      fclose(residualFile[ifo]);
    }

    // Calc and print median PSD
    median_and_CIs(psd_mega[ifo], N / 2, Nwaveform, deltaF, medianPSDFile[ifo], psd_median[ifo]);

    // Calc and print median PSD for LALInference
    for (int ii = 0; ii < N / 2; ii++) {
      fprintf(medianPSDFileLI[ifo], "%g %g\n", ii * deltaF, psd_median[ifo][ii] * 2 * deltaF);
    }

    // Use "psd" vector to store glitch power spectrum, then calc and print median
    if (waveform_flags) {
      for (int n = 0; n < Nwaveform; n++) {
        for (int ii = 0; ii < N / 2; ii++) {
          psd_mega[ifo][n][ii] = waveform_mega[ifo][n][2 * ii] * waveform_mega[ifo][n][2 * ii] +
                                 waveform_mega[ifo][n][2 * ii + 1] * waveform_mega[ifo][n][2 * ii + 1];
        }
      }
      median_and_CIs(psd_mega[ifo], N / 2, Nwaveform, deltaF, medianFrequencyWaveformFile[ifo], hoff_median[ifo]);

      // Get (whitened) time domain waveforms, calc a print median
      for (int n = 0; n < Nwaveform; n++) {
        whiten_data(data->imin, data->imax, waveform_mega[ifo][n], psd[ifo]);
        get_time_domain_waveforms(waveform_mega[ifo][n], waveform_mega[ifo][n], data->N, data->imin, data->imax);
        norm = 1. / sqrt((double) data->N);
        for (int ii = 0; ii < data->N; ii++) {
          waveform_mega[ifo][n][ii] *= norm;
        }
      }
      median_and_CIs(waveform_mega[ifo], N, Nwaveform, deltaT, medianWaveformFile[ifo], hoft_median[ifo]);

      // Use mega waveform vector to store tf tracks. Again, calc and print median
      for (int n = 0; n < Nwaveform; n++) {
        tf_tracks(waveform_mega[ifo][n], waveform_mega[ifo][n], deltaT, N);
      }
      median_and_CIs(waveform_mega[ifo], N, Nwaveform, deltaT, medianTimeFrequencyFile[ifo], tftrack_median[ifo]);

      if (!lite) {
        print_spectrograms(data, outdir, model_type, data->ifos[ifo], hoft_median[ifo], one[ifo]);
      }

      //// residual spectrograms /////
      for (int ii = 0; ii < data->N; ii++) residual[ifo][ii] = hdata[ifo][ii] - hoft_median[ifo][ii];

      char qscan_type[MAXSTRINGSIZE];
      sprintf(qscan_type, "%s_residual", model_type);

      if (!lite) {
        print_spectrograms(data, outdir, qscan_type, data->ifos[ifo], residual[ifo], one[ifo]);
      }
    } // end non-noise model check
  }

  free_double_matrix(residual,NI-1);

  if(cbcFlag || cbcGlitchFlag || cbcSignalFlag)
  {
    fclose(cbcParams);
  }
  
  if(signalFlag)
  {
    for(int ifo=0; ifo<data->Npol; ifo++) fclose(signalParams[ifo]);
    fclose(whitenedGeocenterFile[0]);
    fclose(coloredGeocenterFile[0]);
  }

  for(int ifo=0; ifo<NI; ifo++)
  {
    if(data->bayesLineFlag)
    {
      fclose(splinechain[ifo]);
      fclose(linechain[ifo]);
    }
    
    if(waveform_flags)
    {
      fclose(recWhitenedWaveformFile[ifo]);
      fclose(recColoredWaveformFile[ifo]);
      if(glitchFlag || fullFlag || cbcGlitchFlag) fclose(glitchParams[ifo]);
      fclose(whitenedFile[ifo]);
      fclose(coloredFile[ifo]);
      
      fclose(medianWaveformFile[ifo]);
      fclose(medianFrequencyWaveformFile[ifo]);
      fclose(medianFrequencyFile[ifo]);
      fclose(medianTimeFrequencyFile[ifo]);
    }
    fclose(medianPSDFile[ifo]);
    fclose(medianPSDFileLI[ifo]);
  }

  if(!data->polarizationFlag)
  {
    for(int pol=0; pol<4; pol++)
    {
      if(glitchFlag || signalFlag || fullFlag)
      {
          fclose(medianStokesFile[pol]);
      }
    }
  }

  for(int pol=0; pol<data->Npol; pol++)
  {
    if(glitchFlag || signalFlag || fullFlag)
    {
        fclose(medianFrequencyPolFile[pol]);
    }
  }

  free(glitchParams);
  free(signalParams);
  free(residualFile);
  free(medianPSDFile);
  free(medianPSDFileLI);

  free_double_tensor(waveform_mega, NI-1, Nwaveform-1);
  free_double_tensor(waveform_mega_pol, data->Npol-1, Nwaveform-1);
  free_double_tensor(stokes_mega, 3, Nwaveform-1);
  free_double_tensor(psd_mega, NI-1, Nwaveform-1);
  free_double_matrix(hoft_median,NI-1);
  free_double_matrix(hoff_median,NI-1);
  free_double_matrix(stokes_median,3);
  free_double_matrix(waveform_pol_median, data->Npol-1);
  free_double_matrix(psd_median,NI-1);
  free_double_matrix(tftrack_median,NI-1);
  free_double_matrix(grec,NI-1);
  free_double_matrix(hrec,NI-1);
  free_double_matrix(hcbcrec,NI-1);
  //free_double_matrix(hgeo,data->Npol-1);
  free_double_matrix(hpol,data->Npol-1);
  free_double_vector(Sgeo);
  
  free_moments(whitenedGlitchMoments);
  free_moments(coloredGlitchMoments);
  free_moments(whitenedFullMoments);
  free_moments(coloredFullMoments);

  if(waveform_flags) {
    print_stats(data, GPStrig, injectionFlag, outdir, model_type, dimension);
    if(data->bayesLineFlag) for(int ifo=0; ifo<NI; ifo++) anderson_darling_test(outdir, ifo, data->fmin, data->Tobs, model_type, data->ifos);
  }
  return 0;
}

/************************************************/
/*                                              */
/*         Anderson-Darling Calculator          */
/*                                              */
/************************************************/

/*
 adinf(z) AND AD(n,z) ROUTINES FROM DOI:10.18637/jss.v009.i02
 
 https://www.jstatsoft.org/article/view/v009i02
 
 Anderson-Darling test for uniformity.   Given an ordered set
 x_1<x_2<...<x_n
 of purported uniform [0,1) variates,  compute
 a = -n-(1/n)*[ln(x_1*z_1)+3*ln(x_2*z_2+...+(2*n-1)*ln(x_n*z_n)]
 where z_1=1-x_n, z_2=1-x_(n-1)...z_n=1-x_1, then find
 v=adinf(a) and return  p=v+errfix(v), which should be uniform in [0,1),
 that is, the p-value associated with the observed x_1<x_2<...<x_n.
 */

 /* Short, practical version of full ADinf(z), z>0.   */
static double adinf(double z)
{ if(z<2.) return exp(-1.2337141/z)/sqrt(z)*(2.00012+(.247105-  \
                                                      (.0649821-(.0347962-(.011672-.00168691*z)*z)*z)*z)*z);
  /* max |error| < .000002 for z<2, (p=.90816...) */
  return
  exp(-exp(1.0776-(2.30695-(.43424-(.082433-(.008056 -.0003146*z)*z)*z)*z)*z));
  /* max |error|<.0000008 for 4<z<infinity */
}

/* The function AD(n,z) returns Prob(A_n<z) where
 A_n = -n-(1/n)*[ln(x_1*z_1)+3*ln(x_2*z_2+...+(2*n-1)*ln(x_n*z_n)]
 z_1=1-x_n, z_2=1-x_(n-1)...z_n=1-x_1, and
 x_1<x_2<...<x_n is an ordered set of iid uniform [0,1) variates.
 */
static double AD(int n,double z){
  double c,v,x;
  x=adinf(z);
  /* now x=adinf(z). Next, get v=errfix(n,x) and return x+v; */
  if(x>.8)
  {v=(-130.2137+(745.2337-(1705.091-(1950.646-(1116.360-255.7844*x)*x)*x)*x)*x)/n;
    return x+v;
  }
  c=.01265+.1757/n;
  if(x<c){ v=x/c;
    v=sqrt(v)*(1.-v)*(49*v-102);
    return x+v*(.0037/(n*n)+.00078/n+.00006)/n;
  }
  v=(x-c)/(.8-c);
  v=-.00022633+(6.54034-(14.6538-(14.458-(8.259-1.91864*v)*v)*v)*v)*v;
  return x+v*(.04213+.01365/n)/n;
}

void print_anderson_darling_p_values(char *outDir, int Nsamp)
{
  int i;
  int nA = 30;
  double Amin = log(1.e-2);
  double Amax = log(15.);
  double dA   = (Amax-Amin)/(double)nA;
  double A2;
  
  char filename[MAXSTRINGSIZE];
  sprintf(filename,"%s/anderson_darling_p_values.dat", outDir);
  FILE *ADfile = fopen(filename,"w");
  for(i=0;i<nA; i++)
  {
    A2 = exp(Amin+i*dA);
    fprintf(ADfile,"%lg %lg\n",A2,1.-AD(Nsamp,A2));
  }
  fclose(ADfile);
}

void anderson_darling_test(char *outdir, int ifo, double fmin, double Tobs, char *model, char **ifos)
{
  char filename[MAXSTRINGSIZE];

  FILE *dFile;
  FILE *nFile;
  FILE *qFile;
  FILE *pFile;
  FILE *cFile;

  int N,n,nn, i;
  double f,dre,dim,Sn,Sn25,Sn75,Sn05,Sn95;

  int Nsamp;
  double *samples=NULL;

  // how many samples in the data?
  N=0;
  Nsamp=0;

  //get residual file
  sprintf(filename,"%s/fourier_domain_%s_median_residual_%s.dat",outdir,model,ifos[ifo]);
  dFile = fopen(filename,"r");

  while(!feof(dFile))
  {
    fscanf(dFile,"%lg %lg %lg",&f,&dre,&dim);
    N++;
    if(f>fmin) Nsamp++;
  }
  N--;
  fclose(dFile);

  // get memory to ingest file
  Nsamp *= 2; //re and im parts
  samples=malloc(Nsamp*sizeof(double));
  nn=0;
  
  // combine Fourier coefficints
  //get residual file
  sprintf(filename,"%s/fourier_domain_%s_median_residual_%s.dat",outdir,model,ifos[ifo]);
  dFile = fopen(filename,"r");
  
  //get median PSD file
  sprintf(filename,"%s/%s_median_PSD_%s.dat",outdir,model,ifos[ifo]);
  nFile = fopen(filename,"r");
  
  //whiten residual
  for(n=0; n<N; n++)
  {
    fscanf(dFile,"%lg %lg %lg",&f,&dre,&dim);
    fscanf(nFile,"%lg %lg %lg %lg %lg %lg",&f,&Sn,&Sn25,&Sn75,&Sn05,&Sn95);
    if(f>=fmin)
    {
      //samples[2*nn]   = dre/sqrt(Sn/(Tobs/2.));
      //samples[2*nn+1] = dim/sqrt(Sn/(Tobs/2.));
      samples[2*nn]   = dre/sqrt(Sn/2.);//the factor of 2 works, but we need to understand why better
      samples[2*nn+1] = dim/sqrt(Sn/2.);
      nn++;
    }
  }
  fclose(dFile);
  fclose(nFile);

  // Covariance matrix of residual
  sprintf(filename,"%s/%s_residual_covariance_%s.dat",outdir,model,ifos[ifo]);
  cFile = fopen(filename,"w");
  
  double re,im;
  double mu_re = 0.;
  double mu_im = 0.;
  double rere  = 0.;
  double imim  = 0.;
  double reim  = 0.;
  
  //get means
  for(n=0; n<Nsamp/2; n++)
  {
    re = samples[2*n];
    im = samples[2*n+1];
    
    mu_re += re;
    mu_im += im;
  }
  mu_re/=(double)(Nsamp/2);
  mu_im/=(double)(Nsamp/2);
  
  //get covariances
  for(n=0; n<Nsamp/2; n++)
  {
    re = samples[2*n];
    im = samples[2*n+1];

    rere += (re-mu_re)*(re-mu_re);
    imim += (im-mu_im)*(im-mu_im);
    reim += (re-mu_re)*(im-mu_im);
  }
  rere /= (double)(Nsamp/2);
  reim /= (double)(Nsamp/2);
  imim /= (double)(Nsamp/2);
  
  double det = rere*imim - reim*reim;
  double tr = rere+imim;
  
  double lambda1 = tr/2. + sqrt(tr*tr/4. - det);
  double lambda2 = tr/2. - sqrt(tr*tr/4. - det);
  
  double norm;
  double evector1[2];
  double evector2[2];

  evector1[0] = 1.;
  evector1[1] = (lambda1-rere)/reim;
  norm = sqrt(evector1[0]*evector1[0]+evector1[1]*evector1[1]);
  evector1[0]/=norm;
  evector1[1]/=norm;
  
  evector2[0] = 1.;
  evector2[1] = (lambda2-rere)/reim;
  norm = sqrt(evector2[0]*evector2[0]+evector2[1]*evector2[1]);
  evector2[0]/=norm;
  evector2[1]/=norm;

  double theta = atan2(evector1[1],evector1[0]);
  double Rx = sqrt(lambda1);
  double Ry = sqrt(lambda2);
  double Cx = mu_re;
  double Cy = mu_im;
  
  for(n=0; n<=100; n++)
  {
    double angle = n*(2.*M_PI/100.);
    re = 1.*( Rx*cos(angle)*cos(theta) - Ry*sin(angle)*sin(theta) ) + Cx;
    im = 1.*( Rx*cos(angle)*sin(theta) + Ry*sin(angle)*cos(theta) ) + Cy;
    fprintf(cFile,"%.16lg %.16lg ",re,im);
    
    re = 2.*( Rx*cos(angle)*cos(theta) - Ry*sin(angle)*sin(theta) ) + Cx;
    im = 2.*( Rx*cos(angle)*sin(theta) + Ry*sin(angle)*cos(theta) ) + Cy;
    fprintf(cFile,"%.16lg %.16lg ",re,im);
    
    re = 3.*( Rx*cos(angle)*cos(theta) - Ry*sin(angle)*sin(theta) ) + Cx;
    im = 3.*( Rx*cos(angle)*sin(theta) + Ry*sin(angle)*cos(theta) ) + Cy;
    fprintf(cFile,"%.16lg %.16lg\n",re,im);
  }
  fclose(cFile);
  
  // Amp-phase plot
  sprintf(filename,"%s/%s_residual_Amp-phi_%s.dat",outdir,model,ifos[ifo]);
  pFile = fopen(filename,"w");
  for(n=0; n<Nsamp/2; n++)
  {
    fprintf(pFile,"%lg %lg\n",samples[2*n],samples[2*n+1]);
  }
  fclose(pFile);

  gsl_sort(samples,1,Nsamp);
  
  // Q-q plot
  sprintf(filename,"%s/%s_residual_P-p_%s.dat",outdir,model,ifos[ifo]);
  qFile = fopen(filename,"w");
  for(n=0; n<Nsamp; n++)
  {
    double p = (double)n/(double)Nsamp;
    double s = sqrt(p*(1.0-p)/(double)Nsamp);
    //x,p
    // gsl_cdf_ugaussian is the CDF for the unit gaussian 
    fprintf(qFile,"%lg %lg ",p,gsl_cdf_ugaussian_P(samples[n]));
    //1-sigma
    fprintf(qFile,"%lg %lg ",p+s,p-s);
    //2-sigma
    fprintf(qFile,"%lg %lg ",p+2.*s,p-2.*s);
    //3-sigma
    fprintf(qFile,"%lg %lg ",p+3.*s,p-3.*s);
    fprintf(qFile,"\n");
  }
  fclose(qFile);
  
  double S,A;

  // Anderson-Darling statistic
  double lx,ly;

  //We want to compute AD for different frequency ranges, from srate 256 up to the actual srate of the run.
  
  double factorAD = ((Nsamp+2.*fmin*Tobs)/256./Tobs);// srate/256
  int iAD = (int)(log(factorAD)/log(2.));
  double p, c;
  int newNsamp;

  //We also need the unsorted samples again 

  sprintf(filename,"%s/fourier_domain_%s_median_residual_%s.dat",outdir,model,ifos[ifo]);
  dFile = fopen(filename,"r");
  sprintf(filename,"%s/%s_median_PSD_%s.dat",outdir,model,ifos[ifo]);
  nFile = fopen(filename,"r");  
  nn=0;
  for(n=0; n<N; n++)
  {
    fscanf(dFile,"%lg %lg %lg",&f,&dre,&dim);
    fscanf(nFile,"%lg %lg %lg %lg %lg %lg",&f,&Sn,&Sn25,&Sn75,&Sn05,&Sn95);
    if(f>=fmin)
    {
      //samples[2*nn]   = dre/sqrt(Sn/(Tobs/2.));
      //samples[2*nn+1] = dim/sqrt(Sn/(Tobs/2.));
      samples[2*nn]   = dre/sqrt(Sn/2.);//the factor of 2 works, but we need to understand why better
      samples[2*nn+1] = dim/sqrt(Sn/2.);
      nn++;
    }
  }
  fclose(dFile);
  fclose(nFile);

  sprintf(filename,"%s/%s_anderson_darling_%s.dat",outdir,model,ifos[ifo]);
  FILE *afile = fopen(filename,"w");
  c=1;

  for (i=0; i<=iAD; i++){
    newNsamp = (int)(Nsamp+2.*fmin*Tobs)/(c)-2.*fmin*Tobs;// Samples only up to srate = (Nsamp+2.*fmin*Tobs)/(2**i)/Tobs, namely 256, 512, ...
    S=0.0;

    double *newsamples=NULL;
    newsamples=malloc(newNsamp*sizeof(double));

    for(n=0; n<newNsamp; n++) newsamples[n]=samples[n];

    gsl_sort(newsamples,1,newNsamp);

    for(n=0; n<newNsamp; n++)
    {
      double x = gsl_cdf_ugaussian_P(newsamples[n]);
      if(x<0.999)
      {
        lx = log(x);
        ly = log(1.-x);
      }
      else
      {
        double u = newsamples[n];
        double z = 1.0-1.0/(u*u)+3.0/(u*u*u*u)-15.0/(u*u*u*u*u*u)+105.0/(u*u*u*u*u*u*u*u);
        double y = z*exp(-u*u/2.0)/(u*sqrt(M_PI));
        lx = -y;
        ly = -u*u/2.0 +log(z/(u*sqrt(M_PI)));
      }
      S += ((2.*(double)(n+1)-1.0)*lx + ((double)(2*newNsamp)-2.*(double)(n+1)+1.0)*ly)/(double)(newNsamp);
    }
    A = (double)(-1*newNsamp) - S;
    
    // p-value of A^2
    p = 1.-AD(newNsamp,A);
    fprintf(afile,"%g %lg %lg\n",(Nsamp+2.*fmin*Tobs)/(c)/Tobs, A, p);
    c *=2;
  
    free(newsamples);
  }

  free(samples); 
  fclose(afile);


  // Besides the median PSD, we also compute the AD and p value with the burnin PSD

  double *samplesB=NULL;
  samplesB=malloc(Nsamp*sizeof(double));
  nn=0;
  //get residual file
  sprintf(filename,"%s/fourier_domain_%s_median_residual_%s.dat",outdir,model,ifos[ifo]);
  dFile = fopen(filename,"r");
  
  //get burnin PSD file
  sprintf(filename,"%s_burnin_psd.dat",ifos[ifo]);
  nFile = fopen(filename,"r");

  //whiten residual with the burnin PSD                                                                                                                                          
    for(n=0; n<N; n++)
    {
      fscanf(dFile,"%lg %lg %lg",&f,&dre,&dim);
      fscanf(nFile,"%lg %lg",&f,&Sn);
      if(f>=fmin)
	{
	  //samples[2*nn]   = dre/sqrt(Sn/(Tobs/2.));                                                                                                                                  
	  //samples[2*nn+1] = dim/sqrt(Sn/(Tobs/2.));                                                                                                                                  
	  samplesB[2*nn]   = dre/sqrt(Sn*(Tobs/2.)/2.);//I am totally winging this factor here                                                                              
	  samplesB[2*nn+1] = dim/sqrt(Sn*(Tobs/2.)/2.);
	  nn++;
	}
    }
  fclose(dFile);
  fclose(nFile);
  
  //Compute and print the Anderson-Darling statistic for the residual whitened by the burnin PSD
  S=0.0;

  gsl_sort(samplesB,1,Nsamp);


  for(n=0; n<Nsamp; n++)
    {
       double x = gsl_cdf_ugaussian_P(samplesB[n]);
       if(x<0.999)
	{
	  lx = log(x);
	  ly = log(1.-x);
	}
      else
	{
	  double u = samplesB[n];
	  double z = 1.0-1.0/(u*u)+3.0/(u*u*u*u)-15.0/(u*u*u*u*u*u)+105.0/(u*u*u*u*u*u*u*u);
	  double y = z*exp(-u*u/2.0)/(u*sqrt(M_PI));
	  lx = -y;
	  ly = -u*u/2.0 +log(z/(u*sqrt(M_PI)));
	}
      S += ((2.*(double)(n+1)-1.0)*lx + ((double)(2*Nsamp)-2.*(double)(n+1)+1.0)*ly)/(double)(Nsamp);
    }
  A = (double)(-1*Nsamp) - S;
  
  // p-value of A^2
  p = 1.-AD(Nsamp,A);
  
  free(samplesB);
  sprintf(filename,"%s/%s_anderson_darling_burnin_%s.dat",outdir,model,ifos[ifo]);
  FILE *afileB = fopen(filename,"w");
  fprintf(afileB,"%lg %lg\n",A, p);
  fclose(afileB);



}

void get_time_domain_envelope(double *envelope, double *hdata_sample, int N, int imin, int imax)
{
  //Allocate memory for hoft_original, hdata_shifted, and hoft_shifted                                                                                                       
  double *hoft_original = malloc(N*sizeof(double));
  double *hoft_shifted = malloc(N*sizeof(double));
  double *hdata_shifted = malloc(N*sizeof(double));

  //Get time domain waveform                                                                                                                                                 
  get_time_domain_waveforms(hoft_original, hdata_sample, N, imin, imax);

  //Shift frequency domain waveform by pi/2                                                                                                                                  
  for(int i = 0; i<(int)(N/2.0); i++){
    hdata_shifted[2*i] = -1*hdata_sample[2*i+1];
    hdata_shifted[2*i+1] = hdata_sample[2*i];
  }

  //Get time domain waveform after shift                                                                                                                                     
  get_time_domain_waveforms(hoft_shifted, hdata_shifted, N, imin, imax);

  //Calculate the envelope                                                                                                                                                   
  for(int i=0; i<N; i++){
    envelope[i]=sqrt(hoft_original[i]*hoft_original[i]+hoft_shifted[i]*hoft_shifted[i]);
  }

  free(hoft_original);
  free(hoft_shifted);
  free(hdata_shifted);
}

double get_time_at_max(double *hdata_sample, int N, int imin, int imax, double deltaT)
{
  //grab the time domain envelope                                                                                                                                            
  double *amplitude_envelope = malloc(N*sizeof(double));
  get_time_domain_envelope(amplitude_envelope, hdata_sample, N, imin, imax);
  //find the iteration of the maximum amplitude                                                                                                                              
  int max_iter = 0;
  double max_time = 0;
  double max_amp = 0;
  for(int i=0; i<N; i++){
    if(amplitude_envelope[i]>max_amp){
      max_amp = amplitude_envelope[i];
      max_iter = i;
    }
  }
  //find the time of the maximum amplitude                                                                                                                                   
  max_time = (double)max_iter*deltaT;

  free(amplitude_envelope);

  return(max_time);
}

void get_freq_at_max(double *hdata_sample, int N, int imin, int imax, double deltaT, double *f_at_max_amp)
{
  //grab the time at maximum amplitude                                                                                                                                       
  double max_time = get_time_at_max(hdata_sample, N, imin, imax, deltaT);
  //grab the instantaneous frequency at that time                                                                                                                            
  double *instantaneous_freq = malloc(N*sizeof(double));
  double *hoft_sample = malloc(N*sizeof(double));
  get_time_domain_waveforms(hoft_sample, hdata_sample, N, imin, imax);
  tf_tracks(hoft_sample, instantaneous_freq, deltaT, N);
  *f_at_max_amp = instantaneous_freq[(int)(max_time/deltaT)];

  free(instantaneous_freq);
  free(hoft_sample);
}
