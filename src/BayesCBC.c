/*******************************************************************************************

Copyright (c) 2020-2021 Katerina Chatziioannou, Neil Cornish, Sophie Hourihane, Marcella Wijngaarden 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

**********************************************************************************************/


#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <omp.h>

#include "BayesCBC.h"
#include "BayesWaveMath.h"
#include "BayesCBC_internals.h"
#include "Constants.h"
#include <lal/LALSimInspiral.h>
#include <lal/FrequencySeries.h>

#ifndef _OPENMP
#define omp ignore
#endif

//OSX
// clang -Xpreprocessor -fopenmp -lomp -w -o BayesCBC BayesCBC.c IMRPhenomD_internals.c IMRPhenomD.c -lgsl -lgslcblas  -lm

// CIT
// gcc -std=gnu99 -fopenmp -w -o BayesCBC BayesCBC.c IMRPhenomD_internals.c IMRPhenomD.c -lgsl -lgslcblas  -lm

//##############################################
//MT modifications

// gsl_rng **rvec;
//##############################################


int main(int argc, char *argv[])
{
    int status;
    status =0;
    //status = bayes_cbc(argc, argv);
    return status;
}

int check_bound(const double val, const double lower_bound, const double upper_bound){
  // returns False if val out of bounds (includes edge)
  // returns True if in bounds
  return((lower_bound <= val) & (val <= upper_bound));
}

void print_bounds(double **pallx, const struct bayesCBC *rundata) {
  // prints out pallx and whether value is in bounds
  // pallx should be NC long by NP
  for (int ic = 0; ic < rundata->NC; ic++) {
    for (int p = 0; p < rundata->NP; p++) {
      fprintf(stdout, "\n%i\t", ic);
      int check = check_bound(pallx[ic][p], rundata->min[p], rundata->max[p]);
      fprintf(stdout, " %i: %i, %f < %f < %f", p, check, rundata->min[p], pallx[ic][p], rundata->max[p]);
    }
    fprintf(stdout, "\n\t");
  }
}

void initialize_net(struct Net *net, double Tobs, double trigtime, int Ndet, char *argv[], double twidth){
    int i;

    // Trigtime is given relative to the segment start

    // Note that H is always called 0, L is 1 and V is 2 for data read and antenna response. 
    // But the ifo order can be anything. The labels array handles the mapping. 
    // e.g. if the command line has 2 0 1, then ifo order is 0=Virgo, 1=H and 2=L.
    
    net->Nifo = Ndet-3;
    net->labels = int_vector(net->Nifo);
    for (i = 0; i < net->Nifo; ++i) {  
        // BayesWave give list with named labels
        // Convert to integers for BayesCBC.
        // printf("argvi: %s", )
        if(strcmp(argv[i],"H1")==0) {
            net->labels[i] = 0;
        }
        else if(strcmp(argv[i],"L1")==0) {
            net->labels[i] = 1;
        }
        else if(strcmp(argv[i],"V1")==0) {
            net->labels[i] = 2;
        }
        else {
            net->labels[i] = atoi(argv[3+i]);
        }
        
    }
    net->delays = double_matrix(net->Nifo,net->Nifo); // holds maximum time delay between detectors (ifo1, ifo2)
    
    printf("Detectors: ");
    for (i = 0; i < net->Nifo; ++i) printf("%d ", net->labels[i]);
    printf("\n");

    // time_delays needs labels to be initialized st labels[0])
    time_delays(net); // initializes net->delays

    if(twidth < 0.0) // If twidth < 0, (set by --CBCWindow) it wasn't given so use default window
    {
        net->tmax = trigtime+1.5; // = Tobs-0.5; upper peak time  (Trigger time is Tobs-2)
        net->tmin = trigtime-0.5; // = Tobs-2.5; lower peak time (Trigger time is Tobs-2)
        printf("default merger time window:  Tobs-2.5 < tc < Tobs-0.5\n");
    }
    else
    {
        net->tmax = trigtime+0.5*twidth;
        net->tmin = trigtime-0.5*twidth;
        printf("chosen merger time window:  (Tobs - %f)-%f < tc < (Tobs + %f)+%f\n", Tobs - trigtime, 0.5*twidth, Tobs - trigtime, 0.5*twidth);
    }

    // Antenna patterns and time delays for each detector
    net->Fplus    = malloc(net->Nifo*sizeof(double));
    net->Fcross   = malloc(net->Nifo*sizeof(double));
    net->dtimes   = malloc(net->Nifo*sizeof(double));
        
    net->Tobs = Tobs;
    printf("%f %f\n", net->tmin, net->tmax);

}

// QNM frequency
double fQNM(double m1, double m2, double chi1, double chi2)
{
 
    double q, eta, f;
    double Z1, Z2;
    double risco, Eisco, Lisco;
    double Mf, atot, aeff, af;
    double p0, p1, k00, k01, k02, k10, k11, k12, scri;
    
    p0 = 0.04827;   // fit parameters
    p1 = 0.01707;
    k00 = -3.82;
    k01 = -1.2019;
    k02 = -1.20764;
    k10 = 3.79245;
    k11 = 1.18385;
    k12 = 4.90494;
    scri = 0.41616;

    if(m1 < m2)
    {
        q = m1/m2;
    }
    else
    {
        q = m2/m1;
    }
    
    eta = q/((1.0+q)*(1.0+q));    // symmetric mass ratio
    // if (isnan(eta)) printf("eta is nan! \n");
    atot = (chi1+q*q*chi2)/((1.0+q)*(1.0+q));
    aeff = atot+scri*eta*(chi1+chi2);   //technically, only 1605.01938 uses this, but the result for Mf is the same whether you use atot or aeff
    Z1 = 1.0 + pow((1.0 - aeff*aeff),(1./3.))*(pow((1. + aeff),(1./3.)) + pow((1. - aeff),(1./3.)));
    Z2 = sqrt(3.0*aeff*aeff + Z1*Z1);
    risco = 3.0 + Z2 - (aeff/(fabs(aeff)+1.0e-20))*sqrt((3.0 - Z1)*(3.0 + Z1 + 2.0*Z2));
    Eisco = sqrt(1.0-2./(3.0*risco));
    Lisco = (2./(3.0*sqrt(3.)))*(1.+2.0*sqrt(3.0*risco-2.0));
    
    // printf("%f %f %f %f %f %f\n", eta, atot, Z1, Z2, risco, Eisco);
    
    Mf = (1.0-eta*(1.0-Eisco)-4.0*eta*eta*(4.0*p0+16.0*p1*atot*(atot+1.0)+Eisco-1.0))*(m1+m2);
    // if (isnan(Mf)) printf("Mf is nan! \n");

    af = atot + eta*(Lisco-2*atot*(Eisco-1.0)) + k00*eta*eta + k01*eta*eta*aeff \
    + k02*eta*eta*aeff*aeff + k10*eta*eta*eta + k11*eta*eta*eta*aeff + k12*eta*eta*eta*aeff*aeff;
    // if (isnan(af)) printf("af is nan! \n");

    // af cannot exceed 1.0
    if (af > 1.0) af = 1.0;

    f = (1.5251-1.1568*pow((1.0-af),0.1292))/(TPI*Mf);  // Berti fit, from gr-qc/0512160
    
    if (isnan(f)) printf("%f %f %f %f %f %f\n", eta, atot, Z1, Z2, risco, Eisco);

    return(f);
    
}

// Wrapper for QNM frequency
double fringdown(double *param)
{
    double f, Mchirp, M, eta, dm, m1, m2, chi1, chi2;
    
    Mchirp = exp(param[0])/MSUN_SI*TSUN;
    M = exp(param[1])/MSUN_SI*TSUN;
    eta = pow((Mchirp/M), (5.0/3.0));
    if(eta > 0.25) eta = 0.25;
    dm = sqrt(1.0-4.0*eta);
    m1 = M*(1.0+dm)/2.0;
    m2 = M*(1.0-dm)/2.0;
    chi1 = param[2];
    chi2 = param[3];
    
    f = fQNM(m1, m2, chi1, chi2);
    //printf("fringdown m1, m2, chi1, chi2, Mchirp, Mtot: %f, %f, %f, %f, %f, %f\n", m1/TSUN, m2/TSUN, chi1, chi2, Mchirp/TSUN, M/TSUN);
    
    if (isnan(f)){
        printf("m1, m2, chi1, chi2, Mchirp, Mtot: %f, %f, %f, %f, %f, %f\n", m1/TSUN, m2/TSUN, chi1, chi2, Mchirp/TSUN, M/TSUN);
    }
    return(f);

}

void t_of_f(double *param, double *times, double *freqs, int N)
{
    int i;
    double t, x;
    double f, dt, fring, a, Mchirp, M, eta, dm, m1, m2, chi, chi1, chi2, tc, theta;
    double gamma_E=0.5772156649; //Euler's Constant-- shows up in 3PN term
    
    Mchirp = exp(param[0])/MSUN_SI*TSUN;
    M = exp(param[1])/MSUN_SI*TSUN;
    eta = pow((Mchirp/M), (5.0/3.0));
    if(eta > 0.25) eta = 0.25;
    dm = sqrt(1.0-4.0*eta);
    m1 = M*(1.0+dm)/2.0;
    m2 = M*(1.0-dm)/2.0;
    chi1 = param[2];
    chi2 = param[3];

    chi = (m1*chi1+m2*chi2)/M;
    
    tc = param[5];
    
    for (i = 0; i < N; ++i)
    {
        
    f = freqs[i];
    
    x = pow(PI*M*f,2.0/3.0);
    
    times[i] =  tc-pow(x,-4.0)*5.0*M/(256.0*eta)*(1.0      // 0 PN
                                           +((1/252.0)*(743.0+924.0*eta)*x)  // 1 PN
                                           +(2.0/15.0)*(-48.0*PI+113.0*chi-38.0*eta*(chi1+chi2))*pow(x,3.0/2.0)    // 1.5 PN
                                           + (1.0/508032.0)*(3058673.0+5472432.0*eta+4353552.0*eta*eta-5080320.0*chi*chi+127008.0*eta*chi1*chi2)*pow(x,4.0/2.0) // 2 PN
                                           +(1.0/756.0)*(3.0*PI*(-7729.0+1092.0*eta)+1512.0*chi*chi*chi-56.0*eta*(1285.0+153.0*eta)*(chi1+chi2)+chi*(147101.0+504.0*eta*(13.0-9.0*chi1*chi2)))*pow(x,5.0/2.0)// 2.5 PN
                                                  +((6848.0*gamma_E)/105.0+PI*PI/12.0*(512.0-451.0*eta)+(25565.0*eta*eta*eta)/1296.0+(-10052469856691.0+24236159077900.0*eta)/23471078400.0+(35.0*chi*chi)/3.0+eta*eta*(-(15211.0/1728.0)+19.0*chi1*chi1+(245.0*chi1*chi2)/6.0+19.0*chi2*chi2)+8.0*PI/3.0*(-73.0*chi+28.0*eta*(chi1+chi2))-eta/336.0*(26992.0*chi*chi-995.0*chi1*chi2+30688.0*chi*(chi1+chi2))+3424.0/105.0*log(16.0*x))*pow(x,6.0/2.0));  // 3PN
 
    }
    
    
    
}


void f_of_t(double *param, double *times, double *freqs, int N)
{
    // [0] log(Mc) [1] log(Mt) [2] chi1 [3] chi2 [4] phi0 [5] tp [6] log(DL) [7] alpha [8] sindelta [9] psi [10] ciota
    
    int i;
    double f, t, dt, fring, a, Mchirp, M, eta, dm, m1, m2, chi, chi1, chi2, tc, theta;
    double gamma_E=0.5772156649; //Euler's Constant-- shows up in 3PN term
    double PN1, PN15, PN2, PN25, PN3, PN35;
    double theta2, theta3, theta4, theta5, theta6, theta7;
    
    Mchirp = exp(param[0])/MSUN_SI*TSUN;
    M = exp(param[1])/MSUN_SI*TSUN;
    eta = pow((Mchirp/M), (5.0/3.0));
    if(eta > 0.25) eta = 0.25;
    dm = sqrt(1.0-4.0*eta);
    m1 = M*(1.0+dm)/2.0;
    m2 = M*(1.0-dm)/2.0;
    chi1 = param[2];
    chi2 = param[3];

    chi = (m1*chi1+m2*chi2)/M;
    
    tc = param[5];
    
    fring = fQNM(m1,m2,chi1,chi2);
    
    //printf("fring %f\n", fring);
    
    for (i = 0; i < N; ++i)
    {
        
    t = times[i];
        
    freqs[i] = -1.0;
        
    if(tc > t)
    {
        
        theta = pow(eta*(tc-t)/(5.0*M),-1.0/8.0);
        theta2 = theta*theta;
        theta3 = theta2*theta;
        theta4 = theta2*theta2;
        theta5 = theta2*theta3;
        theta6 = theta3*theta3;
        
        
        PN1 = (11.0/32.0*eta+743.0/2688.0)*theta2;
        PN15 = -3.0*PI/10.0*theta3 + (1.0/160.0)*(113.0*chi-38.0*eta*(chi1+chi2))*theta3;
        PN2 = (1855099.0/14450688.0+56975.0/258048.0*eta+371.0/2048.0*eta*eta)*theta4 + (1.0/14450688.0)*(-3386880.0*chi*chi+1512.0*chi1*chi2)*theta4;
        PN25 = -(7729.0/21504.0-13.0/256.0*eta)*PI*theta5;
        PN3 = (-720817631400877.0/288412611379200.0+53.0/200.0*PI*PI+107.0/280.0*gamma_E+(25302017977.0/4161798144.0-451.0/2048.0*PI*PI)*eta-30913.0/1835008.0*eta*eta+235925.0/1769472.0*eta*eta*eta + 107.0/280.0*log(2.0*theta))*theta6;
        
        //printf("%f %f %e %e %e %e %e\n", t, theta3/(8.0*M*PI), PN1, PN15, PN2, PN25, PN3);
        
        freqs[i] = theta3/(8.0*M*PI)*(1.0 + PN1 + PN15 + PN2 + PN25 + PN3);
        
        
        if(freqs[i] > fring) freqs[i] = fring;
    }
    else
    {
        freqs[i] = fring;
    }
        
    }
    
    return;
    
}

double fbegin(double *param)
{
    // [0] log(Mc) [1] log(Mt) [2] chi1 [3] chi2 [4] phi0 [5] tp [6] log(DL) [7] alpha [8] sindelta [9] psi [10] ciota
    
    double f, Mchirp, M, eta, dm, m1, m2, chi, chi1, chi2, tc, theta;
    double gamma_E=0.5772156649; //Euler's Constant-- shows up in 3PN term
    double PN1, PN15, PN2, PN25, PN3, PN35;
    double theta2, theta3, theta4, theta5, theta6, theta7;
    
    
    Mchirp = exp(param[0])/MSUN_SI*TSUN;
    M = exp(param[1])/MSUN_SI*TSUN;
    eta = pow((Mchirp/M), (5.0/3.0));
    if(eta > 0.25)
    {
        dm = 0.0;
    }
    else
    {
        dm = sqrt(1.0-4.0*eta);
    }
    m1 = M*(1.0+dm)/2.0;
    m2 = M*(1.0-dm)/2.0;
    chi1 = param[2];
    chi2 = param[3];
    chi = (m1*chi1+m2*chi2)/M;
    tc = param[5];
    //fprintf(stdout, "Mchirp %g, M %g, eta %g, dm %g, m1 %g, m2 %g, chi1 %g, chi2 %g, chi %g, tc %g\n",
    //                    Mchirp, M, eta, dm, m1, m2, chi1, chi2, chi, tc);
    
    theta = pow(eta*tc/(5.0*M),-1.0/8.0);
    theta2 = theta*theta;
    theta3 = theta2*theta;
    theta4 = theta2*theta2;
    theta5 = theta2*theta3;
    theta6 = theta3*theta3;
    
    
    PN1 = (11.0/32.0*eta+743.0/2688.0)*theta2;
    PN15 = -3.0*PI/10.0*theta3 + (1.0/160.0)*(113.0*chi-38.0*eta*(chi1+chi2))*theta3;
    PN2 = (1855099.0/14450688.0+56975.0/258048.0*eta+371.0/2048.0*eta*eta)*theta4 + (1.0/14450688.0)*(-3386880.0*chi*chi+1512.0*chi1*chi2)*theta4;
    PN25 = -(7729.0/21504.0-13.0/256.0*eta)*PI*theta5;
    PN3 = (-720817631400877.0/288412611379200.0+53.0/200.0*PI*PI+107.0/280.0*gamma_E+(25302017977.0/4161798144.0-451.0/2048.0*PI*PI)*eta-30913.0/1835008.0*eta*eta+235925.0/1769472.0*eta*eta*eta + 107.0/280.0*log(2.0*theta))*theta6;

    
    f = theta3/(8.0*M*PI)*(1.0 + PN1 + PN15 + PN2 + PN25 + PN3);
    return(f);
    
}

void print_projected_cbc_waveform(double **SN, double Tobs, double ttrig, double **projected, double **data, int N, int iter, struct bayesCBC *rundata)
{
    char command[1024];
    FILE *out;
    double fs;
    int i, id;
    double fac, x, y, f, fr, dt;
    double fmin, fmax;
    double **twave, **dwave;
    double **ww, **dw;
    struct Net *net = rundata->net;
    int Nifo = net->Nifo;
    RealVector *freq = rundata->freq;

    fmin = rundata->fmin;
    fmax = rundata->fmax;

    dt = Tobs/(double)(N);
    
    twave = double_matrix(net->Nifo,N);
    ww = double_matrix(net->Nifo,N); // Template/model whitened

    dwave = double_matrix(net->Nifo,N);
    dw = double_matrix(net->Nifo,N); // Data whitened
        
    fs = fbegin(rundata->pallx[rundata->who[0]]);
    
    // Convert indices for FFT
    for (id = 0; id < Nifo; ++id)
    {
        for (i = 1; i < N/2; ++i) 
        {
            twave[id][i] = projected[id][2*i];
            twave[id][N-i] = projected[id][2*i+1];

            dwave[id][i] = data[id][2*i];
            dwave[id][N-i] = data[id][2*i+1];
        }
        twave[id][0] = 0.0;
        twave[id][N/2] = 0.0;
        dwave[id][0] = 0.0;
        dwave[id][N/2] = 0.0;
    }

    
    // MW: For plotting was fr/3.0, but this also has better overlap at low frequencies
    fr = fs/fmin;
    
    for (i = 1; i < N/2; ++i)
    {
        
        f = freq->data[i];
        x = 0.5*(1.0+tanh((f-(fs+fr))/fr));
        for (id = 0; id < net->Nifo; ++id)
        {
            y = 1.0/sqrt(SN[id][i]*2.0);
            
            twave[id][i] *= x;
            twave[id][N-i] *= x;
            dwave[id][i] *= x;
            dwave[id][N-i] *= x;
            
            ww[id][i] = y*twave[id][i];
            ww[id][N-i] = y*twave[id][N-i];

            dw[id][i] = y*dwave[id][i];
            dw[id][N-i] = y*dwave[id][N-i];
            
        }
    }
    
    x = sqrt(2.0)/dt;
    
    fac = sqrt((double)(N/2));
    for (id = 0; id < Nifo; ++id)
    {
        ww[id][0] = 0.0;
        ww[id][N/2] = 0.0;
        dw[id][0] = 0.0;
        dw[id][N/2] = 0.0;

        gsl_fft_halfcomplex_radix2_inverse(ww[id], 1, N);
        gsl_fft_halfcomplex_radix2_inverse(dw[id], 1, N);
        
        for (i = 0; i < N; ++i)
        {
            ww[id][i] *= fac;
            dw[id][i] *= fac;
        }
    }
    
   
    fac = ceil(sqrt(1.0/4.0));
    for (id = 0; id < net->Nifo; ++id)
    {
        sprintf(command, "waveforms/projected_wavewhite_%d_%d_%d_%d.dat", iter, (int)(Tobs), (int)ttrig, net->labels[id]);
        out = fopen(command,"w");
        for (i = 0; i < N; ++i)
        {
            fprintf(out,"%e %e\n", (double)(i)*dt-Tobs+2.0, ww[id][i]/fac);
        }
        fclose(out);

        sprintf(command, "waveforms/projected_wavedatawhite_%d_%d_%d_%d.dat", iter, (int)(Tobs), (int)ttrig, net->labels[id]);
        out = fopen(command,"w");
        for (i = 0; i < N; ++i)
        {
            fprintf(out,"%e %e\n", (double)(i)*dt-Tobs+2.0, dw[id][i]/fac);
        }
        fclose(out);
    }

    free_double_matrix(dwave,net->Nifo);
    free_double_matrix(twave,net->Nifo);
    free_double_matrix(ww,net->Nifo);
    free_double_matrix(dw,net->Nifo);
    

}


void printwaveall(struct Net *net, int N, RealVector *freq, double *paramx, double **SN, double Tobs, double ttrig, int iter, struct bayesCBC *rundata)
{
    
    double **twave, **tf, **thold, **twave_tmp, **dwave;
    double **Dtime;
    double **ww, **wf, **dw;
    double fmin, fmax;
    double fr, f, x, y, dt, fac, fs;
    int i, j, id;
    char command[1024];
    FILE *out;
    
    dt = Tobs/(double)(N);
    fmin = rundata->fmin;
    fmax = rundata->fmax;
    
    thold = double_matrix(net->Nifo,N);
    twave = double_matrix(net->Nifo,N);
    twave_tmp = double_matrix(net->Nifo,N);
    Dtime = double_matrix(net->Nifo,N);
    tf = double_matrix(net->Nifo,N);
    ww = double_matrix(net->Nifo,N);
    wf = double_matrix(net->Nifo,N);

    dwave = double_matrix(net->Nifo,N);
    dw = double_matrix(net->Nifo,N); // Data whitened
    
    fulltemplates(net, twave_tmp, freq, paramx, N, rundata);

    fs = fbegin(paramx);
    if(fs < fmin) fs = fmin;
    
    // printf("%d %f %f\n", iter, exp(paramx[0])/MSUN_SI, fs);
    for (id = 0; id < net->Nifo; ++id)
    {
        sprintf(command, "waveforms/wavefcheck_%d_%d_%d_%d.dat", iter, (int)(Tobs), (int)ttrig, net->labels[id]);
        out = fopen(command,"w");
        for (i = 1; i < N/2; ++i)
        {
            f = (double)(i)/Tobs;
            if(f > fs)
            {
            fprintf(out,"%e %.16e %.16e %.16e %.16e\n", f, twave_tmp[id][2*i]/(2.0*SN[id][i]), twave_tmp[id][2*i+1]/(2.0*SN[id][i]), rundata->D[id][2*i]/(2.0*SN[id][i]), rundata->D[id][2*i+1])/(2.0*SN[id][i]);
            }
            else
            {
              fprintf(out,"%e %.16e %.16e %.16e %.16e\n", f, 0.0, 0.0, 0.0, 0.0);
            }
        }
        fclose(out);
    }

    // Convert indices for FFT
    for (id = 0; id < net->Nifo; ++id)
    {
        for (i = 1; i < N/2; ++i) 
        {
            twave[id][i] = twave_tmp[id][2*i];
            twave[id][N-i] = twave_tmp[id][2*i+1];
            Dtime[id][i] = rundata->D[id][2*i];
            Dtime[id][N-i] = rundata->D[id][2*i+1];
            dwave[id][i] = rundata->D[id][2*i];
            dwave[id][N-i] = rundata->D[id][2*i+1];
        }
        twave[id][0] = 0.0;
        twave[id][N/2] = 0.0;
        Dtime[id][0] = 0.0;
        Dtime[id][N/2] = 0.0;
        dwave[id][0] = 0.0;
        dwave[id][N/2] = 0.0;
    }

    for (id = 0; id < net->Nifo; ++id)
    {
        thold[id][0] = 0.0;
        thold[id][N/2] = 0.0;
        for (i = 0; i < N; ++i) thold[id][i] = twave[id][i];
    }
    
    // MW: For plotting was fr/3.0, but this also has better overlap at low frequencies
    fr = fs/fmin;
    
    for (i = 1; i < N/2; ++i)
    {
        
        f = freq->data[i];
        x = 0.5*(1.0+tanh((f-(fs+fr))/fr));
        for (id = 0; id < net->Nifo; ++id)
        {
            y = 1.0/sqrt(SN[id][i]*2.0);
            
            twave[id][i] *= x;
            twave[id][N-i] *= x;
            dwave[id][i] *= x;
            dwave[id][N-i] *= x;
            tf[id][i] = twave[id][N-i];
            tf[id][N-i] = -twave[id][i];
            
            ww[id][i] = y*twave[id][i];
            ww[id][N-i] = y*twave[id][N-i];
            wf[id][i] = ww[id][N-i];
            wf[id][N-i] = -ww[id][i];

            dw[id][i] = y*dwave[id][i];
            dw[id][N-i] = y*dwave[id][N-i];
            
        }
    }
    
    x = sqrt(2.0)/dt;
    
    //fac = sqrt(Tobs)*sqrt((double)(N/2));
    fac = sqrt((double)(N/2));
    
    for (id = 0; id < net->Nifo; ++id)
    {
        twave[id][0] = 0.0;
        twave[id][N/2] = 0.0;
        tf[id][0] = 0.0;
        tf[id][N/2] = 0.0;
        ww[id][0] = 0.0;
        ww[id][N/2] = 0.0;
        wf[id][0] = 0.0;
        wf[id][N/2] = 0.0;
        dw[id][0] = 0.0;
        dw[id][N/2] = 0.0;

        gsl_fft_halfcomplex_radix2_inverse(twave[id], 1, N);
        gsl_fft_halfcomplex_radix2_inverse(tf[id], 1, N);
        gsl_fft_halfcomplex_radix2_inverse(ww[id], 1, N);
        gsl_fft_halfcomplex_radix2_inverse(wf[id], 1, N);
        gsl_fft_halfcomplex_radix2_inverse(Dtime[id], 1, N);
        gsl_fft_halfcomplex_radix2_inverse(dw[id], 1, N);
        
        for (i = 0; i < N; ++i)
        {
            twave[id][i] *= x;
            Dtime[id][i] *= x;
            tf[id][i] *= x;
            ww[id][i] *= fac;
            wf[id][i] *= fac;
            dw[id][i] *= fac;
        }
    }
    
    for (id = 0; id < net->Nifo; ++id)
    {
        sprintf(command, "waveforms/wave_%d_%d_%d_%d.dat", iter, (int)(Tobs), (int)ttrig, net->labels[id]);
        out = fopen(command,"w");
        for (i = 0; i < N; ++i)
        {
            fprintf(out,"%e %.16e %e\n", (double)(i)*dt-Tobs+2.0, twave[id][i], sqrt(twave[id][i]*twave[id][i]+tf[id][i]*tf[id][i]));
        }
        fclose(out);

        sprintf(command, "waveforms/wavedata_%d_%d_%d_%d.dat", iter, (int)(Tobs), (int)ttrig, net->labels[id]);
        out = fopen(command,"w");
        for (i = 0; i < N; ++i)
        {
            fprintf(out,"%e %.16e\n", (double)(i)*dt-Tobs+2.0, Dtime[id][i]);
        }
        fclose(out);
    }
    
    for (id = 0; id < net->Nifo; ++id)
    {
        sprintf(command, "waveforms/wavef_%d_%d_%d_%d.dat", iter, (int)(Tobs), (int)ttrig, net->labels[id]);
        out = fopen(command,"w");
        for (i = 1; i < N/2; ++i)
        {
            f = (double)(i)/Tobs;
            if(f > fs)
            {
            fprintf(out,"%e %.16e %.16e %.16e %.16e\n", f, thold[id][i], thold[id][N-i], rundata->D[id][2*i], rundata->D[id][2*i+1]);
            }
            else
            {
              fprintf(out,"%e %.16e %.16e %.16e %.16e\n", f, 0.0, 0.0, 0.0, 0.0);
            }
        }
        fclose(out);
    }
    
    // Why do we need this for segments > 4s to scale back the amplitude???
    //fac = ceil(sqrt(Tobs/4.0));
    fac = ceil(sqrt(1.0/4.0));
    // double fac2 = sqrt((double)(N/2));
    // printf("  amplitude factor: %f\n\n",fac);
    for (id = 0; id < net->Nifo; ++id)
    {
        sprintf(command, "waveforms/wavewhite_%d_%d_%d_%d.dat", iter, (int)(Tobs), (int)ttrig, net->labels[id]);
        out = fopen(command,"w");
        for (i = 0; i < N; ++i)
        {
            fprintf(out,"%e %e %e\n", (double)(i)*dt-Tobs+2.0, ww[id][i]/fac, sqrt(ww[id][i]*ww[id][i]+wf[id][i]*wf[id][i]));
        }
        fclose(out);

        sprintf(command, "waveforms/wavedatawhite_%d_%d_%d_%d.dat", iter, (int)(Tobs), (int)ttrig, net->labels[id]);
        out = fopen(command,"w");
        for (i = 0; i < N; ++i)
        {
            fprintf(out,"%e %.16e\n", (double)(i)*dt-Tobs+2.0, dw[id][i]/fac);
        }
        fclose(out);
    }

    
    free_double_matrix(thold,net->Nifo);
    free_double_matrix(twave,net->Nifo);
    free_double_matrix(dwave,net->Nifo);
    free_double_matrix(twave_tmp,net->Nifo);
    free_double_matrix(Dtime,net->Nifo);
    free_double_matrix(tf,net->Nifo);
    free_double_matrix(ww,net->Nifo);
    free_double_matrix(wf,net->Nifo);
    
}


void printwave(struct Net *net, int N, RealVector *freq, double **paramx, int *who, double Tobs, double ttrig, int iter, struct bayesCBC *rundata)
{
    
    double **twave;
    double fr, f, x, dt;
    int i, j, id;
    double fmin, fmax;
    char command[1024];
    FILE *out;
    
    dt = Tobs/(double)(N);
    fmin = rundata->fmin;
    fmax = rundata->fmax;

    twave = double_matrix(net->Nifo,N);
    
    
    j = who[1];
    templates(net, twave, freq, paramx[j], N, rundata);
    
    printf("%d %f\n", iter, exp(paramx[j][0])/MSUN_SI);
    
    fr = fmin/8.0;
    
    for (i = 1; i < N/2; ++i)
    {
        f = freq->data[i];
        x = 0.5*(1.0+tanh((f-(fmin+2.0*fr))/fr));
        for (id = 0; id < net->Nifo; ++id)
        {
            twave[id][i] *= x;
            twave[id][N-i] *= x;
        }
    }
    
    x = sqrt(2.0)/dt;
    
    for (id = 0; id < net->Nifo; ++id)
    {
        gsl_fft_halfcomplex_radix2_inverse(twave[id], 1, N);
        for (i = 0; i < N; ++i)
        {
            twave[id][i] *= x;
        }
    }
    
    for (id = 0; id < net->Nifo; ++id)
    {
    sprintf(command, "wave_%d_%d_%d_%d.dat", iter, (int)(Tobs), (int)ttrig, net->labels[id]);
    out = fopen(command,"w");
    for (i = 0; i < N; ++i)
    {
        fprintf(out,"%e %e\n", (double)(i)*dt, twave[id][i]);
    }
    fclose(out);
    }
    
    free_double_matrix(twave,net->Nifo);
    
}

void pmap(struct Net *net, double *pall, const double *param, const double *sky, int NX, double gmst)
{
    // Maps parameters TO geocenter
    // takes param (which has parameters wrt ifo 0) and maps to corresponding geocenter parameters
    // WARNING, this function can knock distance and time out of bounds
    //
    // Modifies: pall
    // Requires: param is initialized

    int j;
    double ciota, Ap, Ac, alpha, sindelta, psi, Fs, ecc;
    double *dtimes;
    double Fplus, Fcross, lambda;

    // sky  [0] alpha, [1] sin(delta) [2] psi [3] ciota [4] scale [5] dphi [6] dt
    // param [0] log(Mc) [1] log(Mt) [2] chi1 [3] chi2 [4] phi0  [5] tp0 [6] log(DL0) then relative amplitudes, time, phases
    // pall  [0] log(Mc) [1] log(Mt) [2] chi1 [3] chi2 [4] 2phi0 [5] tp [6] log(DL) [7] alpha [8] sindelta [9] psi [10] ciota

    dtimes = (double*)malloc(sizeof(double)* 5);
    
    for (j = 0; j < NX; ++j)
    {
        pall[j] = param[j];
    }

    ciota = sky[3];
    Ap = (1.0+ciota*ciota)/2.0;
    Ac = ciota;
    ecc = Ac/Ap;
    alpha = sky[0];
    sindelta = sky[1];
    psi = sky[2];

    Fs =  Fmag(Ap, Ac, net->Fcross[0],net->Fplus[0]); // magnitude of response
    lambda = phase_from_polarizations(Ap, Ac, net->Fcross[0], net->Fplus[0]); // get phase from polarizations

    // move reference point from ref detector to geocenter
    pall[4] = 2.0*param[4] - lambda;  // I'm holding the GW phase in pall[4]
    pall[5] = param[5] - net->dtimes[0];
    pall[6] = param[6] + log(Fs);

    pall[NX] = alpha;
    pall[NX+1] = sindelta;
    pall[NX+2] = psi;
    pall[NX+3] = ciota;
    
    free(dtimes);
}

void pmap_back(struct Net *net, double *pall, double *param, double *sky, int NX, double gmst)
{
    // Maps parameters from geocenter to ifo0 frame
    // takes param (which has parameters wrt ifo 0) and maps to corresponding geocenter parameters
    //
    // Modifies: param, sky
    // Requires: pall is initialized

    int j;
    double ciota, Ap, Ac, alpha, sindelta, psi, Fs;
    double *dtimes;
    double Fplus, Fcross, lambda;

    dtimes = (double*)malloc(sizeof(double)* 5);

    // sky  [0] alpha, [1] sin(delta) [2] psi [3] ciota [4] scale [5] dphi [6] dt
    // param [0] log(Mc) [1] log(Mt) [2] chi1 [3] chi2 [4] phi0  [5] tp0 [6] log(DL0) then relative amplitudes, time, phases
    // pall  [0] log(Mc) [1] log(Mt) [2] chi1 [3] chi2 [4] 2phi0 [5] tp [6] log(DL) [7] alpha [8] sindelta [9] psi [10] ciota
    

    for (j = 0; j < NX; ++j)
    {
        param[j] = pall[j];
    }

    ciota = pall[NX+3];
    Ap = (1.0+ciota*ciota)/2.0;
    Ac = ciota;
    alpha = pall[NX];
    sindelta = pall[NX+1];
    psi = pall[NX+2];
    
    sky[0] = alpha;
    sky[1] = sindelta;
    sky[2] = psi;
    sky[3] = ciota;
    sky[4] = 1.0;
    sky[5] = 0.0;
    sky[6] = 0.0;
    
    Fs =  Fmag(Ap, Ac, net->Fcross[0],net->Fplus[0]); // magnitude of response
    lambda = phase_from_polarizations(Ap, Ac, net->Fcross[0], net->Fplus[0]); // get phase from polarizations
    
    param[4] = 0.5*(pall[4]+lambda);
    param[5] = pall[5] + net->dtimes[0];
    param[6] = pall[6] - log(Fs);
    free(dtimes);
}


void time_delays(struct Net *net)
{
    
   int i, j;
    
   for(i = 0; i < net->Nifo; i++) net->delays[i][i] = 0.0;
   for(i = 0; i < net->Nifo; i++)
   {
    for(j = i+1; j < net->Nifo; j++)
     {
     if(net->labels[i]==0 && net->labels[j]==1) net->delays[i][j] = HLdt;
     if(net->labels[i]==1 && net->labels[j]==0) net->delays[i][j] = HLdt;
         
     if(net->labels[i]==0 && net->labels[j]==2) net->delays[i][j] = HVdt;
     if(net->labels[i]==2 && net->labels[j]==0) net->delays[i][j] = HVdt;
         
     if(net->labels[i]==1 && net->labels[j]==2) net->delays[i][j] = LVdt;
     if(net->labels[i]==2 && net->labels[j]==1) net->delays[i][j] = LVdt;

   }
  }
    
  for(i = 0; i < net->Nifo ; i++)
   {
   for(j = i+1; j < net->Nifo; j++)
    {
     net->delays[j][i] = net->delays[i][j];
   }
  }
    
}

void sample_mc_mt(double* pallx, const struct bayesCBC *rundata, gsl_rng *r){
  double eta = 1;
  double mc, mt;
  while (eta > 0.25){
    // makes sure does not go out of bounds
    pallx[0] = rundata->min[0] + (rundata->max[0] - rundata->min[0]) * gsl_rng_uniform(r);
    pallx[1] = rundata->min[1] + (rundata->max[1] - rundata->min[1]) * gsl_rng_uniform(r);
    mc = exp(pallx[0]);
    mt = exp(pallx[1]);
    eta = pow((mc / mt), (5.0 / 3.0));
  }
}

void sample_pallx_uniform(double *pallx, const struct bayesCBC *rundata, gsl_rng *r){
  // sample all parameters uniformly for NP = rundata->NP parameters
  // modifies pallx
  // sample mc and mt first, very likely to be out of bounds together
  double eta = 1;
  double mc, mt, lambda1, lambda2;

  // make sure that eta is within bounds (can jump out from numerical error)
  sample_mc_mt(pallx, rundata, r);
  mc = exp(pallx[0]);
  mt = exp(pallx[1]);
  eta = pow((mc / mt), (5.0 / 3.0));

  for (int k = 2; k < rundata->NP; k++) {
    pallx[k] = rundata->min[k] + (rundata->max[k] - rundata->min[k]) * gsl_rng_uniform(r);
  }

  // make sure lambda parameters are in bounds
  if (rundata->NX > 7) {
    if (rundata->lambda_type_version == (Lambda_type) lambdaTilde) {
      LambdaTsEta2Lambdas(pallx[7], pallx[8], eta, &lambda1, &lambda2);
      do {
        pallx[7] = rundata->min[7] + (rundata->max[7] - rundata->min[7]) * gsl_rng_uniform(r);
        pallx[8] = rundata->min[8] + (rundata->max[8] - rundata->min[8]) * gsl_rng_uniform(r);

        LambdaTsEta2Lambdas(pallx[7], pallx[8], eta, &lambda1, &lambda2);
      } while (lambda1 < 0.00 || lambda2 < 0.00 || isnan(lambda1) || isnan(lambda2) || eta > 0.25);
    }
  }
}

void burnin_bayescbc(int N, double Tobs, const double ttrig, double **D, double **SN, gsl_rng *r, char *xml_file_path, int start_at_injection, struct bayesCBC *rundata)
{
    // ttrig should be given wrt segment_start
    int i, j, jj, k, id;
    double *params, *sky;
    double *logL, *logLsky, *logLstart, *logLfull;
    double q, mc, m2, m1, mt;
    double x, y, qx, tx;
    double lMc, lMcmin, lMcmax, dlMc, lmsun;
    double lMcx, lMtx;
    int imin, imax;
    double fmin, fmax;
    int nt, bn;
    double dtx;
    double *SNRsq;
    double *heat;
    double **rho;
    double Lmax;
    int jmax;
    FILE *chain;

    double lambda1, lambda2, lambdaT, dLambdaT;
    Lambda_type lambda_type_version; 

    
    int NX = rundata->NX;
    int NP = rundata->NP;
    int NS = rundata->NS;
    int NC = rundata->NC; // number of chains
    int NCC = rundata->NCC; // number of cold chains 
    int NH = rundata->NH; // length of history
    int NQ = rundata->NQ; // number of mass ratios in global proposal
    int NM = rundata->NM; // number of chirp masses in global proposal

    // heat = double_vector(NC); // local heat for burnin only

    if (start_at_injection == 0) {
        // The CBC_start function does a search to find the signal and initializes the arrays
        // pallx should be initialized, but it is not, let's initialize them
        for (int j = 0; j < NC; j++){
          sample_pallx_uniform(rundata->pallx[j], rundata, r);
        }
        CBC_start(rundata->net, rundata->mxc, rundata->chainS, rundata->paramx, rundata->skyx, rundata->pallx, rundata->heat, rundata->history, rundata->global, rundata->freq, D, SN, N, Tobs, r, rundata);
    } 
    else // places CBC at the injection parameter values
    {
      for(j = 0; j < NC; j++)
      {
          for(k = 0; k <= NH; k++)
          {
          // initialize history from prior
          sample_CBC_intinsic_prior(rundata->history[j][k], rundata, r);
          }
      }
      rundata->mxc[0] = 0;
      rundata->mxc[1] = 0;

      logLfull = double_vector(NC);

      rho = double_matrix(NC,rundata->net->Nifo);
      params = (double*)malloc(sizeof(double)* (NP));
      sky = (double*)malloc(sizeof(double)* NS);

      char *injection_columns[58];
      char *injection_values[58];
      char filename[1024];

      // Read in injection parameters from injection XML file.
      printf("Reading XML injection parameters from file: %s\n", xml_file_path);
      sprintf(filename, "%s", xml_file_path);

      ParseInjectionXMLParameters(filename, injection_columns, injection_values);

      // Set chirp mass and total mass
      GetXMLInjectionParameter("sim_inspiral:mchirp", &params[0], injection_columns, injection_values);
      params[0] = log(params[0]*MSUN_SI);  // log(Mc)

      // Set total mass from component masses & chirp mass
      GetXMLInjectionParameter("sim_inspiral:mass1", &m1, injection_columns, injection_values);
      GetXMLInjectionParameter("sim_inspiral:mass2", &m2, injection_columns, injection_values);
      q = m1 / m2; // q = m1/m2
      mc = exp(params[0]);
      m2 = mc * pow(1.0 + q, 0.2) / pow(q, 0.6);
      m1 = q * m2;
      mt = m1 + m2;
      params[1] = log(mt);  // log(Mt)

      // Set spins
      GetXMLInjectionParameter("sim_inspiral:spin1z", &params[2], injection_columns, injection_values);
      GetXMLInjectionParameter("sim_inspiral:spin2z", &params[3], injection_columns, injection_values);

      // Set geocenter gravitational wave phase (2phi0)
      GetXMLInjectionParameter("sim_inspiral:phi0", &params[4], injection_columns, injection_values);
      params[4] = 2 * params[4];

      // Set peak time geocenter (currently set at 2s before end of segment)
      // GetXMLInjectionParameter("sim_inspiral:end_time_gmst", &params[5], injection_columns, injection_values);
      params[5] = ttrig;

      // Set distance
      GetXMLInjectionParameter("sim_inspiral:distance", &params[6], injection_columns, injection_values);
      params[6] = log(params[6] * 1e6 * PC_SI);

      // Add tidal parameters (these are not in XML injection file for historical reasons)
      lambda_type_version = rundata->lambda_type_version;
      if (NX > 7) {
          params[7] = rundata->init_tidal1;
          params[8] = rundata->init_tidal2;
      }

      // Set sky parameter: alpha, RA
      GetXMLInjectionParameter("sim_inspiral:longitude", &sky[0], injection_columns, injection_values);

      // Set sky parameter: sin(delta) = sin(declination)
      GetXMLInjectionParameter("sim_inspiral:latitude", &sky[1], injection_columns, injection_values);
      sky[1] = sin(sky[1]);

      // Set polarization phase psi
      GetXMLInjectionParameter("sim_inspiral:polarization", &sky[2], injection_columns, injection_values);

      // Set inclination angle orbital plane: cos(iota)
      GetXMLInjectionParameter("sim_inspiral:inclination", &sky[3], injection_columns, injection_values);
      sky[3] = cos(sky[3]);

      // Initialize [4] scale [5] dphi [6] dt
      // Scale changes the overall distance reference at geocenter
      // dt shifts the geocenter time
      // dphi shifts the geocenter phase
      sky[4] = 1.0;
      sky[5] = 0.0;
      sky[6] = 0.0;



      printf("\n");
      printf(" Skipping initial intrinsic MCMC signal search. Starting at initial values.");

      // Start chains at given parameter values
      for (i=0; i<NC; i++) {

          // Set full parameter array
          for (j = 0; j < NP; ++j) {

              // Quasi-intrinsic parameters
              if (j < NX) {
                  rundata->pallx[i][j] = params[j];
              } else {
              // Quasi-extrensic parameters
                  rundata->pallx[i][j] = sky[j-NX];
              }
          }
      }

      for(jj = 0; jj < NC; jj++)
      {
          logLfull[jj] = log_likelihood_full(rundata->net, D, rundata->pallx[jj], rundata->freq, SN, rho[jj], N, Tobs, rundata, 0);
          printf("logLfull[%i]: %f\n", jj, logLfull[jj]);
      }

      free_double_matrix(rho,NC);
      free_double_vector(logLfull);
      //free_double_vector(heat); // this was unallocated
      free_double_vector(params);
    }

    for (int ic = 0; ic < rundata->NC; ic++){
      // enforce prior on phase (rundata->max[4] holds either pi or 2pi...)
      while(rundata->pallx[ic][4] < 0){ rundata->pallx[ic][4] += rundata->max[4]; }
      while(rundata->pallx[ic][4] > rundata->max[4]){ rundata->pallx[ic][4] -= rundata->max[4]; }
    }
}

void reset_search_chains(struct Net *net, int *indices, int NCmain, gsl_rng *r, struct bayesCBC *rundata) 
{
    int i, ic, id;
    int whoi;

    // printf(" ======= Dynamically set search chains =======\n");
    printf("  ..Dynamically resetting chain numbers\n");

    double **pallxtemp = double_matrix(NCmain, rundata->NP);
    double *logLxtemp = double_vector(NCmain);
    int *whotemp = int_vector(NCmain);

    // Reshuffle pallx array for original NC + reset chain index
    for(ic=0; ic<NCmain; ic++)
    {   
        for(i=0; i<rundata->NP; i++) {
            pallxtemp[ic][i] = rundata->pallx[rundata->who[ic]][i];
        }
        whotemp[ic] = ic;
        logLxtemp[ic] =  rundata->logLx[rundata->who[ic]];
    }   

    free_int_vector(rundata->who);
    free_double_vector(rundata->heat);
    free_double_vector(rundata->logLx);
    free_double_matrix(rundata->paramx,rundata->NC);
    free_double_matrix(rundata->skyx,rundata->NC);
    free_double_matrix(rundata->pallx,rundata->NC);

    rundata->NC = NCmain;

    // Reallocate/initialize local chain index array
    rundata->who = int_vector(rundata->NC);
    for (ic=0; ic<rundata->NC; ic++) {
        rundata->who[ic] = whotemp[ic];
        indices[ic] = whotemp[ic];
    }

    // Resize parameter arrays
    rundata->heat = double_vector(rundata->NC);
    rundata->logLx = double_vector(rundata->NC);
    rundata->paramx = double_matrix(rundata->NC,rundata->NX+3*net->Nifo); // TODO what is in extra paramx params?
    rundata->skyx = double_matrix(rundata->NC,rundata->NS);
    rundata->pallx = double_matrix(rundata->NC,rundata->NP);

    // Refill CBC arrays
    for(ic=0; ic<rundata->NC; ic++) {
        for(i=0; i<rundata->NP; i++) rundata->pallx[ic][i] = pallxtemp[ic][i];
        rundata->logLx[ic] = logLxtemp[ic];
        rundata->heat[ic] = 0.0;
        for(i=0; i<rundata->NX; i++) rundata->paramx[ic][i] = 0.0;
        for(i=0; i<rundata->NS; i++) rundata->skyx[ic][i] = 0.0;
    }

    free_double_matrix(pallxtemp, rundata->NC);
    free_double_vector(logLxtemp);
    free_int_vector(whotemp);

}

/*
 *  Adjust the search chains such that the SNR of the last chain
 *  is about ~5-8, either by adjust the temperature spacing or by 
 *  adding chains. The temperature spacing and nr. of chains will be
 *  reset for the BayesWave main PTMCMC.
 */
void resize_search_chains(struct Net *net, double Tobs, double **D, double **SN, int N, int NCmain, gsl_rng *r, struct bayesCBC *rundata) 
{
    int i, j, ic, id;

    printf(" ====== Dynamically set search chains ======\n");
    printf("  ..setting search chain numbers and spacing\n");
      
    int imin = (int)((rundata->fmin*Tobs));
    int imax = (int)((rundata->fmax*Tobs));

    // Calculate network SNR assuming the data given to search is mostly signal
    double *dSNR = double_vector(net->Nifo);
    double nSNR = 0;
    for(id=0; id<net->Nifo; id++)
    {
        dSNR[id] = fourier_nwip_cbc(D[id], D[id], SN[id], imin, imax, N);
        nSNR += dSNR[id];
        dSNR[id] = sqrt(dSNR[id]);

        printf("    SNR detector %d: %f\n", id, dSNR[id]);
    }
    nSNR = sqrt(nSNR);
    printf("    SNR network    : %f\n",  nSNR);

    // Get current nr of chains and lowest chain SNR
    int NCCmain = rundata->NCC;
    int NC = rundata->NC; 
    int NCC = rundata->NCC;

    double spacing = rundata->dTsearch;

    // Use a maximum of 6 chains running cold
    if (NCCmain > 6) NCC = 6;

    // Check if increased spacing is needed
    double SNRmin = nSNR/(pow(spacing, (NC-NCC-1)));

    // Adjust spacing to try and achieve SNRmin ~ 8 starting from
    // cold chains at T = 0.25
    if (SNRmin > 8.0)
    {
        spacing = pow(nSNR/8.0, 1.0/(NC-NCC-1));
        SNRmin = nSNR/(pow(spacing, (NC-NCC-1)));

        // If adjusting spacing wasn't enough, add chains instead
        while (SNRmin > 7.5 && NC <100)
        {
            spacing = 1.25;
            NC += 1;
            SNRmin = nSNR/(pow(spacing, (NC-NCC-1)));
        }
    }

    rundata->NC = NC;
    rundata->NCC = NCC;
    rundata->dTsearch = spacing;

    // Copy inital external params
    double **pallxtmp = double_matrix(NCmain, rundata->NP);
    for (ic=0; ic<NCmain; ic++) {
        for (i=0; i<rundata->NP; i++) pallxtmp[ic][i] = rundata->pallx[ic][i];
    }

    // Reallocate local chain index array
    free_int_vector(rundata->who);
    rundata->who = int_vector(rundata->NC);
    // for (i=0; i<NC; i++) rundata->who[i] = i;

    // Resize parameter arrays
    free_double_vector(rundata->heat);
    free_double_vector(rundata->logLx);
    free_double_matrix(rundata->paramx,NCmain);
    free_double_matrix(rundata->skyx,NCmain);
    free_double_matrix(rundata->pallx,NCmain);

    rundata->heat = double_vector(rundata->NC);
    rundata->logLx = double_vector(rundata->NC);
    rundata->paramx = double_matrix(rundata->NC,rundata->NX+3*net->Nifo);
    rundata->skyx = double_matrix(rundata->NC,rundata->NS);
    rundata->pallx = double_matrix(rundata->NC,rundata->NP);

    // Set initial external params for all chains
    for (ic=0; ic<rundata->NC; ic++) {

        if (ic<NCmain) j = ic;
        else j = (int) (gsl_rng_uniform(r) * (NCmain-1));

        for (i=0; i<rundata->NP; i++) rundata->pallx[ic][i] = pallxtmp[j][i];
    }

    printf("  nr. of cold chains ..  %d (from default = %d)\n", rundata->NCC, NCCmain);
    printf("  nr. of chains .......  %d (from default = %d)\n", rundata->NC, NCmain);
    printf("  warm chain spacing ..  %f \n", rundata->dTsearch);
    printf("  hot chain SNR .......  %f \n", SNRmin);

    free_double_matrix(pallxtmp,NCmain);
    free_double_vector(dSNR);
}

void CBC_start(struct Net *net, int *mxc, FILE *chainS, double **paramx, double **skyx, double **pallx, double *heat, double ***history, double ***global, RealVector *freq, double **D, double **SN, int N, double Tobs, gsl_rng *r, struct bayesCBC *rundata)
{
    // THIS function initializes the CBC parameters for every chain, assumes pallx is initialized
    int i, j, jj, k, id, ic;
    double *logL, *logLstart, *logLfull;
    double q, mc, m2, m1, mt, eta;
    double x, y, qx, tx;
    double fmin, fmax;
    int imin, imax;
    int nt, bn;
    double dtx;
    double **rho;
    double ***localhistory;
    double Lmax, spacing;
    int jmax;

    double lambda1, lambda2, lambdaT, dLambdaT;
    Lambda_type lambda_type_version; 

    FILE *chain;
    
    int NX = rundata->NX;
    int NP = rundata->NP;
    int NS = rundata->NS;
    int NC = rundata->NC; // number of chains
    int NCC = rundata->NCC; // number of cold chains 
    int NH = rundata->NH; // length of history
    int NQ = rundata->NQ; // number of mass ratios in global proposal
    int NM = rundata->NM; // number of chirp masses in global proposal

    int *who = rundata->who;
    for (i=0; i<NC; i++) rundata->who[i] = i;

    fmin = rundata->fmin;
    fmax = rundata->fmax;
       
    imin = (int)((fmin*Tobs));
    imax = (int)((fmax*Tobs));

    // intialize the local heat and history & counter arrays
    // run cold to force ML
    for(i=0; i<NCC; i++) heat[i] = 0.25;
    for(i=NCC; i<NC; i++) heat[i] = heat[i-1]*rundata->dTsearch;  // spacing can be big here since we start at low temperature
    // with 10 hot chains this gets the hottest up to heat > 1.
    
    // Now create all local variables with local NC
    logL = double_vector(NC);
    logLstart = double_vector(NC);
    logLfull = double_vector(NC);
    rho = double_matrix(NC,net->Nifo);
    localhistory = double_tensor(NC,NH,NX);

    for(j = 0; j < NC; j++)
    {

        for(k = 0; k < 4; k++) skyx[j][k] = pallx[j][NX+k];

        skyx[j][4] = 1.0;
        skyx[j][5] = 0.0;
        skyx[j][6] = 0.0;

        for (i = 1; i < net->Nifo; ++i)
        {
            paramx[j][NX+(i-1)*3] = 0.0; // phase offset
            paramx[j][NX+(i-1)*3+1] = 0.0; // time offset
            paramx[j][NX+(i-1)*3+2] = 1.0; // amplitude ratio
        }
    }

    

    // Initial search to find CBC intrinsic model signal
    if (rundata->constantLogLFlag == 0) MCMC_intrinsic(net, 1, mxc, rundata->MI, chainS, paramx, skyx, pallx, who, heat, localhistory, global, freq, D, SN, N, Tobs, r, rundata);


    for(j = 0; j < NC; j++)
    {   
      // If we are only doing intrinsic burnin, use sky parameters given by BW
      // SH: TODO make sure that these are set
      for(k = 0; k < 4; k++)
      {
        skyx[j][k] = pallx[j][NX+k];
      }

      if (rundata->constantLogLFlag == 1) {
        sample_pallx_uniform(pallx[j], rundata, r);
      } else {

          logL[j] = log_likelihood(net, D, paramx[j], freq, SN, N, Tobs, rundata);
          pmap(net, pallx[j], paramx[j], skyx[j], NX, rundata->gmst);
          
          logLfull[j] = log_likelihood_full(net, D, pallx[j], freq, SN, rho[j], N, Tobs, rundata, 0);
          printf("%i: logL %f, logLfull %f\n", j, logL[j], logLfull[j]);
      }
      for (int ic = 0; ic < rundata->NC; ic++){
        // enforce prior on phase (rundata->max[4] holds either pi or 2pi...)
        while(rundata->pallx[ic][4] < 0){rundata->pallx[ic][4]  += rundata->max[4];}
        while(rundata->pallx[ic][4] > rundata->max[4]){rundata->pallx[ic][4] -= rundata->max[4];}
      }
    }

    chain = fopen("chain_init.dat", "w");
    for(k=0; k < NC; k++)
    {
        for(j=0; j<NP; j++) {
            fprintf(chain,"%e ", pallx[k][j]); 
        }
        fprintf(chain,"\n");
    }
    fclose(chain);

    if (rundata->debug) 
    {
        for(jj = 0; jj < NC; jj++)
        {
            printf("\n");
            printf("%d logL full %f\n", jj, logLfull[jj]);
            for (id = 0; id < net->Nifo; ++id)
            {
                printf("ifo %d %f ", net->labels[id], rho[jj][id]);
            }
             printf("\n");
        }
        printf("\n");       
    }
    
    
    // set threshold that all chains start withing 20% of highest likelihood
    if (rundata->debug) printf("  Chains CBC logL after intrinsic burnin:\n");

    // Map all chains to highest likelihood chain
    if (rundata->constantLogLFlag == 0)
    {
        x = -1e4;
        k = rundata->who[0];
        for(jj = 0; jj < NC; jj++)
        {
            if(logL[jj] > x)
            {
                x = logL[jj];
                k = jj;
            }
        }

        x *= 0.8;
        for(jj = 0; jj < NC; jj++)
        {
                    
            if(logL[jj] < x)
            {
                for(i = 0; i < NP; i++)
                {
                    pallx[jj][i] = pallx[k][i];
                }
            }
            
            logLfull[jj] = log_likelihood_full(net, D, pallx[jj], freq, SN, rho[jj], N, Tobs, rundata, 0);
            rundata->logLx[jj] = logLfull[jj]; // BW shared array
            if(rundata->debug) printf("   %d logL full %f\n", jj, logLfull[jj]);
        }
    }

    free_double_matrix(rho,NC);
    free_double_vector(logL);
    free_double_vector(logLfull);
    free_double_vector(logLstart);
    free_double_tensor(localhistory,NC,NH);

    return;
}


double log_likelihood(struct Net *net, double **D, double *params, RealVector *freq, double **SN, int N, double Tobs, struct bayesCBC *rundata)
{
    int i;
    double logL;
    double **twave, **twavei;
    double HH, HD;
    int imin, imax;
    double fmin, fmax;

    fmin = rundata->fmin;
    fmax = rundata->fmax;
    logL = 0.0;
        
    imin = (int)(Tobs*fmin);
    imax = (int)(Tobs*fmax);
    if(imax > N/2) imax = N/2;

    twave = double_matrix(net->Nifo,N);
    
    templates(net, twave, freq, params, N, rundata);

    // Convert to intrinsic array format
    twavei = double_matrix(net->Nifo,N);

    for(int id = 0; id < net->Nifo; id++)
    {    
        twavei[id][0] = 0.0;
        twavei[id][1] = 0.0;
        for (i=1; i< N/2; i++)
        {   
            twavei[id][2*i] = twave[id][i];
            twavei[id][2*i+1] =  twave[id][N-i];               
        }
    }

    logL = 0.0;
    for(i = 0; i < net->Nifo; i++)
    {
        HH = fourier_nwip_cbc(twavei[i], twavei[i], SN[i], imin, imax, N);
        HD = fourier_nwip_cbc(D[i], twavei[i], SN[i], imin, imax, N);
        logL += HD-0.5*HH;
    }
    
    free_double_matrix(twave,net->Nifo);
    free_double_matrix(twavei,net->Nifo);
    
    return(logL);
    
}

void SNRvsf(struct Net *net, double **D, double *params, RealVector *freq, double **SN, int N, double Tobs, struct bayesCBC *rundata)
{
    int i, j;
    double logL;
    double **twave;
    double HH, HD, x, y;
    int imin, imax;
    char filename[1024];
    FILE *out;
    
    
        
        twave = double_matrix(net->Nifo,N);
        
        fulltemplates(net, twave, freq, params, N, rundata);
        
    
        for(i = 0; i < net->Nifo; i++)
        {
            
            sprintf(filename, "SNRf_%d.dat", net->labels[i]);
            out = fopen(filename,"w");
            
            HH = 0.0;
            HD = 0.0;
            
            for(j = 1; j < N/2; j++)
            {
                
            x = 4.0*(twave[i][j]*twave[i][j]+twave[i][N-j]*twave[i][N-j])/(SN[i][j]*2.0);
            y = 4.0*(twave[i][j]*D[i][j]+twave[i][N-j]*D[i][N-j])/(SN[i][j]*2.0);
                
            HH += x;
            HD += y;
                
                fprintf(out, "%e %e %e %e %e %e\n", (double)(j)/Tobs, x, y, HH, HD, HD/sqrt(HH+1.0e-6));
                
            }
            
            fclose(out);
            
        }
        
        free_double_matrix(twave,net->Nifo);

    
}

/*
 *  Custom version to replace GSL `gsl_sort_vector2` which is only 
 *  available in gsl versions >=1.16 (whereas LAL requires >=1.15)
 */
void gsl_sort_vector2_custom(gsl_vector * v1, gsl_vector * v2)
{
  double v1i, v2i;
  size_t n = v1->size;

  if (n==0) return;

  gsl_permutation * p = gsl_permutation_alloc(n);

  // Get sorted indices of v1
  gsl_sort_vector_index(p, v1);

  // Copy current vectors
  gsl_vector *v1_copy = gsl_vector_alloc(n);
  gsl_vector *v2_copy = gsl_vector_alloc(n);
  gsl_vector_memcpy(v1_copy, v1);
  gsl_vector_memcpy(v2_copy, v2);

  // Shuffle vectors based on new sorted indices
  for (size_t i = 0; i < n; i++)
  {
    v1i = gsl_vector_get(v1, p->data[i]);
    v2i = gsl_vector_get(v2, p->data[i]);

    gsl_vector_set(v1, i, gsl_vector_get(v1_copy, p->data[i]));
    gsl_vector_set(v2, i, gsl_vector_get(v2_copy, p->data[i]));
  }

  gsl_vector_free(v1_copy);
  gsl_vector_free(v2_copy);
  gsl_permutation_free(p);
}

double t_at_f(double *param, double f)
{
    int i;
    double t, x;
    double dt, fring, a, Mchirp, M, eta, dm, m1, m2, chi, chi1, chi2, tc, theta;
    double gamma_E=0.5772156649; //Euler's Constant-- shows up in 3PN term
    
    Mchirp = exp(param[0])/MSUN_SI*TSUN;
    M = exp(param[1])/MSUN_SI*TSUN;
    eta = pow((Mchirp/M), (5.0/3.0));
    if(eta > 0.25) eta = 0.25;
    dm = sqrt(1.0-4.0*eta);
    m1 = M*(1.0+dm)/2.0;
    m2 = M*(1.0-dm)/2.0;
    chi1 = param[2];
    chi2 = param[3];

    chi = (m1*chi1+m2*chi2)/M;
    
    tc = param[5];
    
    x = pow(PI*M*f,2.0/3.0);
    
    t =  tc-pow(x,-4.0)*5.0*M/(256.0*eta)*(1.0      // 0 PN
                        +((1/252.0)*(743.0+924.0*eta)*x)  // 1 PN
                        +(2.0/15.0)*(-48.0*PI+113.0*chi-38.0*eta*(chi1+chi2))*pow(x,3.0/2.0)    // 1.5 PN
                        +(1.0/508032.0)*(3058673.0+5472432.0*eta+4353552.0*eta*eta-5080320.0*chi*chi+127008.0*eta*chi1*chi2)*pow(x,4.0/2.0) // 2 PN
                        +(1.0/756.0)*(3.0*PI*(-7729.0+1092.0*eta)+1512.0*chi*chi*chi-56.0*eta*(1285.0+153.0*eta)*(chi1+chi2)+chi*(147101.0+504.0*eta*(13.0-9.0*chi1*chi2)))*pow(x,5.0/2.0)// 2.5 PN
                        +((6848.0*gamma_E)/105.0+PI*PI/12.0*(512.0-451.0*eta)+(25565.0*eta*eta*eta)/1296.0+(-10052469856691.0+24236159077900.0*eta)/23471078400.0+(35.0*chi*chi)/3.0+eta*eta*(-(15211.0/1728.0)+19.0*chi1*chi1+(245.0*chi1*chi2)/6.0+19.0*chi2*chi2)+8.0*PI/3.0*(-73.0*chi+28.0*eta*(chi1+chi2))-eta/336.0*(26992.0*chi*chi-995.0*chi1*chi2+30688.0*chi*(chi1+chi2))+3424.0/105.0*log(16.0*x))*pow(x,6.0/2.0));  // 3PN
    
    return(t);
    
}

double log_likelihood_penalized(int ii, double *params, double *D, double *H, double *AU, double *PU, double *TDU, double *SN, int N, double Tobs, int pflag, struct bayesCBC *rundata)
{
    int i, j, k;
    double logL, f, t;
    double HHX, HCX, HSX;
    double *twave;
    double hh, dh, dhs;
    double *rho, *snr, *rhoc;
    double HH, HD, HC, HS;
    double ph, cph, sph, x, xs, y;
    double max, min, alpha;
    double maxL, minL;
    int imin, imax;
    int *fbs, *bs;
    
    double norm, pshift, tshift;
    double fmin, fmax;
    int NX = rundata->NX;

    fmin = rundata->fmin;
    fmax = rundata->fmax;  

    norm = AU[ii];
    pshift = PU[ii];
    tshift = TDU[ii];
    
    int bw, nb, ib, kend, flag, kstart;
    
    bw = (int)(rundata->fbw*Tobs);
    logL = 0.0;
    
    imin = (int)(Tobs*fmin);
    imax = (int)(Tobs*fmax);
    if(imax > N/2) imax = N/2;  
    nb = (imax-imin)/bw;
    if(nb < 1) nb = 1;
    
    twave = double_vector(N);
    rho = double_vector(nb);
    rhoc = double_vector(nb);
    snr = double_vector(nb);
    fbs = int_vector(nb);
    bs = int_vector(nb);
    
    // apply the phase, time and amplitude shifts
    for(i = imin; i < imax; i++)
    {
      f = (double)(i)/Tobs;
      ph = pshift-TPI*f*tshift;
      // could use recursion to make this faster
      cph = cos(ph);
      sph = sin(ph);
      twave[i] = norm*(H[2*i]*cph-H[2*i+1]*sph);
      twave[N-i] = norm*(H[2*i+1]*cph+H[2*i]*sph);
    }

    for(i = 0; i < nb; i++) rho[i] = 0.0;
    
    flag = 0;
    j = 0;
    k = -1;
    x = 0.0;
    xs = 0.0;
    y = 0.0;
    
    for(i = imin; i < imax; i++)
    {
        if(j%bw==0) // moving to next block
        {
            if(y > 0.0)
            {
            x = sqrt(x*x+xs*xs); // phase maximized over frequency band
            rho[k] = (x-y)/sqrt(y);
            }
            
            if(k > -1) snr[k] = y;

            k++;
            x = 0.0;
            xs = 0.0;
            y = 0.0;
        }
        
        dh =  4.0*(D[2*i]*twave[i]+D[2*i+1]*twave[N-i])/(2.0*SN[i]);
        dhs = 4.0*(D[2*i]*twave[N-i]-D[2*i+1]*twave[i])/(2.0*SN[i]);
        hh =  4.0*(twave[i]*twave[i]+twave[N-i]*twave[N-i])/(2.0*SN[i]);

        HD += dh;
        HH += hh;
        x += dh;
        xs += dhs;
        y += hh;
        
        j++;
    }
    
    // find the region where the SNR is significant
    flag = 0;
    kstart = 0;
    for(k = 0; k < nb; k++)
    {
        if(snr[k] > 0.1 && flag == 0)
        {
            flag = 1;
            kstart = k;
        }
    }
    
    flag = 0;
    kend = nb;
    for(k = kstart; k < nb; k++)
    {
        if(snr[k] < 0.1 && flag == 0)
        {
            flag = 1;
            kend = k;
        }
    }
    
    // now cut out any low SNR bands
    j = 0;
    for(k = kstart; k < kend; k++)
    {
            fbs[j] = imin + k*bw;
            bs[j] = 0;
            rhoc[j] = rho[k];
            j++;
    }
    
       for(k = 0; k < j; k++)
        {
            if(rhoc[k] > 3.0) bs[k] = 1;  // exclude this band
        }
    
    // re-do amplitude and phase maximization with bad bands excluded
    HH = 0.0001;
    HC = 0.0;
    HS = 0.0;

    for(k = 0; k < j; k++)
    {
         
        if(pflag == 1)
        {
            f = ((double)(fbs[k])+0.5*(double)(bw))/Tobs;
            t = t_at_f(params, f);
            HHX = 0.0;
            HCX = 0.0;
            HSX = 0.0;
            for(i = fbs[k]; i < (fbs[k]+bw); i++)
            {
              HHX +=  4.0*(twave[i]*twave[i]+twave[N-i]*twave[N-i])/(2.0*SN[i]);
              HCX +=  4.0*(D[2*i]*twave[i]+D[2*i+1]*twave[N-i])/(2.0*SN[i]);
              HSX +=  4.0*(D[2*i]*twave[N-i]-D[2*i+1]*twave[i])/(2.0*SN[i]);
            }
            HD = sqrt(HCX*HCX+HSX*HSX);
            printf("%f %f %f %f\n", f, t+tshift, rhoc[k], HD-0.5*HHX);
        }
         
        if(bs[k] == 0)
        {
            for(i = fbs[k]; i < (fbs[k]+bw); i++)
            {
                HH +=  4.0*(twave[i]*twave[i]+twave[N-i]*twave[N-i])/(2.0*SN[i]);
                HC +=  4.0*(D[2*i]*twave[i]+D[2*i+1]*twave[N-i])/(2.0*SN[i]);
                HS +=  4.0*(D[2*i]*twave[N-i]-D[2*i+1]*twave[i])/(2.0*SN[i]);
            }
        }
    }


    HD = sqrt(HC*HC+HS*HS);
    AU[ii] *= HD/HH;
    PU[ii] -= atan2(HS,HC);
    
    logL = (HD*HD/HH)/2.0;
        
     free_int_vector(fbs);
     free_int_vector(bs);
     free_double_vector(rho);
     free_double_vector(rhoc);
     free_double_vector(snr);
     free_double_vector(twave);
        
    
    return(logL);
    
}


double log_likelihood_max(struct Net *net, double **D, double *params, RealVector *freq, double **SN, int N, double Tobs, double tmin, double tmax, int pflag, struct bayesCBC *rundata)
{
    // SN is the PSD
    int NMAX, M;
    int ii, jj, id, flag;
    int imin, imax;
    int mn, mx;
    double sum;
    double *HH;
    double logL;
    double *lmax;
    double HD, LD, dt, x, f;
    double *norm, *delt, *pshift;
    double **HS, *HLS, **LX;
    double **LL, **TD;
    double **LLU, **TDU;
    double **PU, **AU;
    double **maxL, ***tx;
    double ***px, ***ax;
    int **shift;
    int **max, *tk;
    int *NU;
    double **HC, **HF;
    
    double t0, t1, t2;
    
    double *h;
    
    double fmin, fmax;
    int NX = rundata->NX;

    fmin = rundata->fmin;
    fmax = rundata->fmax;    
    logL = 0.0;
        
    f = fbegin(params);
        
    if(f < fmax){

      imin = (int)(Tobs*fmin);
      imax = (int)(Tobs*fmax);
      if(imin < 1) imin = 1;
      if(imax > N/2) imax = N/2;

      mn = (int)(tmin/Tobs*(double)(N))-1; // minimum index in data (corresponds to time prior)
      mx = (int)(tmax/Tobs*(double)(N))+1; // maximum index in data (corresponds to time prior)
      if(mn < 0) mn = 0;
      if(mx > N) mx = N;

      M = mx-mn;
      LL = double_matrix(net->Nifo,M);
      TD = double_matrix(net->Nifo,M);

      LLU = double_matrix(net->Nifo,M);
      TDU = double_matrix(net->Nifo,M); // TDU is the distance between the center and the allowed time
      PU = double_matrix(net->Nifo,M);
      AU = double_matrix(net->Nifo,M);

      NU = int_vector(net->Nifo);

      maxL = double_matrix(net->Nifo,net->Nifo);

      tx = double_tensor(net->Nifo,net->Nifo,net->Nifo);
      px = double_tensor(net->Nifo,net->Nifo,net->Nifo);
      ax = double_tensor(net->Nifo,net->Nifo,net->Nifo);


      h = double_vector(N); // initialized waveform
      HH = double_vector(net->Nifo);
      HS = double_matrix(net->Nifo,N);
      HC = double_matrix(net->Nifo,N);
      HF = double_matrix(net->Nifo,N);
      LX = double_matrix(net->Nifo,N);
      lmax = double_vector(N);
      tk = int_vector(net->Nifo);
      max = int_matrix(net->Nifo,N); // holds reference offset for each detector
      delt = double_vector(net->Nifo);  // distance (s) between (time) center of data and reference
      norm = double_vector(net->Nifo);
      pshift = double_vector(net->Nifo);

      dt = Tobs/(double)(N);

      // We always shift reference time to center
      double initial_t = params[5]; // initial value of time
      double initial_delt = params[5] - Tobs / 2.0; // initial difference in time between center
      params[5] = Tobs/2.0;

      // reference intrinsic waveform
      PDwave(h, freq, params, N, rundata);

      for(int k = 0; k < net->Nifo; k++) HH[k] = fourier_nwip_cbc(h, h, SN[k], imin, imax, N);

      for(int k = 0; k < net->Nifo; k++)
      {
          pbt_shift(HC[k], HF[k], D[k], h, SN[k], imin, imax, N);
          for(int i = 0; i < N; i++) HS[k][i] = 0.0;
          for(int i = 0; i < N/2; i++) HS[k][i+N/2] += sqrt(HC[k][i]*HC[k][i]+HF[k][i]*HF[k][i]);
          for(int i = -N/2; i < 0; i++) HS[k][i+N/2] += sqrt(HC[k][N+i]*HC[k][N+i]+HF[k][N+i]*HF[k][N+i]);
      }

      // Allow time-travel time difference between detectors (between ifo 0 and ifo id)
      for(id = 1; id < net->Nifo; id++)  tk[id] = (int)((net->delays[0][id])/dt);

      gsl_vector *v1 = gsl_vector_alloc (M);
      gsl_vector *v2 = gsl_vector_alloc (M);

      for(int k = 0; k < net->Nifo; k++){
        NU[k] = -1;
        int index = 0;
        for (int i = mn; i < mx; ++i){
            TD[k][index] = dt*(double)(i-N/2); // Difference (s) between signal and center
            HD = 2.0*(double)(N)*HS[k][i];
            LL[k][index] = (HD*HD/HH[k])/2.0;
            index++;
        }

        for(int j=0; j< M; j++){
          gsl_vector_set(v1, j, LL[k][j]);
          gsl_vector_set(v2, j, TD[k][j]);
        }

        gsl_sort_vector2_custom(v1,v2);

        for(int j=0; j < M; j++){
          LL[k][j] = gsl_vector_get(v1,M-1-j);
          TD[k][j] = gsl_vector_get(v2,M-1-j);
        }

        // iterate through the CBC Window
        for(int j=0; j<M; j++)
        {
          if(j == 0)
          {
            NU[k]++;
            LLU[k][NU[k]] = LL[k][j];
            TDU[k][NU[k]] = TD[k][j];
          }
          else
          {
            flag = 0;

            if(LL[k][j] > 9.0)  // check if loud enough
            {
              for (int i = 0; i <= NU[k]; ++i) // check if a new peak
              {
                if(fabs(TD[k][j]-TDU[k][i]) < 0.05) flag = 1;
              }
            }
            else{
              flag = 1;
            }

            if(flag == 0)
            {
              NU[k]++;
              LLU[k][NU[k]] = LL[k][j];
              TDU[k][NU[k]] = TD[k][j];
            }
          }
        }
      }
        
      gsl_vector_free(v1);
      gsl_vector_free(v2);
        
        
      for(int k = 0; k < net->Nifo; k++){
        for (int i = 0; i <= NU[k]; ++i){
           int j = (int)(rint(TDU[k][i]/dt)) + N / 2;
           HD = 2.0*(double)(N)*HS[k][j];
           delt[k] = TDU[k][i]; // TDU is the distance between the center and the allowed time

           if(j >= N/2){
             PU[k][i]  = atan2(HF[k][j-N/2],HC[k][j-N/2]);
           }
           else{
             PU[k][i]  = atan2(HF[k][j+N/2],HC[k][j+N/2]);
           }
           AU[k][i] = HD/HH[k];

            x = LLU[k][i];

            LLU[k][i] = log_likelihood_penalized(i, params, D[k], h, AU[k], PU[k], TDU[k], SN[k], N, Tobs, pflag, rundata);

           // printf("%d %d %f %f %f\n", k, i,  x, LLU[k][i], TDU[k][i]+Tobs/2);
        }
      }
     
        
    // need to do a network maximization if more than 1 detector
    if(net->Nifo > 1)
    {
      for(int i = 0; i < net->Nifo; i++){
        for(int j = i + 1; j < net->Nifo; j++){
          maxL[i][j] = 0.0;
          for(int k=0; k<= NU[i]; k++){
            // accept the loud points
            if(LLU[i][k] > 9.0){
              for(int kk=0; kk <= NU[j]; kk++){
                if(LLU[j][kk] > 9.0){
                  if(fabs(TDU[i][k]-TDU[j][kk]) < net->delays[i][j]){
                    x = LLU[i][k] + LLU[j][kk];
                    if(x > maxL[i][j])
                    {
                      maxL[i][j] = x;
                      tx[i][j][i] = TDU[i][k];
                      px[i][j][i] = PU[i][k];
                      ax[i][j][i] = AU[i][k];
                      tx[i][j][j] = TDU[j][kk];
                      px[i][j][j] = PU[j][kk];
                      ax[i][j][j] = AU[j][kk];
                    }
                  }
                }
              }
            }
          }
        }
      }

      // Note: there may not be a sky location that allows for the time delays between all detector pairs
      // the skyfind subroutine does its best to find something close to an unphysical maximum

      // if more than two detectors fold in the additional power
      if(net->Nifo > 2){
        for(int i = 0; i < net->Nifo; i++){
          // iterate to all ifos after ifo[i]
          for(int j = i+1; j < net->Nifo; j++){
            if(maxL[i][j] > 0.0){
              t0 = tx[i][j][i];
              t1 = tx[i][j][j];
              for(int k = 0; k < net->Nifo; k++){
                x = 0.0;
                if(k != i && k != j){
                  // initialize just in case no good value found
                  tx[i][j][k] = (t0+t1)/2.0;
                  px[i][j][k] = 0.0;
                  ax[i][j][k] = 1.0;

                  for (ii = 0; ii < NU[k]; ++ii){
                    t2 = TDU[k][ii];
                    // make sure that the time delays are physical
                    if(fabs(t0-t2) < net->delays[i][k] && fabs(t1-t2) < net->delays[j][k]){
                      if(LLU[k][ii] > x){
                          x = LLU[k][ii];
                          tx[i][j][k] = TDU[k][ii];
                          px[i][j][k] = PU[k][ii];
                          ax[i][j][k] = AU[k][ii];
                      }
                    }
                  }
                }
                maxL[i][j] += x;
              }
            }
          }
        }
      }

      // find the overall maximum
      x = 0.0;
      for(int i = 0; i < net->Nifo; i++){
        for(int j = i+1; j < net->Nifo; j++){
          if(maxL[i][j] > x){
            x = maxL[i][j];
            ii = i;
            jj = j;
          }
        }
      }
      if(x > 0.0){
        logL = maxL[ii][jj];
        for(int k = 0; k < net->Nifo; k++)
        {
          delt[k] = tx[ii][jj][k];
          pshift[k] = px[ii][jj][k];
          norm[k] = ax[ii][jj][k];
        }
      }
      else{
        logL = 0.0;
        for(int k = 0; k < net->Nifo; k++)
        {
            norm[k] = 1.0;
            delt[k] = initial_delt;
            pshift[k] = 0.0;
        }
      }
    } // end multi-detector section
    else{
      // for a single detector
      x = 0.0;
      for(int k=0; k<= NU[0]; k++){
        if(LLU[0][k] > x){
          x = LLU[0][k];
          jj = k;
        }
      }

      if(x > 0.0){
        delt[0] = TDU[0][jj];
        norm[0] = AU[0][jj];
        pshift[0] = PU[0][jj];
        logL = LLU[0][jj];
      }
      else{
        norm[0] = 1.0;
        delt[0] = initial_delt;
        pshift[0] = 0.0;
        logL = 0.0;
      }
    }
    // Shift the waverform to match in reference detector
    
    params[6] -= log(norm[0]);
    params[5] += delt[0];
    params[4] += pshift[0]/2.0;  // PhenomD uses orbital phase, while here we have GW phase
    
    while(params[4] < 0.0) params[4] += PI;
    while(params[4] > PI) params[4] -= PI;
    
    for(int k = 1; k < net->Nifo; k++){
      params[NX+(k-1)*3] = pshift[k]-pshift[0]; // GW phase offset
      params[NX+(k-1)*3+1] = delt[k]-delt[0]; // time offset
      params[NX+(k-1)*3+2] = norm[k]/norm[0]; // amplitude ratio
      while(params[NX+(k-1)*3] < 0.0) params[NX+(k-1)*3] += TPI;
      while(params[NX+(k-1)*3] > TPI) params[NX+(k-1)*3] -= TPI;
    }

    free_double_matrix(maxL,net->Nifo);
    free_double_tensor(tx,net->Nifo,net->Nifo);
    free_double_tensor(px,net->Nifo,net->Nifo);
    free_double_tensor(ax,net->Nifo,net->Nifo);
        
    free_double_matrix(LL,net->Nifo);
    free_double_matrix(TD,net->Nifo);
        
    free_double_matrix(LLU,net->Nifo);
    free_double_matrix(TDU,net->Nifo);
    free_double_matrix(PU,net->Nifo);
    free_double_matrix(AU,net->Nifo);
    free_int_vector(NU);
        
    free_double_matrix(LX,net->Nifo);
    free_double_matrix(HS,net->Nifo);
    free_double_matrix(HC,net->Nifo);
    free_double_matrix(HF,net->Nifo);
    free_double_vector(lmax);
    free_int_vector(tk);
    free_int_matrix(max,net->Nifo);
    free_double_vector(delt);
    free_double_vector(norm);
    free_double_vector(pshift);
    free_double_vector(HH);
    free_double_vector(h);
    }
    return logL;
}

void pbt_shift(double *corr, double *corrf, double *data1, double *data2, double *Sn, int imin, int imax, int N)
{
    int nb2, i, l, k, j;
    double *idata1, *idata2;

    idata1 = double_vector(N);
    idata2 = double_vector(N);

    for(i=1; i< N/2; i++)
    {
        idata1[i] = data1[2*i];
        idata1[N-i] = data1[2*i+1];
        idata2[i] = data2[2*i];
        idata2[N-i] = data2[2*i+1];
    }


    idata1[0] = 0.0;
    idata1[N/2] = 0.0;
 
    idata2[0] = 0.0;
    idata2[N/2] = 0.0;

    for (i = 0; i < N; i++)
    {
        corr[i] = 0.0;
        corrf[i] = 0.0;
    }
    
    for (i=imin; i < imax; i++)
    {
        l=i;
        k=N-i;
        
        corr[l] = (idata1[l]*idata2[l] + idata1[k]*idata2[k])/(Sn[i]*2.0);
        corr[k] = (idata1[k]*idata2[l] - idata1[l]*idata2[k])/(Sn[i]*2.0);
        corrf[l] = corr[k];
        corrf[k] = -corr[l];
    }
    
    gsl_fft_halfcomplex_radix2_inverse(corr, 1, N);
    gsl_fft_halfcomplex_radix2_inverse(corrf, 1, N);
    
    free_double_vector(idata1);
    free_double_vector(idata2);
    
}


double fourier_nwip_cbc(double *a, double *b, double *Sn, int imin, int imax, int N)
{
    int i, j, k;
    double arg, product;
    double test;
    double ReA, ReB, ImA, ImB;
    
    arg = 0.0;
    for(i=imin; i<imax; i++)
    {
        j = i*2;
        k = j+1;
        ReA = a[j]; ImA = a[k];
        ReB = b[j]; ImB = b[k];
        product = ReA*ReB + ImA*ImB;
        arg += product/(2.0*Sn[i]);
    }
    
    return(4.0*arg);
    
}

double fourier_nwip_bw(double *a, double *b, double *Sn, int imin, int imax, int N)
{
    int i, j, k;
    double arg, product;
    double test;
    double ReA, ReB, ImA, ImB;
    
    arg = 0.0;
    for(i=imin; i<imax; i++)
    {
        j = i;
        k = N-i;
        ReA = a[j]; ImA = a[k];
        ReB = b[j]; ImB = b[k];
        product = ReA*ReB + ImA*ImB;
        arg += product/(2.0*Sn[i]);
    }
    
    return(4.0*arg);
    
}

double globe(double ***global, double *max, double *min, double Tobs, double *params, int N, gsl_rng *r, int lmax, struct bayesCBC *rundata)
{
    double lMc, q, lq, tc;
    double lqmax, lqmin, dlq, dlMc, dt, alpha;
    double Mc, m1, m2, mt;
    double den;
    double eta;
    double lambda1, lambda2;
    int flag;
    int k, i, j;
    int out_of_bounds = 1; // boolean checks if chirp and total mass in bounds
    int NX = rundata->NX;
    int NP = rundata->NP;
    int NS = rundata->NS;
    int NH = rundata->NH; // length of history
    int NQ = rundata->NQ; // number of mass ratios in global proposal
    int NM = rundata->NM; // number of chirp masses in global proposal

    for(k = 2; k < NX; k++){
        params[k] = min[k] + (max[k]-min[k])*gsl_rng_uniform(r);
        //printf("globe() param %i, min %g, max %g, val %g\n",k, min[k], max[k], params[k]);
    }


    dlq = log(rundata->qfac);
    dlMc = (max[0]-min[0])/(double)(NM);
    dt = (max[5]-min[5])/(double)(N);
    
    lqmin = log(1.0);
    lqmax = (double)(NQ-1)*dlq;
    // sample in chirp mass / q binned space (held in global)
    do
    {
        lMc = min[0] + (max[0]-min[0]) * gsl_rng_uniform(r);
        lq = lqmin + (lqmax-lqmin) * gsl_rng_uniform(r);
        tc = min[5] + (max[5]-min[5]) * gsl_rng_uniform(r);
        
        k = (int)((lq-lqmin)/dlq);
        i = (int)((lMc-min[0])/dlMc);
        j = (int)((tc-min[5])/dt);
            
        den = 0.0;
        if(k >= 0 && i >=0 && j >= 0 && k < NQ && i < NM && j < N) den = global[k][i][j];
        if(den > rundata->cap) den = rundata->cap;
            
        alpha = gsl_rng_uniform(r) * rundata->cap;
        // make sure that total mass is in bounds after sampling with q
        q = exp(lq);
        Mc = exp(lMc);
        m2 = Mc*pow(1.0+q, 0.2)/pow(q,0.6);
        m1 = q*m2;
        mt = m1+m2;
        out_of_bounds = !check_bound(log(mt), min[1], max[1]);
    } while((den < alpha) | out_of_bounds);

    params[0] = lMc;
    params[1] = log(mt);   // Mt
    params[5] = tc; // PhenomD puts merger at Tobs, and params[5] tells us how far to pull it back


    // Prevent drawing lambdaT and dLambdaT that lead to negative lambdas when
    // drawing from global (to increase acceptance rate of proposal)
    if (NX > 7)
    {
        if (rundata->lambda_type_version == (Lambda_type) lambdaTilde)
        {
            eta = pow((Mc/mt), (5.0/3.0));
            LambdaTsEta2Lambdas(params[7], params[8], eta, &lambda1, &lambda2);
            do
            {
                params[7] = min[7] + (max[7]-min[7])*gsl_rng_uniform(r);
                params[8] = min[8] + (max[8]-min[8])*gsl_rng_uniform(r);
                LambdaTsEta2Lambdas(params[7], params[8], eta, &lambda1, &lambda2);
            }
            while (lambda1 < 0.00 || lambda2 < 0.00);
        }
    }

    return(den);
}


double globeden(double ***global, double *max, double *min, double Tobs, double *params, int N)
{
    double lMc, q, lq, tc;
    double lqmax, lqmin, dlq, dlMc, dt;
    double den, mc, mt, eta, dm;
    int k, i, j;
    
    den = 1.0;
    
    /*
    
    dlq = log(qfac);
    dlMc = (max[0]-min[0])/(double)(NM);
    dt = (max[5]-min[5])/(double)(N);
    
    lqmin = log(1.0);
    lqmax = (double)(NQ-1)*dlq;
    
    mc = exp(params[0]);
    mt = exp(params[1]);
    
    eta = pow((mc/mt), (5.0/3.0));
    if(eta > 0.25)
    {
        dm = 0.0;
    }
    else
    {
        dm = sqrt(1.0-4.0*eta);
    }
    
    q = (1.0+dm)/(1.0-dm);
    
    lMc = params[0];
    lq = log(q);
    tc = Tobs - params[5]; // PhenomD puts merger at Tobs, and params[5] tells us how far to pull it back
    
    k = (int)((lq-lqmin)/dlq);
    i = (int)((lMc-min[0])/dlMc);
    j = (int)((tc-min[5])/dt);
    
    // printf("%d %d %d %f %f %e %e %e %f\n", k, i, j, q, exp(lMc)/MSUN_SI, min[0], lMc, dlMc, tc);
    
    den = 0.0;
    if(k >= 0 && i >=0 && j >= 0 && k < NQ && i < NM && j < N) den = global[k][i][j];
    
    if(den > cap) den = cap;
     
    */
    
    return(den);
}



void MCMC_intrinsic(struct Net *net, int lmax, int *mxc, int M, FILE *chain, double **paramx, double **skyx, double **pallx, int *who, double *heat, double ***history, double ***global, RealVector *freq, double **D, double **SN, int N, double Tobs, gsl_rng *r, struct bayesCBC *rundata)
{
    Lambda_type lambda_type_version;
    int i, j, q, k, l, mc, flag, pflag;
    int *m, *evolveparams;
    double *logLx, logLy, logL, x, logLfull, *rhox;
    double **paramy;
    double fmin, fmax;
    double alpha, beta, H;
    double Mchirp, Mtot, M1, M2, ciota;
    double eta, dm, m1, m2, chieff, DL, f, z;
    int **av, **cv;
    int typ, hold;
    int scount, sacc, mcount;
    double *Fscale;
    double qx;
    double maxL;
    double *pmax;
    double lambda1, lambda2, lambdaT, dLambdaT;
    double ***fish, ***evec;
    double **ejump;

    FILE *like;

    rhox = double_vector(net->Nifo);

    // Local pointers to run settings
    int NX = rundata->NX;
    int NP = rundata->NP;
    int NS = rundata->NS;
    int NC = rundata->NC; // number of chains
    int NCC = rundata->NCC; // number of cold chains 
    int NH = rundata->NH; // length of history
    int NQ = rundata->NQ; // number of mass ratios in global proposal
    int NM = rundata->NM; // number of chirp masses in global proposal
    double *max = rundata->max;
    double *min = rundata->min;
    fmin = rundata->fmin;
    fmax = rundata->fmax;
    lambda_type_version=rundata->lambda_type_version;

    av = int_matrix(4,NC);
    cv = int_matrix(4,NC);
    m = int_vector(NC);
    ejump = double_matrix(NC,NX);
    fish = double_tensor(NC,NX,NX);
    evec = double_tensor(NC,NX,NX);
    pmax = double_vector(NX+3*net->Nifo);
    paramy = double_matrix(NC,NX+3*net->Nifo);
    logLx = double_vector(NC);
    Fscale = double_vector(NC);

    // Debug flag. Note: Prints a lot of output if pflag = 1.
    // if (rundata->debug == 1 && rundata->verbose == 1)
    // {
    //     pflag = 1;
    // } else {
    //     pflag = 0;
    // }
    pflag = 0;

    // Determine which parameters to evolve in MCMC
    evolveparams = int_vector(NX);
    for (i=0; i<NX; i++)
    {
        evolveparams[i] = 1;
    }

    for(k = 0; k < NC; k++)
    {
        Fscale[k] =  1.0;
    }
    
    max[4] = PI;  // phi0. Note: Should be TPI in main cbc, and PI for intrinsic param array

    
    // random start drawn from the global
    if(rundata->injectionStartFlag==0)
    {
        printf(" ..getting global inits for intrinsic parameters burnin\n");
        for(k = 0; k < NC; k++)
        {
            qx = globe(global, max, min, Tobs, paramx[k], N, r, lmax, rundata);
            //printf("%i, %g,",k, qx);
        }
    }
    else // TODO: start from injection parameters (This whole burnin is skipped when starting at injection)
    {
        printf(" ..starting intrinsic parameter burnin from injection parameters\n");
        for(k = 0; k < NC; k++)
        {
             qx = globe(global, max, min, Tobs, paramx[k], N, r, lmax, rundata);
        }
    }

    // inet->Nifotialize the likelihoods
    for(k = 0; k < NC; k++)
    {
        logLx[k] = 0.0;

        if(lmax == 0)
        {
            logLx[k] = log_likelihood(net, D, paramx[k], freq, SN, N, Tobs, rundata);
        }
        else
        {   
            logLx[k] = log_likelihood_max(net, D, paramx[k], freq, SN, N, Tobs, min[5], max[5], pflag, rundata);
        }
        Fisher_Full(net, fish[k], NX, paramx[k], freq, SN, N, Tobs, rundata);
        FisherEvec(fish[k], ejump[k], evec[k], NX);
    }
    
    
    printf(" ..starting intrinsic search MCMC\n");
    

    for(j = 1; j <= 4; j++)
    {
     for(k=0; k < NC; k++)
     {
         av[j][k] = 0;
         cv[j][k] = 0;
     }
    }
    
    scount = 1;
    sacc = 0;
    mcount = 1;
    
    for(k=0; k < NC; k++) m[k] = 0;
    
    q = who[0];    
    maxL = -1.0e20;
    
    // Only do single iteration and skip burnin 
    if (rundata->constantLogLFlag == 1) M=1;

    for(mc = 0; mc < M; mc++)
    {
        if(lmax == 1 && mc%510==0 && mc > 0)
        {
            x = -1.0;
            for(k = 0; k < NC; k++)
            {
             if(logLx[k] > x)
             {
                 x = logLx[k];
                 j = k;
             }
            }
            if(x > 25.0)
            {
              for(k = 0; k < NC; k++)
               {
                if(logLx[k] < 32.0)
                {
                    for(i = 0; i < NX+3*net->Nifo; i++)
                    {
                        paramx[k][i] = paramx[j][i];
                    }
                    logLx[k] = logLx[j];
                }
               }
            }
            
            // always map hottest chain to highest likelihood chain
            logLx[who[NC-1]] = logLx[j];
            for(i = 0; i < NX+3*net->Nifo; i++)
            {
                paramx[who[NC-1]][i] = paramx[j][i];
            }
            
        }
        
        if(mc%500==0 && mc > 0)
        {
            // update the Fisher matrices
            for(k = 0; k < NC; k++)
            {   
                Fisher_Full(net, fish[k], NX, paramx[k], freq, SN, N, Tobs, rundata);
                FisherEvec(fish[k], ejump[k], evec[k], NX);
            }
        }

        alpha = gsl_rng_uniform(r);

        if((NC > 1) && (alpha < 0.2))  // decide if we are doing a MCMC update of all the chains or a PT swap
        {
            
            // chain swap
            scount++;
            
            alpha = (double)(NC-1)*gsl_rng_uniform(r);
            j = (int)(alpha);
            beta = exp((logLx[who[j]]-logLx[who[j+1]])/heat[j+1] - (logLx[who[j]]-logLx[who[j+1]])/heat[j]);
            alpha = gsl_rng_uniform(r);
            if(beta > alpha)
            {
                hold = who[j];
                who[j] = who[j+1];
                who[j+1] = hold;
                sacc++;
            }
            
        }
        else     // MCMC update
        {
            
            alpha = gsl_rng_uniform(r);
            mcount++;
                
            for(j = 0; j < NC; j++)
            {
                for(i = 0; i < NX+3*net->Nifo; i++)
                {
                    paramy[j][i] = paramx[j][i];
                }
            }

            for(k=0; k < NC; k++)
            {
            updatei(k, net, lmax, logLx, paramx, paramy, min, max, Fscale, who, heat, history, global, freq, D, SN, ejump, evec, N, Tobs, cv, av, r, rundata);
            }

            // add to the history file
            if(mc%20 == 0 && mc >0)
            {
                for(k=0; k < NC; k++)
                {
                    q = who[k];
                    i = m[k]%1000;
                    // the history file is kept for each temperature
                    for(j=0; j<NX; j++) history[k][i][j] = paramx[q][j];
                    m[k]++;
                }
            }
                           
            // update maxL parameters
            if(logLx[who[0]] > maxL)
            {
                maxL = logLx[who[0]];
                for(i = 0; i < NX+3*net->Nifo; i++) pmax[i] = paramx[who[0]][i];
            }
            

            if(mc%100 == 0 && rundata->debug)
            {   
                l=0;
                // for (l=0; l<NC; l++)
                // {
                q = who[l];
                // pall  [0] log(Mc) [1] log(Mt) [2] chi1 [3] chi2 [4] phi0 [5] tp [6] log(DL) [7] alpha [8] sindelta [9] psi [10] ciota
                pmap(net, pallx[q], paramx[q], skyx[q], NX, rundata->gmst);

                Mchirp = exp(pallx[q][0])/MSUN_SI;
                Mtot = exp(pallx[q][1])/MSUN_SI;
                eta = pow((Mchirp/Mtot), (5.0/3.0));
                dm = sqrt(1.0-4.0*eta);
                m1 = Mtot*(1.0+dm)/2.0;
                m2 = Mtot*(1.0-dm)/2.0;
                chieff = (m1*pallx[q][2]+m2*pallx[q][3])/Mtot;
                ciota = pallx[q][NX+3];

                DL = Fscale[q]*exp(pallx[q][6])/(1.0e6*PC_SI);  // includes conversion to Mpc
                z = z_DL(DL);
                            
                // counter, log likelihood, chirp mass, total mass, effective spin, geocenter GW phase, geocenter arrival time, distance, RA , sine of DEC,
                // polarization angle, cos inclination
                
                // param [0] log(Mc) [1] log(Mt) [2] chi1 [3] chi2 [4] phi0  [5] tp0 [6] log(DL0) then relative amplitudes, time, phases

                    logL = log_likelihood(net, D, paramx[q], freq, SN, N, Tobs, rundata);

                    if (NX > 7) {
                        if (lambda_type_version == (Lambda_type) lambdaTilde) {
                            lambdaT = paramx[q][7];
                            dLambdaT = paramx[q][8];
                            LambdaTsEta2Lambdas(lambdaT, dLambdaT, eta, &lambda1, &lambda2);
                        } else {
                            lambda1 = paramx[q][7];
                            lambda2 = paramx[q][8]; 
                            lambdaT = 0.0; 
                            dLambdaT = 0.0;                 
                        }
                        fprintf(chain,"%d %e %e %e %e %e %e %e %e %e %e %e %e %e %e %e %e %e %e %e %e %e %e %e\n", q, logLx[q], Mchirp, Mtot, chieff, paramx[q][4], \
                                         pallx[q][5]-(Tobs-2.0), DL, pallx[q][NX+0], pallx[q][NX+1], \
                                         pallx[q][NX+2], ciota, z, Mchirp/(1.0+z), Mtot/(1.0+z), m1/(1.0+z), m2/(1.0+z), m1, m2, logL,
                                         lambda1, lambda2, lambdaT, dLambdaT);
                        printf("%d %e %e %e %e %e %e %e %e %e %e %e %e %e %e %e %e %e %e %e %e %e %e %e\n", mc, logLx[q], Mchirp, Mtot, chieff, paramx[q][4], \
                                         pallx[q][5]-(Tobs-2.0), DL, pallx[q][NX+0], pallx[q][NX+1], \
                                         pallx[q][NX+2], ciota, z, Mchirp/(1.0+z), Mtot/(1.0+z), m1/(1.0+z), m2/(1.0+z), m1, m2, logL,
                                         lambda1, lambda2, lambdaT, dLambdaT);
                    } else {
                        fprintf(chain,"%d %e %e %e %e %e %e %e %e %e %e %e %e %e %e %e %e %e %e %e\n", q, logLx[q], Mchirp, Mtot, chieff, paramx[q][4], \
                                         pallx[q][5]-(Tobs-2.0), DL, pallx[q][NX+0], pallx[q][NX+1], \
                                         pallx[q][NX+2], ciota, z, Mchirp/(1.0+z), Mtot/(1.0+z), m1/(1.0+z), m2/(1.0+z), m1, m2, logL);
                    }
                // }
            }
        }    
    }
    
    like = fopen("chain_intrinsic.dat", "w");
    for(k=0; k < NC; k++)
    {
        q = who[k];
        // the history file is kept for each temperature
        for(j=0; j<NX+3*net->Nifo; j++) {
            fprintf(like,"%e ", paramx[k][j]); 
        }
        fprintf(like,"\n");
    }
    fclose(like);

    // record maxL parameters
    like = fopen("maxlike_intrinsic.dat", "w");
    fprintf(like,"%f ", maxL);
    for(i = 0; i < NX+3*net->Nifo; i++) fprintf(like,"%e ", pmax[i]);
    fprintf(like,"\n");
    fclose(like);
    
    // Reset max Phi0 for main MCMC
    max[4] = TPI;  // phi0. Note: Should be TPI in main cbc, and PI for intrinsic param array

    free_int_matrix(av,4);
    free_int_matrix(cv,4);
    
    free_int_vector(m);
    
    free_double_matrix(ejump,NC);
    free_double_tensor(fish,NC,NX);
    free_double_tensor(evec,NC,NX);
    
    free_double_vector(rhox);
    free_double_vector(pmax);
    free_double_matrix(paramy,NC);
    free_double_vector(logLx);
    free_double_vector(Fscale);
}
    
void updatei(int k, struct Net *net, int lmax, double *logLx, double **paramx, 
    double **paramy, double *min, double *max, double *Fscale, int *who, 
    double *heat, double ***history, double ***global, RealVector *freq, 
    double **D, double **SN, double **ejump, double ***evec, int N, double Tobs, 
    int **cv, int **av, gsl_rng *r, struct bayesCBC *rundata)
{
    int q, i, j;
    double qx, qy, a, b, c;
    double alpha, beta, DL, pDx, pDy, H;
    double logLy, eta, leta, pMcMx, pMcMy;
    double mc, mt, lambda1y, lambda2y;
    int typ, flag, pflag;
    int *evolveparams;

    int NX = rundata->NX;
    int NP = rundata->NP;
    int NS = rundata->NS;
    int NC = rundata->NC; // number of chains
    int NCC = rundata->NCC; // number of cold chains 
    int NH = rundata->NH; // length of history
    int NQ = rundata->NQ; // number of mass ratios in global proposal
    int NM = rundata->NM; // number of chirp masses in global proposal
        
    // Debug flag. Note: Prints a lot of output if pflag = 1.
    // TODO: uncomment
    // if (rundata->debug == 1 && rundata->verbose == 1)
    // {
    //     pflag = 1;
    // } else {
    //     pflag = 0;
    // }
    pflag = 0;

    // Determine which parameters to evolve in MCMC
    // Currently hardcoded here for testing
    evolveparams = int_vector(NP);
    for (i=0; i<NX; i++)
    {   
        evolveparams[i] = 1;
    }

    q = who[k];
    
    qx = qy = 0.0;    // log proposal densities
    
    alpha = gsl_rng_uniform(r);
    
    a = 0.7;
    b = 0.5;
    c = 0.45;
    
    // Adjust proposal ratios when signal not found
    if(logLx[q] < 20.0)
    {
        a = 1.0;
        b = 1.0;
        c = 0.9;
    }


    if(alpha > a) // fisher jump
    {
        typ = 1;
        
        // pick an eigendirection to jump in
        beta = gsl_rng_uniform(r);
        i = (int)(beta*NX);
        
        // draw the jump size
        beta = sqrt(heat[k])*ejump[q][i]*gsl_ran_gaussian(r,1.0);
        
        for(j = 0; j < NX; j++) paramy[q][j] = paramx[q][j]+beta*evec[q][i][j];
                        
    }
    else if (alpha > b) // differential evolution
    {
        typ = 2;
        
        // the history file is kept for each temperature
        de_jump(paramx[q], paramy[q], history[k], NH, NX, r, evolveparams);

        // sometimes jump uniformly in tidal parameters
        beta = gsl_rng_uniform(r);
        if (NX>7 && beta > 0.8) for(j = 7; j < NX; j++) paramy[q][j] = rundata->min[j] + (rundata->max[j]-rundata->min[j])*gsl_rng_uniform(r);
    }
    else if (alpha > c) // jiggle (most usefl early when Fisher not effective)
    {
        typ = 3;

        beta = 0.01*pow(10.0, -floor(3.0*gsl_rng_uniform(r)))*sqrt(heat[k]);
        for(j = 0; j < NX; j++) paramy[q][j] = paramx[q][j]+beta*gsl_ran_gaussian(r,1.0);

    }
    else   // global
    {
        typ = 4;

        qy = globe(global, max, min, Tobs, paramy[q], N, r, lmax, rundata);
        qx = globeden(global, max, min, Tobs, paramx[q], N);
    }
    
    cv[typ][k]++;
    
    flag = 0; // flag = False -> parameter is in bounds
              // flag = True -> Parameter is out of bounds

		// TODO SHOULD THIS BE CHECKING ALL?
		// CURRENTLY WE DON'T CHECK TIME (paramy[q][5] (time))
    // check intrinsic bounds
    for(i = 0; i < 5; i++) {
      if (!check_bound(paramy[q][i], min[i], max[i])) flag = 1;
    }
		// check distance
    if (!check_bound(paramy[q][6], min[6], max[6])) flag = 1;
    if (NX > 7){
      // (I believe these are the lambda parameters)
      if (!check_bound(paramy[q][7], min[7], max[7])) flag = 1;
      if (!check_bound(paramy[q][8], min[8], max[8])) flag = 1;
    }

    // eta cannot exceed 0.25 (it does sometimes from numerical error)
    leta = (5.0 / 3.0) * (paramy[q][0] - paramy[q][1]); // log eta
    if (!check_bound(leta,-INFINITY, log(0.25))) flag = 1;

    // extra check that lambdas are positive when using dLambdaT sampler
    if (NX > 7 && flag == 0)
    {
        if (rundata->lambda_type_version == (Lambda_type) lambdaTilde)
        {

            mc = exp(paramy[q][0]);
            mt = exp(paramy[q][1]);
            eta = pow((mc/mt), (5.0/3.0));

            LambdaTsEta2Lambdas(paramy[q][7], paramy[q][8], eta, &lambda1y, &lambda2y);
            if (lambda1y < 0.00 || lambda2y < 0.00) flag = 1;
        }
    }

    // Jacobian that makes the prior flat in m1, m2.
    if(flag == 0)
    {
        eta = exp(leta);
        pMcMy = 2.0 * paramy[q][1] + leta - 0.5 * log(1.0 - 4.0 * eta);
        
        leta = (5.0/3.0) * (paramx[q][0] - paramx[q][1]);
        eta = exp(leta);
        pMcMx = 2.0 * paramx[q][1] + leta - 0.5 * log(1.0 - 4.0 * eta);
    }
    
    logLy = -1.0e20;
    if(flag == 0)
    {
        logLy = 0.0;
        if(lmax == 0)
        {
            logLy = log_likelihood(net, D, paramy[q], freq, SN, N, Tobs, rundata);
        }
        else
        {
            logLy = log_likelihood_max(net, D, paramy[q], freq, SN, N, Tobs, min[5], max[5], pflag, rundata);
            // maximization can throw distance out of allowed range
            DL = Fscale[q]*exp(paramy[q][6])/PC_SI;
            if(DL > rundata->DLmax) flag = 1;
            if(DL < rundata->DLmin) flag = 1;
        }
    }
    
    // now check the extrinsic parameters
    if(paramy[q][5] > max[5] || paramy[q][5] < min[5]) flag = 1;
    DL = Fscale[q] * exp(paramy[q][6]) / PC_SI;
    if(DL > rundata->DLmax) flag = 1;
    if(DL < rundata->DLmin) flag = 1;
    
    if(flag == 1) logLy = 0.0;
    
    //  DLx = Fscale[q]*exp(paramx[q][6]);
    //  DLy = Fscale[q]*exp(paramy[q][6]);
    // since the Fscale and sky parameters are held fixed in the extrinsic MCMC can
    // use the expresion below for the DL^2 prior
    
    // variable in MCMC is x=logD, so p(x) = dD/dx p(D) = D p(D) = D^3
    
    pDx = 3.0*paramx[q][6];   // unet->Nifoform in volume prior
    pDy = 3.0*paramy[q][6];   // unet->Nifoform in volume prior
    
    
    if(lmax == 1)  // no need to maintain balance if in search mode
    {
        pDx = pDy = 0.0;   // otherwise the distance prior fights with the likelihood during search
        qy = qx = 0.0;
    }
    
    H = (logLy-logLx[q])/heat[k] + pMcMy + pDy - qy - pDx - pMcMx + qx;
    
    alpha = log(gsl_rng_uniform(r));
    
    if(H > alpha && flag == 0)
    {
        // copy over new state if accepted
        logLx[q] = logLy;
        for(i = 0; i < NX+3*net->Nifo; i++) paramx[q][i] = paramy[q][i];
        av[typ][k]++;
    }

    free_int_vector(evolveparams);
        
}

/*
 * Convert symmetric mass ratio (eta) to 
 * mass ratio (q)
 */
// double sym_mass_ratio_to_mass_ratio(eta){
//     double temp;

//     temp = 1.0/(eta*2) - 1.0;
//     return temp - sqrt(pow(temp, 2) - 1.0);
// }

/**
 * Set prior boundaries for bayescbc full parameter array (NP).
'* Boundaries are stored in min and max array.
 * This could be moved to BayesWavePrior.c. 
 */
void set_bayescbc_priors(struct Net *net, struct bayesCBC *rundata)
{   
    // If no tidal parameters:
    // [0] log(Mc) [1] log(Mt) [2] chi1 [3] chi2 [4] phi0 [5] tp [6] log(DL) [7] alpha [8] sindelta [9] psi [10] ciota
    // With tidal parameters:
    // [0] log(Mc) [1] log(Mt) [2] chi1 [3] chi2 [4] phi0 [5] tp [6] log(DL) [7] tidal1 [8] tidal2 [9] alpha [10] sindelta [11] psi [12] ciota
    int NX = rundata->NX;

    // Prior boundaries
    rundata->max = (double*)malloc(sizeof(double)* (rundata->NP));
    rundata->min = (double*)malloc(sizeof(double)* (rundata->NP));

    // Set default boundaries intrinsic parameters
    rundata->DLmin = 1.0e6;           // Minimum  distance in pc
    rundata->DLmax = 1.0e10;          // Maximum  distance in pc

    // Set default prior range for BNS
    if (rundata->NRTidal_version != (NRTidal_version_type) NoNRT_V)
    {
        // Tidal parameter ranges
        rundata->lambda1min = 0.0;       // Min/max for lambda1 and lambda2
        rundata->lambda1max = 1000.0; // 1000.0;    // Only used if NRTidal_value != NoNRT_V and
        rundata->lambda2min = 0.0;       // lambda_type_value = lambda 
        rundata->lambda2max = 1000.0;

        rundata->lambdaTmin = 0.0;       // Min/max for lambdaT and dlambdaT
        rundata->lambdaTmax = 1000.0;    // Only used if NRTidal_value != NoNRT_V and
        rundata->dlambdaTmin = -500.0;     // lambda_type_value = lambdaTilde 
        rundata->dlambdaTmax = 500.0;

        // Update spin prior range for BNSs
        rundata->chi1min = -0.05;         // Default min, max for chi1 and chi2
        rundata->chi2min = -0.05;
        rundata->chi1max = 0.05;
        rundata->chi2max = 0.05;

        // Update mass range for BNSs
        rundata->mcmax = 2.6;             // maximum chirp mass in Msun.  Mc is at most 0.435 times Mt
        rundata->mcmin = 0.23;            // minimum chirp mass in Msun
        rundata->mtmax = 6.0;             // maximum total mass in Msun (should be not more than 2.3 times mcmax)
        rundata->mtmin = 1.0;             // minimum total mass in Msun
    }
    else
    {
        //rundata->chi1min = -1.0;  // Default min, max for chi1 and chi2
        //rundata->chi2min = -1.0;
        //rundata->chi1max = 1.0;
        //rundata->chi2max = 1.0;
        rundata->chi1min = -0.99;  // Default min, max for chi1 and chi2
        rundata->chi2min = -0.99;
        rundata->chi1max = 0.99;
        rundata->chi2max = 0.99;

        rundata->mcmax = 174.0;   // maximum chirp mass in Msun.  Mc is at most 0.435 times Mt
        rundata->mcmin = 0.23;    // minimum chirp mass in Msun
        rundata->mtmax = 400.0;   // maximum total mass in Msun (should be not more than 2.3 times mcmax)
        rundata->mtmin = 1.0;     // minimum total mass in Msun
    }

    rundata->max[0] = log(rundata->mcmax*MSUN_SI);  // Mc is at most 0.435 times Mt
    rundata->max[1] = log(rundata->mtmax*MSUN_SI);  // Mt
    rundata->max[2] = rundata->chi1max;  // chi1
    rundata->max[3] = rundata->chi2max;  // chi2
    rundata->max[4] = TPI;  // phi0. Note: Should be TPI in main cbc, and PI for intrinsic param array
    rundata->max[5] = net->tmax; // peak time (geocenter?) (Trigger time is Tobs-2)
    rundata->max[6] = log(rundata->DLmax * PC_SI); //distance

    if (NX > 7) 
    {
        if (rundata->lambda_type_version == (Lambda_type) lambdaTilde) {
            rundata->max[7] = rundata->lambdaTmax; // lambdaT
            rundata->max[8] = rundata->dlambdaTmax; // dlambdaT
        } else {
            rundata->max[7] = rundata->lambda1max; // lambda1
            rundata->max[8] = rundata->lambda2max; // lambda2
        }
    }

    rundata->max[NX] = TPI;  // alpha
    rundata->max[NX+1] = 1.0;  // sindelta
    rundata->max[NX+2] = PI; // psi
    rundata->max[NX+3] = 1.0; // ciota
    
    
    rundata->min[0] = log(rundata->mcmin*MSUN_SI);  // Mc
    rundata->min[1] = log(rundata->mtmin*MSUN_SI);  // Mt
    rundata->min[2] = rundata->chi1min;  // chi1
    rundata->min[3] = rundata->chi2min;  // chi2
    rundata->min[4] = 0.0;  // phi0
    rundata->min[5] = net->tmin; // peak time (Trigger time is Tobs-2)
    rundata->min[6] = log(rundata->DLmin * PC_SI); //distance

    if (NX > 7) 
    {
        if (rundata->lambda_type_version == (Lambda_type) lambdaTilde) 
        {
            rundata->min[7] = rundata->lambdaTmin; // lambdaT
            rundata->min[8] = rundata->dlambdaTmin; // dlambdaT
        } else {
            rundata->min[7] = rundata->lambda1min; // lambda1
            rundata->min[8] = rundata->lambda2min; // lambda2
        }
    }

    rundata->min[NX] = 0.0;  // alpha
    rundata->min[NX+1] = -1.0;  // sindelta 
    rundata->min[NX+2] = 0.0; // psi
    rundata->min[NX+3] = -1.0; // ciota

}


void reinitialize_history(double ***history, double **pallx, int NH, int NC, int NX, gsl_rng *r)
{
    int i, j, ic, q;

    printf("reinitialize_history: %d\n", NC);
    // re-initialize history
    for(i=0; i<NH; i++)
    {
        for(ic=0; ic<NC; ic++)
        {
            q = (int)(gsl_rng_uniform(r)*(double)(NC-1)); // use values from all temperatures
            for(j=0; j< NX; j++) history[ic][i][j] = pallx[q][j];
        }

        if(i==0) {
            printf("parsed history for ic: %d\n", ic);
            fflush(stdout);
        }
    }
}

/**
 * Run MCMC_all for a single chain with the full parameter array (NP)
 */
void BayesCBC_MCMC(int M, int N, double Tobs, double ttrig, gsl_rng *r, int ncycle, struct bayesCBC *rundata, int ic, double *amphase)
{
    int i, j, q, k, mc, flag;
    int m, *evolveparams;
    double logLy, logL, x;
    double *paramy, *paramx;
    double Mchirp, Mtot, M1, M2, ciota, tc;
    double eta, dm, m1, m2, chieff, DL;
    double *rhox;
    double mapL;
    double *pmax;
    
    // Tidal parameters
    double lambda1, lambda2, lambdaT, dLambdaT;
    Lambda_type lambda_type_version;
    
    FILE *like;
    FILE *ring;
    FILE *out;
    
    double **fish, **evec;
    double *ejump;
    int *av, *cv;
    char filename[1024];
    
    // Local pointers
    int NX = rundata->NX;
    int NP = rundata->NP;
    int NS = rundata->NS;
    int NCC = rundata->NCC;
    int NH = rundata->NH; // length of history
    int NQ = rundata->NQ; // number of mass ratios in global proposal
    int NM = rundata->NM; // number of chirp masses in global proposal

    struct Net *net = rundata->net;
    double ***global = rundata->global;
    RealVector *freq = rundata->freq;
    double *max = rundata->max;
    double *min = rundata->min;
    int *who = rundata->who;

    int icbc = who[ic];

    av = int_vector(4);
    cv = int_vector(4);
        
    rhox = double_vector(net->Nifo);
    ejump = double_vector(NP);
    fish = double_matrix(NP,NP);
    evec = double_matrix(NP,NP);
    pmax = double_vector(NP);
    paramy = double_vector(NP);

    lambda_type_version=rundata->lambda_type_version;    

    paramx = rundata->pallx[icbc];
   
    // Determine which parameters to evolve in MCMC
    evolveparams = int_vector(NP);
    for (i=0; i<NX; i++) evolveparams[i] = 1;


    // Initialize the cbc likelihood
    rundata->logLx[icbc] = log_likelihood_full(net, rundata->D, paramx, freq, rundata->SN, rhox, N, Tobs, rundata, 0);
    // if(ic==0) printf("logLx at start %i: %f \n", icbc, rundata->logLx[icbc]);

    if(rundata->debug) {
        for (i=0; i<NX; i++){
            if (paramx[i] > rundata->max[i] || paramx[i] < rundata->min[i]) {
                printf("paramx[%i]: %f ", i, paramx[i]);
                printf(" min[i] = %f, max[i] = %f, ", rundata->min[i], rundata->max[i]);
                printf("logLx at start %i: %f \n", icbc, rundata->logLx[icbc]);
                printf("\n");
            }

        } 
    }


    if (rundata->heterodyneFlag) Fisher_All(net, fish, paramx, rundata->het->freq, rundata->het->SN, 2*rundata->het->M, Tobs, rundata, N/(2*rundata->het->M));
    else Fisher_All(net, fish, paramx, freq, rundata->SN, N, Tobs, rundata, 1);

    FisherEvec(fish, ejump, evec, NX);

    if (rundata->heterodyneFlag) eigen_value_rescaling(net, paramx, rundata->het->freq, rundata->het->SN, 2*rundata->het->M, Tobs, ejump, evec, 1.0, rundata, N/(2*rundata->het->M));
    else eigen_value_rescaling(net, paramx, freq, rundata->SN, N, Tobs, ejump, evec, 1.0, rundata, 1);

    // keep track of acceptances
    for(j = 1; j <= 4; j++)
      {
          av[j] = 0;
          cv[j] = 0;
      }

   // Set history start index of current iteration (saved every 20 iterations)

    m = (int) ncycle/20;

    mapL = -1.0e20;
    for(mc = 0; mc < M; mc++)
    {
                           
        for(i = 0; i < NP; i++)
        {
            paramy[i] = paramx[i];
        }

        // Actual parameter updates
        update_chain(icbc, net, rundata->logLx, rhox, paramx, paramy, min, max, rundata->heat[ic], rundata->history[ic], global, freq, rundata->D, rundata->SN, ejump, evec, N, Tobs, cv, av, r, evolveparams, rundata);

        // add to the history file
        if(mc%20 == 0)
        {
            i = m%NH;
            for(j=0; j<NX; j++){ rundata->history[ic][i][j] = paramx[j];}
            m++;
        }

        // update MAP parameters
        if(rundata->logLx[icbc] > mapL)
        {
            mapL = rundata->logLx[icbc];
            for(i = 0; i < NP; i++) pmax[i] = paramx[i];
        }
        if (ic == 0){rundata->iteration++;}
    }

    // Print acceptance rates for CBC
    if (ic == 0 && ( (rundata->iteration % (rundata->count/10)==0) ||rundata->debug == 1)) {
        printf("\n-- Acceptance rates ic = %i, %i / %i--\n", ic, rundata->iteration, rundata->count);
        printf("Fisher Acceptance  = %f, %i/%i\n",
               (double) rundata->fisher_acc / (double)(rundata->fisher_prop),
               rundata->fisher_acc, rundata->fisher_prop);
        printf("DE Acceptance      = %f, %i/%i\n",
               (double) rundata->diff_evolution_acc / (double)(rundata->diff_evolution_prop),
               rundata->diff_evolution_acc, rundata->diff_evolution_prop);
        printf("Jiggle Acceptance  = %f, %i/%i\n",
               (double)rundata->jiggle_acc / (double)(rundata->jiggle_prop),
               rundata->jiggle_acc, rundata->jiggle_prop);
        printf("History Acceptance = %f, %i/%i\n",
               (double)rundata->history_acc / (double)(rundata->history_prop),
               rundata->history_acc, rundata->history_prop);
        if (NX>7) printf("Uniform tidal Acceptance = %f\n", (double) av[4]/(double)(cv[4]));
        printf("\n");
    }
    
    if(ic==0 && rundata->debug==1)
    {
        Mchirp = exp(paramx[0])/MSUN_SI;
        Mtot = exp(paramx[1])/MSUN_SI;
        
        if (NX > 7) {
            if (lambda_type_version == (Lambda_type) lambdaTilde) {
                eta = pow((Mchirp/Mtot), (5.0/3.0));
                lambdaT = paramx[7];
                dLambdaT = paramx[8];
                LambdaTsEta2Lambdas(lambdaT, dLambdaT, eta, &lambda1, &lambda2);
            } else {
                lambda1 = paramx[7];
                lambda2 = paramx[8];
                lambdaT = 0.0;
                dLambdaT = 0.0;
            }
            // Also print tidal parameters
            printf("%d %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n", rundata->mxc[0], rundata->logLx[icbc], Mchirp, Mtot, paramx[4], paramx[5], paramx[6], paramx[7], paramx[8], paramx[NX], paramx[NX+1], paramx[NX+2], \
                   lambda1, lambda2, lambdaT, dLambdaT);
            for (i = 0; i < NP; i++) printf("%f \t", paramx[i]);
            printf("\n");
        } else {
            printf("%d %f %f %f %f %f %f %f %f %f %f \n", rundata->mxc[0], rundata->logLx[icbc], Mchirp, Mtot, paramx[4], paramx[5], paramx[6], paramx[NX], paramx[NX+1], paramx[NX+2], paramx[NX+3]);
            for (i = 0; i < NP; i++) printf("%f \t", paramx[i]);
            printf("\n");
        }  
    } 

    // Store Amplitude and Phase for geotemplate
    geowave(amphase, rundata->freq,  rundata->pallx[icbc], N, rundata);

    free_int_vector(av);
    free_int_vector(cv);
    free_int_vector(evolveparams);
            
    free_double_vector(rhox);
    free_double_vector(ejump);
    free_double_matrix(fish,NP);
    free_double_matrix(evec,NP);
    
    free_double_vector(paramy);

}

void update_chain(int k, struct Net *net, double *logLx, double *rhox, double *paramx, double *paramy, double *min,
      double *max, double heat, double **history, double ***global, RealVector *freq, double **D, double **SN, double *ejump,
      double **evec, int N, double Tobs, int *cv, int *av, gsl_rng *r, int *evolveparams, struct bayesCBC *rundata)
{
    int q, i, j;
    double qx, qy, a, b, c;
    double alpha, beta, DL, pDx, pDy, H, base;
    double logLy, eta, leta, pMcMx, pMcMy;
    double mc, mt;
    double *rhoy;
    int typ, flag, id1, id2;
  
    int NX = rundata->NX;
    int NP = rundata->NP;
    int NS = rundata->NS;
    int NC = rundata->NC; // number of chains
    int NCC = rundata->NCC; // number of cold chains 
    int NH = rundata->NH; // length of history
    int NQ = rundata->NQ; // number of mass ratios in global proposal
    int NM = rundata->NM; // number of chirp masses in global proposal        
    
    double logxfull, logyfull;
    double lambda1y, lambda2y;

    rhoy = (double*)malloc(sizeof(double)* (net->Nifo));

    a = 0.4;

    // Include tidal uniform proposal if BNSs
    if (NX > 7)
    {
        b = 0.2;
        c = 0.05; 
    } else {
        b = 0.1;
        c = 0.0;
    }

    q = k; // No chain swaps
        
    qx = qy = 0.0;    // log proposal densities
    
    alpha = gsl_rng_uniform(r);

    if(alpha > a) // fisher jump
    {
        typ = 1;
        // pick an eigendirection to jump in
        beta = gsl_rng_uniform(r);
        i = (int)(beta*NX);
        
        // draw the jump size
        beta = sqrt(heat)*ejump[i]*gsl_ran_gaussian(r,1.0);
        
        for(j = 0; j < NX; j++) 
        {
            paramy[j] = paramx[j];
            if (evolveparams[j] > 0) 
            {
                paramy[j] += beta*evec[i][j];
            }
        }
        if (k == 0) rundata->fisher_prop++;
    }
    else if (alpha > b) // differential evolution
    {
        typ = 2;
        // the history file is kept for each temperature
        de_jump(paramx, paramy, history, NH, NX, r, evolveparams);
        if (k == 0) rundata->diff_evolution_prop++;
    }
    else if (alpha > c)  // jiggle (most useful early when Fisher not effective)
    {
        typ = 3;
        beta = pow(10.0, -floor(3.0*gsl_rng_uniform(r)))*sqrt(heat);
        for(j = 0; j < NX; j++) 
        {
            paramy[j] = paramx[j];
            if (evolveparams[j] > 0) 
            {   
                if (j<7) base = 0.01;
                else base = 10.0;
                paramy[j] += base*beta*gsl_ran_gaussian(r,1.0);
            }
        }
        if (k == 0) rundata->jiggle_prop++;
    }
    else // uniform tidal params and grab others from history
    {
        typ = 4;

        de_jump(paramx, paramy, history, NH, NX, r, evolveparams);

        if (NX>7)
        {
            for(j = 7; j < NX; j++) 
            {
                if (evolveparams[j] > 0) 
                {
                    paramy[j] = rundata->min[j] + (rundata->max[j]-rundata->min[j])*gsl_rng_uniform(r);
                }
            }
        }
        if (k == 0) rundata->history_prop++;
    }
    
    cv[typ]++;
    
    // [0] log(Mc) [1] log(Mt) [2] chi1 [3] chi2 [4] phi0 [5] tp [6] log(DL) [7] alpha [8] sindelta [9] psi [10] ciota
    
    // re-map angular parameters to their proper range
    while(paramy[4] > TPI)   paramy[4] -= TPI;
    while(paramy[4] < 0.0)  paramy[4] += TPI;
    
    int r1;
    r1 = 0;

    // check proposed values are in prior range
    flag = 0;
    for(i = 0; i < NX; i++)
    {   
        if (evolveparams[i] > 0) {
            if(paramy[i] > max[i] || paramy[i] < min[i]) flag = 1;
            // {
                // printf("paramy[%i] outside of prior: %f. min: %f, max: %f, paramx[i]: %f\n", i, paramy[i], min[i], max[i], paramx[i]);
                // flag = 1;
            // }
        }
    }

    if (flag == 1) r1 = 1;

    // extra check that lambdas are positive when using dLambdaT sampler
    if (NX > 7 && flag == 0)
    {
        if (rundata->lambda_type_version == (Lambda_type) lambdaTilde)
        {

            mc = exp(paramy[0]);
            mt = exp(paramy[1]);
            eta = pow((mc/mt), (5.0/3.0));

            LambdaTsEta2Lambdas(paramy[7], paramy[8], eta, &lambda1y, &lambda2y);
            if (lambda1y < 0.00 || lambda2y < 0.00) flag = 1;
        }
    }
    if (flag == 1 && r1 == 0) r1 = 2;
    
    
    // eta cannot exceed 0.25
    leta = (5.0/3.0)*(paramy[0]-paramy[1]);
    if(leta > log(0.25)) flag = 1;
    if (flag == 1 && r1 == 0) r1 = 3;
    
    // Jacobian that makes the prior flat in m1, m2.
    if(flag == 0)
    {
        eta = exp(leta);
        pMcMy = 2.0*paramy[1]+leta-0.5*log(1.0-4.0*eta);
        
        leta = (5.0/3.0)*(paramx[0]-paramx[1]);
        eta = exp(leta);
        pMcMx = 2.0*paramx[1]+leta-0.5*log(1.0-4.0*eta);
    }
    
    logLy = -1.0e20;
    if(flag == 0)
    {
        logLy = 0.0;
        if(rundata->constantLogLFlag == 0) logLy = log_likelihood_full(net, rundata->D, paramy, freq, rundata->SN, rhoy, N, Tobs, rundata, 0);
    }
    if (flag == 1 && r1 == 0) r1 = 4;
    
    // variable in MCMC is x=logD, so p(x) = dD/dx p(D) = D p(D) = D^3
    pDx = 3.0*paramx[6];   // unifoform in volume prior
    pDy = 3.0*paramy[6];   // unifoform in volume prior
    
    H = (logLy-logLx[q])/heat + pMcMy + pDy - qy - pDx - pMcMx + qx;
    
    alpha = log(gsl_rng_uniform(r));

    // printf("H: %f, alpha: %f\n;", H, alpha);
    // if (q == 0) printf("from: %f, %f, proposed values: %f, %f, ep: %i, sqrt(h): %f, beta: %f", paramx[7], paramx[8], paramy[7], paramy[8], evolveparams[7], sqrt(heat), beta);
    if(H > alpha && flag == 0)
    {   
        // printf("logx-logy (het): %e, logx-logy (full): %e, diff: %e, logLy_het: %f, logLy_full: %f\n", logLx[q]-logLy, logxfull-logyfull, (logLx[q]-logLy) - (logxfull-logyfull), logLy, logyfull);
        // if (q == 0) printf("  ACCEPTED by %i\n", typ);
        for (i=0; i<NX; i++){
            if (paramy[i] > rundata->max[i] || paramy[i] < rundata->min[i]) {
                printf("WARNING update_chain Accepted: paramx[%i]: %f out of bounds (%f, %f)\n", i, paramy[i], rundata->min[i], rundata->max[i]);
            }
        } 

        // copy over new state if accepted
        logLx[q] = logLy;
        for(i = 0; i < net->Nifo; i++) rhox[i] = rhoy[i];
        for(i = 0; i < NP; i++) paramx[i] = paramy[i];

        // update acceptance trackers (only if on coldest chain!)
        if (k == 0){
          if (typ == 1){rundata->fisher_acc ++;}
          else if (typ == 2){rundata->diff_evolution_acc++;}
          else if (typ == 3){rundata->jiggle_acc++;}
          else if (typ == 4){rundata->history_acc++;}
        }
        av[typ]++;
    } else 
    {
        // if (H <= alpha) r1 = 5;
        if (flag == 0 && r1 == 0) r1 = 5;
        // if (q == 0) printf("  REJECTED by %i due to %i\n", typ, r1);
    }
    free(rhoy);
}

    
void de_jump(double *paramsx, double *paramsy, double **history, int m, int d, gsl_rng *r, int *evolveparams)
{
    int i, j, k, NH;
    double alpha, beta;
    double h1, h2;
    
    NH = m - 1;

    // pick two points from the history
    i = (int)((double)(NH)*gsl_rng_uniform(r));
    do
    {   
        j = (int)((double)(NH)*gsl_rng_uniform(r));
    } while(i==j);

    alpha = 1.0000e0;
    beta = gsl_rng_uniform(r);
    if(beta < 0.9) alpha = gsl_ran_gaussian(r,0.5);


    for(k=0; k<d; k++) 
    {
        paramsy[k] = paramsx[k];
        if (evolveparams[k] > 0)
        {   
            // Large number multiplication to keep the double precision history operations deterministic
            h1 = history[i][k] * 1e6;
            h2 = history[j][k] * 1e6;
            paramsy[k] += alpha*(history[i][k]+history[j][k])/1e6;
        }
    } 
}

void Fisher_Full(struct Net *net, double **fish, int d, double *params, RealVector *freq, double **SN, int N, double Tobs, struct bayesCBC *rundata)
{
    double *paramsP, *paramsM;
    double epsilon;
    double Scale;
    double *AH, *PH, **AHP, **AHM, **PHP, **PHM;
    int i, j, k, l;
    int imin, imax;
    double fmin, fmax;

    int NX = rundata->NX;

    fmin = rundata->fmin;
    fmax = rundata->fmax;
       
    for (i = 0; i < d; i++)
    {
        for (j = 0; j < d; j++) fish[j][i] = 0.0;
    }
    
    for (i = 0; i < d; i++) fish[i][i] = 1.0e4;
    
    if(fabs(params[2]) < 0.9999 &&  fabs(params[3]) < 0.9999)
    {
        
        
        imin = (int)(Tobs*fmin);
        imax = (int)(Tobs*fmax);
        if(imax > N/2) imax = N/2;

    
        epsilon = 1.0e-5;
        
        paramsP = (double*)malloc(sizeof(double)*(d));
        paramsM = (double*)malloc(sizeof(double)*(d));
        
        AH = (double*)malloc(sizeof(double)*(N/2));
        PH = (double*)malloc(sizeof(double)*(N/2));
        
        AHP = double_matrix(d,N/2+1);
        PHP = double_matrix(d,N/2+1);
        AHM = double_matrix(d,N/2+1);
        PHM = double_matrix(d,N/2+1);
     
        
        // Reference phase and amplitude
        AmpPhase(AH, PH, freq, params, N, rundata);
        // Note that since this Fisher matrix only varies intrinsic parameters, the different overall phasing and arrival time in each
        // detector is irrelvant. Only the overall amplitude differences matter, and that are taken care of by the Scale parameter below.
        
        for (i = 0 ; i < d ; i++)
        {
            
            for (k = 0 ; k < d ; k++)
            {
                paramsP[k] = params[k];
                paramsM[k] = params[k];
            }
            
            paramsP[i] += epsilon;
            paramsM[i] -= epsilon;

            AmpPhase(AHP[i], PHP[i], freq, paramsP, N, rundata);
            AmpPhase(AHM[i], PHM[i], freq, paramsM, N, rundata);
            
            // store the central difference in the Plus arrays
            for (k = 0 ; k < N/2 ; k++)
            {
                AHP[i][k] -= AHM[i][k];
                PHP[i][k] -= PHM[i][k];
                AHP[i][k] /= (2.0*epsilon);
                PHP[i][k] /= (2.0*epsilon);
            }
            
           // printf("%d ", i);
            
        }
        
        
         //printf("\n");
        
        for (i = 0; i < d; i++)
        {
            for (j = 0; j < d; j++) fish[j][i] = 0.0;
        }

        for (l = 0 ; l < net->Nifo ; l++)  // loop over detectors
        {
            if(l==0)
            {
                Scale = 4.0;
            }
            else
            {
                Scale = 4.0*params[(l-1)*3+NX+2]*params[(l-1)*3+NX+2];
            }
            
            for (i = 0 ; i < d ; i++)
            {
                for (j = i ; j < d ; j++)
                {
                    for (k = imin ; k < imax ; k++) fish[i][j] += Scale*(AHP[i][k]*AHP[j][k]+AH[k]*AH[k]*PHP[i][k]*PHP[j][k])/(2.0*SN[l][k]);
                }
            }
            
        }
        
        
        /* fill in lower triangle */
        
        for (i = 0; i < d; i++)
        {
            for (j = i+1; j < d; j++)
                fish[j][i] = fish[i][j];
        }
        
        for (i = 0; i < d; i++) if(fish[i][i] < 0.001) fish[i][i] = 0.001;
        
        /*
        for (i = 0; i < d; i++)
        {
            for (j = 0; j < d; j++)
            {
                printf("%.3e ", fish[i][j]);
            }
            printf("\n");
        }
        */
        
        free(paramsP);
        free(paramsM);
        free(AH);
        free(PH);
        free_double_matrix(AHP,d);
        free_double_matrix(PHP,d);
        free_double_matrix(AHM,d);
        free_double_matrix(PHM,d);
    
   }

}




void AmpPhase(double *Af, double *Pf, RealVector *freq, double *params, int N, struct bayesCBC *rundata)
{
    
    AmpPhaseFDWaveform *ap = NULL;
    NRTidal_version_type NRTidal_version;
    Lambda_type lambda_type_version;
    double phi0, fRef_in, mc, q, m1_SI, m2_SI, chi1, chi2, f_min, f_max, distance;
    int ret, flag, i;
    double p, cp, sp;
    double f, x, y, deltaF, ts, Tobs;
    double *extraParams;
    double mt, eta, dm;
    double lambda1, lambda2, lambdaT, dLambdaT, sym_mass_ratio_eta;

    extraParams = (double*)malloc(sizeof(double)* 2);
    
    Tobs = 1.0/freq->data[1];
    
    mc = exp(params[0]);
    mt = exp(params[1]);
    
    eta = pow((mc/mt), (5.0/3.0));
    if(eta > 0.25)
    {
        dm = 0.0;
    }
    else
    {
        dm = sqrt(1.0-4.0*eta);
    }
    m1_SI = mt*(1.0+dm)/2.0;
    m2_SI = mt*(1.0-dm)/2.0;
    
 
    chi1 = params[2];
    chi2 = params[3];
    
    phi0 = params[4];
    ts = Tobs-params[5];
    distance = exp(params[6]);

    /* ==== TIDAL PARAMETERS ==== */        
    fRef_in = rundata->fref;
    NRTidal_version = rundata->NRTidal_version;
    lambda_type_version = rundata->lambda_type_version;

    if (NRTidal_version != NoNRT_V) {
        // Compute lambda1 & lambda2
        if (lambda_type_version == (Lambda_type) lambdaTilde) {
            lambdaT = params[7];
            dLambdaT = params[8];
            sym_mass_ratio_eta = m1_SI*m2_SI/((m1_SI+m2_SI)*(m1_SI+m2_SI));
            // printf(" DEBUG: sym_mass_ratio_eta, eta = %f, %f \n", eta, sym_mass_ratio_eta);
            LambdaTsEta2Lambdas(lambdaT, dLambdaT, sym_mass_ratio_eta, &lambda1, &lambda2);

            extraParams[0] = lambda1; // lambda1
            extraParams[1] = lambda2; // lambda2         
        } else {
            extraParams[0] = params[7]; // lambda1
            extraParams[1] = params[8]; // lambda2            
        }
    }
    
    GenerateWaveform(&ap, freq, phi0, fRef_in, m1_SI, m2_SI, chi1, chi2, distance, extraParams, NRTidal_version, rundata->waveformFlag
        );
    //fprintf(stdout, "\t done Ampphase \n");
    // ret = IMRPhenomDGenerateh22FDAmpPhase(&ap, freq, phi0, fRef_in, m1_SI, m2_SI, chi1, chi2, distance, extraParams, NRTidal_version);

    for (i=1; i< N/2; i++)
    {
       f = freq->data[i];
       Pf[i] = TPI*f*ts-ap->phase[i];
       Af[i] = h22fac*ap->amp[i];
    }
    
    
    DestroyAmpPhaseFDWaveform(ap);
    free(extraParams);
}



void PDwave(double *wavef, RealVector *freq, double *params, int N, struct bayesCBC *rundata)
{
    
    AmpPhaseFDWaveform *ap = NULL;
    NRTidal_version_type NRTidal_version;
    Lambda_type lambda_type_version;
    double phi0, fRef_in, mc, q,  m1_SI, m2_SI, chi1, chi2, f_min, f_max, distance;
    int ret, flag, i;
    double p, cp, sp, Amp, fs;
    double f, x, y, deltaF, ts, Tobs, sqT;
    double *extraParams;
    double mt, eta, dm;
    double lambda1, lambda2; 
    double lambdaT, dLambdaT, sym_mass_ratio_eta;

    extraParams = (double*)malloc(sizeof(double)* 2);
    
    Tobs = 1.0/freq->data[1];
    sqT = 1.0;//sqrt(Tobs);
    
    // want to find lowest possible start frequency for the current masses, spins
    x = params[5];
    params[5] = Tobs; // temporarily set merger time to end of segement
    fs = fbegin(params);
    params[5] = x;  // restore to assigned value
    
    mc = exp(params[0]);
    mt = exp(params[1]);
    
    eta = pow((mc/mt), (5.0/3.0));
    if(eta > 0.25)
    {
        dm = 0.0;
    }
    else
    {
    dm = sqrt(1.0-4.0*eta);
    }
    m1_SI = mt*(1.0+dm)/2.0;
    m2_SI = mt*(1.0-dm)/2.0;
    
    /*q = params[1];
    m2_SI = mc*pow(1.0+q, 0.2)/pow(q,0.6);
    m1_SI = q*m2_SI;*/
    
    chi1 = params[2];
    chi2 = params[3];
    
    phi0 = params[4];
    ts = Tobs-params[5];
    distance = exp(params[6]);
    
    /* ==== TIDAL PARAMETERS ==== */        
    fRef_in = rundata->fref;
    NRTidal_version = rundata->NRTidal_version;
    lambda_type_version = rundata->lambda_type_version;

    if (NRTidal_version != NoNRT_V) {
        // Compute lambda1 & lambda2
        if (lambda_type_version == (Lambda_type) lambdaTilde) {
            lambdaT = params[7];
            dLambdaT = params[8];
            sym_mass_ratio_eta = m1_SI*m2_SI/(mt*mt);
            // printf("mc, mt: %e, %e\n", mc, mt);
            // printf("symm mas: %f, m1_si: %e, m2_si: %e\n", sym_mass_ratio_eta, m1_SI, m2_SI);
            LambdaTsEta2Lambdas(lambdaT, dLambdaT, sym_mass_ratio_eta, &lambda1, &lambda2);

            extraParams[0] = lambda1; // lambda1
            extraParams[1] = lambda2; // lambda2       
            // printf("lambda1, lambda2: %f, %f\n", lambda1, lambda2);
        } else {
            extraParams[0] = params[7]; // lambda1
            extraParams[1] = params[8]; // lambda2     
        }
    }
    //fprintf(stdout, "Pdwave\n");
    GenerateWaveform(&ap, freq, phi0, fRef_in, m1_SI, m2_SI, chi1, chi2, distance, extraParams, NRTidal_version, rundata->waveformFlag);
    //fprintf(stdout, "Pdwave over\n");
    // ret =  IMRPhenomDGenerateh22FDAmpPhase(&ap, freq, phi0, fRef_in, m1_SI, m2_SI, chi1, chi2, distance, extraParams, NRTidal_version);
    for (i=1; i< N/2; i++)
    {
        wavef[2*i] = 0.0;
        wavef[2*i+1] = 0.0;
        
        f = freq->data[i];
        if(f > fs)
        {
        p = TPI*f*ts;
        cp = cos(p);
        sp = sin(p);
        x = cos(ap->phase[i]);
        y = sin(ap->phase[i]);
        Amp = h22fac*ap->amp[i]/sqT;
        wavef[2*i] = Amp*(x*cp+y*sp);
        wavef[2*i+1] = Amp*(x*sp-y*cp);
        }
    }
    
    wavef[0] = 0.0;
    wavef[1] = 0.0;
    
    DestroyAmpPhaseFDWaveform(ap);
    free(extraParams);
    
}


/*
 *  Projects the CBC model amplitudate and phase onto the detectors
 *  when the new sky location/extrinsic parameters have not been 
 *  absorbed in the cbc intrinsic parameters used to produce the
 *  waveform model. So this applies extra shifts for t0, phi0 and scale.
 * `response` holds the computed response for each detector. 
 *  
 */
void projectCBCWaveformExtrinsic(double *ampphase, int N, int NI, double fmin, double Tobs, double *extParams, double **response, double *dtimes, double *Fplus, double *Fcross, double *proposal_shifts)
{
    int i,id;
    double Fs, lambda;
    double A, x, td, f;
    
    double ciota = extParams[3];
    double phi0 = extParams[4];

    // Geocenter shifts due to new sky location
    double dt0 = proposal_shifts[0];
    double dphi0 = proposal_shifts[1];
    double scale = proposal_shifts[2];  // Scale can be < 1.0 during extrinsic updates

    double Ap = (1.0+ciota*ciota)/2.0;
    double Ac = ciota;

    double df = 1./Tobs;

    for (id=0; id<NI; id++)
    {        

        Fs =  Fmag(Ap, Ac, Fcross[id], Fplus[id]); // magnitude of response
        lambda = phase_from_polarizations(Ap, Ac, Fcross[id], Fplus[id]); // get phase from polarizations

        td = dtimes[id]+dt0;
        
        response[id][0] = 0.0;
        response[id][1] = 0.0;
        
        for (i=1; i< N/2; i++)
        {   
            f = i*df;
            if (f <= fmin) {
                response[id][2*i] = 0.0;
                response[id][2*i+1] = 0.0;
            }
            else 
            {
                A = ampphase[2*i]*Fs*scale;
                x = ampphase[2*i+1]+lambda-TPI*f*td+dphi0;
                response[id][2*i] = A*cos(x);
                response[id][2*i+1] = A*sin(x);               
            }
        }
    }
}


/*
 *  Projects the CBC model amplitudate and phase onto the detectors.
 * `response` holds the computed response for each detector. 
 */
void projectCBCWaveform(double *ampphase, int N, int NI, double fmin, double Tobs, double *extParams, double **response, double *dtimes, double *Fplus, double *Fcross)
{
    int i,id;
    double Fs, lambda;
    double A, x, td, f;
    int real, imag;
    
    double ciota = extParams[3];
    double phi0 = extParams[4];
    double scale = extParams[5];  // Scale can be < 1.0 during extrinsic updates

    double Ap = (1.0+ciota*ciota)/2.0;
    double Ac = ciota;

    double df = 1./Tobs;

    for (id=0; id<NI; id++)
    {        

        Fs =  Fmag(Ap, Ac, Fcross[id], Fplus[id]); // magnitude of response
        lambda = phase_from_polarizations(Ap, Ac, Fcross[id], Fplus[id]); // get phase from polarizations

        td = dtimes[id];
        
        response[id][0] = 0.0;
        response[id][1] = 0.0;
        
        for (i=1; i< N/2; i++)
        {   
            real = 2*i;
            imag = real + 1;

            f = i*df;

            if (f <= fmin) {
                response[id][real] = 0.0;
                response[id][imag] = 0.0;
            }
            else 
            {
                A = ampphase[real]*Fs;
                x = ampphase[imag]+lambda-TPI*f*td;
                response[id][real] = A*cos(x);
                response[id][imag] = A*sin(x);               
            }
        }
    }
}

/*
 *  Computes the CBC model intrinsic amplitudes and phase at geocenter.
 *  `gwave` holds the computed amplitude (at 2*i indices) and phase (at
 *  2*i+1 indices).
 */
void geowave(double *gwave, RealVector *freq, double *params, int N, struct bayesCBC *rundata)
{
    
    AmpPhaseFDWaveform *ap = NULL;
    NRTidal_version_type NRTidal_version;
    double phi0, fRef_in, mc, q, m1_SI, m2_SI, chi1, chi2, f_min, f_max, distance;
    int ret, flag, i, j, id;
    double p, cp, sp;
    double f, x, y, deltaF, ts, Tobs, sqT;
    double pd, Ar, td, A;
    double *extraParams;
    double mt, eta, dm, fs;
    double lambda1, lambda2;
    
    extraParams = (double*)malloc(sizeof(double)* 2);

    fs = fbegin(params);
        
    Tobs = 1.0/freq->data[1];
    sqT = 1.0;//sqrt(Tobs);
    
    mc = exp(params[0]);
    mt = exp(params[1]);
    

    eta = pow((mc/mt), (5.0/3.0));
    if(eta > 0.25)
    {
        dm = 0.0;
    }
    else
    {
        dm = sqrt(1.0-4.0*eta);
    }
    // printf("  mc = %f, mt = %f, eta = %f ", mc, mt, eta);

    m1_SI = mt*(1.0+dm)/2.0;
    m2_SI = mt*(1.0-dm)/2.0;
    
    
    chi1 = params[2];
    chi2 = params[3];
    
    phi0 = 0.5*params[4];  // I'm holding the GW phase in [4], while PhenomD wants orbital
    ts = Tobs-params[5];
    
    // printf("ts = %f, params[5] = %f, phi0 = %f, Tobs =%f\n", ts, params[5], phi0, Tobs);

    distance = exp(params[6]);
    
    /* ==== TIDAL PARAMETERS ==== */        
    fRef_in = rundata->fref;
    NRTidal_version = rundata->NRTidal_version;

    double lambdaT, dLambdaT, sym_mass_ratio_eta;
    Lambda_type lambda_type;
    lambda_type = rundata->lambda_type_version;

    if (NRTidal_version != NoNRT_V) {

        // Compute lambda1 & lambda2
        if (lambda_type == (Lambda_type) lambdaTilde) {
            lambdaT = params[7];
            dLambdaT = params[8];
            sym_mass_ratio_eta = m1_SI*m2_SI/((m1_SI+m2_SI)*(m1_SI+m2_SI));
            LambdaTsEta2Lambdas(lambdaT, dLambdaT, sym_mass_ratio_eta, &lambda1, &lambda2);;

            extraParams[0] = lambda1; // lambda1
            extraParams[1] = lambda2; // lambda2         
        } else {
            extraParams[0] = params[7]; // lambda1
            extraParams[1] = params[8]; // lambda2            
        }
    }

    GenerateWaveform(&ap, freq, phi0, fRef_in, m1_SI, m2_SI, chi1, chi2, distance, extraParams, NRTidal_version, rundata->waveformFlag);
    //fprintf(stdout, "\t geowave over \n");
    // ret = IMRPhenomDGenerateh22FDAmpPhase(&ap, freq, phi0, fRef_in, m1_SI, m2_SI, chi1, chi2, distance, extraParams, NRTidal_version);

    gwave[0] = 0.0;
    gwave[1] = 0.0;
    
    for (i=1; i< N/2; i++)
    {
        f = freq->data[i];
       
        if(f > fs)
        {
        A = h22fac*ap->amp[i];
        p = ap->phase[i];
        x = TPI*f*ts-p;
        gwave[2*i] = A;
        gwave[2*i+1] = x;
        }
        else
        {
            gwave[2*i] = 0.0;
            gwave[2*i+1] = 0.0;
        }
        // y = 1.0;
        // if(f<fs) y = exp(10.0*(f-fs)/fs);
        // gwave[2*i] = A*y;
        // gwave[2*i+1] = x;
    }


    DestroyAmpPhaseFDWaveform(ap);
    free(extraParams);
}

void fulltemplates(struct Net *net, double **hwave, RealVector *freq, double *params, int N, struct bayesCBC *rundata)
{
    
    AmpPhaseFDWaveform *ap = NULL;
    NRTidal_version_type NRTidal_version;
    double phi0, fRef_in, mc, q, m1_SI, m2_SI, chi1, chi2, f_min, f_max, distance;
    int ret, flag, i, j, id;
    double p, cp, sp;
    double f, x, y, deltaF, ts, Tobs, sqT;
    double pd, Ar, td, A;
    double alpha, sindelta, psi, ciota, lambda;
    double Ap, Ac;
    double Fplus, Fcross, Fs;
    double *extraParams;    
    double mt, eta, dm, fs;
    double *dtimes;
    double lambda1, lambda2;

    int NX = rundata->NX;
    int NP = rundata->NP;
    int NS = rundata->NS;
    int NC = rundata->NC; // number of chains
    int NCC = rundata->NCC; // number of cold chains 
    int NH = rundata->NH; // length of history
    int NQ = rundata->NQ; // number of mass ratios in global proposal
    int NM = rundata->NM; // number of chirp masses in global proposal

    extraParams = (double*)malloc(sizeof(double)* 2);  
    dtimes = (double*)malloc(sizeof(double)* 5);
    
    fs = fbegin(params);
    
    Tobs = net->Tobs;
    sqT = 1.0; //sqrt(Tobs);
    
    mc = exp(params[0]);
    mt = exp(params[1]);
    
    eta = pow((mc/mt), (5.0/3.0));
    if(eta > 0.25)
    {
        dm = 0.0;
    }
    else
    {
        dm = sqrt(1.0-4.0*eta);
    }
    m1_SI = mt*(1.0+dm)/2.0;
    m2_SI = mt*(1.0-dm)/2.0;
    
    
    chi1 = params[2];
    chi2 = params[3];
    
    phi0 = 0.5*params[4];  // I'm holding the GW phase in [4], while PhenomD wants orbital
    ts = Tobs-params[5];
    
    distance = exp(params[6]);
    
    /* ==== TIDAL PARAMETERS ==== */        
    fRef_in = rundata->fref;
    NRTidal_version = rundata->NRTidal_version;

    double lambdaT, dLambdaT, sym_mass_ratio_eta;
    Lambda_type lambda_type;
    lambda_type = rundata->lambda_type_version;
    
    if (NRTidal_version != NoNRT_V) {

        // Compute lambda1 & lambda2
        if (lambda_type == (Lambda_type) lambdaTilde) {
            lambdaT = params[7];
            dLambdaT = params[8];
            sym_mass_ratio_eta = m1_SI*m2_SI/((m1_SI+m2_SI)*(m1_SI+m2_SI));
            LambdaTsEta2Lambdas(lambdaT, dLambdaT, sym_mass_ratio_eta, &lambda1, &lambda2);

            extraParams[0] = lambda1; // lambda1
            extraParams[1] = lambda2; // lambda2         
        } else {
            extraParams[0] = params[7]; // lambda1
            extraParams[1] = params[8]; // lambda2            
        }
    }

    GenerateWaveform(&ap, freq, phi0, fRef_in, m1_SI, m2_SI, chi1, chi2, distance, extraParams, NRTidal_version, rundata->waveformFlag);
    //fprintf(stdout, "\t fulltemplates over \n");
    // ret = IMRPhenomDGenerateh22FDAmpPhase(&ap, freq, phi0, fRef_in, m1_SI, m2_SI, chi1, chi2, distance, extraParams, NRTidal_version);

    alpha = params[NX];
    sindelta = params[NX+1];
    psi = params[NX+2];
    ciota = params[NX+3];
    
    Ap = (1.0+ciota*ciota)/2.0;
    Ac = ciota;
    
    TimeDelays(net, alpha, sindelta, net->dtimes, rundata->gmst);
    
    for (id=0; id< net->Nifo; id++)
    {      
        // F_ant(psi, alpha, sindelta, &Fplus, &Fcross, net->labels[0], rundata->gmst);
        // Fs = sqrt(Ap*Ap*Fplus*Fplus+Ac*Ac*Fcross*Fcross);
        // lambda = atan2(Ac*Fcross,Ap*Fplus);

        // (re)compute Fcross & Fplus
        ComputeDetFant(net, psi, alpha, sindelta, &net->Fplus[id], &net->Fcross[id], id, rundata->gmst);

        // magnitude of response
        Fs = sqrt(Ap*Ap*net->Fplus[id]*net->Fplus[id]+Ac*Ac*net->Fcross[id]*net->Fcross[id]);
        lambda = atan2(-Ac*net->Fcross[id],Ap*net->Fplus[id]);
        while(lambda < 0.0) lambda += TPI;
        td = net->dtimes[id];

        hwave[id][0] = 0.0;
        hwave[id][1] = 0.0;
        
        for (i=1; i< N/2; i++)
        {
            f = freq->data[i];
            if(f > fs)
            {
            A = Fs*h22fac*ap->amp[i]/sqT;
            p = ap->phase[i];
            x = TPI*f*(ts-td)+lambda-p;
            hwave[id][2*i] = A*cos(x);
            hwave[id][2*i+1] = A*sin(x);
            }
            else
            {
                hwave[id][2*i] = 0.0;
                hwave[id][2*i+1] = 0.0;
            }
        }
    }
    
    free(dtimes);
    DestroyAmpPhaseFDWaveform(ap);
    free(extraParams);
    
}


void fullphaseamp(struct Net *net, double **amp, double **phase, RealVector *freq, double *params, int N, struct bayesCBC *rundata, int recomputeFlag)
{
    
    AmpPhaseFDWaveform *ap = NULL;
    NRTidal_version_type NRTidal_version;
    double phi0, fRef_in, mc, q, m1_SI, m2_SI, chi1, chi2, f_min, f_max, distance;
    int ret, flag, i, j, id;
    double p, cp, sp;
    double f, x, y, deltaF, ts, Tobs, sqT;
    double pd, Ar, td, A;
    double alpha, sindelta, psi, ciota, lambda;
    double Ap, Ac;
    double Fplus, Fcross, Fs;
    double *extraParams;    
    double mt, eta, dm, fs;
    double *dtimes;
    double lambda1, lambda2;

    int NX = rundata->NX;

    //if (!within_CBC_prior(params, rundata, 1)){
    //  fprintf(stdout, "\t full_phase_amp \n");
    //}

    extraParams = (double*)malloc(sizeof(double)* 2);
    dtimes = (double*)malloc(sizeof(double)* 5);
    
    fs = fbegin(params);
    Tobs = net->Tobs;
    sqT = 1.0;// sqrt(Tobs);
    
    mc = exp(params[0]);
    mt = exp(params[1]);
    
    eta = pow((mc/mt), (5.0/3.0));
    if(eta > 0.25)
    {
        dm = 0.0;
    }
    else
    {
        dm = sqrt(1.0-4.0*eta);
    }
    m1_SI = mt*(1.0+dm)/2.0;
    m2_SI = mt*(1.0-dm)/2.0;
    
    
    chi1 = params[2];
    chi2 = params[3];
    
    phi0 = 0.5*params[4]; // I'm holding the GW phase in [4], while PhenomD wants orbital
    ts = Tobs-params[5];
    
    distance = exp(params[6]);
    
    /* ==== TIDAL PARAMETERS ==== */        
    fRef_in = rundata->fref;
    NRTidal_version = rundata->NRTidal_version;

    double lambdaT, dLambdaT, sym_mass_ratio_eta;
    Lambda_type lambda_type;
    lambda_type = rundata->lambda_type_version;
    
    if (NRTidal_version != NoNRT_V) {

        // Compute lambda1 & lambda2
        if (lambda_type == (Lambda_type) lambdaTilde) {
            lambdaT = params[7];
            dLambdaT = params[8];
            sym_mass_ratio_eta = m1_SI*m2_SI/((m1_SI+m2_SI)*(m1_SI+m2_SI));
            LambdaTsEta2Lambdas(lambdaT, dLambdaT, sym_mass_ratio_eta, &lambda1, &lambda2);

            extraParams[0] = lambda1; // lambda1
            extraParams[1] = lambda2; // lambda2         
        } else {
            extraParams[0] = params[7]; // lambda1
            extraParams[1] = params[8]; // lambda2            
        }

        // Prevent negative lambdas
        if (extraParams[0] < 0.0) extraParams[0] = 0.0;
        if (extraParams[1] < 0.0) extraParams[1] = 0.0;
    }


    GenerateWaveform(&ap, freq, phi0, fRef_in, m1_SI, m2_SI, chi1, chi2, distance, extraParams, NRTidal_version, rundata->waveformFlag);
    //fprintf(stdout, "\t fullphaseamp over\n");
    // ret = IMRPhenomDGenerateh22FDAmpPhase(&ap, freq, phi0, fRef_in, m1_SI, m2_SI, chi1, chi2, distance, extraParams, NRTidal_version);
    
    alpha = params[NX];
    sindelta = params[NX+1];
    psi = params[NX+2];
    ciota = params[NX+3];
    
    Ap = (1.0+ciota*ciota)/2.0;
    Ac = ciota;
        
    if (recomputeFlag == 1) TimeDelays(net, alpha, sindelta, net->dtimes, rundata->gmst);

    for (id=0; id< net->Nifo; id++)
    {

        if (recomputeFlag == 1) ComputeDetFant(net, psi, alpha, sindelta, &net->Fplus[id], &net->Fcross[id], id, rundata->gmst);

        Fs =  Fmag(Ap, Ac, net->Fcross[id], net->Fplus[id]); // magnitude of response
        lambda = phase_from_polarizations(Ap, Ac, net->Fcross[id], net->Fplus[id]); // get phase from polarizations
        td = net->dtimes[id];

        amp[id][0] = 0.0;
        phase[id][0] = 0.0;
        
        for (i=1; i< N/2; i++)
        {
            f = freq->data[i];

            if (isnan(freq->data[i])) printf("freq->data[i] %f\n",freq->data[i]);
            A = Fs*h22fac*ap->amp[i]/sqT;
            p = ap->phase[i];
            x = TPI*f*(ts-td)+lambda-p;
            y = 1.0;
            if(f<fs) y = exp(10.0*(f-fs)/fs);
            amp[id][i] = A*y;
            phase[id][i] = x;
        }        
    }
    
    free(dtimes);
    DestroyAmpPhaseFDWaveform(ap);
    free(extraParams);
    
}

void Fisher_All(struct Net *net, double **fish, double *params, RealVector *freq, double **SN, int N, double Tobs, struct bayesCBC *rundata, int scale)
{
    double *paramsP, *paramsM;
    double *epsilon, epsilon_base;
    double Scale;
    double **AH, **PH, ***AHP, ***AHM, ***PHP, ***PHM;
    double ***AHPP, ***AHMM, ***PHPP, ***PHMM, ***Pdiv, ***Adiv;
    double logLP, logLM;
    int i, j, k, l, id;
    int imin, imax;
    double fmin, fmax, range;
    double lambda1min, lambda1max, lambda2min, lambda2max;
    double mc, mt, eta;

    double *rhox = double_vector(net->Nifo);

    int NX = rundata->NX;
    int NP = rundata->NP;

    fmin = rundata->fmin;
    fmax = rundata->fmax;
    
    epsilon = double_vector(NX);

    if(fabs(params[2]) < 0.9999 &&  fabs(params[3]) < 0.9999)
    {
        
        imin = (int)(Tobs*fmin);
        imax = (int)(Tobs*fmax);
        if(imax > N/2) imax = N/2;
        
        
        paramsP = (double*)malloc(sizeof(double)*(NP));
        paramsM = (double*)malloc(sizeof(double)*(NP));
        
        AH = double_matrix(net->Nifo,N/2+1);
        PH = double_matrix(net->Nifo,N/2+1);
        AHP = double_tensor(NP,net->Nifo,N/2+1);
        PHP = double_tensor(NP,net->Nifo,N/2+1);
        AHM = double_tensor(NP,net->Nifo,N/2+1);
        PHM = double_tensor(NP,net->Nifo,N/2+1);


        Adiv = double_tensor(NP,net->Nifo,N/2+1);
        Pdiv = double_tensor(NP,net->Nifo,N/2+1);


        AHPP = double_tensor(NP,net->Nifo,N/2+1);
        PHPP = double_tensor(NP,net->Nifo,N/2+1);
        AHMM = double_tensor(NP,net->Nifo,N/2+1);
        PHMM = double_tensor(NP,net->Nifo,N/2+1);

        // Reference phase and amplitude
        fullphaseamp(net, AH, PH, freq, params, N, rundata, 0);
        
        for (i = 0 ; i < NX ; i++)
        {
            
            for (k = 0 ; k < NP ; k++)
            {
                paramsP[k] = params[k];
                paramsM[k] = params[k];
            }

            epsilon_base = 1e-5;

            if (i > 3)
            {

                // Compute initial range for differencing in parameter
                range = fabs(rundata->max[i] - rundata->min[i]);
                epsilon[i] = range*epsilon_base;

                paramsP[i] += epsilon[i];
                paramsM[i] -= epsilon[i];

                // Check that the spins will be in a range that the waveform can handle
                if ((i == 2 || i == 3) && (paramsP[i] >= 0.999 || paramsM[i] <= -0.999)) {
                    epsilon[i] = 1e-5;
                }

                // Check that masses & Dl don't become negative when differencing
                if (i == 0|| i == 1 || i == 6) {
                    if (paramsM[i] <= rundata->min[i]) epsilon[i] = 1e-5;
                }

                paramsP[i] = params[i] + epsilon[i];
                paramsM[i] = params[i] - epsilon[i];

                // Evaluate logL at plus and minus location
                logLP = log_likelihood_full(rundata->net, rundata->D, paramsP, freq, rundata->SN, rhox, N, Tobs, rundata, 0);
                logLM = log_likelihood_full(rundata->net, rundata->D, paramsM, freq, rundata->SN, rhox, N, Tobs, rundata, 0);

                // Adjust epsilon based on likelihood difference
                epsilon[i] = 0.1/sqrt(-(logLM+logLP)/(epsilon[i]*epsilon[i]));
                if(epsilon[i]!=epsilon[i]) epsilon[i] = epsilon_base;
                if(epsilon[i]<epsilon_base) epsilon[i] = epsilon_base;

                // Check that the spins will be in a range that the waveform can handle
                if ((i == 2 || i == 3) && (fabs(params[i]) + 2*epsilon[i] >= 0.999)) {
                    epsilon[i] = 1e-5;
                }

                // Check that masses & Dl don't become negative when differencing
                if (i == 0 || i == 1 || i == 6) {
                    if (params[i] - 2*epsilon[i] <= rundata->min[i]) epsilon[i] = 1e-6;
                }

            }
            else
            {
                epsilon[i] = epsilon_base/2.0;
            }
            // if (i >= 7) epsilon_base = 1e-2;
        }

        // Check that lambdas don't become negative when differencing
        if (NX > 7 && (i == 7 || i == 8)) {
            if (rundata->lambda_type_version == (Lambda_type) lambdaTilde)
            {

                mc = exp(params[0]);
                mt = exp(params[1]);
                eta = pow((mc/mt), (5.0/3.0));

                LambdaTsEta2Lambdas(params[7]+2*epsilon[i], params[8]+2*epsilon[i], eta, &lambda1max, &lambda2max);
                if (lambda1max < 0.00 || lambda2max < 0.00) {
                    epsilon[7] = 1e-5;
                    epsilon[8] = 1e-5;
                } else{
                    LambdaTsEta2Lambdas(params[7]-2*epsilon[i], params[8]-2*epsilon[i], eta, &lambda1min, &lambda2min);
                    if (lambda1min < 0.00 || lambda2min < 0.00) {
                        epsilon[7] = 1e-5;
                        epsilon[8] = 1e-5;
                    }
                }
            } else {
                if (params[7]-2*epsilon[7] < 0.0) epsilon[7] = 1e-5;
                if (params[8]-2*epsilon[8] < 0.0) epsilon[8] = 1e-5;
            }
        }

        for (i = 0 ; i < NX ; i++)
        {
            
            for (k = 0 ; k < NP ; k++)
            {
                paramsP[k] = params[k];
                paramsM[k] = params[k];
            }

            paramsP[i] += epsilon[i];
            paramsM[i] -= epsilon[i];

            // Compute first step from reference parameters
            fullphaseamp(net, AHP[i], PHP[i], freq, paramsP, N, rundata, 0);
            fullphaseamp(net, AHM[i], PHM[i], freq, paramsM, N, rundata, 0);
 
            paramsP[i] += epsilon[i];
            paramsM[i] -= epsilon[i];

            // Compute second step from reference parameters
            fullphaseamp(net, AHPP[i], PHPP[i], freq, paramsP, N, rundata, 0);
            fullphaseamp(net, AHMM[i], PHMM[i], freq, paramsM, N, rundata, 0);

            // Store the central differences in the Plus arrays
            for (id = 0 ; id < net->Nifo ; id++)  // loop over detectors
            {
                for (k = 1 ; k < N/2 ; k++)
                {
                    Adiv[i][id][k] = (AHMM[i][id][k] + 8*AHP[i][id][k] - 8*AHM[i][id][k] - AHPP[i][id][k])/(12.0*epsilon[i]);
                    Pdiv[i][id][k] = (PHMM[i][id][k] + 8*PHP[i][id][k] - 8*PHM[i][id][k] - PHPP[i][id][k])/(12.0*epsilon[i]);
                }
            }
        }
        
        // Initialize fisher matrix elements
        for (i = 0; i < NX; i++)
        {
            for (j = 0; j < NX; j++) fish[j][i] = 0.0;
        }
        
        // Set stabilizers for diagonal intrinsic
        for (i = 0; i < 4; i++) fish[i][i] = 1.0e1;

        // fish[0][0] = 10.0;
        // fish[1][1] = 10.0;
        // fish[2][2] += 10.0;
        // fish[3][3] += 10.0;

        // Set smaller stabilizers for tidal parameters
        // if (NX > 7) {
        //     fish[7][7] = 0.01;
        //     fish[8][8] = 0.01;
        // }

        // Loop over the detectors
        for (id = 0; id < net->Nifo; id++)  
        {
            for (i = 0; i < NX; i++)
            {
                for (j = i; j < NX; j++)
                {
                    //if scale!=1, then we are using the heterodyned frequencies to compute Fisher, not the full frequency array
                    if (scale ==1)
                    {
                        for (k = imin ; k < imax ; k++) {
                            // fish[i][j] += 4.0*(AHP[i][id][k]*AHP[j][id][k]+AH[id][k]*AH[id][k]*PHP[i][id][k]*PHP[j][id][k])/(2.0*SN[id][k]);
                            fish[i][j] += 4.0*(Adiv[i][id][k]*Adiv[j][id][k]+AH[id][k]*AH[id][k]*Pdiv[i][id][k]*Pdiv[j][id][k])/(2.0*SN[id][k]);
                        }
                        
                    }
                    else
                    {
                        for (k = 1 ; k < N/2 ; k++) {
                            // fish[i][j] += scale*4.0*(AHP[i][id][k]*AHP[j][id][k]+AH[id][k]*AH[id][k]*PHP[i][id][k]*PHP[j][id][k])/(2.0*SN[id][k]);
                            fish[i][j] += scale*4.0*(Adiv[i][id][k]*Adiv[j][id][k]+AH[id][k]*AH[id][k]*Pdiv[i][id][k]*Pdiv[j][id][k])/(2.0*SN[id][k]);
                        }
                    }
                }
            }
        }
        
        // Set stabilizers for diagonal
        // if (NX>7) imax = 7;
        // else imax = NX;
        for (i = 0; i < NX; i++) if(fish[i][i] < 0.01) fish[i][i] += 0.01;
        while(fish[0][0] < 10.0) fish[0][0] += 10.0;
        while(fish[1][1] < 10.0) fish[1][1] += 10.0;
        while(fish[2][2] < 10.0) fish[2][2] += 10.0;
        while(fish[3][3] < 10.0) fish[3][3] += 10.0;

        // Fill in lower triangle
        for (i = 0; i < NX; i++)
        {
            for (j = i+1; j < NX; j++) fish[j][i] = fish[i][j];
        }

        // printf("\n");
        // for (i = 0 ; i < NX ; i++)
        // {
        //     for (j = 0 ; j < NX ; j++)
        //     {
        //         printf("fish_i[%i][%i]: %e ", i, j, fish[i][j]);
        //     }
        //     printf("\n");
        // } 


        free(paramsP);
        free(paramsM);
        free_double_vector(epsilon);
        free_double_vector(rhox);
        free_double_matrix(AH,net->Nifo);
        free_double_matrix(PH,net->Nifo);
        free_double_tensor(AHP,NP,net->Nifo);
        free_double_tensor(PHP,NP,net->Nifo);
        free_double_tensor(AHM,NP,net->Nifo);
        free_double_tensor(PHM,NP,net->Nifo);
        free_double_tensor(Adiv,NP,net->Nifo);
        free_double_tensor(Pdiv,NP,net->Nifo);
        free_double_tensor(AHPP,NP,net->Nifo);
        free_double_tensor(PHPP,NP,net->Nifo);
        free_double_tensor(AHMM,NP,net->Nifo);
        free_double_tensor(PHMM,NP,net->Nifo);   
    }
}

void upsample(int n, double Tobs, int *nt, int *bn)
{
    // TODO Comment: provides some kind of binning in the timedomain... not 100% sure
    double dt, dtres;
    int k;
    
    dtres = 1.0e-4;  // desired time resolution
    
    dt = Tobs/(double)(n);
    
    // figure out the upscaling, must be a power of 2
    k = (int)(pow(2.0,ceil(log(dt/dtres)/log(2.0)))); 
    
    // upsampled
    *bn = n*k;
    
    dt = Tobs/(double)(n*k);
    
    // allow for geocenter - surface light travel time
    // and for attempts to slide the waveform by the max delay time
    *nt = 2*(int)(dtmax/dt)+1;
    
}



// // Takes the psd and returns a smoothed version in `smooth_psd`. Adapted from
// // the spectrum(..) routine in BayesLine.
// void compute_smooth_psd_model(double Tobs, int N, double *psd, double *smooth_psd)
// {

//     double Df, Dfmax, x, y;
//     double Df1, Df2;
//     int mw, k, i, j;
//     int mm, kk;
//     int end1, end2, end3;
//     double med;
//     double *chunk;

//     double *Sn, *Smooth;

//     // log(2) is median/2 of chi-squared with 2 dof
    
//     double df = 1./Tobs;

//     Smooth = (double*)malloc(sizeof(double)*(N/2));
//     Sn = (double*)malloc(sizeof(double)*(N/2));
    
//     Dfmax = 32.0; // is the  width of smoothing window in Hz
    
//     // Smaller windows used initially where the spectrum is steep
//     Df2 = Dfmax/1.5;
//     Df1 = Dfmax; // /2.0;
    
//     // defines the ends of the segments where smaller windows are used
//     end1 = (int)(32.0/df);
//     end2 = 2*end1;
    
//     mw = (int)(Dfmax/df)+1;  // size of median window
//     //printf("numer of bins in smoothing window %d\n", mw);
//     k = (mw+1)/2;
//     chunk = double_vector(mw);
    
//     end3 = N/2-k;  // end of final chunk
    
//     // Fill the array so the ends are not empty - just to be safe
//     for(i=0;i< N/2;i++)
//     {
//         Sn[i] = psd[i];
//         Smooth[i] = psd[i];
//     }

//     // int imin = (int)16.0/df;

//     // mw = (int)(Df1/df)+1;  // size of median window
//     // k = (mw+1)/2;
    
//     for(i=4; i< end1; i++)
//     {
//         mm = i/2;
//         kk = (mm+1)/2;
        
//         for(j=0;j< mm;j++)
//         {
//             chunk[j] = psd[i-kk+j];
//         }
        
//         gsl_sort(chunk, 1, mm);
//         Sn[i] = gsl_stats_median_from_sorted_data(chunk, 1, mm);
    
//         Smooth[i] = Sn[i];
        
//     }
    
    
//     i = end1;
//     do
//     {
//         for(j=0;j< mw;j++)
//         {
//             chunk[j] = psd[i-k+j];
//         }
        
//         gsl_sort(chunk, 1, mw);
//         Sn[i] = gsl_stats_median_from_sorted_data(chunk, 1, mw);
        
//         Smooth[i] = Sn[i];
        
//         i++;
        
//     }while(i < N/2);
    
       
//     free_double_vector(chunk);
    
//     // zap the lines.
//     for(i=1;i< N/2;i++)
//     {
//         x = psd[i]/Sn[i];
//         if(x > 9.0)
//         {
//             Sn[i] = psd[i];
//         }

//         smooth_psd[i-1] = Smooth[i];
//     }

//     free_double_vector(Sn);
//     free_double_vector(Smooth);

// }


/*
* fmx: input data fmax
*/
void compute_smooth_psd_model_quickcbc(double Tobs, int N, double fmx, double *psd, double *smooth_psd, gsl_rng *r)
{
    double Df, Dfmax, x, y;
    double Df1, Df2;
    int mw, k, i, j;
    int mm, kk;
    int end1, end2, end3;
    double med;
    double *chunk;

    double *Sn, *Smooth;
    double f;
    double *SN, *SM, *SL, *PS, *S1, *S2;
    double min, max;
    int Ns;
    int flag;
    int sm, Nspline;
    int Nsp, Nl;

    // log(2) is median/2 of chi-squared with2 dof
    
    double df = 1./Tobs;
    double dfmin = 4.0;  // minimum spline spacing
    double dfmax = 32.0;  // maximum spline spacing
    double smooth = 8.0; // moving averige
    double tol = 0.2; // tolerance for difference in averages
    int MM = 10000;    // iterations of MCMC
    double pmul = 20.0;   // strength of prior on spline second derivatives

    int ND, Nm, m, mc, dec;
    int imin, imax, acS, acL, cS, cL;
    int typ;
    double *timeF, *dataF;
    double *times, *data, *Hf;
    double z, dt, fny, fmn;
    double finc;
    // double *SN, *SM, *SL, *PS, *S1, *S2;
    char command[1024];
    double spread, *deltafmax, *linew;
    double *linef, *lineh, *lineQ;
    double f2, f4, ff4, ff2, df2, df4;
    double ylinew, ylinef, ylineh, ylineQ, ydeltafmax;
    int ii, Nlines;
    double xold, xnext, Abar;
    double alpha;
    double tuke = 0.4;    // Tukey window rise (s)

    Ns = (int)(Tobs*fmx);
    
    S1 = (double*)malloc(sizeof(double)*(Ns));
    S2 = (double*)malloc(sizeof(double)*(Ns));
    SL = (double*)malloc(sizeof(double)*(Ns));
    SM = (double*)malloc(sizeof(double)*(Ns));
    SN = (double*)malloc(sizeof(double)*(Ns));
    PS = (double*)malloc(sizeof(double)*(Ns));

    // initial smooth psd
    // compute_smooth_psd_model(Tobs, N, psd, SN, SM);

    for (i = 0; i < Ns; ++i)
    {   
        // SN[i] = psd[i];
        PS[i] = psd[i];
        // printf("SM[%i] = %e, psd[%i] = %e, Sn = %e\n", i, SM[i], i, psd[i], SN[i]);
    }

    // moving average
    sm = (int)(smooth*Tobs);
    x = 0.0;
    for (i = 0; i < sm; ++i) x += SM[i];
    for (i = sm; i < Ns; ++i)
    {
        S1[i-sm/2] = x/(double)(sm);
        x += SM[i] - SM[i-sm];
    }

    // moving average with wider window
    sm *= 2;
    x = 0.0;
    for (i = 0; i < sm; ++i) x += SM[i];
    for (i = sm; i < Ns; ++i)
    {
        S2[i-sm/2] = x/(double)(sm);
        x += SM[i] - SM[i-sm];
    }
    
    // fill initial bins
    for (i = 0; i < sm/2; ++i)
    {
        S1[i] = SM[i];
        S2[i] = 2.0*S1[i];
    }

   // count the number of spline knots
    Nspline = 1;
    k = (int)(dfmin*Tobs);
    kk = (int)(dfmax*Tobs);
    j = 0;
    flag = 0;
    max = 0.0;
    for (i = 1; i < Ns; ++i)
    {
        x = fabs(S2[i]/S1[i]-1.0);
        if(x > max) max = x;
        j++;
         if(i%k == 0)
          {
              if(max > tol || j == kk)
              {
                 // printf("%f %f %f\n", (double)(i-j/2)/Tobs, (double)(j)/Tobs, max);
                  max = 0.0;
                  j = 0;
                  Nspline++;
              }
          }
    }
    
     Nspline++;
    
     // printf("a: There are %d spline knots\n", Nspline);

     double *ffit, *Xspline, *Yspline;
     
     ffit = (double*)malloc(sizeof(double)*(Nspline));
     Xspline = (double*)malloc(sizeof(double)*(Nspline));
     Yspline = (double*)malloc(sizeof(double)*(Nspline));
    
    Nspline = 1;
    j = 0;
    flag = 0;
    max = 0.0;
    for (i = 1; i < Ns; ++i)
    {
        x = fabs(S2[i]/S1[i]-1.0);
        if(x > max) max = x;
        j++;
         if(i%k == 0)
          {
              if(max > tol || j == kk)
              {
                  max = 0.0;
                  ffit[Nspline] = (double)(i-j/2)/Tobs;
                  Xspline[Nspline] = log(S1[i-j/2]);
                  j = 0;
                  Nspline++;
              }
          }
        
        
    }
    
    Nspline++;
    
    ffit[0] = 0.0;
    Xspline[0] = log(SM[0]);
    ffit[Nspline-1] = (double)(Ns-1)/Tobs;
    Xspline[Nspline-1] = log(SM[Ns-1]);
    
    // printf("b: There are %d spline knots\n", Nspline);

     for (i = 0; i < Nspline; ++i)
        {
            Yspline[i] = Xspline[i];
        }
    
    // Allocate spline
    gsl_spline   *cspline = gsl_spline_alloc(gsl_interp_cspline, Nspline);
    gsl_interp_accel *acc    = gsl_interp_accel_alloc();
    
    /* compute spline */
    gsl_spline_init(cspline,ffit,Xspline,Nspline);




     // count the number of lines
     j = 0;
     flag = 0;
     for (i = 0; i < Ns; ++i)
     {
         x = PS[i]/SM[i];
         // start of a line
         if(x > 9.0 && flag == 0)
         {
             k = 1;
             flag = 1;
             max = x;
             ii = i;
         }
         // in a line
         if(x > 9.0  && flag ==1)
         {
             k++;
             if(x > max)
             {
                 max = x;
                 ii = i;
             }
         }
         // have reached the end of a line
         if(flag == 1)
         {
             if(x < 9.0)
             {
                 flag = 0;
                 j++;
             }
         }
     }
    
     
     Nlines = j;

       linef = (double*)malloc(sizeof(double)*(Nlines));  // central frequency
       lineh = (double*)malloc(sizeof(double)*(Nlines));  // line height
       lineQ = (double*)malloc(sizeof(double)*(Nlines)); // line Q
       linew = (double*)malloc(sizeof(double)*(Nlines));  // line width
       deltafmax = (double*)malloc(sizeof(double)*(Nlines));  // cut-off
    
    j = -1;
    xold = 1.0;
    flag = 0;
    for (i = 0; i < Ns; ++i)
    {
        x = PS[i]/SM[i];
        // start of a line
        if(x > 9.0 && flag == 0)
        {
            k = 1;
            flag = 1;
            max = x;
            ii = i;
        }
        // in a line
        if((x > 9.0) && flag ==1)
        {
            k++;
            if(x > max)
            {
                max = x;
                ii = i;
            }
        }
        // have reached the end of a line
        if(flag == 1)
        {
            if(x < 9.0)
            {
                flag = 0;
                j++;
                linef[j] = (double)(ii)/Tobs;
                lineh[j] = (max-1.0)*SM[ii];
                Abar = 0.5*(SN[ii+1]/SM[ii+1]+SN[ii-1]/SM[ii-1]);
                //lineQ[j] = sqrt((max/Abar-1.0))*linef[j]*Tobs;
                 lineQ[j] = sqrt(max)*linef[j]*Tobs/(double)(k);
                
                  spread = (1.0e-2*lineQ[j]);
                  if(spread < 50.0) spread = 50.0;  // maximum half-width is f_resonance/50
                  deltafmax[j] = linef[j]/spread;
                  linew[j] = 8.0*deltafmax[j];
                
                //printf("%d %e %e %e %e %e\n", j, linef[j], lineh[j], linew, (double)(k)/Tobs, lineQ[j]);
               
            }
        }

    }
    


    // initialize smooth spline spectrum
    SM[0] = exp(Xspline[0]);
    SM[Ns-1] = exp(Xspline[Nspline-1]);
    for (i = 1; i < Ns-1; ++i)
    {
        f = (double)(i)/Tobs;
        SM[i] = exp(gsl_spline_eval(cspline,f,acc));
        // printf("after SM[%i] %f = %e, %e\n", i, f, SM[i], gsl_spline_eval(cspline,f,acc));
        // printf("after SM[%i] %f = %e\n", i, f, SM[i]);
    }

    FILE *out;

    out = fopen("control.dat","w");
       for (i = 0; i < Nspline; ++i)
       {
           fprintf(out,"%e %e\n", ffit[i], exp(Xspline[i]));
       }
       fclose(out);
     
    // initialize line spectrum
    //out = fopen("lines.dat","w");
    for (i = 0; i < Ns; ++i)
    {
        f = (double)(i)/Tobs;
        y = 0.0;
        for (j = 0; j < Nlines; ++j)
        {
           
            x = fabs(f - linef[j]);
            
            if(x < linew[j])
            {
                z = 1.0;
                if(x > deltafmax[j]) z = exp(-(x-deltafmax[j])/deltafmax[j]);
                f2 = linef[j]*linef[j];
                ff2 = f*f;
                df2 = lineQ[j]*(1.0-ff2/f2);
                df4 = df2*df2;
                y += z*lineh[j]/(ff2/f2+df4);
               // printf("%d %d %e %e %e\n", i, j, f2, ff2, df4);
            }
        }
        SL[i] = y;
        //fprintf(out,"%e %e %e\n", f, SM[i] + SL[i], SL[i]);
        
    }

    // MCMC

    // changing a control point value impacts the spline in two segments
    // either side of the point changed.
    
    double *freqs, *lnLR, *lnpR;
    
    freqs = (double*)malloc(sizeof(double)*(Ns));
    lnLR = (double*)malloc(sizeof(double)*(Ns));
    lnpR = (double*)malloc(sizeof(double)*(Ns));
    
    /*
       out = fopen("specstart.dat","w");
       for (i = 0; i < Ns; ++i)
       {
           f = (double)(i)/Tobs;
           fprintf(out,"%e %e %e %e\n", f, SM[i], SL[i], SM[i]+SL[i]);
       }
       fclose(out);
    */
   
    for (i = 0; i < Ns; ++i)
     {
         freqs[i] = (double)(i)/Tobs;
         x = SM[i] + SL[i];
         lnLR[i] = -(log(x) + PS[i]/x);
         lnpR[i] = 0.0;
     }
    
     for (i = 1; i < Ns-1; ++i)
        {
          lnpR[i] = -fabs(gsl_spline_eval_deriv2(cspline, freqs[i], acc));
        }
    
     double *DlnLR, *DlnpR, *DS;
     
    // This holds the updated values in the region impacted by the change in the
    // spline point. Allocaing these to the size of the full spectrum
     DlnLR = (double*)malloc(sizeof(double)*(Ns));
     DlnpR = (double*)malloc(sizeof(double)*(Ns));
     DS = (double*)malloc(sizeof(double)*(Ns));
    
    double logLx, logLy, logpx, logpy;
    double H;
    
    logLx = 0.0;
    for (i = 0; i < Ns; ++i) logLx += lnLR[i];

    logpx = 0.0;
    for (i = 1; i < Ns-1; ++i) logpx += lnpR[i];
    
    acS = 0;
    acL = 0;
    cS = 1;
    cL = 1;
    
   // out = fopen("schain.dat","w");
    
     for (mc = 0; mc < MM; ++mc)
     {
         
         // prune any weak lines
         if(mc == MM/2)
         {
             j = 0;
             for (k = 0; k < Nlines; ++k)
             {
                 i = (int)(linef[k]*Tobs);  // bin where line peaks
                 x = lineh[k]/SM[i];     // height of line relative to smooth
                 if(x > 5.0)  // keep this line
                 {
                     linef[j] = linef[k];
                     lineh[j] = lineh[k];
                     lineQ[j] = lineQ[k];
                     linew[j] = linew[k];
                     deltafmax[j] = deltafmax[k];
                     j++;
                 }
             }
             
             Nlines = j;
             
             // reset line spectrum
             for (i = 0; i < Ns; ++i)
             {
                 f = (double)(i)/Tobs;
                 y = 0.0;
                 for (j = 0; j < Nlines; ++j)
                 {
                    
                     x = fabs(f - linef[j]);
                     
                     if(x < linew[j])
                     {
                         z = 1.0;
                         if(x > deltafmax[j]) z = exp(-(x-deltafmax[j])/deltafmax[j]);
                         f2 = linef[j]*linef[j];
                         ff2 = f*f;
                         df2 = lineQ[j]*(1.0-ff2/f2);
                         df4 = df2*df2;
                         y += z*lineh[j]/(ff2/f2+df4);
                     }
                 }
                 SL[i] = y;
             }
             
             // reset likelihood
             for (i = 0; i < Ns; ++i)
             {
                 freqs[i] = (double)(i)/Tobs;
                 x = SM[i] + SL[i];
                 lnLR[i] = -(log(x) + PS[i]/x);
             }
          }
         
         
         alpha = gsl_rng_uniform(r);
         
         if(alpha < 0.6) // update spline
         {
             
          typ = 0;
          cS++;
             
        // pick a knot to update
        k = (int)((double)(Nspline)*gsl_rng_uniform(r));
        Yspline[k] =  Xspline[k] + gsl_ran_gaussian(r,0.05);
        gsl_spline_init(cspline,ffit,Yspline,Nspline);
         
         if(k > 1)
         {
             imin = (int)(ffit[k-2]*Tobs);
         }
         else
         {
             imin = (int)(ffit[0]*Tobs);
         }
         
         if(k < Nspline-2)
         {
             imax = (int)(ffit[k+2]*Tobs);
         }
         else
         {
             imax = (int)(ffit[Nspline-1]*Tobs);
         }
             
            // Delta the smooth spectrum and prior
             logpy = logpx;
             logLy = logLx;
            for (i = imin; i < imax; ++i)
               {
                          y = 0.0;
                           if(i > 0 && i < Ns-1)
                           {
                            x = gsl_spline_eval(cspline,freqs[i],acc);
                            y = -fabs(gsl_spline_eval_deriv2(cspline, freqs[i], acc));
                           }
                           else
                           {
                               if(i==0) x = Yspline[0];
                               if(i==Ns-1) x = Yspline[Nspline-1];
                           }
                           DS[i-imin] = exp(x);
                           DlnpR[i-imin] = y;
                           x = DS[i-imin] + SL[i];
                           DlnLR[i-imin] = -(log(x) + PS[i]/x);
                           logLy += (DlnLR[i-imin] - lnLR[i]);
                           logpy += (DlnpR[i-imin] - lnpR[i]);
               }
         
         
             
         }
         else  // line update
         {
             
            typ = 1;
            cL++;
             
            // pick a line to update
            k = (int)((double)(Nlines)*gsl_rng_uniform(r));
             
            ylinef = linef[k] + gsl_ran_gaussian(r,0.1);
            ylineh = lineh[k]*(1.0+gsl_ran_gaussian(r,0.05));
            ylineQ = lineQ[k] + gsl_ran_gaussian(r,1.0);
            if(ylineQ < 10.0) ylineQ = 10.0;
           
             spread = (1.0e-2*ylineQ);
             if(spread < 50.0) spread = 50.0;  // maximum half-width is f_resonance/20
             ydeltafmax = ylinef/spread;
             ylinew = 8.0*ydeltafmax;
             
             
            // need to cover old and new line
             imin = (int)((linef[k]-linew[k])*Tobs);
             imax = (int)((linef[k]+linew[k])*Tobs);
             i = (int)((ylinef-ylinew)*Tobs);
             if(i < imin) imin = i;
             i = (int)((ylinef+ylinew)*Tobs);
             if(i > imax) imax = i;
             if(imin < 0) imin = 0;
             if(imax > Ns-1) imax = Ns-1;
             
            // new line contribution
            f2 = ylinef*ylinef;
            f4 = f2*f2;
            for (i = imin; i <= imax; ++i)
               {
                    f = (double)(i)/Tobs;
                    y = 0.0;
                    x = fabs(f - ylinef);
                    if(x < ylinew)
                    {
                    z = 1.0;
                    if(x > ydeltafmax) z = exp(-(x-ydeltafmax)/ydeltafmax);
                    ff2 = f*f;
                    df2 = ylineQ*(1.0-ff2/f2);
                    df4 = df2*df2;
                    y += z*ylineh/(ff2/f2+df4);
                    }
                    DS[i-imin] = y;
               }
            
             
            // have to recompute and remove current line since lines can overlap
            f2 = linef[k]*linef[k];
            for (i = imin; i <= imax; ++i)
               {
                    f = (double)(i)/Tobs;
                    y = 0.0;
                    x = fabs(f - linef[k]);
                    if(x < linew[k])
                    {
                    z = 1.0;
                    if(x > deltafmax[k]) z = exp(-(x-deltafmax[k])/deltafmax[k]);
                    ff2 = f*f;
                    df2 = lineQ[k]*(1.0-ff2/f2);
                    df4 = df2*df2;
                    y += z*lineh[k]/(ff2/f2+df4);
                    }
                    DS[i-imin] -= y;
               }
            
             logpy = logpx;
             logLy = logLx;
             for (i = imin; i <= imax; ++i)
             {
                 x = DS[i-imin] + SM[i] + SL[i];
                 DlnLR[i-imin] = -(log(x) + PS[i]/x);
                 logLy += (DlnLR[i-imin] - lnLR[i]);
             }

             
             
         }
         
         

         
         
         H = (logLy-logLx) + pmul*(logpy - logpx);
         alpha = log(gsl_rng_uniform(r));
         
         if(H > alpha)
         {
             logLx = logLy;
             logpx = logpy;
             
             if(typ == 0)
             {
             acS++;
             Xspline[k] = Yspline[k];
             for (i = imin; i < imax; ++i)
                {
                    SM[i] = DS[i-imin];  // replace this segment
                    lnLR[i] = DlnLR[i-imin];
                    lnpR[i] = DlnpR[i-imin];
                }
             }
             
             if(typ == 1)
             {
             acL++;
               
                linef[k] = ylinef;
                lineh[k] = ylineh;
                lineQ[k] = ylineQ;
                linew[k] = ylinew;
                deltafmax[k] = ydeltafmax;
                
               for (i = imin; i <= imax; ++i)
                {
                    SL[i] += DS[i-imin];   // delta this segment
                    lnLR[i] = DlnLR[i-imin];
                }
                 
             }
               
           }
          else
          {
           // have to re-set if not accepted
           if(typ == 0) Yspline[k] = Xspline[k];
          }
         
        // if(mc%100 == 0) printf("%d %e %e %f %f\n", mc, logLx, logpx, (double)acS/(double)(cS), (double)acL/(double)(cL));
         
        // if(mc%10 == 0) fprintf(out, "%d %e %e %f %f\n", mc, logLx, logpx, (double)acS/(double)(cS), (double)acL/(double)(cL));
    
     }
    //fclose(out);
    
    // printf("There are %d lines\n", Nlines);
    
    // Save the smoothed spline
    for(i=0;i< N/2;i++)
    {
        smooth_psd[i] = SM[i];
    }

    free_double_vector(Sn);
    free_double_vector(Smooth);
}

// void compute_smooth_psd_model(double Tobs, int N, double *psd, double *Sn, double *smooth_psd)
void compute_smooth_psd_model(double Tobs, int N, double *psd, double *smooth_psd)
{

    double Df, Dfmax, x, y;
    double Df1, Df2;
    int mw, k, i, j;
    int mm, kk;
    int end1, end2, end3;
    double med;
    double *chunk;

    double *Smooth, *S, *Sn;

    // log(2) is median/2 of chi-squared with 2 dof
    
    double df = 1./Tobs;

    // This way of allocating was causing segfaults since it was being freed with free_double_vector
    //Smooth = (double*)malloc(sizeof(double)*(N/2));
    //Sn = (double*)malloc(sizeof(double)*(N/2));
    //S = (double*)malloc(sizeof(double)*(N/2));

    Smooth = double_vector(N/2);
    Sn     = double_vector(N/2);
    S      = double_vector(N/2);
    
    // Dfmax = 16.0; // is the  width of smoothing window in Hz
    Dfmax = 64.0; // is the  width of smoothing window in Hz

    // Smaller windows used initially where the spectrum is steep
    Df2 = Dfmax/4.0;
    Df1 = Dfmax/8.0;
    
    // defines the ends of the segments where smaller windows are used
    end1 = (int)(32.0/df);
    end2 = 2*end1;
    
    mw = (int)(Dfmax/df)+1;  // size of median window
    //printf("numer of bins in smoothing window %d\n", mw);
    k = (mw+1)/2;
    chunk = double_vector(mw);
    
    end3 = N/2-k;  // end of final chunk
    
    // Fill the array so the ends are not empty - just to be safe
    for(i=0;i< N/2+1;i++)
    {
        Sn[i] = psd[i];
        Smooth[i] = psd[i];
        S[i] = psd[i];
    }

    // Sn[0] = psd[0];
    // Smooth[0] = psd[0];
    // S[0] = psd[0];

    mw = (int)(Df1/df)+1;  // size of median window
    k = (mw+1)/2;
    
    for(i=4; i< k; i++)
    {
        mm = i/2;
        kk = (mm+1)/2;
        
        for(j=0;j< mm;j++)
        {
            chunk[j] = S[i-kk+j];
        }
        
        gsl_sort(chunk, 1, mm);
        Sn[i] = gsl_stats_median_from_sorted_data(chunk, 1, mm);
    
        Smooth[i] = Sn[i];
        
    }
    
    
    i = k;
    do
    {
        for(j=0;j< mw;j++)
        {
            chunk[j] = S[i-k+j];
        }
        
        gsl_sort(chunk, 1, mw);
        Sn[i] = gsl_stats_median_from_sorted_data(chunk, 1, mw);
        
        Smooth[i] = Sn[i];
        
        i++;
        
    }while(i < end1);
    
    
    
    mw = (int)(Df2/df)+1;  // size of median window
    k = (mw+1)/2;
    
    
    do
    {
        for(j=0;j< mw;j++)
        {
            chunk[j] = S[i-k+j];
        }
        
        gsl_sort(chunk, 1, mw);
        Sn[i] = gsl_stats_median_from_sorted_data(chunk, 1, mw);
        
        Smooth[i] = Sn[i];
        
        i++;
        
    }while(i < end2);
    
    mw = (int)(Dfmax/df)+1;  // size of median window
    k = (mw+1)/2;
    
    do
    {
        for(j=0;j< mw;j++)
        {
            chunk[j] = S[i-k+j];
        }
        
        gsl_sort(chunk, 1, mw);
        Sn[i] = gsl_stats_median_from_sorted_data(chunk, 1, mw);
    
        Smooth[i] = Sn[i];
        
        i++;
        
    }while(i < end3);
    
    
    for(i=end3; i< N/2-4; i++)
    {
        mm = (N/2-i)/2;
        kk = (mm+1)/2;
        
        for(j=0;j< mm;j++)
        {
            chunk[j] = S[i-kk+j];
        }
        
        gsl_sort(chunk, 1, mw);
        Sn[i] = gsl_stats_median_from_sorted_data(chunk, 1, mm);
        
        Smooth[i] = Sn[i];
        
    }
    
    free_double_vector(chunk);
    
    // zap the lines.
    for(i=0;i< N/2;i++)
    {
        x = S[i]/Sn[i];
        if(x > 1.0001) smooth_psd[i] = Smooth[i];
        else smooth_psd[i] = psd[i];
        // smooth_psd[i] = Smooth[i];
    }   

    free_double_vector(S);
    free_double_vector(Sn);
    free_double_vector(Smooth);

}


double skydensity(struct Net *net, double *paramsx, double *paramsy, int ifo1, int ifo2, int iref, int NS, double gmst)
{
    int i, ii, j;
    double *paramsxp, *paramsyp;
    double *Jmat;
    double x, Jack;
    double ep;
    
    ep = 1.0e-6;
    
    paramsxp = double_vector(NS);
    paramsyp = double_vector(NS);
    Jmat = double_vector(16);
    
    // Geocenter arrival time is also adjusted. Probably should be included in the Jacobian, currently isn't
    
    // At this stage all we have for the new location y is the sky location. So first we find what the rest
    // of the parameters (amplitude, polarization, ciota, phase) need to be to keep the signal the same
    
    skymap(net, paramsx, paramsy, ifo1, ifo2, iref, gmst);
    
    // The sky locations are chosen to be on the equal time delay sky ring. Presumably the density factors for that
    // mapping have been taken care of. What we need to know here is how the volume of a small volume
    // dAx /\ dpsix /\ dphix /\ dciotax onto the image volume dAy /\ dpsiy /\ dphiy /\ dciotay. This is
    // given by the Jacobian.
    
    // compute the Jacobian
    for(i=0; i< 4; i++)  // step through the parameters at x
    {
        
        for(j=0; j<= 5; j++) paramsxp[j] = paramsx[j];
        for(j=0; j<= 1; j++) paramsyp[j] = paramsy[j];
        paramsxp[i+2] += ep;
        
        skymap(net, paramsxp, paramsyp, ifo1, ifo2, iref, gmst);
        
        // can get thrown to the alternative solution of phi, psi. Map these back
        while( fabs(paramsyp[2]+PI/2.0-paramsy[2]) < fabs(paramsyp[2]-paramsy[2]) ) paramsyp[2] += PI/2.0;
        while( fabs(paramsyp[2]-PI/2.0-paramsy[2]) < fabs(paramsyp[2]-paramsy[2]) ) paramsyp[2] -= PI/2.0;
        while( fabs(paramsyp[5]+PI-paramsy[5]) < fabs(paramsyp[5]-paramsy[5]) ) paramsyp[5] += PI;
        while( fabs(paramsyp[5]-PI-paramsy[5]) < fabs(paramsyp[5]-paramsy[5]) ) paramsyp[5] -= PI;
        
        for(j=0; j< 4; j++) Jmat[i*4+j] = (paramsyp[j+2]-paramsy[j+2])/ep;
        
    }
    
    
    Jack = fabs(det(Jmat, 4));
    
    // printf("%f %f %f\n", Jmat[2*4+2], paramsx[4], paramsy[4]);
    
     /*      printf("\n");
     for(i=0; i< 4; i++)
     {
     for(j=0; j< 4; j++)
     {
     printf("%f ", Jmat[i*4+j]);
     }
     printf("\n");
     }
     printf("\n");
    
     */
    
    free_double_vector(paramsyp);
    free_double_vector(paramsxp);
    free_double_vector(Jmat);
    
    return Jack;
    
    
}

double det(double *A, int N)
{
    int *IPIV;
    int LWORK = N*N;
    int INFO;
    int i, j;
    double dx, dy;
    int s;
    
    gsl_permutation *p = gsl_permutation_alloc(N);
    
    gsl_matrix *m = gsl_matrix_alloc (N, N);
    
    for (i = 0 ; i < N ; i++)
    {
        for (j = 0 ; j < N ; j++)
        {
            gsl_matrix_set(m, i, j, A[j*N+i]);
        }
    }
    
    // Compute the LU decomposition of this matrix
    gsl_linalg_LU_decomp(m, p, &s);
    
    dx = 1.0;
    for (i = 0; i < N; i++) dx *= gsl_matrix_get (m, i, i);
    dx = fabs(dx);
    
    //returns the absolute value of the determinant.
    
    gsl_permutation_free(p);
    gsl_matrix_free(m);
    
    
    return dx;    
}


double f_nwip(double *a, double *b, int n)
{
    int i, j, k;
    double arg, product;
    double test;
    double ReA, ReB, ImA, ImB;
    
    arg = 0.0;
    for(i=1; i<n/2; i++)
    {
        j = i;
        k = n-j;
        ReA = a[j]; ImA = a[k];
        ReB = b[j]; ImB = b[k];
        product = ReA*ReB + ImA*ImB;
        arg += product;
    }
    
    return(arg);
    
}

double fb_nwip(double *a, double *b, int n, int imin, int imax)
{
    int i, j, k;
    double arg, product;
    double test;
    double ReA, ReB, ImA, ImB;
    
    arg = 0.0;
    for(i=1; i<n/2; i++)
    {
        if(i > imin && i < imax)
        {
            j = i * 2;
            k = j + 1;
            ReA = a[j]; ImA = a[k];
            ReB = b[j]; ImB = b[k];
            product = ReA*ReB + ImA*ImB;
            arg += product;
        }
    }
    
    return(arg);
    
}
double z_DL(double DL)
{
    double zlow, zhigh;
    double zmid;
    double Dlow, Dhigh, Dmid;
    double u, v, w;
    int i;
    
    zlow = 1.0;
    zhigh = 1000.0;
    
    if(DL < 1.0)
    {
        zlow = 1.0e-10;
        zhigh = 3.0e-4;
    }
    else if (DL < 100.0)
    {
        zlow = 2.0e-4;
        zhigh = 5.0e-2;
    }
    else if (DL < 1000.0)
    {
        zlow = 1.0e-2;
        zhigh = 3.0e-1;
    }
    else if (DL < 10000.0)
    {
        zlow = 1.0e-1;
        zhigh = 2.0;
    }
    
    zmid = (zhigh+zlow)/2.0;
    
    Dlow = DL_fit(zlow);
    Dhigh = DL_fit(zhigh);
    Dmid = DL_fit(zmid);
    
    for (i=0; i< 30; i++)
    {
        
        u = Dlow-DL;
        v = Dmid-DL;
        
        if(u*v < 0.0)
        {
            zhigh = zmid;
            Dhigh = Dmid;
        }
        else
        {
            zlow = zmid;
            Dlow = Dmid;
        }
        zmid = (zhigh+zlow)/2.0;
        Dmid = DL_fit(zmid);
        
        
    }
    
    //  printf("%e %e\n", Dmid-DL, zmid);
    
    return(zmid);
    
    
}


double DL_fit(double z)
{
    double D;
    // Planck values https://arxiv.org/abs/1807.06209
    double Om = 0.315;
    double H0 = 67.4;
    double RH, x1, x2, Om1, Om2;
    
    // See http://arxiv.org/pdf/1111.6396v1.pdf
    
    RH = CLIGHT/H0*(1.0e-3*MPC);
    
    x1 = (1.0-Om)/(Om);
    
    x2 = (1.0-Om)/(Om*pow((1.0+z),3.0));
    
    Om1 = (1.0+1.32*x1+0.4415*x1*x1+0.02656*x1*x1*x1)/(1.0+1.392*x1+0.5121*x1*x1+0.03944*x1*x1*x1);
    
    Om2 = (1.0+1.32*x2+0.4415*x2*x2+0.02656*x2*x2*x2)/(1.0+1.392*x2+0.5121*x2*x2+0.03944*x2*x2*x2);
    
    D = 2.0*RH*(1.0+z)/sqrt(Om)*(Om1-Om2/sqrt(1.0+z));
    
    return(D/MPC);
}


// MW: Adapted from LALInference (LALInferenceLambdaTsEta2Lambdas)
void LambdaTsEta2Lambdas(double lambdaT, double dLambdaT, double eta, double *lambda1, double *lambda2){
  double a=(8./13.)*(1.+7.*eta-31.*eta*eta);
  double b=(8./13.)*sqrt(1.-4.*eta)*(1.+9.*eta-11.*eta*eta);
  double c=(1./2.)*sqrt(1.-4.*eta)*(1.-13272.*eta/1319.+8944.*eta*eta/1319.);
  double d=(1./2.)*(1.-15910.*eta/1319.+32850.*eta*eta/1319.+3380.*eta*eta*eta/1319.);
  *lambda1=((c-d)*lambdaT-(a-b)*dLambdaT)/(2.*(b*c-a*d));
  *lambda2=((c+d)*lambdaT-(a+b)*dLambdaT)/(2.*(a*d-b*c));

  // printf(" DEBUG: in  LambdaTsEta2Lambdas. lambdaT %f & dLambdaT %f\n", lambdaT, dLambdaT);
  // printf(" DEBUG: in  LambdaTsEta2Lambdas. Lambda1 %f & lambda2 %f\n", *lambda1, *lambda2);
  
  return;
}

void GetXMLInjectionParameter(char *name, double *value, char **injection_columns, char **injection_values) {
    int i, j;
    char *lala;
    /*
    Returns values of pallx parameter list from injected XML parameter lists.
    */

    // TODO: Remove hardcoded nr. of columns
    for (i=0; i<58; i++) {
        if (strstr(injection_columns[i], name) != NULL) {
            *value = atof(injection_values[i]);
            break;
        }
    }
}

void ParseInjectionXMLParameters(char *file_path, char **injection_columns, char **injection_values) {
    int i, j, k, m, ii, jj, flag;
    char Dname[1024];
    char stream[4096];
    char substream[1024];
    char *ptr, *ptr2;
    FILE *in;
    size_t len;
    char *result;

    /* 
    Simple XML injection file parser. Currently it parses the first event only and updates two
    character arrays: one for column names and one for the values. The values that are
    actually used should be cast into their proper type, as that is not included here.
    This could be easily extended by also parsing the Type tag of the columns and including
    the type casting here. However, as currently BayesCBC and Bayeswave use different datatypes
    and this needs some more care, I have not included this here.
    */

    // Read in injection file parameters if fixing initial parameters at injection 

    in = fopen(file_path, "r");

    // printf(" starting in ParseInjectionXMLParameters! \n");
    flag = 0;
    j=0; // Will hold the number of columns -1
    m=0; // 
    while(!feof(in) && flag<=2) {

        fscanf(in, "%500[^\n]\n", stream);

        // If flag == 1, we start reading the table column
        // names into char array. 
        if (flag == 1) {
            if (strstr(stream, "Column") != NULL) {
                
                // Column names
                ptr2 = strstr(stream, "Name=");
                len = 5;
                ptr2 += len;
                ptr =strstr(stream, "/>");

                len = ptr - ptr2; 
                result = malloc(1+len);
                result[len] = '\0';

                memcpy(result, ptr2, len);
                injection_columns[j] = result;
                j++;
            }
        }

        // Next we parse the table values (only the first row)
        // It's assumed that the row delimiter is ',', this could
        // be made more general by reading the delimiter attribute
        // of the table
        if (flag == 2) {

            ptr=strtok(stream, ","); 
            ii=0;
            while (ptr != NULL) {
                injection_values[ii]=ptr;
                ptr=strtok(NULL, ",");
                ii++;
                memset(substream, 0, 1024);
            }

            // Update flag to finish
            flag=3;
        }

        // Check if start tag of inspiral table sim_inspiral:table
        if (strstr(stream, "sim_inspiral:table") != NULL) {
            flag += 1;
        }
    }
    fclose(in);
}

void fixedheterodyne(struct Net *net, struct Het *het, double *params, RealVector *freq, double **SN, double **SM, int N, double Tobs, struct bayesCBC *rundata, int ic)
{   
    int i, j, k, n, id;
    int icbc;
    double f, fstop;
    int M, MM, ds, ell;

    double **ampb, **phaseb, **hb_tmp;

    ell = 2;  // Legendre order (max is ell-1). Currently only set up for ell=2
    het->ell = ell;

    // Setup fixed part of the heterodyne for all chains
    icbc = ic;  // Chain index

    f = fringdown(params);  // ringdown frequency

    fstop = 2*f; // twice the ringdown frequency
    if(fstop < 64.0) fstop = 64.0; // just to be safe

    // printf("fringdown:  %f, fstop: %f\n", f, fstop);

    // With the piecewise Legendre polynomial approach it is not necessary to use powers of two or equal sized frequency bins.
        
    // fres is now the spacing between samples. The integration region is ell*fres wide.

    MM = (int)(fstop*Tobs); // Covers the signal range
    if(MM > N/2) MM = N/2;  // can't exceed Nyqusit
    ds = (int)(Tobs*rundata->fres);  // The downsample amount. Set by the frequency resolution fres. Default fres = 4 Hz.
    M = MM/ds +1 ;  // number of points used in the heterodyne

    if(M*ds > N/2) M -= 1;

    het->M = M;
    het->MM = MM;
    het->icbc = icbc;
    het->ds = ds;

    // Memory allocation for elements that are only computed 
    // at the start and don't change when the data changes
    het->freq = CreateRealVector(M);
    het->S0 = double_matrix(net->Nifo,M);
    het->S1 = double_matrix(net->Nifo,M);
    het->AS = double_matrix(net->Nifo,M);
    het->SN = double_matrix(net->Nifo,M);
    het->amp = double_matrix(net->Nifo,M);
    het->phase = double_matrix(net->Nifo,M);

    het->hb = double_matrix(net->Nifo,N);
    het->cp = double_matrix(net->Nifo,MM);
    het->sp = double_matrix(net->Nifo,MM);
    het->x = double_matrix(net->Nifo,MM);


    // Allocate heterodyne
    het->rc = double_matrix(net->Nifo,M);
    het->rs = double_matrix(net->Nifo,M);
    het->lc = double_tensor(net->Nifo,ell,M);
    het->ls = double_tensor(net->Nifo,ell,M);

    // Local structures
    hb_tmp = double_matrix(net->Nifo,N);
    ampb = double_matrix(net->Nifo,N);
    phaseb = double_matrix(net->Nifo,N);

    // full frequency template, amplitude and phase
    fulltemplates(net, hb_tmp, freq, params, N, rundata);
    fullphaseamp(net, ampb, phaseb, freq, params, N, rundata, 1);

    for (id=0; id< net->Nifo; id++)
    { 
        het->hb[id][0] = 0.0;
        het->hb[id][N/2] = 0.0;
        
        for (i=1; i< N/2; i++)
        {
            het->hb[id][i] = hb_tmp[id][2*i];
            het->hb[id][N-i] = hb_tmp[id][2*i+1];
        }
    }

    // store the downsampled frequency array
    for(j = 0; j < M; j++)
    {
        n = j*ds;
        het->freq->data[j] = freq->data[n];
    }

    // store the downsampled smooth ASD and full PSD
    // store the downsampled reference amplitude and phase
    for(id = 0; id < net->Nifo; id++)
    {
        for(j = 0; j < M; j++)
        {
            n = j*ds;
            het->AS[id][j] = sqrt(2.0*SM[id][n]);
            het->SN[id][j] = SN[id][n];
            het->amp[id][j] = ampb[id][n];
            het->phase[id][j] = phaseb[id][n];
        }
    }
    
    // Legendre polynomial expansion of 1/SN across each frequency band.
    // Zeroth order, S0, is pretty much good enough
    for(id = 0; id < net->Nifo; id++)
    {
      for(j = 0; j < M; j++)
        {
           het->S0[id][j] = 0.0;
           het->S1[id][j] = 0.0;
           for(n = 0; n <= ds; n++)
            {
                het->S0[id][j] += 1.0/(2.0*SN[id][n+j*ds]);
                het->S1[id][j] += (1.0-(double)(2*n)/(double)(ds))/(2.0*SN[id][n+j*ds]);
            }
        }
    }

    // Compute these once at the start because math functions
    // are time expensive
    for(id = 0; id < net->Nifo; id++)
    {
      for(j = 1; j < MM; j++)
      {
        het->cp[id][j] = cos(phaseb[id][j]);
        het->sp[id][j] = sin(phaseb[id][j]);
        het->x[id][j] = sqrt(2.0*SM[id][j])/(2.0*SN[id][j]);
      }
    }

    het->initFlag = 1; 
    
    free_double_matrix(ampb,net->Nifo);
    free_double_matrix(phaseb,net->Nifo);
    free_double_matrix(hb_tmp, net->Nifo);

}

double fullheterodyne_likelihood(struct Net *net, struct Het *het, double **D, double *cbcparams, double *extparams, double Tobs, struct bayesCBC *rundata, int N)
{

    double logL;
    double *params;
    int i, id, j, n;
    double f, fstop, x, y, z;
    double **hb, **rb, **ampb, **phaseb, **hb_tmp;
    double cp, sp;
    double **RC, **RS;
    double HH, HR;

    int M = het->M;
    int MM = het->MM;
    int ds = het->ds;

    // Setup CBC intrinsic parameters (fixed) and extrinsic parameters
    params = double_vector(rundata->NP);
    for (i=0; i<rundata->NX; i++) params[i] = cbcparams[i];
    for (i=rundata->NX; i<rundata->NP; i++) params[i] = extparams[i-rundata->NX];  

    // Allocate local structure
    rb = double_matrix(net->Nifo,N);
    RS = double_matrix(net->Nifo,N);
    RC = double_matrix(net->Nifo,N);

    // Form up residual
    for(id = 0; id < net->Nifo; id++)
    {
     for(j = 0; j < N/2; j++) 
     {
        rb[id][j] = D[id][2*j] - het->hb[id][j];
        rb[id][N-j] = D[id][2*j+1] - het->hb[id][N-j];
     }
    }


    // Set reference likelihood
    logL = 0.0;
    for(id = 0; id < net->Nifo; id++)
    {
        HH = fourier_nwip_bw(het->hb[id], het->hb[id], rundata->SN[id], 1, MM, N);
        HR = fourier_nwip_bw(rb[id], het->hb[id], rundata->SN[id], 1, MM, N);
        logL += HR+0.5*HH;
    }
    het->logLb = logL;

    // printf("het->logLb: %f\n", het->logLb);
    // printf("het->M is: %i, MM is: %i, N is: %i, freq len is: %i \n", M, MM, N, het->freq->length);

    for(id = 0; id < net->Nifo; id++)
    {
        RC[id][0] = 0.0;
        RS[id][0] = 0.0;
      for(j = 1; j < MM; j++)
      {
        cp = het->cp[id][j];
        sp = het->sp[id][j];
        x = het->x[id][j];
        RC[id][j] = 2.0*(rb[id][j]*cp+rb[id][N-j]*sp)*x;
        RS[id][j] = 2.0*(-rb[id][j]*sp+rb[id][N-j]*cp)*x;
      }
    }
    
    // Compute Legendre coefficients for hetrodyned residual
    for(id = 0; id < net->Nifo; id++)
    {
        for(j = 0; j < M; j++)
        {
            het->rc[id][j] = RC[id][j*ds];
            het->rs[id][j] = RS[id][j*ds];
            
            het->lc[id][0][j] = 0.0;
            het->lc[id][1][j] = 0.0;
            het->ls[id][0][j] = 0.0;
            het->ls[id][1][j] = 0.0;
           for(n = 0; n <= ds; n++)
            {
                het->lc[id][0][j] += RC[id][n+j*ds];
                het->lc[id][1][j] += (1.0-(double)(2*n)/(double)(ds))*RC[id][n+j*ds];
                het->ls[id][0][j] += RS[id][n+j*ds];
                het->ls[id][1][j] += (1.0-(double)(2*n)/(double)(ds))*RS[id][n+j*ds];
            }
        }
    }
    

    // Get heterodyned likelihood
    logL = log_likelihood_het(net, het, params, Tobs, rundata, 1);

    free_double_vector(params);
    free_double_matrix(rb,net->Nifo);
    free_double_matrix(RS,net->Nifo);
    free_double_matrix(RC,net->Nifo);

    return(logL);
}


void heterodyne(struct Net *net, struct Het *het, double **D, double *params, RealVector *freq, double **SN, double **SM, int N, double Tobs, struct bayesCBC *rundata, int icbc)
{
    int i, id, j, n;
    double f, fstop, x, y, z;
    double **hb, **rb, **ampb, **phaseb, **hb_tmp;
    double cp, sp;
    double **RC, **RS;
    double logL, HH, HR;
    int M, MM, ds, ell;

    ell = 2;  // Legendre order (max is ell-1). Currently only set up for ell=2
  
    f = fringdown(params);  // ringdown frequency
    //fprintf(stdout, "heterodyne %f fringdown \n", f);
    fstop = 2*f; // twice the ringdown frequency
    if(fstop < 64.0) fstop = 64.0; // just to be safe
    
    // printf("heterodyne fringdown:  %f, fstop: %f, fres: %f\n", f, fstop, rundata->fres);
    
    // With the piecewise Legendre polynomial approach it is not necessary to use powers of two or equal sized frequency bins.
    
    // fres is now the spacing between samples. The integration region is ell*fres wide.
    //fprintf(stdout, "heterodyne mm %i %i %i %i %i %i\n", N, M, MM, ell, icbc, ds);
    
    MM = (int)(fstop*Tobs); // Covers the signal range
    //fprintf(stdout, "fstop = %g, Tobs = %g, MM = %i\n", fstop, Tobs, MM);
    if(MM > N/2) MM = N/2;  // can't exceed Nyqusit
    ds = (int)(Tobs*rundata->fres);  // The downsample amount. Set by the frequency resolution fres. Default fres = 4 Hz.
    M = MM/ds +1 ;  // number of points used in the heterodyne
    //fprintf(stdout, "heterodyne mm %i %i %i %i %i %i\n", N, M, MM, ell, icbc, ds);

    if(M*ds > N/2) M -= 1;
    
    // printf("%d %d %d %d\n", N/2, MM, M, ds);
    //fprintf(stdout, "heterodyne setting \n");
    
    het->M = M;
    het->MM = MM;
    het->ell = ell;
    het->icbc = icbc;
    //fprintf(stdout, "heterodyne matrices %i %i %i %i\n", M, MM, ell, icbc);
    
    het->S0 = double_matrix(net->Nifo,M);
    het->S1 = double_matrix(net->Nifo,M);
    het->AS = double_matrix(net->Nifo,M);
    het->SN = double_matrix(net->Nifo,M);
    het->amp = double_matrix(net->Nifo,M);
    het->phase = double_matrix(net->Nifo,M);
    het->rc = double_matrix(net->Nifo,M);
    het->rs = double_matrix(net->Nifo,M);
    het->lc = double_tensor(net->Nifo,ell,M);
    het->ls = double_tensor(net->Nifo,ell,M);
    het->freq = CreateRealVector(M);
    // het->pref = double_vector(rundata->NP);  // keep a copy of the parameters used for the heterodyne (used for testing)
    
    // These are only used in fixed heterodyne extrinsic version
    het->hb = double_matrix(net->Nifo,N);
    het->cp = double_matrix(net->Nifo,MM);
    het->sp = double_matrix(net->Nifo,MM);
    het->x = double_matrix(net->Nifo,MM);

    // for(j = 0; j < rundata->NP; j++) het->pref[j] = params[j];
    
    hb = double_matrix(net->Nifo,N);
    hb_tmp = double_matrix(net->Nifo,N);
    rb = double_matrix(net->Nifo,N);
    RS = double_matrix(net->Nifo,N);
    RC = double_matrix(net->Nifo,N);
    ampb = double_matrix(net->Nifo,N);
    phaseb = double_matrix(net->Nifo,N);

    // full frequency template, amplitude and phase
    fulltemplates(net, hb_tmp, freq, params, N, rundata);
    fullphaseamp(net, ampb, phaseb, freq, params, N, rundata, 0);
        
    for (id=0; id< net->Nifo; id++)
    { 
        hb[id][0] = 0.0;
        hb[id][N/2] = 0.0;
        
        for (i=1; i< N/2; i++)
        {
            hb[id][i] = hb_tmp[id][2*i];
            hb[id][N-i] = hb_tmp[id][2*i+1];
        }
    }

    // store the downsampled frequency array
    for(j = 0; j < M; j++)
    {
        n = j*ds;
        het->freq->data[j] = freq->data[n];
    }

    het->freq->data[0] = rundata->fres; 
    
    // store the downsampled smooth ASD and full PSD
    // store the downsampled reference amplitude and phase
    for(id = 0; id < net->Nifo; id++)
       {
           for(j = 0; j < M; j++)
              {
                n = j*ds;
                het->AS[id][j] = sqrt(2.0*SM[id][n]);
                het->SN[id][j] = SN[id][n];
                het->amp[id][j] = ampb[id][n];
                het->phase[id][j] = phaseb[id][n];
              }
       }

    // FILE *out=fopen("downsampled_smooth.dat","w");
    // id=0;
    // for(j = 0; j < M; j++)
    //   {
    //     fprintf(out,"%e %e\n", het->freq->data[j], (het->AS[id][j]*het->AS[id][j])/2.0);
    //   }
    // fclose(out);
    
    // Legendre polynomial expansion of 1/SN across each frequency band.
    // Zeroth order, S0, is pretty much good enough
    for(id = 0; id < net->Nifo; id++)
    {
      for(j = 0; j < M; j++)
        {
           het->S0[id][j] = 0.0;
           het->S1[id][j] = 0.0;
           for(n = 0; n <= ds; n++)
            {
                het->S0[id][j] += 1.0/(2.0*SN[id][n+j*ds]);
                het->S1[id][j] += (1.0-(double)(2*n)/(double)(ds))/(2.0*SN[id][n+j*ds]);
            }
        }
    }
    
    // form up resdiual
    for(id = 0; id < net->Nifo; id++)
    {
     for(j = 0; j < N/2; j++) 
     {
        rb[id][j] = D[id][2*j] - hb[id][j];
        rb[id][N-j] = D[id][2*j+1] - hb[id][N-j];
     }
    }

    logL = 0.0;
    for(id = 0; id < net->Nifo; id++)
    {
        HH = fourier_nwip_bw(hb[id], hb[id], SN[id], 1, MM, N);
        HR = fourier_nwip_bw(rb[id], hb[id], SN[id], 1, MM, N);
        logL += HR+0.5*HH;
    }

    het->logLb = logL;
    // printf("het->logLb: %f\n", het->logLb);
    // printf("het->M is: %i, N is: %i, freq len is: %i \n", M, N, freq->length);

    for(id = 0; id < net->Nifo; id++)
    {
        RC[id][0] = 0.0;
        RS[id][0] = 0.0;
      for(j = 1; j < MM; j++)
      {
        cp = cos(phaseb[id][j]);
        sp = sin(phaseb[id][j]);
        x = 1.0*sqrt(2.0*SM[id][j])/(2.0*SN[id][j]);
        RC[id][j] = 2.0*(rb[id][j]*cp+rb[id][N-j]*sp)*x;
        RS[id][j] = 2.0*(-rb[id][j]*sp+rb[id][N-j]*cp)*x;
      }
    }
    
    // Legendre coefficients for hetrodyned residual
    
    for(id = 0; id < net->Nifo; id++)
    {
        for(j = 0; j < M; j++)
        {
            het->rc[id][j] = RC[id][j*ds];
            het->rs[id][j] = RS[id][j*ds];
            
            het->lc[id][0][j] = 0.0;
            het->lc[id][1][j] = 0.0;
            het->ls[id][0][j] = 0.0;
            het->ls[id][1][j] = 0.0;
           for(n = 0; n <= ds; n++)
            {
                het->lc[id][0][j] += RC[id][n+j*ds];
                het->lc[id][1][j] += (1.0-(double)(2*n)/(double)(ds))*RC[id][n+j*ds];
                het->ls[id][0][j] += RS[id][n+j*ds];
                het->ls[id][1][j] += (1.0-(double)(2*n)/(double)(ds))*RS[id][n+j*ds];
            }
        }
    }
    
    het->initFlag = 1; 

    free_double_matrix(RC,net->Nifo);
    free_double_matrix(RS,net->Nifo);
    free_double_matrix(hb,net->Nifo);
    free_double_matrix(rb,net->Nifo);
    free_double_matrix(ampb,net->Nifo);
    free_double_matrix(phaseb,net->Nifo);

    free_double_matrix(hb_tmp, net->Nifo);
        
    
}

double log_likelihood_het(struct Net *net, struct Het *het, double *params, double Tobs, struct bayesCBC *rundata, int recomputeFlag)
{
    double **amp, **phase;
    double **hc, **hs;
    double HH, HR, logL;
    double x, y;
    double L0,L1;
    double **cc, **ss;
    int j, id, M;
    
    logL = 0.0;
    
    if(rundata->constantLogLFlag == 0)
    {
    
    M = het->M;

    amp = double_matrix(net->Nifo,M);
    phase = double_matrix(net->Nifo,M);
    cc = double_matrix(net->Nifo,M);
    ss = double_matrix(net->Nifo,M);
    hc = double_matrix(net->Nifo,M);
    hs = double_matrix(net->Nifo,M);
    
    fullphaseamp(net, amp, phase, het->freq, params, 2*M, rundata, recomputeFlag);

    logL = het->logLb;
    for(id = 0; id < net->Nifo; id++)
    {
        for(j = 0; j < M; j++)
        {
            cc[id][j] = 2.0*(het->amp[id][j]-amp[id][j]*cos(het->phase[id][j]-phase[id][j]));
            ss[id][j] = 2.0*amp[id][j]*sin(het->phase[id][j]-phase[id][j]);
            hc[id][j] = cc[id][j]/(het->AS[id][j]);
            hs[id][j] = ss[id][j]/(het->AS[id][j]);
            cc[id][j] = cc[id][j]*cc[id][j]+ss[id][j]*ss[id][j];
        }
        
        HH = 0.0;
        HR = 0.0;
        x = 0.0;
        y = 0.0;
        for(j = 0; j < M-1; j++)
        {
            L0 = (cc[id][j+1]+cc[id][j])/2.0;
            L1 = (cc[id][j]-cc[id][j+1])/2.0;
            HH += L0*het->S0[id][j]+L1*het->S1[id][j];
            x += cc[id][j+1]/(2.0*het->SN[id][j]);  // correction for overcount
            
            L0 = (hc[id][j+1]+hc[id][j])/2.0;
            L1 = (hc[id][j]-hc[id][j+1])/2.0;
            HR += L0*het->lc[id][0][j]+L1*het->lc[id][1][j];
            y += hc[id][j+1]*het->rc[id][j+1]; // correction for overcount
            
            L0 = (hs[id][j+1]+hs[id][j])/2.0;
            L1 = (hs[id][j]-hs[id][j+1])/2.0;
            HR += L0*het->ls[id][0][j]+L1*het->ls[id][1][j];
            y += hs[id][j+1]*het->rs[id][j+1]; // correction for overcount
        
        }
        
        // correction for overcount of edge values
        HH -= x;
        HR -= y;
        
        logL -= (HR+0.5*HH);
        
        //printf("Het %d %f %f %f %f\n", id, HH, HR, x, y);
        
    }
        
    // if (logL>1e6) logL = -1e5; // Just in case, something is wrong. 

    free_double_matrix(hc,net->Nifo);
    free_double_matrix(hs,net->Nifo);
    free_double_matrix(amp,net->Nifo);
    free_double_matrix(phase,net->Nifo);
    free_double_matrix(cc,net->Nifo); 
    free_double_matrix(ss,net->Nifo);
    }
    
    return(logL);
    
}

void sample_CBC_intinsic_prior(double *params, const struct bayesCBC *rundata, gsl_rng *seed){
  // sample unformly in priors
  // SOPHIE TODO ADD TIDAL PRIOR CHECK and actual priors
  double mc, mt;
  double log_eta = (5.0/3.0) * (params[0] - params[1]); // log M_chirp - log M_total
  double eta = exp(log_eta);

  while (eta > 0.25) {
    params[0] = rundata->min[0] + (rundata->max[0] - rundata->min[0]) * gsl_rng_uniform(seed);
    params[1] = rundata->min[1] + (rundata->max[1] - rundata->min[1]) * gsl_rng_uniform(seed);
    mc = exp(params[0]);
    mt = exp(params[1]);
    eta = pow((mc/mt), (5.0/3.0));
  }
  for(int k = 2; k < rundata->NX; k++) {
    params[k] = rundata->min[k] + (rundata->max[k]-rundata->min[k])*gsl_rng_uniform(seed);
  }
}

void sample_CBC_extrinsic_prior(double *params, const struct bayesCBC *rundata, gsl_rng *seed){
  // NOTE! params should be of the form rundata->pall
  // SOPHIE TODO Add actual priors
  for(int k = rundata->NX; k < rundata->NP; k++) {
    params[k] = rundata->min[k] + (rundata->max[k]-rundata->min[k])*gsl_rng_uniform(seed);
  }
}

bool within_CBC_prior(const double *params, const struct bayesCBC *rundata, const bool verbose){
  // checks intrinsic (NX) and semi intrinsic parameters (NX )
  // it is lenient with geocenter time (params[5])
  int within_prior = 1;
  int time_index = 5;
  bool isNAN = 0;
  double log_eta = (5.0/3.0) * (params[0] - params[1]); // log M_chirp - log M_total
  double eta = exp(log_eta);
  if (eta - 0.25 > 1e-4) {
    within_prior = 0;
    if (verbose) {
      fprintf(stdout, "within_CBC_prior eta: %f not in (0, 0.25) \n", eta);
      fflush(stdout);
    }
    else{
      return within_prior;
    }
  }
  for (int i = 0; i < rundata->NX; i++){
    // check param priors (also check if nan )
    isNAN = (params[i] != params[i]);
    if ((params[i] < rundata->min[i]) || (params[i] > rundata->max[i]) || isNAN) {

      // lenient with geocenter time (allows error of light travel time = 0.05 seconds)
      if ((i == 5) && ((fabs(params[i] - rundata->min[i]) < 0.05) || (fabs(params[i]- rundata->max[i]) < 0.05)))
        continue;
      else {
        within_prior = 0;
        if (verbose) {
          fprintf(stdout, "within_CBC_prior %i: %f not in (%f, %f) \n", i,
                        params[i], rundata->min[i], rundata->max[i]);
          fflush(stdout);
        }
        // if we are not being verbose, simply return false now
        else{
          return within_prior;
        }
      }
    }
  }
  return within_prior;
}

double return_above_or_below(const double param, const double min, const double max){
  // given a parameter and a bound on the parameter,
  // returns param if in bound, and closest bound if out of bounds
  if (param > max){
    return max;
  }
  if (param < min){
    return min;
  }
  return param;
}
void enforce_CBC_priors(double *params, const struct bayesCBC *rundata){
  // given an array params, checks intrinsic and semi-intrinsic are in bounds
  // if they are not, parameters are changed in order to force them in bounds
  // pall  [0] log(Mc) [1] log(Mt) [2] chi1 [3] chi2 [4] 2phi0 [5] tp [6] log(DL)
  bool using_lambda_tilde = (rundata->NX > 7) && (rundata->lambda_type_version == (Lambda_type) lambdaTilde);

  // first we should handle the periodic parameter (phase, which is stored in 4)
  // TODO should this be pi or 2pi?
  while (params[4] < 0){ params[4]  += TPI; }
  while (params[4] > TPI){ params[4] -= TPI; }

  // Now for all our parameters, we enforce that they exist within the prior bound
  // NOTE we are a little lenient with geocenter time TODO should we be?
  for (int i = 0; i < rundata->NX; i++){
    // Be lenient with time (allow to be off in geocenter by light travel time between detectors )
    if ((i == 5) && ((fabs(params[i] - rundata->min[i]) < 0.05) || (fabs(params[i]- rundata->max[i]) < 0.05)))
      continue;
    params[i] = return_above_or_below(params[i], rundata->min[i], rundata->max[i]);
  }

  // Now check if eta is in bounds
  double log_eta = (5.0/3.0) * (params[0] - params[1]); // log M_chirp - log M_total
  double eta = exp(log_eta);
  if (eta > 0.25){
    // change value of total mass to be consistent with eta = 0.25
    eta = 0.25;
    params[1] = params[0] - 3.0/5.0 * log(eta);
    // raise warning if we pushed params[1] out of bounds
    if ((params[1] < rundata->min[1]) || (params[1] > rundata->max[1])){
      fprintf(stderr, "WARNING enforce_CBC_priors: log M_total %f is not in (%f, %f)",
                        params[1], rundata->min[1], rundata->max[1]);
    }
  }

  // Enforce priors on tidal parameters
  if (using_lambda_tilde){
      double lambda1y, lambda2y;
      LambdaTsEta2Lambdas(params[7], params[8], eta, &lambda1y, &lambda2y);
      // if we accidentally get a negative lambda1 or 2, (which is possible within parameter space)
      // just set lambdaT and dlambda to 0
      if (lambda1y < 0 || lambda2y < 0) {
        params[7] = 0;
        params[8] = 0;
      }
  }
}

void eigen_value_rescaling(struct Net *net, double *params, RealVector *freq, double **SM, int N, double Tobs, double *eval, double **evec,
    double z_ref, struct bayesCBC *rundata, int scale)
{
  //fprintf(stdout, "INSIDE efix \n");
  int k, flag;
  // " fixes " eigen vectors and sets any that have horrible jumps to 0
  // POSSIBLE BUG: BayesCBC_MCMC only finds the first NX parameters of fisher, so the eigen vectors are garbage after those values
  // here 'z' refers to |h_params1 - h_params2|)
  double alpha0, z, z0, alpha, alphanew;
  double dz, alphax, LDLmin;
  double leta, eta, mc, mt;
  double lambda1y, lambda2y;
  double dzmin, alpham, zm;
  double *px;
  double alphascale;

  // Local pointers
  int NP = rundata->NP;
  int NX = rundata->NX;

  LDLmin = rundata->min[6];
  int max_iterations = 15;
  double z_max = z_ref * 2.0; // maximum amplitude difference in waveforms
  double z_min = z_ref / 2.0; // minimum amplitude difference in waveforms

  px = double_vector(NP);

  // double etamin = 0.05;

  // For BNSs max mass ratio q = 10
  double etamin = 0.082644628;

  // Loop trough intrinsic values only because
  // extrinsic don't need to be updated
  for (int i = 0; i < NX; ++i)
  {
    // if the eigenvalue is too big, give up
    if (i <2 && (eval[i]*evec[i][i]) > 100) continue;

    dzmin = 1.0e20; // keeps track of the smallest difference
    alpham = 1.0;

    alpha0 = 1.0;

    k = 0;
    // sample along each eigen vector with jump size alpha0
    // such that the SNR of the waveform difference (z0 = |h_params - h_px|)
    // is between z_max and z_min
    // scale alpha0 up or down until we are in the right zone
    do
    {
      // Fisher is only defined for NX by NX
      for (int j = 0; j < NX; ++j)
      {
        // fixed jump along eigenvector
        if (j<NX) px[j] = params[j] + alpha0 * eval[i] * evec[i][j];
        else px[j] = params[j];
      }

      flag = 0;
      // make sure parameters remain in bounds
      enforce_CBC_priors(px, rundata);
      // amplitude difference between params and params + an eigenjump
      z0 = het_diff(net, params, px, freq, SM,  N, Tobs, rundata, scale);

      // checks if the allowed jump is reasonable (dont want the change in amplitude to be too big or too small)
      dz = fabs(z0 - z_ref);
      if(dz < dzmin){
        dzmin = dz;
        alpham = alpha0;
        zm = z0;
      }

      if(z0 < z_min) alpha0 *= 2.0; // if the difference is too small, make the jumps bigger
      if(z0 > z_max) alpha0 /= 1.9; // if the difference is too big, make the jumps smaller

      k++;

    } while ( (z0 > z_max || z0 < z_min) && k < max_iterations);

    if (i<2) alpha = 1.01; // scale the mass parameters less than the others
    else alpha = 1.1;

    k = 0;
    do
    {
      for (int j = 0; j < NP; ++j)
      {
        // eigen jump along vector
        if (j<NX) px[j] = params[j] + alpha * eval[i] * evec[i][j];
        else px[j] = params[j];
      }
      // make sure parameters remain in bounds
      enforce_CBC_priors(px, rundata);

      z = het_diff(net, params, px, freq, SM,  N, Tobs, rundata, scale);
      if(alpha > alpha0) alphanew = alpha0 + (z_ref - z0) / (z-z0) * (alpha - alpha0);
      else alphanew = alpha + (z_ref - z) / (z0 - z) * (alpha0-alpha);
      if (alphanew != alphanew){
        // that is, we divided by 0
        alphanew = alpha0;
      }

      dz = fabs(z - z_ref);
      if(dz < dzmin)
      {
        dzmin = dz;
        alpham = alpha;
        zm = z;
      }

      z0 = z;
      alpha0 = alpha;
      alpha = alphanew;

      k++;

      // Stop if things are going wrong
      flag = 0;
      if ((fabs(z - z_ref) > fabs(z0 - z_ref)) || (alpha > 1e5) || alpha < 0.0 || alpha != alpha){
          alpham = 1.0;
          flag = 1;
          // printf("flagged : ");
          break;
      }

    } while (fabs(z - z_ref) > 0.2 && k < max_iterations);

    // printf("for param[%d]: F %f %f %f, %f, %f\n", i, alpham, zm, dzmin/zs, fabs(z-zs), eval[i]);

    // printf("\n");

    // kill any failed direction
    if(dzmin / z_ref > 1.0 && flag == 0) alpham = 0.0;

    eval[i] *= alpham;
  }// end loop over extrinsic parameters
  //fprintf(stdout, "OUTSIDE efix \n");
}



double het_diff(struct Net *net, double *params, double *dparams, RealVector *freq, double **SM, int N, double Tobs, struct bayesCBC *rundata, int scale)
{
    int i, id, j, M, imin;
    double **Ar, **Ap, **Pr, **Pp;
    double Re, Im;
    double fstop, dx, fs;

    double fmin = rundata->fmin;

    // Don't need to recompute heterodyne frequencies
    // if Fisher was already computed on heterodyne
    if (rundata->het->initFlag == 0)
    {
        fs = fbegin(params);

        if(fmin > fs) imin = (int)(Tobs*fmin); // gets index of initial frequency
        else imin = (int)(Tobs*fs);

        fstop = 2.0*fringdown(params);  // twice ringdown frequency
        if(fstop < 64.0) fstop = 64.0; // just to be safe
        M = 2*(int)(fstop*Tobs); // Covers the signal range
        if(M > N) M = N;  // can't exceed Nyqusit
    } else {
        M = N;
        imin = 1;
    }

    // store amplitudes of frequency domain waveform (projected in each detector)
    Ar = double_matrix(net->Nifo,M/2+1); // params waveform
    Ap = double_matrix(net->Nifo,M/2+1); // dparams waveform

    // store phases of frequency domain waveform (projected in each detector)
    Pr = double_matrix(net->Nifo,M/2+1);
    Pp = double_matrix(net->Nifo,M/2+1);

    // check param priors
    //if (!within_CBC_prior(params, rundata, 1)){
    //  fprintf(stdout, "het_diff params\n");
    //}
    //if (!within_CBC_prior(dparams, rundata, 1)){
    //  fprintf(stdout, "het_diff dParams\n");
    //}

    //fprintf(stdout, "\thet_diff params\n");
    fullphaseamp(net, Ar, Pr, freq, params, M, rundata, 0);
    //fprintf(stdout, "\thet_diff dparams\n");
    fullphaseamp(net, Ap, Pp, freq, dparams, M, rundata, 0);

    dx = 0.0;

    for(id = 0; id < net->Nifo; id++)
    {
        for(j = imin; j < M/2; j++)
        {
          Re = (Ar[id][j]-Ap[id][j]*cos(Pr[id][j]-Pp[id][j]));
          Im = Ap[id][j]*sin(Pr[id][j]-Pp[id][j]);
          dx += 4.0*(Re*Re+Im*Im)/(SM[id][j]);
        }
    }

    free_double_matrix(Ar,net->Nifo);
    free_double_matrix(Ap,net->Nifo);
    free_double_matrix(Pr,net->Nifo);
    free_double_matrix(Pp,net->Nifo);

    return(dx);

}

double log_likelihood_test(struct Net *net, struct Het *het, double **D, double *params, RealVector *freq, double **SN, int N, double Tobs, struct bayesCBC *rundata)
{
    int i, j, MM;
    double logL;
    double **twave;
    double **res, **rwave, **dh;
    double *rc, *rs;
    double HH, HD, x;
    int imin, imax;
    
    MM = het->MM;
    
        rc = double_vector(MM);
        rs = double_vector(MM);
    
        twave = double_matrix(net->Nifo,N);
        rwave = double_matrix(net->Nifo,N);
        res = double_matrix(net->Nifo,N);
        dh = double_matrix(net->Nifo,N);
    
        fulltemplates(net, twave, freq, params, N, rundata);
        // fulltemplates(net, rwave, freq, het->pref, N, rundata);
        
        for(i = 0; i < net->Nifo; i++)
        {
          for(j=0; j<N/2; j++) {
            dh[i][j] =  rwave[i][2*j]-twave[i][2*j];
            dh[i][N-j] =  rwave[i][2*j+1]-twave[i][2*j+1];

            res[i][j] = D[i][2*j] - rwave[i][2*j];
            res[i][N-j] = D[i][2*j+1] - rwave[i][2*j+1];
          }

          res[i][0] = 0.0;
           
          
          // apply the same Tukey window to the resdiuals as was done in heterodyne
          rc[0] = res[i][0];
          rs[0] = 0.0;
          for(j = 1; j < MM; j++)
          {
            rc[j] = res[i][j];
            rs[j] = res[i][N-j];
          }

            tukey(rc,0.05,MM);
            tukey(rs,0.05,MM);
            
            res[i][0] = rc[0];
            for(j = 1; j < MM; j++)
            {
                res[i][j] = rc[j];
                res[i][N-j] = rs[j];
            }
            
        }
        
        logL = het->logLb;

        for(i = 0; i < net->Nifo; i++)
        {
            HH = fourier_nwip_bw(dh[i], dh[i], SN[i], 1, MM, N);
            HD = fourier_nwip_bw(res[i], dh[i], SN[i], 1, MM, N);
            logL -= (HD+0.5*HH);
            printf("Test %d %f %f\n", i, HH, HD);
        }

        printf("Test logB: %f, logL: %f\n", het->logLb, logL);
        
        free_double_matrix(dh,net->Nifo);
        free_double_matrix(twave,net->Nifo);
        free_double_matrix(rwave,net->Nifo);
        free_double_matrix(res,net->Nifo);
        free_double_vector(rc);
        free_double_vector(rs);
    
    return(logL);
    
}

void freehet(struct Net *net, struct Het *het)
{   

    free_double_matrix(het->S0,net->Nifo);
    free_double_matrix(het->S1,net->Nifo);
    free_double_matrix(het->AS,net->Nifo);
    free_double_matrix(het->SN,net->Nifo);
    free_double_matrix(het->amp,net->Nifo);
    free_double_matrix(het->phase,net->Nifo);

    free_double_matrix(het->rc,net->Nifo);
    free_double_matrix(het->rs,net->Nifo);
    free_double_tensor(het->lc,net->Nifo,het->ell);
    free_double_tensor(het->ls,net->Nifo,het->ell);
    DestroyRealVector(het->freq);
    // free_double_vector(het->pref);

    free_double_matrix(het->hb,net->Nifo);
    free_double_matrix(het->cp,net->Nifo);
    free_double_matrix(het->sp,net->Nifo);
    free_double_matrix(het->x,net->Nifo);

}

void extrinsic_proposal_cbc_geoshifts(double *geoshifts, double *proposed_shifts, double dtime0, double phi0y, double scaley)
{
    double dtimesx = geoshifts[3];
    proposed_shifts[0] = geoshifts[0] + dtimesx - dtime0; // tshift for proposal
    proposed_shifts[1] = phi0y - geoshifts[1];
    proposed_shifts[2] = scaley;
}

void GenerateWaveform(
    // double *wave,           /**< [out] FD waveform */
    AmpPhaseFDWaveform** htilde,           /**< [out] FD waveform */
    RealVector* freq,                   /**< Input: frequencies (Hz) on which to evaluate h22 FD - will be copied in the output AmpPhaseFDWaveform. Frequencies exceeding max freq covered by PhenomD will be given 0 amplitude and phase. */
    const double phi0,                  /**< Orbital phase at fRef (rad) */
    const double fRef_in,               /**< reference frequency (Hz) */
    const double m1_SI,                 /**< Mass of companion 1 (kg) */
    const double m2_SI,                 /**< Mass of companion 2 (kg) */
    const double chi1,                  /**< Aligned-spin parameter of companion 1 */
    const double chi2,                  /**< Aligned-spin parameter of companion 2 */
    const double distance,               /**< Distance of source (m) */
    double* extraParams,
    NRTidal_version_type NRTidal_version, /**< Version of NRTides; can be one of NRTidal versions or NoNRT_V for the BBH baseline */
    int customFlag                  /**< Temporary flag to use custom (faster) WF generation */
) {
		if (distance <= 0){
			fprintf(stdout, "GenerateWaveform WARNING: DISTANCE is < 0!! distance = %f\n", distance);
		}

    // Final failsafe (but is caught before this in main MCMCs)
    if (extraParams[0] < 0.0) extraParams[0] = 0.0;
    if (extraParams[1] < 0.0) extraParams[1] = 0.0;


    // use custom waveform generation routine
    if (customFlag) 
    {
        // printf("%f, %f, %f, %e, %e, %f, %f, %e, %f, %f, %i", freq->data[1], phi0, fRef_in, m1_SI, m2_SI, chi1, chi2, distance, extraParams[0], extraParams[1], customFlag);
        // fflush(stdout);
        int ret = IMRPhenomDGenerateh22FDAmpPhase(htilde, freq, phi0, fRef_in, m1_SI, m2_SI, chi1, chi2, distance, extraParams, NRTidal_version);
        return ;
    }
    int status, i;
    double f;

    COMPLEX16FrequencySeries *hptilde = NULL;
    COMPLEX16FrequencySeries *hctilde = NULL;

    size_t n = freq->length;
    double fmin = freq->data[1];
    double fmax = freq->data[n-1];
    double deltaF = freq->data[2] - freq->data[1];


    double unused_inclination = 0.0;
    Approximant approximant;
    LALDict *LALparams = XLALCreateDict();

    // TODO: This can be extended to take approximant as parameter
    // That could also replace our custom NRTidal_version
    //int len = (int) freq->length;
    //if (fabs(deltaF - (freq->data[len - 1] - freq->data[len - 2])) > 1e-4){
    //  fprintf(stdout, "HOLY HECK");
    //}
    //fprintf(stdout, "dict(phiref = %f, deltaF = %f, fmin = %f, fmax = %f, fref = %f, m1 = %e, m2 = %e, spin1z = %f, spin2z = %f, dist = %e)\n",
    //        phi0, deltaF, fmin, fmax, fRef_in, m1_SI, m2_SI, chi1, chi2, distance);
    if (NRTidal_version != NoNRT_V) 
    {
        if(NRTidal_version == NRTidalv2_V) approximant = (Approximant) IMRPhenomD_NRTidalv2;
        else if(NRTidal_version == NRTidal_V) approximant = (Approximant) IMRPhenomD_NRTidal;
        XLALSimInspiralWaveformParamsInsertTidalLambda1(LALparams, (REAL8) extraParams[0]);
        XLALSimInspiralWaveformParamsInsertTidalLambda2(LALparams, (REAL8) extraParams[1]);
    } else 
    {
        approximant = (Approximant) IMRPhenomD;
    }
    status = XLALSimInspiralChooseFDWaveform(&hptilde, &hctilde,  // or XLALSimInspiralFD ?
        (const REAL8) m1_SI,(const REAL8) m2_SI,  // Masses
        (const REAL8) 0.,(const REAL8) 0., (const REAL8) chi1, (const REAL8) 0.,(const REAL8) 0., (const REAL8) chi2,  // Spins
        (const REAL8) distance, 
        (const REAL8) unused_inclination, // Inclination not used, but needed to compute hcross (not used)
        (const REAL8) phi0,
        (const REAL8) 0., 
        (const REAL8) 0., // eccentricity at reference epoch
        (const REAL8) 0., // mean anomaly of periastron 
        (const REAL8) deltaF, (const REAL8) fmin, (const REAL8) fmax, (const REAL8) fRef_in,
        LALparams,
        approximant

    );

    *htilde = CreateAmpPhaseFDWaveform(n);
    // Split signal in amplitude and phase (note sign flip)
    for (i = 1; i < n; i++)
    {
        (*htilde)->amp[i] = (double) cabs(hptilde->data->data[i]) / h22fac;
        (*htilde)->phase[i] = -1.0* (double) carg(hptilde->data->data[i]);


    }

    XLALDestroyDict(LALparams);
    XLALDestroyCOMPLEX16FrequencySeries(hptilde);
    XLALDestroyCOMPLEX16FrequencySeries(hctilde);

}

